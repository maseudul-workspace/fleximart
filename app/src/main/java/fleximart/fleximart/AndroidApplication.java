package fleximart.fleximart;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import fleximart.fleximart.domain.model.UserInfo;
import fleximart.fleximart.domain.model.WishlistProducts;

public class AndroidApplication extends Application {

    UserInfo userInfo;
    WishlistProducts[] wishlistProducts;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public void setUserInfo(Context context, UserInfo userInfo){
        this.userInfo = userInfo;
        SharedPreferences sharedPref = context.getSharedPreferences(
                "FLEXIMART", Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();

        if(userInfo != null) {
            editor.putString("USER", new Gson().toJson(userInfo));
        } else {
            editor.putString("USER", "");
        }

        editor.commit();
    }

    public UserInfo getUserInfo(Context context){
        UserInfo user;
        if(userInfo != null){
            user = userInfo;
        }else{
            SharedPreferences sharedPref = context.getSharedPreferences(
                    "FLEXIMART", Context.MODE_PRIVATE);
            String userInfoJson = sharedPref.getString("USER","");
            if(userInfoJson.isEmpty()){
                user = null;
            }else{
                try {
                    user = new Gson().fromJson(userInfoJson, UserInfo.class);
                    this.userInfo = user;
                }catch (Exception e){
                    user = null;
                }
            }
        }
        return user;
    }

    public void setWishlistProducts(Context context, WishlistProducts[] wishlistProducts){
        this.wishlistProducts = wishlistProducts;
        SharedPreferences sharedPref = context.getSharedPreferences(
                "FLEXIMART", Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();

        if(userInfo != null) {
            editor.putString("WISHLIST", new Gson().toJson(userInfo));
        } else {
            editor.putString("WISHLIST", "");
        }

        editor.commit();
    }

    public WishlistProducts[] getWishlistProducts(Context context){
        WishlistProducts[] wishlistProducts;
        if(this.wishlistProducts != null){
            wishlistProducts = this.wishlistProducts;
        }else{
            SharedPreferences sharedPref = context.getSharedPreferences(
                    "FLEXIMART", Context.MODE_PRIVATE);
            String wishlistJson = sharedPref.getString("WISHLIST","");
            if(wishlistJson.isEmpty()){
                wishlistProducts = null;
            }else{
                try {
                    wishlistProducts = new Gson().fromJson(wishlistJson, WishlistProducts[].class);
                    this.wishlistProducts = wishlistProducts;
                }catch (Exception e){
                    wishlistProducts = null;
                }
            }
        }
        return wishlistProducts;
    }

}
