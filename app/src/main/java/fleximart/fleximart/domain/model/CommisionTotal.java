package fleximart.fleximart.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CommisionTotal {

    @SerializedName("approved")
    @Expose
    public String approved;

    @SerializedName("unapproved")
    @Expose
    public String unapproved;

}
