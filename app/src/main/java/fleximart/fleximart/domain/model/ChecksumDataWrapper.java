package fleximart.fleximart.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChecksumDataWrapper {

    @SerializedName("status")
    @Expose
    public boolean status;

    @SerializedName("login_error")
    @Expose
    public int login_error = 0;

    @SerializedName("data")
    @Expose
    public ChecksumDataOriginal checksumDataOriginal;

}
