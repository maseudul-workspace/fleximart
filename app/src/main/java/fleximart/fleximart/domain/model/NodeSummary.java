package fleximart.fleximart.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NodeSummary {

    @SerializedName("total_downline")
    @Expose
    public int totalDownline;

    @SerializedName("total_level_completed")
    @Expose
    public int totalLevelComplete;

    @SerializedName("my_nodes")
    @Expose
    public int myNodes;

}
