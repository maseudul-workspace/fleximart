package fleximart.fleximart.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductDetails {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("tag_name")
    @Expose
    public String tagName;

    @SerializedName("size_wearing")
    @Expose
    public String sizeWearing;

    @SerializedName("fit_wearing")
    @Expose
    public String fitWearing;

    @SerializedName("material")
    @Expose
    public String material;

    @SerializedName("care")
    @Expose
    public String care;

    @SerializedName("brand_id")
    @Expose
    public String brandId;

    @SerializedName("seller_id")
    @Expose
    public String sellerId;

    @SerializedName("price")
    @Expose
    public String price;

    @SerializedName("mrp")
    @Expose
    public String mrp;

    @SerializedName("category")
    @Expose
    public int category;

    @SerializedName("first_category")
    @Expose
    public int firstCategory;

    @SerializedName("second_category")
    @Expose
    public int secondCategory;

    @SerializedName("main_image")
    @Expose
    public String mainImage;

    @SerializedName("short_description")
    @Expose
    public String shortDescription;

    @SerializedName("long_description")
    @Expose
    public String longDescription;

    public boolean isWishlistPresent = false;

}
