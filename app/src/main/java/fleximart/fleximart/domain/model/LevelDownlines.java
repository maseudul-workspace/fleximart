package fleximart.fleximart.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LevelDownlines {

    @SerializedName("node_id")
    @Expose
    public int nodeId;

    @SerializedName("left")
    @Expose
    public int left;

    @SerializedName("right")
    @Expose
    public int right;

    @SerializedName("u_name")
    @Expose
    public String name;

}
