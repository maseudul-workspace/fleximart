package fleximart.fleximart.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WalletDetails {

    @SerializedName("amount")
    @Expose
    public Double amount;

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("withdrawal_amount")
    @Expose
    public double withdrawalAmount;

    @SerializedName("withdraw_lock_amount")
    @Expose
    public double withdrawLockAmount;

}
