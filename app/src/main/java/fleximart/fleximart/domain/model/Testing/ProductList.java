package fleximart.fleximart.domain.model.Testing;

/**
 * Created by Raj on 23-07-2019.
 */

public class ProductList {
    public String productImage;
    public String productName;
    public String price;

    public ProductList(String productImage, String productName, String price) {
        this.productName = productName;
        this.price = price;
        this.productImage = productImage;
    }
}
