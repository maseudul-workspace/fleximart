package fleximart.fleximart.domain.model;

import de.blox.graphview.Node;

public class NodeArray {

    public int nodeId;
    public Node node;

    public NodeArray(int nodeId, Node node) {
        this.nodeId = nodeId;
        this.node = node;
    }

}
