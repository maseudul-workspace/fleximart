package fleximart.fleximart.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserProfile {
    @SerializedName("user_id")
    @Expose
    public int userId;

    @SerializedName("dob")
    @Expose
    public String dob;

    @SerializedName("pan")
    @Expose
    public String pan;

    @SerializedName("aadhar")
    @Expose
    public String aadhar;

    @SerializedName("gender")
    @Expose
    public String gender;

}
