package fleximart.fleximart.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WalletSummary {

    @SerializedName("total_balance")
    @Expose
    public float totalBalance;

    @SerializedName("shopping_balance")
    @Expose
    public float shoppingBalance;

    @SerializedName("withdrawal_amount")
    @Expose
    public float withdrawalAmount;

}
