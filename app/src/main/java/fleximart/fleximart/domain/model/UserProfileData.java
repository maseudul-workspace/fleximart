package fleximart.fleximart.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserProfileData {

    @SerializedName("user")
    @Expose
    public User user;

    @SerializedName("user_profile")
    @Expose
    public UserProfile userProfile;

    @SerializedName("user_address")
    @Expose
    public UserAddress userAddress;

}
