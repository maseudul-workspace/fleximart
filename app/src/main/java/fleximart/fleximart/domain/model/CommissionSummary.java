package fleximart.fleximart.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CommissionSummary {

    @SerializedName("approved")
    @Expose
    public float approved;

    @SerializedName("unapproved")
    @Expose
    public float unapproved;

}
