package fleximart.fleximart.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MyLevels {

    @SerializedName("label")
    @Expose
    public int level;

    @SerializedName("total_node")
    @Expose
    public int totalNode;

    @SerializedName("is_complete")
    @Expose
    public boolean isComplete;

    @SerializedName("total_joined")
    @Expose
    public int totalJoined;

    @SerializedName("total_left")
    @Expose
    public int totalLeft;

}
