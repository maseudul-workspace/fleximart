package fleximart.fleximart.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChecksumDataOriginal {

    @SerializedName("original")
    @Expose
    public ChecksumData checksumData;

}
