package fleximart.fleximart.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Tree {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("left_id")
    @Expose
    public int leftId;

    @SerializedName("right_id")
    @Expose
    public int rightId;

    @SerializedName("is_paid")
    @Expose
    public boolean isPaid;

    @SerializedName("rank")
    @Expose
    public int rank;

}
