package fleximart.fleximart.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderPlaceData {

    @SerializedName("order")
    @Expose
    public int orderId;

    @SerializedName("amount")
    @Expose
    public double amount;

}
