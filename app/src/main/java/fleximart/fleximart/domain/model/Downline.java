package fleximart.fleximart.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Downline {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("left_id")
    @Expose
    public String leftUser;

    @SerializedName("right_id")
    @Expose
    public String rightUser;

    @SerializedName("parrent_id")
    @Expose
    public String parrentId;

    @SerializedName("order_by")
    @Expose
    public String orderBy;

    @SerializedName("transaction_id")
    @Expose
    public String transactionId;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("order_by_name")
    @Expose
    public String orderByName;

    @SerializedName("created_at")
    @Expose
    public String date;

}
