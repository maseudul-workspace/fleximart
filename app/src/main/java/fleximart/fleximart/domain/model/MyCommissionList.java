package fleximart.fleximart.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MyCommissionList {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("pay_to_node")
    @Expose
    public String payToNode;

    @SerializedName("pay_from_node")
    @Expose
    public String payFromNode;

    @SerializedName("level")
    @Expose
    public String level;

    @SerializedName("amount")
    @Expose
    public String amount;

    @SerializedName("created_at")
    @Expose
    public String date;

}
