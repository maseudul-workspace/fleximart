package fleximart.fleximart.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserAddress {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("user_id")
    @Expose
    public int userId;

    @SerializedName("address")
    @Expose
    public String address;

    @SerializedName("pin")
    @Expose
    public String pin;

    @SerializedName("s_name")
    @Expose
    public String stateName;

    @SerializedName("c_name")
    @Expose
    public String cityName;

    @SerializedName("area")
    @Expose
    public String area;

    @SerializedName("state_id")
    @Expose
    public int stateId;

    @SerializedName("city_id")
    @Expose
    public int cityId;
}
