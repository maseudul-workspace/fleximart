package fleximart.fleximart.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DashboardData {

    @SerializedName("node")
    @Expose
    public NodeSummary nodeSummary;

    @SerializedName("wallet")
    @Expose
    public WalletSummary walletSummary;

    @SerializedName("commission")
    @Expose
    public CommissionSummary commissionSummary;

}
