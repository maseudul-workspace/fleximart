package fleximart.fleximart.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WalletHistory {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("wallet_id")
    @Expose
    public int walletId;

    @SerializedName("type")
    @Expose
    public int type;

    @SerializedName("amount")
    @Expose
    public double amount;

    @SerializedName("total_amount")
    @Expose
    public double totalAmount;

    @SerializedName("comment")
    @Expose
    public String comment;

    @SerializedName("created_at")
    @Expose
    public String date;

}
