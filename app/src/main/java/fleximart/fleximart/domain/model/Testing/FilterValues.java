package fleximart.fleximart.domain.model.Testing;

/**
 * Created by Raj on 11-09-2019.
 */

public class FilterValues {

    public int filterValueId;
    public String filterValue;
    public boolean isSelected = false;

    public FilterValues(int filterValueId, String filterValue, boolean isSelected) {
        this.filterValueId = filterValueId;
        this.filterValue = filterValue;
        this.isSelected = isSelected;
    }
}
