package fleximart.fleximart.domain.model.Testing;

import java.util.ArrayList;

/**
 * Created by Raj on 11-09-2019.
 */

public class FilterNames {

    public int filterType;
    public String filterName;
    public ArrayList<FilterValues> filterValues;

    public FilterNames(int filterType, String filterName, ArrayList<FilterValues> filterValues) {
        this.filterType = filterType;
        this.filterName = filterName;
        this.filterValues = filterValues;
    }
}
