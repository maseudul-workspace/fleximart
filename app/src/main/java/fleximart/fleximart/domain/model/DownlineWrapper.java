package fleximart.fleximart.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DownlineWrapper {

    @SerializedName("status")
    @Expose
    public boolean status;

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("total_page")
    @Expose
    public int totalPage;

    @SerializedName("current_page")
    @Expose
    public int currentPage;

    @SerializedName("data")
    @Expose
    public Downline[] downlines;

    @SerializedName("login_error")
    @Expose
    public int login_error = 0;

}
