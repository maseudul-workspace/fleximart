package fleximart.fleximart.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LevelSponsors {

    @SerializedName("node_id")
    @Expose
    public int nodeId;

    @SerializedName("left_id")
    @Expose
    public int left;

    @SerializedName("right_id")
    @Expose
    public int right;

    @SerializedName("name")
    @Expose
    public String name;

}
