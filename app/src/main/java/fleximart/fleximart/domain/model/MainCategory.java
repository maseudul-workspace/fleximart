package fleximart.fleximart.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MainCategory {

    @SerializedName("id")
    @Expose
    public int categoryId;

    @SerializedName("name")
    @Expose
    public String categoryName;

    @SerializedName("image")
    @Expose
    public String categoryImage;

}
