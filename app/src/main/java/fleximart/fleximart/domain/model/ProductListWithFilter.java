package fleximart.fleximart.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductListWithFilter {

    @SerializedName("products")
    @Expose
    public ProductDetails[] productDetails;

    @SerializedName("second_category")
    @Expose
    public SecondCategory[] secondCategories;

    @SerializedName("color")
    @Expose
    public Color[] colors;

    @SerializedName("brands")
    @Expose
    public Brands[] brands;

    @SerializedName("sizes")
    @Expose
    public Sizes[] sizes;

}
