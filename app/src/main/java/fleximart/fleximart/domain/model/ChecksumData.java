package fleximart.fleximart.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChecksumData {

    @SerializedName("CHECKSUMHASH")
    @Expose
    public String checkSum;

    @SerializedName("ORDER_ID")
    @Expose
    public int paytmOrderId;

}
