package fleximart.fleximart.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserInfo {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("email")
    @Expose
    public String email;

    @SerializedName("mobile")
    @Expose
    public String mobile;

    @SerializedName("api_token")
    @Expose
    public String apiToken;

    @SerializedName("member_status")
    @Expose
    public int memberStatus;

    @SerializedName("registered_by")
    @Expose
    public String registered_by;

    @SerializedName("status")
    @Expose
    public int status;

    @SerializedName("user_role")
    @Expose
    public int userRole;

}
