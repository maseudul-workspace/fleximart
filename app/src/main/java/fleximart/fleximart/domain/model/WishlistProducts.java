package fleximart.fleximart.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WishlistProducts {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("product_id")
    @Expose
    public int productId;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("price")
    @Expose
    public String price;

    @SerializedName("mrp")
    @Expose
    public String mrp;

    @SerializedName("main_image")
    @Expose
    public String mainImage;

    @SerializedName("tag_name")
    @Expose
    public String tagName;

}
