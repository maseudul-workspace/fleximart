package fleximart.fleximart.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductDetailsData {

    @SerializedName("product")
    @Expose
    public ProductDetails productDetails;

    @SerializedName("colors")
    @Expose
    public Color[] colors;

    @SerializedName("sizes")
    @Expose
    public ProductDetailsSize[] sizes;

    @SerializedName("images")
    @Expose
    public Images[] images;

    @SerializedName("releted_products")
    @Expose
    public ProductDetails[] products;

}
