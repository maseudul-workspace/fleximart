package fleximart.fleximart.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MyNodes {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("left_user")
    @Expose
    public String leftUser;

    @SerializedName("right_user")
    @Expose
    public String rightUser;

    @SerializedName("downline_info")
    @Expose
    public DownlineInfo downlineInfo;

}
