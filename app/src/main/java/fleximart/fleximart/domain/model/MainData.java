package fleximart.fleximart.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MainData {

    @SerializedName("main_category_list")
    @Expose
    public MainCategory[] mainCategories;

    @SerializedName("popular_products")
    @Expose
    public ProductDetails[] popularProducts;

    @SerializedName("new_arrivals")
    @Expose
    public ProductDetails[] new_arrivals;

}
