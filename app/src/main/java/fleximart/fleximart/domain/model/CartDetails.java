package fleximart.fleximart.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CartDetails {

    @SerializedName("cart_id")
    @Expose
    public int cartId;

    @SerializedName("product_id")
    @Expose
    public int productId;

    @SerializedName("color_id")
    @Expose
    public int colorId;

    @SerializedName("size_id")
    @Expose
    public int sizeId;

    @SerializedName("quantity")
    @Expose
    public int quantity;

    @SerializedName("product_name")
    @Expose
    public String productName;

    @SerializedName("product_price")
    @Expose
    public double productPrice;

    @SerializedName("product_mrp")
    @Expose
    public double product_mrp;

    @SerializedName("product_tag_name")
    @Expose
    public String productTagName;

    @SerializedName("product_image")
    @Expose
    public String productImage;

    @SerializedName("color_name")
    @Expose
    public String colorName;

    @SerializedName("color_value")
    @Expose
    public String colorValue;

    @SerializedName("size_name")
    @Expose
    public String sizeName;

    @SerializedName("sizes_list")
    @Expose
    public ProductDetailsSize[] sizes;

}
