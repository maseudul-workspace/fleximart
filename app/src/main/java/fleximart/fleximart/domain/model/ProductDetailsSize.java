package fleximart.fleximart.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductDetailsSize {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("size_id")
    @Expose
    public int sizeId;

    @SerializedName("stock")
    @Expose
    public int stock;

    @SerializedName("size_name")
    @Expose
    public String sizeName;

    public boolean isSelected = false;

}
