package fleximart.fleximart.domain.interactors.impl;

import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.base.AbstractInteractor;
import fleximart.fleximart.domain.model.UserInfo;
import fleximart.fleximart.domain.model.UserInfoWrapper;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class RegisterUserInteractorImpl extends AbstractInteractor implements fleximart.fleximart.domain.interactors.RegisterUserInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String name;
    String email;
    String password;
    String confirmPassword;
    String mobile;

    public RegisterUserInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String name, String email, String password, String confirmPassword, String mobile) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.name = name;
        this.email = email;
        this.password = password;
        this.confirmPassword = confirmPassword;
        this.mobile = mobile;
    }

    private void notifyError(UserInfoWrapper userInfoWrapper) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onRegisterFail(userInfoWrapper);
            }
        });
    }

    private void postMessage(UserInfo userInfo){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onRegisterUser(userInfo);
            }
        });
    }

    @Override
    public void run() {
        final UserInfoWrapper userInfoWrapper = mRepository.registerUser(name, email, password, confirmPassword, mobile);
        if (userInfoWrapper == null) {
            notifyError(null);
        } else if (!userInfoWrapper.status) {
            notifyError(userInfoWrapper);
        } else {
            postMessage(userInfoWrapper.userInfo);
        }

    }
}
