package fleximart.fleximart.domain.interactors;

import fleximart.fleximart.domain.model.Downline;

public interface FetchMyDownlinesInteractor {
    interface Callback {
        void onFetchMyDownlineSuccess(Downline[] downlines, int currentPage, int totalPage);
        void onFetchMyDownlineFail(String errorMsg, int loginError);
    }
}
