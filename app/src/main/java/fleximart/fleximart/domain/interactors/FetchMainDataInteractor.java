package fleximart.fleximart.domain.interactors;

import fleximart.fleximart.domain.model.MainData;

public interface FetchMainDataInteractor {
    interface Callback {
        void onGettingMainDataSuccess(MainData mainData);
        void onGettingMaiDataFail();
    }
}
