package fleximart.fleximart.domain.interactors.impl;

import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.FetchStateListInteractor;
import fleximart.fleximart.domain.interactors.base.AbstractInteractor;
import fleximart.fleximart.domain.model.StateList;
import fleximart.fleximart.domain.model.StateListWrapper;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class FetchStateListInteractorImpl extends AbstractInteractor implements FetchStateListInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;

    public FetchStateListInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
    }

    private void notifyError() {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingStateListFail();
            }
        });
    }

    private void postMessage(StateList[] states){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingStateListSuccess(states);
            }
        });
    }

    @Override
    public void run() {
        StateListWrapper stateListWrapper = mRepository.fetchStateList();
        if (stateListWrapper == null) {
            notifyError();
        } else if (!stateListWrapper.status) {
            notifyError();
        } else {
            postMessage(stateListWrapper.stateLists);
        }
    }
}
