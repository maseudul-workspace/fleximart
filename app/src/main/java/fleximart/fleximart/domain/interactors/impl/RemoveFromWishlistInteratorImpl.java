package fleximart.fleximart.domain.interactors.impl;

import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.RemoveFromWishlistInteractor;
import fleximart.fleximart.domain.interactors.base.AbstractInteractor;
import fleximart.fleximart.domain.model.CommonResponse;
import fleximart.fleximart.repository.AppRepository;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class RemoveFromWishlistInteratorImpl extends AbstractInteractor implements RemoveFromWishlistInteractor {

    Callback mCallback;
    AppRepositoryImpl mRepository;
    String apiToken;
    int wishlistId;

    public RemoveFromWishlistInteratorImpl(Executor threadExecutor, MainThread mainThread, Callback callback, AppRepositoryImpl repository, String apiToken, int wishlistId) {
        super(threadExecutor, mainThread);
        mCallback = callback;
        mRepository = repository;
        this.apiToken = apiToken;
        this.wishlistId = wishlistId;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onWishlistRemoveFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage() {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onWishlistRemoveSuccess();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = mRepository.removeFromWishlist(apiToken, wishlistId);
        if (commonResponse == null) {
            notifyError("Something Went Wrong", 0);
        } else if (!commonResponse.status) {
            notifyError(commonResponse.message, commonResponse.login_error);
        } else {
            postMessage();
        }
    }
}
