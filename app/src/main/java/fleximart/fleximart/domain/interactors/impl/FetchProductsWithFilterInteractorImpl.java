package fleximart.fleximart.domain.interactors.impl;

import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.FetchProductsWithFilterInteractor;
import fleximart.fleximart.domain.interactors.base.AbstractInteractor;
import fleximart.fleximart.domain.model.ProductListWithFilter;
import fleximart.fleximart.domain.model.ProductListWithFilterWrapper;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class FetchProductsWithFilterInteractorImpl extends AbstractInteractor implements FetchProductsWithFilterInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int subCategory;
    int page;

    public FetchProductsWithFilterInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int subCategory, int page) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.subCategory = subCategory;
        this.page = page;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onFetchingProductListWithFilterFail(errorMsg);
            }
        });
    }

    private void postMessage(ProductListWithFilter productListWithFilter, int totalPage){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onFetchingProductListWithFilterSuccess(productListWithFilter, totalPage);
            }
        });
    }

    @Override
    public void run() {
        final ProductListWithFilterWrapper productListWithFilterWrapper = mRepository.getProductListWithFilter(subCategory, page);
        if (productListWithFilterWrapper == null) {
            notifyError("Something went wrong");
        } else if (!productListWithFilterWrapper.status) {
            notifyError(productListWithFilterWrapper.message);
        } else {
            postMessage(productListWithFilterWrapper.productListWithFilter, productListWithFilterWrapper.totalPage);
        }
    }
}
