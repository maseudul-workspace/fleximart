package fleximart.fleximart.domain.interactors;

import fleximart.fleximart.domain.model.ErrorMesage;

public interface BuyNewNodeInteractor {
    interface Callback {
        void onBuyNewNodeSuccess();
        void onBuyNewNodeFail(String errorMsg, int loginError);
    }
}
