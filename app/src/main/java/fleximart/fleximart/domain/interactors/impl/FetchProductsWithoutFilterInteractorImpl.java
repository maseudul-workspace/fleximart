package fleximart.fleximart.domain.interactors.impl;

import java.util.ArrayList;

import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.FetchProductsWithoutFilterInteractor;
import fleximart.fleximart.domain.interactors.base.AbstractInteractor;
import fleximart.fleximart.domain.model.ProductDetails;
import fleximart.fleximart.domain.model.ProductListWithFilter;
import fleximart.fleximart.domain.model.ProductListWithoutFilterWrapper;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class FetchProductsWithoutFilterInteractorImpl extends AbstractInteractor implements FetchProductsWithoutFilterInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int secondCategory;
    ArrayList<Integer> brandIds;
    ArrayList<Integer> sizeIds;
    ArrayList<Integer> colorIds;
    long priceFrom;
    long priceTo;
    int sortBy;
    int page;

    public FetchProductsWithoutFilterInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int secondCategory, ArrayList<Integer> brandIds, ArrayList<Integer> sizeIds, ArrayList<Integer> colorIds, long priceFrom, long priceTo, int sortBy, int page) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.secondCategory = secondCategory;
        this.brandIds = brandIds;
        this.sizeIds = sizeIds;
        this.colorIds = colorIds;
        this.priceFrom = priceFrom;
        this.priceTo = priceTo;
        this.sortBy = sortBy;
        this.page = page;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onFetchingProductListWithoutFilterFail(errorMsg);
            }
        });
    }

    private void postMessage(ProductDetails[] productDetails, int totalPage){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onFetchingProductListWithoutFilterSuccess(productDetails, totalPage);
            }
        });
    }

    @Override
    public void run() {
        final ProductListWithoutFilterWrapper productListWithoutFilterWrapper = mRepository.getProductListWithoutFilter(secondCategory, brandIds, sizeIds, colorIds, priceFrom, priceTo, sortBy, page);
        if (productListWithoutFilterWrapper == null) {
            notifyError("Something went wrong");
        } else if (!productListWithoutFilterWrapper.status) {
            notifyError(productListWithoutFilterWrapper.message);
        } else {
            postMessage(productListWithoutFilterWrapper.productDetails, productListWithoutFilterWrapper.totalPage);
        }
    }
}
