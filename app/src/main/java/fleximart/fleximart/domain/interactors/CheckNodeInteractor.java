package fleximart.fleximart.domain.interactors;

import fleximart.fleximart.domain.model.NodeDetails;

public interface CheckNodeInteractor {
    interface Callback {
        void onNodeCheckingSuccess(NodeDetails nodeDetails);
        void onNodeCheckingFail(String errorMsg, int loginError);
    }
}
