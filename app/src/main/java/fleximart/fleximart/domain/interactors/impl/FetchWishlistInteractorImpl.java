package fleximart.fleximart.domain.interactors.impl;

import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.FetchWishlistInteractor;
import fleximart.fleximart.domain.interactors.base.AbstractInteractor;
import fleximart.fleximart.domain.model.ProductDetails;
import fleximart.fleximart.domain.model.ProductListWithoutFilterWrapper;
import fleximart.fleximart.domain.model.WishlistProducts;
import fleximart.fleximart.domain.model.WishlistWrapper;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class FetchWishlistInteractorImpl extends AbstractInteractor implements FetchWishlistInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int userId;
    String apiToken;

    public FetchWishlistInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int userId, String apiToken) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.userId = userId;
        this.apiToken = apiToken;
    }

    private void notifyError(String errorMsg, int isLoginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onWishlistFetchFail(errorMsg, isLoginError);
            }
        });
    }

    private void postMessage(WishlistProducts[] wishlistProducts){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onWishlistFetchSuccess(wishlistProducts);
            }
        });
    }

    @Override
    public void run() {
        final WishlistWrapper wishlistWrapper = mRepository.fetchWishlist(apiToken, userId);
        if (wishlistWrapper == null) {
            notifyError("Something went wrong", 0);
        } else if (!wishlistWrapper.status) {
            notifyError(wishlistWrapper.message, wishlistWrapper.login_error);
        } else {
            postMessage(wishlistWrapper.wishlistProducts);
        }
    }
}
