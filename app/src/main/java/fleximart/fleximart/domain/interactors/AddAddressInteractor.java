package fleximart.fleximart.domain.interactors;

public interface AddAddressInteractor {
    interface Callback {
        void onAddAddressSuccess();
        void onAddAddressFail(String errorMsg, int loginError);
    }
}
