package fleximart.fleximart.domain.interactors.impl;

import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.FetchLevelDowlinesInteractor;
import fleximart.fleximart.domain.interactors.base.AbstractInteractor;
import fleximart.fleximart.domain.model.LevelDownlines;
import fleximart.fleximart.domain.model.LevelDownlinesWrapper;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class FetchLevelDownlinesInteractorImpl extends AbstractInteractor implements FetchLevelDowlinesInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int nodeId;
    int level;

    public FetchLevelDownlinesInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int nodeId, int level) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.nodeId = nodeId;
        this.level = level;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onFetchLevelDownlinesFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(LevelDownlines[] levelDownlines){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onFetchLevelDownlinesSuccess(levelDownlines);
            }
        });
    }

    @Override
    public void run() {
        final LevelDownlinesWrapper levelDownlinesWrapper = mRepository.fetchMyDownlinesOfLevels(apiToken, nodeId, level);
        if (levelDownlinesWrapper == null) {
            notifyError("Something Went Wrong", levelDownlinesWrapper.login_error);
        } else if (!levelDownlinesWrapper.status) {
            notifyError(levelDownlinesWrapper.message, levelDownlinesWrapper.login_error);
        } else {
            postMessage(levelDownlinesWrapper.levelDownlines);
        }
    }
}
