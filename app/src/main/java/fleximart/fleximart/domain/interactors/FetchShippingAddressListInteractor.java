package fleximart.fleximart.domain.interactors;

import fleximart.fleximart.domain.model.Address;

public interface FetchShippingAddressListInteractor {
    interface Callback {
        void onGettingShippingAddressSuccess(Address[] addresses);
        void onGettingShippingAddressFail(String errorMsg, int isLoginError);
    }
}
