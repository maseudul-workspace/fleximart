package fleximart.fleximart.domain.interactors;

public interface AddToCartInteractor {
    interface Callback {
        void onAddToCartSuccess();
        void onAddToCartFail(String errorMsg);
    }
}
