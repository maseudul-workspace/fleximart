package fleximart.fleximart.domain.interactors;

import fleximart.fleximart.domain.model.ErrorMesage;

public interface BuyNodeInteractor {
    interface Callback {
        void onBuyNodeSuccess();
        void onBuyNodeFail(String errorMsg, int loginError, boolean error, ErrorMesage errorMesage);
    }
}
