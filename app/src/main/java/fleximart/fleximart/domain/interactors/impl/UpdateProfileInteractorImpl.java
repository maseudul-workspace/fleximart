package fleximart.fleximart.domain.interactors.impl;

import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.UpdateProfileInteractor;
import fleximart.fleximart.domain.interactors.base.AbstractInteractor;
import fleximart.fleximart.domain.model.CommonResponse;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class UpdateProfileInteractorImpl extends AbstractInteractor implements UpdateProfileInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String authorization;
    int userId;
    String name;
    String email;
    String mobile;
    String DOB;
    String gender;
    String area;
    int state;
    int city;
    String pin;
    String address;

    public UpdateProfileInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String authorization, int userId, String name, String email, String mobile, String DOB, String gender, String area, int state, int city, String pin, String address) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.authorization = authorization;
        this.userId = userId;
        this.name = name;
        this.email = email;
        this.mobile = mobile;
        this.DOB = DOB;
        this.gender = gender;
        this.area = area;
        this.state = state;
        this.city = city;
        this.pin = pin;
        this.address = address;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onProfileUpdateFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onProfileUpdateSuccess();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = mRepository.updateProfile(authorization, userId, name, email, mobile, DOB, gender, area, state, city, pin, address);
        if (commonResponse == null) {
            notifyError("Something went wrong", 0);
        } else if (!commonResponse.status) {
            notifyError(commonResponse.message, commonResponse.login_error);
        } else {
            postMessage();
        }
    }
}
