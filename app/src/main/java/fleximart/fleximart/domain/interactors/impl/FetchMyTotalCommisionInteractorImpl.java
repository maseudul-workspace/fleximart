package fleximart.fleximart.domain.interactors.impl;

import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.FetchMyTotalCommisionInteractor;
import fleximart.fleximart.domain.interactors.base.AbstractInteractor;
import fleximart.fleximart.domain.model.CommisionTotal;
import fleximart.fleximart.domain.model.CommisionTotalWrapper;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class FetchMyTotalCommisionInteractorImpl extends AbstractInteractor implements FetchMyTotalCommisionInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int userId;

    public FetchMyTotalCommisionInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int userId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.userId = userId;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingTotalCommisionFail(loginError, errorMsg);
            }
        });
    }

    private void postMessage(CommisionTotal commisionTotal){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingTotalCommisionSuccess(commisionTotal);
            }
        });
    }

    @Override
    public void run() {
        final CommisionTotalWrapper commisionTotalWrapper = mRepository.fetchMyTotalCommision(apiToken, userId);
        if (commisionTotalWrapper == null) {
            notifyError("Something went wrong", 0);
        } else if (!commisionTotalWrapper.status) {
            notifyError(commisionTotalWrapper.message, commisionTotalWrapper.login_error);
        } else {
            postMessage(commisionTotalWrapper.commisionTotal);
        }
    }
}
