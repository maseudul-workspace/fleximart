package fleximart.fleximart.domain.interactors.impl;

import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.FetchUserProfileInteractor;
import fleximart.fleximart.domain.interactors.base.AbstractInteractor;
import fleximart.fleximart.domain.model.UserProfileData;
import fleximart.fleximart.domain.model.UserProfileDataWrapper;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class FetchUserProfileInteractorImpl extends AbstractInteractor implements FetchUserProfileInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int userId;
    String apiKey;

    public FetchUserProfileInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int userId, String apiKey) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.userId = userId;
        this.apiKey = apiKey;
    }

    private void notifyError(String errorMsg, int isLoginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingUserProfileFail(errorMsg, isLoginError);
            }
        });
    }

    private void postMessage(UserProfileData userProfileData){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingUserProfileSuccess(userProfileData);
            }
        });
    }

    @Override
    public void run() {
        final UserProfileDataWrapper userProfileDataWrapper = mRepository.fetchUserProfile(userId, apiKey);
        if (userProfileDataWrapper == null) {
            notifyError("Something went wrong", 0);
        } else if (!userProfileDataWrapper.status) {
            notifyError(userProfileDataWrapper.message, userProfileDataWrapper.login_error);
        } else {
            postMessage(userProfileDataWrapper.userProfileData);
        }
    }
}
