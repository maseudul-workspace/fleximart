package fleximart.fleximart.domain.interactors;

import fleximart.fleximart.domain.model.MyCommissionList;

public interface FetchApprovedCommissionListInteractor {
    interface Callback {
        void onFetchApprovedCommisionListSuccess(MyCommissionList[] commissionLists, int totalPage, int currentPage);
        void onFetchApprovedCommisionListFail(String errorMsg, int loginError);
    }
}
