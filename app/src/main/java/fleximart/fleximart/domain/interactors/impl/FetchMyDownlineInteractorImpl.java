package fleximart.fleximart.domain.interactors.impl;

import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.FetchMyDownlinesInteractor;
import fleximart.fleximart.domain.interactors.base.AbstractInteractor;
import fleximart.fleximart.domain.model.Downline;
import fleximart.fleximart.domain.model.DownlineWrapper;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class FetchMyDownlineInteractorImpl extends AbstractInteractor implements FetchMyDownlinesInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int nodeId;
    int page;

    public FetchMyDownlineInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int nodeId, int page) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.nodeId = nodeId;
        this.page = page;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onFetchMyDownlineFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(Downline[] downlines, int currentPage, int totalPage){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onFetchMyDownlineSuccess(downlines, currentPage, totalPage);
            }
        });
    }

    @Override
    public void run() {
        final DownlineWrapper downlineWrapper = mRepository.fetchMyDownlines(apiToken, nodeId, page);
        if (downlineWrapper == null) {
            notifyError("Something went wrong", 0);
        } else if (!downlineWrapper.status) {
            notifyError(downlineWrapper.message, downlineWrapper.login_error);
        } else {
            postMessage(downlineWrapper.downlines, downlineWrapper.currentPage, downlineWrapper.totalPage);
        }
    }

}
