package fleximart.fleximart.domain.interactors;

import fleximart.fleximart.domain.model.WalletHistory;

public interface FetchWalletHistoryInteractor {
    interface Callback {
        void onWalletHistoryFetchSuccess(WalletHistory[] walletHistories, int totalPage, int currentPage);
        void onWalletHistoryFetchFail(String errorMsg, int loginError);
    }
}
