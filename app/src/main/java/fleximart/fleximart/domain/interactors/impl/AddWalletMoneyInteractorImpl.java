package fleximart.fleximart.domain.interactors.impl;

import android.telecom.Call;

import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.AddWalletMoneyInteractor;
import fleximart.fleximart.domain.interactors.base.AbstractInteractor;
import fleximart.fleximart.domain.model.OrderPlaceData;
import fleximart.fleximart.domain.model.PlaceOrderResponse;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class AddWalletMoneyInteractorImpl extends AbstractInteractor implements AddWalletMoneyInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int userId;
    String amount;

    public AddWalletMoneyInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int userId, String amount) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.userId = userId;
        this.amount = amount;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onAddWalletMoneyFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(OrderPlaceData orderPlaceData){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onAddWalletMoneySuccess(orderPlaceData);
            }
        });
    }

    @Override
    public void run() {
        final PlaceOrderResponse placeOrderResponse = mRepository.addWalletMoney(apiToken, userId, amount);
        if (placeOrderResponse ==  null) {
            notifyError("Something Went Wrong", placeOrderResponse.login_error);
        } else if (!placeOrderResponse.status) {
            notifyError(placeOrderResponse.message, placeOrderResponse.login_error);
        } else {
            postMessage(placeOrderResponse.orderPlaceData);
        }
    }
}
