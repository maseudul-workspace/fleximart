package fleximart.fleximart.domain.interactors;

public interface CompleteWalletPayInteractor {
    interface Callback {
        void onCompleteWalletPaySuccess();
        void onCompleteWalletPayFail(String errorMsg, int loginError);
    }
}
