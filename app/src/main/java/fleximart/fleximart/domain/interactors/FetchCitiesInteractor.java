package fleximart.fleximart.domain.interactors;

import fleximart.fleximart.domain.model.Cities;

public interface FetchCitiesInteractor {
    interface Callback {
        void onGettingCitiesSuccess(Cities[] cities);
        void onGettingCitiesFail();
    }
}
