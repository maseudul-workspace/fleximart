package fleximart.fleximart.domain.interactors.impl;

import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.CheckBuyerPhoneInteractor;
import fleximart.fleximart.domain.interactors.base.AbstractInteractor;
import fleximart.fleximart.domain.model.CheckPhoneData;
import fleximart.fleximart.domain.model.CheckPhoneDataResponse;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class CheckBuyerPhoneInteractorImpl extends AbstractInteractor implements CheckBuyerPhoneInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apitoken;
    String phoneNo;

    public CheckBuyerPhoneInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apitoken, String phoneNo) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apitoken = apitoken;
        this.phoneNo = phoneNo;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onCheckingPhoneFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(CheckPhoneData checkPhoneData){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onCheckingPhoneSuccess(checkPhoneData);
            }
        });
    }


    @Override
    public void run() {
        final CheckPhoneDataResponse checkPhoneDataResponse = mRepository.checkBuyerMobile(apitoken, phoneNo);
        if (checkPhoneDataResponse == null) {
            notifyError("Something went wrong", 0);
        } else if (!checkPhoneDataResponse.status) {
            notifyError(checkPhoneDataResponse.message, checkPhoneDataResponse.login_error);
        } else {
            postMessage(checkPhoneDataResponse.checkPhoneData);
        }
    }
}
