package fleximart.fleximart.domain.interactors;

import fleximart.fleximart.domain.model.MyNodes;

public interface FetchMyNodesInteractor {
    interface Callback {
        void onFetchingNodesSuccess(MyNodes[] myNodes);
        void onFetchingNodesFail(String errorMsg, int loginError);
    }
}
