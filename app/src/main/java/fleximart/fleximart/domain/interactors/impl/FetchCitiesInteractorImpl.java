package fleximart.fleximart.domain.interactors.impl;

import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.FetchCitiesInteractor;
import fleximart.fleximart.domain.interactors.base.AbstractInteractor;
import fleximart.fleximart.domain.model.Cities;
import fleximart.fleximart.domain.model.CitiesWrapper;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class FetchCitiesInteractorImpl extends AbstractInteractor implements FetchCitiesInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int stateId;

    public FetchCitiesInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int stateId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.stateId = stateId;
    }

    private void notifyError() {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingCitiesFail();
            }
        });
    }

    private void postMessage(Cities[] cities){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingCitiesSuccess(cities);
            }
        });
    }

    @Override
    public void run() {
        final CitiesWrapper citiesWrapper = mRepository.fetchCities(stateId);
        if (citiesWrapper == null) {
            notifyError();
        } else if (!citiesWrapper.status) {
            notifyError();
        } else {
            postMessage(citiesWrapper.cities);
        }
    }
}
