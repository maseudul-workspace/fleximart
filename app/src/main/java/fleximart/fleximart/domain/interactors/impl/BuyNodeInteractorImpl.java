package fleximart.fleximart.domain.interactors.impl;

import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.BuyNodeInteractor;
import fleximart.fleximart.domain.interactors.base.AbstractInteractor;
import fleximart.fleximart.domain.model.CommonResponse;
import fleximart.fleximart.domain.model.ErrorMesage;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class BuyNodeInteractorImpl extends AbstractInteractor implements BuyNodeInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String authorization;
    int userId;
    int registrationMethod;
    String sponsorNode;
    int totalNode;
    String leg;
    String name;
    String email;
    String DOB;
    String gender;
    int state;
    int city;
    String aadhar;
    String pan;
    String pin;
    String address;
    String mobile;

    public BuyNodeInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String authorization, int userId, int registrationMethod, String sponsorNode, int totalNode, String leg, String name, String email, String DOB, String gender, int state, int city, String aadhar, String pan, String pin, String address, String mobile) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.authorization = authorization;
        this.userId = userId;
        this.registrationMethod = registrationMethod;
        this.sponsorNode = sponsorNode;
        this.totalNode = totalNode;
        this.leg = leg;
        this.name = name;
        this.email = email;
        this.DOB = DOB;
        this.gender = gender;
        this.state = state;
        this.city = city;
        this.aadhar = aadhar;
        this.pan = pan;
        this.pin = pin;
        this.address = address;
        this.mobile = mobile;
    }

    private void notifyError(String errorMsg, int loginError, boolean error, ErrorMesage errorMesage) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onBuyNodeFail(errorMsg, loginError, error, errorMesage);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onBuyNodeSuccess();
            }
        });
    }

    @Override
    public void run() {
        CommonResponse commonResponse = mRepository.buyNodes(authorization, userId, registrationMethod, sponsorNode, totalNode, leg, name, email, DOB, gender, state, city, aadhar, pan, pin, address, mobile);
        if (commonResponse == null) {
            notifyError("Something went wrong", 0, false, null);
        } else if (!commonResponse.status) {
            notifyError(commonResponse.message, commonResponse.login_error, commonResponse.error, commonResponse.errorMesage);
        } else {
            postMessage();
        }
    }
}
