package fleximart.fleximart.domain.interactors;

import fleximart.fleximart.domain.model.OrderPlaceData;

public interface AddWalletMoneyInteractor {
    interface Callback {
        void onAddWalletMoneySuccess(OrderPlaceData orderPlaceData);
        void onAddWalletMoneyFail(String errorMsg, int loginError);
    }
}
