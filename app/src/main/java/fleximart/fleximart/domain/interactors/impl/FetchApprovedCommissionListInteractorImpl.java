package fleximart.fleximart.domain.interactors.impl;

import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.FetchApprovedCommissionListInteractor;
import fleximart.fleximart.domain.interactors.base.AbstractInteractor;
import fleximart.fleximart.domain.model.MyCommissionList;
import fleximart.fleximart.domain.model.MyCommissionListWrapper;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class FetchApprovedCommissionListInteractorImpl extends AbstractInteractor implements FetchApprovedCommissionListInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int userId;
    String apiToken;
    int page;

    public FetchApprovedCommissionListInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int userId, String apiToken, int page) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.userId = userId;
        this.apiToken = apiToken;
        this.page = page;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onFetchApprovedCommisionListFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(MyCommissionList[] commissionLists, int totalPage, int currentPage){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onFetchApprovedCommisionListSuccess(commissionLists, totalPage, currentPage);
            }
        });
    }

    @Override
    public void run() {
        final MyCommissionListWrapper myCommissionListWrapper = mRepository.fetchApprovedCommission(apiToken, userId, page);
        if (myCommissionListWrapper == null) {
            notifyError(myCommissionListWrapper.message, 0);
        } else if (!myCommissionListWrapper.status) {
            notifyError(myCommissionListWrapper.message, myCommissionListWrapper.login_error);
        } else {
            postMessage(myCommissionListWrapper.commissionLists, myCommissionListWrapper.totalPage, myCommissionListWrapper.currentPage);
        }
    }
}
