package fleximart.fleximart.domain.interactors;

import fleximart.fleximart.domain.model.ProductDetails;

public interface FetchProductsWithoutFilterInteractor {
    interface Callback {
        void onFetchingProductListWithoutFilterSuccess(ProductDetails[] productDetails, int totalPage);
        void onFetchingProductListWithoutFilterFail(String errorMsg);
    }
}
