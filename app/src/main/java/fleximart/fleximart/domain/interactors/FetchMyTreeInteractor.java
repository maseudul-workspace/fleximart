package fleximart.fleximart.domain.interactors;

import fleximart.fleximart.domain.model.Tree;

public interface FetchMyTreeInteractor {
    interface Callback {
        void onMyTreeFetchSuccess(Tree[] trees);
        void onMyTreeFetchFail(String errorMsg, int loginError);
    }
}
