package fleximart.fleximart.domain.interactors;

import fleximart.fleximart.domain.model.ChecksumData;

public interface GenerateChecksumInteractor {
    interface Callback {
        void onGenerateChecksumSuccess(ChecksumData checksumData);
        void onGenerateChecksumFail(String errorMsg, int loginError);
    }
}
