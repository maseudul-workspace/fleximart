package fleximart.fleximart.domain.interactors;

public interface DeleteFromCartInteractor {
    interface Callback {
        void onDeleteFromCartSuccess();
        void onDeleteFromCartFailed(String errorMsg);
    }
}
