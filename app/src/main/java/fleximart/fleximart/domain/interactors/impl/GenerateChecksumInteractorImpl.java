package fleximart.fleximart.domain.interactors.impl;

import android.util.Log;

import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.GenerateChecksumInteractor;
import fleximart.fleximart.domain.interactors.base.AbstractInteractor;
import fleximart.fleximart.domain.model.ChecksumData;
import fleximart.fleximart.domain.model.ChecksumDataWrapper;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class GenerateChecksumInteractorImpl extends AbstractInteractor implements GenerateChecksumInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String authorization;
    String MID;
    int ORDER_ID;
    int CUST_ID;
    String MOBILE_NO;
    String EMAIL;
    String CHANNEL_ID;
    String TXN_AMOUNT;
    String WEBSITE;
    String INDUSTRY_TYPE_ID;
    String CALLBACK_URL;

    public GenerateChecksumInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String authorization, String MID, int ORDER_ID, int CUST_ID, String MOBILE_NO, String EMAIL, String CHANNEL_ID, String TXN_AMOUNT, String WEBSITE, String INDUSTRY_TYPE_ID, String CALLBACK_URL) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.authorization = authorization;
        this.MID = MID;
        this.ORDER_ID = ORDER_ID;
        this.CUST_ID = CUST_ID;
        this.MOBILE_NO = MOBILE_NO;
        this.EMAIL = EMAIL;
        this.CHANNEL_ID = CHANNEL_ID;
        this.TXN_AMOUNT = TXN_AMOUNT;
        this.WEBSITE = WEBSITE;
        this.INDUSTRY_TYPE_ID = INDUSTRY_TYPE_ID;
        this.CALLBACK_URL = CALLBACK_URL;
    }

    private void notifyError(String errorMsg, int isLoginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGenerateChecksumFail(errorMsg, isLoginError);
            }
        });
    }

    private void postMessage(ChecksumData checksumData){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGenerateChecksumSuccess(checksumData);
            }
        });
    }

    @Override
    public void run() {
        final ChecksumDataWrapper checksumDataWrapper = mRepository.generateChecksum(authorization, MID, ORDER_ID, CUST_ID, MOBILE_NO, EMAIL, CHANNEL_ID, TXN_AMOUNT, WEBSITE, INDUSTRY_TYPE_ID, CALLBACK_URL);
        if (checksumDataWrapper == null) {
            notifyError("Something Went Wrong", checksumDataWrapper.login_error);
        } else if (!checksumDataWrapper.status) {
            notifyError("Something Went Wrong", checksumDataWrapper.login_error);
        } else {
            postMessage(checksumDataWrapper.checksumDataOriginal.checksumData);
        }
    }
}
