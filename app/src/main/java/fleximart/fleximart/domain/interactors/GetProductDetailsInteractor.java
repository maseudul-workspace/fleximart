package fleximart.fleximart.domain.interactors;

import fleximart.fleximart.domain.model.ProductDetailsData;

public interface GetProductDetailsInteractor {
    interface Callback {
        void onGettingProductDetailsSuccess(ProductDetailsData productDetailsData);
        void onProductDetailsFail(String errorMsg);
    }
}
