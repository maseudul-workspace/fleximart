package fleximart.fleximart.domain.interactors.impl;

import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.FetchMemberDashboardSummaryInteractor;
import fleximart.fleximart.domain.interactors.base.AbstractInteractor;
import fleximart.fleximart.domain.model.DashboardData;
import fleximart.fleximart.domain.model.DashboardSummaryWrapper;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class FetchMemberDashboardSummaryInteractorImpl extends AbstractInteractor implements FetchMemberDashboardSummaryInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int userId;

    public FetchMemberDashboardSummaryInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback callback, String apiToken, int userId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = callback;
        this.apiToken = apiToken;
        this.userId = userId;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onSummaryFetchFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(DashboardData dashboardData){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onSummaryFetchSuccess(dashboardData);
            }
        });
    }


    @Override
    public void run() {
        final DashboardSummaryWrapper dashboardSummaryWrapper = mRepository.fetchMemberDashboardSummary(apiToken, userId);
        if (dashboardSummaryWrapper == null) {
            notifyError("Something Went Wrong", dashboardSummaryWrapper.login_error);
        } else if (!dashboardSummaryWrapper.status) {
            notifyError(dashboardSummaryWrapper.message, dashboardSummaryWrapper.login_error);
        } else {
            postMessage(dashboardSummaryWrapper.dashboardData);
        }
    }
}
