package fleximart.fleximart.domain.interactors;

import fleximart.fleximart.domain.model.OrderPlaceData;

public interface PlaceOrderInteractor {
    interface Callback {
        void onOrderPlaceSuccess(OrderPlaceData orderPlaceData);
        void onOrderPlaceFail(String errorMsg, int loginError);
    }
}
