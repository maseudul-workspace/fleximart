package fleximart.fleximart.domain.interactors.impl;

import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.FetchWalletHistoryInteractor;
import fleximart.fleximart.domain.interactors.base.AbstractInteractor;
import fleximart.fleximart.domain.model.WalletHistory;
import fleximart.fleximart.domain.model.WalletHistoryWrapper;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class FetchWalletHistoryInteractorImpl extends AbstractInteractor implements FetchWalletHistoryInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int userId;
    int page;

    public FetchWalletHistoryInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int userId, int page) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.userId = userId;
        this.page = page;
    }

    private void notifyError(String errorMsg, int isLoginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onWalletHistoryFetchFail(errorMsg, isLoginError);
            }
        });
    }

    private void postMessage(WalletHistory[] walletHistories, int totalPage, int currentPage){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onWalletHistoryFetchSuccess(walletHistories, totalPage, currentPage);
            }
        });
    }

    @Override
    public void run() {
        final WalletHistoryWrapper walletHistoryWrapper = mRepository.fetchWalletHistory(apiToken, userId, page);
        if (walletHistoryWrapper == null) {
            notifyError("Somehthing went wrong", 0);
        } else if (!walletHistoryWrapper.status) {
            notifyError(walletHistoryWrapper.message, walletHistoryWrapper.login_error);
        } else {
            postMessage(walletHistoryWrapper.walletHistories, walletHistoryWrapper.totalPage, walletHistoryWrapper.currentPage);
        }
    }
}
