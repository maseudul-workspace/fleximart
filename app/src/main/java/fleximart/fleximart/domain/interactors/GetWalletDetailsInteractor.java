package fleximart.fleximart.domain.interactors;

import fleximart.fleximart.domain.model.WalletDetails;

public interface GetWalletDetailsInteractor {
    interface Callback {
        void onGettingWalletDetailsSuccess(WalletDetails walletDetails);
        void onGettingWalletDetailsFail(String errorMsg, int loginError);
    }
}
