package fleximart.fleximart.domain.interactors;

public interface RemoveShippingAddressInteractor {
    interface Callback {
        void onRemoveAddressSuccess();
        void onRemoveAddressFail(String errorMsg, int loginError);
    }
}
