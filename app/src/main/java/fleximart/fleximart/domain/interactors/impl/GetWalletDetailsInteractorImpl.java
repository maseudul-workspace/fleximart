package fleximart.fleximart.domain.interactors.impl;

import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.GetWalletDetailsInteractor;
import fleximart.fleximart.domain.interactors.base.AbstractInteractor;
import fleximart.fleximart.domain.model.WalletDetails;
import fleximart.fleximart.domain.model.WalletDetailsWrapper;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class GetWalletDetailsInteractorImpl extends AbstractInteractor implements GetWalletDetailsInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int userId;

    public GetWalletDetailsInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int userId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.userId = userId;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingWalletDetailsFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(WalletDetails walletDetails) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingWalletDetailsSuccess(walletDetails);
            }
        });
    }

    @Override
    public void run() {
        final WalletDetailsWrapper walletDetailsWrapper = mRepository.getWalletDetails(apiToken, userId);
        if (walletDetailsWrapper == null) {
            notifyError("Something went wrong", 0);
        } else if (!walletDetailsWrapper.status) {
            notifyError(walletDetailsWrapper.message, walletDetailsWrapper.login_error);
        } else {
            postMessage(walletDetailsWrapper.walletDetails);
        }
    }
}
