package fleximart.fleximart.domain.interactors.impl;

import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.AddToCartInteractor;
import fleximart.fleximart.domain.interactors.base.AbstractInteractor;
import fleximart.fleximart.domain.model.CommonResponse;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class AddToCartInteractorImpl extends AbstractInteractor implements AddToCartInteractor {

    AppRepositoryImpl appRepository;
    Callback mCallback;
    String authorization;
    int userId;
    int productId;
    int quantity;
    int colorId;
    int sizeId;

    public AddToCartInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl appRepository, Callback mCallback, String authorization, int userId, int productId, int quantity, int colorId, int sizeId) {
        super(threadExecutor, mainThread);
        this.appRepository = appRepository;
        this.mCallback = mCallback;
        this.authorization = authorization;
        this.userId = userId;
        this.productId = productId;
        this.quantity = quantity;
        this.colorId = colorId;
        this.sizeId = sizeId;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onAddToCartFail(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onAddToCartSuccess();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = appRepository.addToCart(authorization, userId, productId, quantity, colorId, sizeId);
        if (commonResponse == null) {
            notifyError("Something went wrong");
        } else if(!commonResponse.status) {
            notifyError(commonResponse.message);
        } else {
            postMessage();
        }
    }
}
