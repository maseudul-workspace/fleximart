package fleximart.fleximart.domain.interactors.impl;

import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.FetchMyTreeInteractor;
import fleximart.fleximart.domain.interactors.base.AbstractInteractor;
import fleximart.fleximart.domain.model.Tree;
import fleximart.fleximart.domain.model.TreeWrapper;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class FetchMyTreeInteractorImpl extends AbstractInteractor implements FetchMyTreeInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int nodeId;
    int payNodeId;
    int rank;

    public FetchMyTreeInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int nodeId, int payNodeId, int rank) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.nodeId = nodeId;
        this.payNodeId = payNodeId;
        this.rank = rank;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onMyTreeFetchFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(Tree[] trees){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onMyTreeFetchSuccess(trees);
            }
        });
    }


    @Override
    public void run() {
        final TreeWrapper treeWrapper = mRepository.fetchMyTree(apiToken, nodeId, payNodeId, rank);
        if (treeWrapper == null) {
            notifyError("Something Went Wrong", 0);
        } else if (!treeWrapper.status) {
            notifyError(treeWrapper.message, treeWrapper.login_error);
        } else {
            postMessage(treeWrapper.trees);
        }
    }
}
