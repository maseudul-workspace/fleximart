package fleximart.fleximart.domain.interactors.impl;

import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.CompleteWalletPayInteractor;
import fleximart.fleximart.domain.interactors.base.AbstractInteractor;
import fleximart.fleximart.domain.model.CommonResponse;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class CompleteWalletPayInteractorImpl extends AbstractInteractor implements CompleteWalletPayInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int orderId;
    String transactionId;

    public CompleteWalletPayInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int orderId, String transactionId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.orderId = orderId;
        this.transactionId = transactionId;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onCompleteWalletPayFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onCompleteWalletPaySuccess();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = mRepository.completeWalletPay(apiToken, orderId, transactionId);
        if (commonResponse == null) {
            notifyError("Something Went Wrong", 0);
        } else if (!commonResponse.status) {
            notifyError(commonResponse.message, commonResponse.login_error);
        } else {
            postMessage();
        }
    }
}
