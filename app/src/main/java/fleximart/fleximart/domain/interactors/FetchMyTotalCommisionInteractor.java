package fleximart.fleximart.domain.interactors;

import fleximart.fleximart.domain.model.CommisionTotal;

public interface FetchMyTotalCommisionInteractor {
    interface Callback {
        void onGettingTotalCommisionSuccess(CommisionTotal commisionTotal);
        void onGettingTotalCommisionFail(int loginError, String errorMsg);
    }
}
