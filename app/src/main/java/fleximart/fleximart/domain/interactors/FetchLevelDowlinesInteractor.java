package fleximart.fleximart.domain.interactors;

import fleximart.fleximart.domain.model.LevelDownlines;

public interface FetchLevelDowlinesInteractor {
    interface Callback {
        void onFetchLevelDownlinesSuccess(LevelDownlines[] levelDownlines);
        void onFetchLevelDownlinesFail(String errorMsg, int loginError);
    }
}
