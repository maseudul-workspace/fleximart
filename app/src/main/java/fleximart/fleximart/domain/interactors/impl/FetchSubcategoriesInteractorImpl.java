package fleximart.fleximart.domain.interactors.impl;

import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.FetchSubcategoriesInteractor;
import fleximart.fleximart.domain.interactors.base.AbstractInteractor;
import fleximart.fleximart.domain.model.FirstCategory;
import fleximart.fleximart.domain.model.MainData;
import fleximart.fleximart.domain.model.SubcategoryWrapper;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class FetchSubcategoriesInteractorImpl extends AbstractInteractor implements FetchSubcategoriesInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int id;

    public FetchSubcategoriesInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int id) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.id = id;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingSubcategoriesFail(errorMsg);
            }
        });
    }

    private void postMessage(FirstCategory[] firstCategories){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingSubcategoriesSuccess(firstCategories);
            }
        });
    }

    @Override
    public void run() {
        final SubcategoryWrapper subcategoryWrapper = mRepository.getSubcategories(id);
        if (subcategoryWrapper == null) {
            notifyError("Something went wrong");
        } else if (!subcategoryWrapper.status) {
            notifyError(subcategoryWrapper.message);
        } else {
            postMessage(subcategoryWrapper.firstCategories);
        }
    }

}
