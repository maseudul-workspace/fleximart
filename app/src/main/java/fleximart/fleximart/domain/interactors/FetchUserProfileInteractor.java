package fleximart.fleximart.domain.interactors;

import fleximart.fleximart.domain.model.UserProfileData;

public interface FetchUserProfileInteractor {
    interface Callback {
        void onGettingUserProfileSuccess(UserProfileData userProfileData);
        void onGettingUserProfileFail(String errorMsg, int loginError);
    }
}
