package fleximart.fleximart.domain.interactors;

import fleximart.fleximart.domain.model.CheckPhoneData;

public interface CheckBuyerPhoneInteractor {
    interface Callback {
        void onCheckingPhoneSuccess(CheckPhoneData checkPhoneData);
        void onCheckingPhoneFail(String errorMsg, int loginError);
    }
}
