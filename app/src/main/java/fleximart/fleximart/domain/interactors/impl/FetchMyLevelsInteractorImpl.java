package fleximart.fleximart.domain.interactors.impl;

import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.FetchMyLevelsInteractor;
import fleximart.fleximart.domain.interactors.base.AbstractInteractor;
import fleximart.fleximart.domain.model.MyLevels;
import fleximart.fleximart.domain.model.MyLevelsWrapper;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class FetchMyLevelsInteractorImpl extends AbstractInteractor implements FetchMyLevelsInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int nodeId;

    public FetchMyLevelsInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int nodeId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.nodeId = nodeId;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingMyLevelFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(MyLevels[] myLevels){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingMyLevelSuccess(myLevels);
            }
        });
    }

    @Override
    public void run() {
        final MyLevelsWrapper myLevelsWrapper = mRepository.fetchMyLevels(apiToken, nodeId);
        if (myLevelsWrapper == null) {
            notifyError("Something Went Wrong", myLevelsWrapper.login_error);
        } else if (!myLevelsWrapper.status) {
            notifyError(myLevelsWrapper.message, myLevelsWrapper.login_error);
        } else {
            postMessage(myLevelsWrapper.myLevels);
        }
    }
}
