package fleximart.fleximart.domain.interactors;

import fleximart.fleximart.domain.model.FirstCategory;

public interface FetchSubcategoriesInteractor {
    interface Callback {
        void onGettingSubcategoriesSuccess(FirstCategory[] firstCategories);
        void onGettingSubcategoriesFail(String errorMsg);
    }
}
