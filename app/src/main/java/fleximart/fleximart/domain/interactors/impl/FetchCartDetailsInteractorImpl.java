package fleximart.fleximart.domain.interactors.impl;

import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.FetchCartDetailsInteractor;
import fleximart.fleximart.domain.interactors.base.AbstractInteractor;
import fleximart.fleximart.domain.model.CartDetails;
import fleximart.fleximart.domain.model.CartDetailsWrapper;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class FetchCartDetailsInteractorImpl extends AbstractInteractor implements FetchCartDetailsInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int userId;
    String apiToken;

    public FetchCartDetailsInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int userId, String apiToken) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.userId = userId;
        this.apiToken = apiToken;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingCartDetailsFail(errorMsg);
            }
        });
    }

    private void postMessage(CartDetails[] cartDetails){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingCartDetailsSucess(cartDetails);
            }
        });
    }

    @Override
    public void run() {
        CartDetailsWrapper cartDetailsWrapper = mRepository.fetchCartDetails(userId, apiToken);
        if (cartDetailsWrapper == null) {
            notifyError("Something went wrong");
        } else if (!cartDetailsWrapper.status) {
            notifyError(cartDetailsWrapper.message);
        } else {
            postMessage(cartDetailsWrapper.cartDetails);
        }
    }
}
