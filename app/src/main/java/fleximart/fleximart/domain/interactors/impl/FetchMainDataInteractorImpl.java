package fleximart.fleximart.domain.interactors.impl;

import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.FetchMainDataInteractor;
import fleximart.fleximart.domain.interactors.base.AbstractInteractor;
import fleximart.fleximart.domain.model.MainData;
import fleximart.fleximart.domain.model.MainDataWrapper;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class FetchMainDataInteractorImpl extends AbstractInteractor implements FetchMainDataInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;

    public FetchMainDataInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
    }

    private void notifyError() {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingMaiDataFail();
            }
        });
    }

    private void postMessage(MainData mainData){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingMainDataSuccess(mainData);
            }
        });
    }

    @Override
    public void run() {
        final MainDataWrapper mainDataWrapper = mRepository.getMainData();
        if (mainDataWrapper == null) {
            notifyError();
        } else if (!mainDataWrapper.status) {
            notifyError();
        } else {
            postMessage(mainDataWrapper.mainData);
        }
    }
}
