package fleximart.fleximart.domain.interactors;

import fleximart.fleximart.domain.model.MyCommissionList;

public interface FetchUnapprovedCommissionListInteractor {
    interface Callback {
        void onFetchUnapprovedCommisionListSuccess(MyCommissionList[] commissionLists, int totalPage, int currentPage);
        void onFetchUnapprovedCommisionListFail(String errorMsg, int loginError);
    }
}
