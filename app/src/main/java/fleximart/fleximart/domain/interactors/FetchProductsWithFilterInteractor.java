package fleximart.fleximart.domain.interactors;

import fleximart.fleximart.domain.model.ProductListWithFilter;

public interface FetchProductsWithFilterInteractor {
    interface Callback {
        void onFetchingProductListWithFilterSuccess(ProductListWithFilter productListWithFilter, int totalPage);
        void onFetchingProductListWithFilterFail(String errorMsg);
    }
}
