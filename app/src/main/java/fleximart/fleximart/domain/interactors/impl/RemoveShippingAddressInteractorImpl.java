package fleximart.fleximart.domain.interactors.impl;

import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.RemoveShippingAddressInteractor;
import fleximart.fleximart.domain.interactors.base.AbstractInteractor;
import fleximart.fleximart.domain.model.CommonResponse;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class RemoveShippingAddressInteractorImpl extends AbstractInteractor implements RemoveShippingAddressInteractor {

    AppRepositoryImpl mRepostory;
    Callback mCallback;
    String apiToken;
    int addressId;

    public RemoveShippingAddressInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepostory, Callback mCallback, String apiToken, int addressId) {
        super(threadExecutor, mainThread);
        this.mRepostory = mRepostory;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.addressId = addressId;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onRemoveAddressFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage() {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onRemoveAddressSuccess();
            }
        });
    }

    @Override
    public void run() {
        CommonResponse commonResponse = mRepostory.removeShippingAddress(apiToken, addressId);
        if (commonResponse == null) {
            notifyError("Something went wrong", 0);
        } else if (!commonResponse.status) {
            notifyError(commonResponse.message, commonResponse.login_error);
        } else {
            postMessage();
        }
    }
}
