package fleximart.fleximart.domain.interactors;

import fleximart.fleximart.domain.model.MyLevels;

public interface FetchMyLevelsInteractor {
    interface Callback {
        void onGettingMyLevelSuccess(MyLevels[] myLevels);
        void onGettingMyLevelFail(String errorMsg, int loginError);
    }
}
