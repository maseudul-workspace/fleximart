package fleximart.fleximart.domain.interactors.impl;

import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.PlaceOrderInteractor;
import fleximart.fleximart.domain.interactors.base.AbstractInteractor;
import fleximart.fleximart.domain.model.OrderPlaceData;
import fleximart.fleximart.domain.model.PlaceOrderResponse;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class PlaceOrderInteractorImpl extends AbstractInteractor implements PlaceOrderInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int userId;
    int shippingAddresssId;
    int paymentMethod;
    int walletPay;

    public PlaceOrderInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int userId, int shippingAddresssId, int paymentMethod, int walletPay) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.userId = userId;
        this.shippingAddresssId = shippingAddresssId;
        this.paymentMethod = paymentMethod;
        this.walletPay = walletPay;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onOrderPlaceFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(OrderPlaceData orderPlaceData){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onOrderPlaceSuccess(orderPlaceData);
            }
        });
    }

    @Override
    public void run() {
        final PlaceOrderResponse placeOrderResponse = mRepository.placeOrder(apiToken, userId, shippingAddresssId, paymentMethod, walletPay);
        if (placeOrderResponse == null) {
            notifyError("Something Went Wrong", 0);
        } else if (!placeOrderResponse.status) {
            notifyError(placeOrderResponse.message, placeOrderResponse.login_error);
        } else {
            postMessage(placeOrderResponse.orderPlaceData);
        }
    }
}
