package fleximart.fleximart.domain.interactors;

public interface UpdateCartInteractor {
    interface Callback {
        void onUpdateSuccess();
        void onUpdateFailed(String errorMsg);
    }
}
