package fleximart.fleximart.domain.interactors.impl;

import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.UpdateCartInteractor;
import fleximart.fleximart.domain.interactors.base.AbstractInteractor;
import fleximart.fleximart.domain.model.CommonResponse;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class UpdateCartInteractorImpl extends AbstractInteractor implements UpdateCartInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String authorization;
    int userId;
    int cartId;
    int quantity;
    int sizeId;

    public UpdateCartInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String authorization, int userId, int cartId, int quantity, int sizeId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.authorization = authorization;
        this.userId = userId;
        this.cartId = cartId;
        this.quantity = quantity;
        this.sizeId = sizeId;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onUpdateFailed(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onUpdateSuccess();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = mRepository.updateCart(authorization, userId, cartId, quantity, sizeId);
        if (commonResponse == null) {
            notifyError("Something went wrong");
        } else if (!commonResponse.status) {
            notifyError(commonResponse.message);
        } else {
            postMessage();
        }
    }
}
