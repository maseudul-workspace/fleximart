package fleximart.fleximart.domain.interactors;

import fleximart.fleximart.domain.model.UserInfo;

public interface CheckLoginInteractor {
    interface Callback {
        void onLoginSuccess(UserInfo userInfo);
        void onLoginFail(String errorMsg);
    }
}
