package fleximart.fleximart.domain.interactors.impl;

import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.UpdateAddressInteractor;
import fleximart.fleximart.domain.interactors.base.AbstractInteractor;
import fleximart.fleximart.domain.model.CommonResponse;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class UpdateAddressInteractorImpl extends AbstractInteractor implements UpdateAddressInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String authorization;
    int shippingAddressId;
    String name;
    String email;
    String mobile;
    String alternativeMobile;
    String landmark;
    int state;
    int city;
    String pin;
    String address;

    public UpdateAddressInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String authorization, int shippingAddressId, String name, String email, String mobile, String alternativeMobile, String landmark, int state, int city, String pin, String address) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.authorization = authorization;
        this.shippingAddressId = shippingAddressId;
        this.name = name;
        this.email = email;
        this.mobile = mobile;
        this.alternativeMobile = alternativeMobile;
        this.landmark = landmark;
        this.state = state;
        this.city = city;
        this.pin = pin;
        this.address = address;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onUpdateAddressFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage() {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onUpdateAddressSuccess();
            }
        });
    }

    @Override
    public void run() {
        CommonResponse commonResponse = mRepository.updateShippingAddress(authorization, shippingAddressId, name, email, mobile, alternativeMobile, landmark, state, city, pin, address);
        if (commonResponse == null) {
            notifyError("Something went wrong", 0);
        } else if (!commonResponse.status) {
            notifyError(commonResponse.message, commonResponse.login_error);
        } else {
            postMessage();
        }
    }
}
