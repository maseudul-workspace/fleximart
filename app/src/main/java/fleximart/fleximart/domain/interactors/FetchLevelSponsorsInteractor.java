package fleximart.fleximart.domain.interactors;

import fleximart.fleximart.domain.model.LevelSponsors;

public interface FetchLevelSponsorsInteractor {
    interface Callback {
        void onGettingLevelSponsorsSuccess(LevelSponsors[] levelSponsors);
        void onGettingLevelSponsorsFail(String errorMsg, int loginError);
    }
}
