package fleximart.fleximart.domain.interactors;

public interface MoveToCartFromWishlistInteractor {
    interface Callback {
        void onMoveToCartSuccess();
        void onMoveToCartFail(String errorMsg, int loginError);
    }
}
