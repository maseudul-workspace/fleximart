package fleximart.fleximart.domain.interactors.impl;

import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.FetchMyNodesInteractor;
import fleximart.fleximart.domain.interactors.base.AbstractInteractor;
import fleximart.fleximart.domain.model.MyNodes;
import fleximart.fleximart.domain.model.MyNodesWrapper;
import fleximart.fleximart.repository.AppRepository;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class FetchMyNodesInteractorImpl extends AbstractInteractor implements FetchMyNodesInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int userId;

    public FetchMyNodesInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int userId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.userId = userId;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onFetchingNodesFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(MyNodes[] myNodes){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onFetchingNodesSuccess(myNodes);
            }
        });
    }

    @Override
    public void run() {
        final MyNodesWrapper myNodesWrapper = mRepository.fetchMyNodes(apiToken, userId);
        if (myNodesWrapper == null) {
            notifyError("Something went wrong", 0);
        } else if (!myNodesWrapper.status) {
            notifyError(myNodesWrapper.message, myNodesWrapper.login_error);
        } else {
            postMessage(myNodesWrapper.myNodes);
        }
    }
}
