package fleximart.fleximart.domain.interactors.impl;

import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.GetProductDetailsInteractor;
import fleximart.fleximart.domain.interactors.base.AbstractInteractor;
import fleximart.fleximart.domain.model.ProductDetailsData;
import fleximart.fleximart.domain.model.ProductDetailsWrapper;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class GetProductDetailsInteractorImpl extends AbstractInteractor implements GetProductDetailsInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int productId;

    public GetProductDetailsInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRespository, Callback mCallback, int productId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRespository;
        this.mCallback = mCallback;
        this.productId = productId;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onProductDetailsFail(errorMsg);
            }
        });
    }

    private void postMessage(ProductDetailsData productDetailsData){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingProductDetailsSuccess(productDetailsData);
            }
        });
    }

    @Override
    public void run() {
        final ProductDetailsWrapper productDetailsWrapper = mRepository.getProductDetails(productId);
        if (productDetailsWrapper == null) {
            notifyError("Something went wrong");
        } else if (!productDetailsWrapper.status) {
            notifyError(productDetailsWrapper.message);
        } else {
            postMessage(productDetailsWrapper.productDetailsData);
        }
    }
}
