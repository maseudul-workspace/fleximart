package fleximart.fleximart.domain.interactors;

import fleximart.fleximart.domain.model.ProductDetails;
import fleximart.fleximart.domain.model.WishlistProducts;

public interface FetchWishlistInteractor {
    interface Callback {
        void onWishlistFetchSuccess(WishlistProducts[] wishlistProducts);
        void onWishlistFetchFail(String errorMsg, int loginError);
    }
}
