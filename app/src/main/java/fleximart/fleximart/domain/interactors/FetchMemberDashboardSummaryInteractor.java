package fleximart.fleximart.domain.interactors;

import fleximart.fleximart.domain.model.DashboardData;

public interface FetchMemberDashboardSummaryInteractor {
    interface Callback {
        void onSummaryFetchSuccess(DashboardData dashboardData);
        void onSummaryFetchFail(String errorMsg, int loginError);
    }
}
