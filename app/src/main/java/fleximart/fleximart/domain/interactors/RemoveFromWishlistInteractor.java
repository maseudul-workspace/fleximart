package fleximart.fleximart.domain.interactors;

import fleximart.fleximart.domain.interactors.base.AbstractInteractor;

public interface RemoveFromWishlistInteractor {
    interface Callback {
        void onWishlistRemoveSuccess();
        void onWishlistRemoveFail(String errorMsg, int loginError);
    }
}
