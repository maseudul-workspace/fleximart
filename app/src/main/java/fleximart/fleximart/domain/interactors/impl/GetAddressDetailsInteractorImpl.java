package fleximart.fleximart.domain.interactors.impl;

import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.GetAddressDetailsInteractor;
import fleximart.fleximart.domain.interactors.base.AbstractInteractor;
import fleximart.fleximart.domain.model.Address;
import fleximart.fleximart.domain.model.AddressDetailsWrapper;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class GetAddressDetailsInteractorImpl extends AbstractInteractor implements GetAddressDetailsInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int addressId;

    public GetAddressDetailsInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int addressId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.addressId = addressId;
    }

    private void notifyError(String errorMsg, int isLoginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingAddressDetailsFail(errorMsg, isLoginError);
            }
        });
    }

    private void postMessage(Address addresses){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingAddressDetailsSuccess(addresses);
            }
        });
    }

    @Override
    public void run() {
        AddressDetailsWrapper addressDetailsWrapper = mRepository.getShippingAddressDetails(apiToken, addressId);
        if (addressDetailsWrapper == null) {
            notifyError("Something went wrong", 0);
        } else if (!addressDetailsWrapper.status) {
            notifyError(addressDetailsWrapper.message, addressDetailsWrapper.login_error);
        } else {
            postMessage(addressDetailsWrapper.addresses);
        }
    }
}
