package fleximart.fleximart.domain.interactors;

public interface AddToWishlistInteractor {
    interface Callback {
        void onAddToWishlistSuccess();
        void onAddToWishlistFail(String errorMsg, int loginError);
    }
}
