package fleximart.fleximart.domain.interactors.impl;

import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.CheckNodeInteractor;
import fleximart.fleximart.domain.interactors.base.AbstractInteractor;
import fleximart.fleximart.domain.model.NodeDetails;
import fleximart.fleximart.domain.model.NodeDetailsWrapper;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class CheckNodeInteractorImpl extends AbstractInteractor implements CheckNodeInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    String nodeId;

    public CheckNodeInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, String nodeId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.nodeId = nodeId;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onNodeCheckingFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(NodeDetails nodeDetails){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onNodeCheckingSuccess(nodeDetails);
            }
        });
    }

    @Override
    public void run() {
        final NodeDetailsWrapper nodeDetailsWrapper = mRepository.checkNode(apiToken, nodeId);
        if (nodeDetailsWrapper == null) {
            notifyError("Something went wrong", 0);
        } else if (!nodeDetailsWrapper.status) {
            notifyError(nodeDetailsWrapper.message, nodeDetailsWrapper.login_error);
        } else {
            postMessage(nodeDetailsWrapper.nodeDetails);
        }
    }
}
