package fleximart.fleximart.domain.interactors;

import fleximart.fleximart.domain.model.UserInfo;
import fleximart.fleximart.domain.model.UserInfoWrapper;

public interface RegisterUserInteractor {
    interface Callback {
        void onRegisterUser(UserInfo userInfo);
        void onRegisterFail(UserInfoWrapper userInfoWrapper);
    }
}
