package fleximart.fleximart.domain.interactors;

import fleximart.fleximart.domain.model.CartDetails;

public interface FetchCartDetailsInteractor {
    interface Callback {
        void onGettingCartDetailsSucess(CartDetails[] cartDetails);
        void onGettingCartDetailsFail(String errorMsg);
    }
}
