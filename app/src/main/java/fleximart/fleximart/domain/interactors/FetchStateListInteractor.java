package fleximart.fleximart.domain.interactors;

import fleximart.fleximart.domain.model.Address;
import fleximart.fleximart.domain.model.StateList;

public interface FetchStateListInteractor {
    interface Callback {
        void onGettingStateListSuccess(StateList[] states);
        void onGettingStateListFail();
    }
}
