package fleximart.fleximart.domain.interactors;

import fleximart.fleximart.domain.model.Address;

public interface GetAddressDetailsInteractor {
    interface Callback {
        void onGettingAddressDetailsSuccess(Address address);
        void onGettingAddressDetailsFail(String errorMsg, int loginError);
    }
}
