package fleximart.fleximart.domain.interactors.impl;

import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.FetchLevelSponsorsInteractor;
import fleximart.fleximart.domain.interactors.base.AbstractInteractor;
import fleximart.fleximart.domain.model.LevelDownlines;
import fleximart.fleximart.domain.model.LevelSponsors;
import fleximart.fleximart.domain.model.LevelSponsorsWrapper;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class FetchLevelSponsorsInteractorImpl extends AbstractInteractor implements FetchLevelSponsorsInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int nodeId;
    int level;

    public FetchLevelSponsorsInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int nodeId, int level) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.nodeId = nodeId;
        this.level = level;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingLevelSponsorsFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(LevelSponsors[] levelSponsors){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingLevelSponsorsSuccess(levelSponsors);
            }
        });
    }

    @Override
    public void run() {

        final LevelSponsorsWrapper levelSponsorsWrapper = mRepository.fetchLevelSponsors(apiToken, nodeId, level);
        if (levelSponsorsWrapper == null) {
            notifyError("Something Went Wrong", levelSponsorsWrapper.login_error);
        } else if (!levelSponsorsWrapper.status) {
            notifyError(levelSponsorsWrapper.message, levelSponsorsWrapper.login_error);
        } else {
            postMessage(levelSponsorsWrapper.levelSponsors);
        }

    }
}
