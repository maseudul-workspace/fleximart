package fleximart.fleximart.repository;

import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;

import fleximart.fleximart.domain.model.AddressDetailsWrapper;
import fleximart.fleximart.domain.model.AddressListWrapper;
import fleximart.fleximart.domain.model.CartDetailsWrapper;
import fleximart.fleximart.domain.model.CheckPhoneData;
import fleximart.fleximart.domain.model.CheckPhoneDataResponse;
import fleximart.fleximart.domain.model.ChecksumDataWrapper;
import fleximart.fleximart.domain.model.CitiesWrapper;
import fleximart.fleximart.domain.model.CommisionTotalWrapper;
import fleximart.fleximart.domain.model.CommonResponse;
import fleximart.fleximart.domain.model.DashboardData;
import fleximart.fleximart.domain.model.DashboardSummaryWrapper;
import fleximart.fleximart.domain.model.DownlineWrapper;
import fleximart.fleximart.domain.model.LevelDownlinesWrapper;
import fleximart.fleximart.domain.model.LevelSponsorsWrapper;
import fleximart.fleximart.domain.model.MainDataWrapper;
import fleximart.fleximart.domain.model.MyCommissionListWrapper;
import fleximart.fleximart.domain.model.MyLevelsWrapper;
import fleximart.fleximart.domain.model.MyNodes;
import fleximart.fleximart.domain.model.MyNodesWrapper;
import fleximart.fleximart.domain.model.NodeDetailsWrapper;
import fleximart.fleximart.domain.model.PlaceOrderResponse;
import fleximart.fleximart.domain.model.ProductDetails;
import fleximart.fleximart.domain.model.ProductDetailsWrapper;
import fleximart.fleximart.domain.model.ProductListWithFilter;
import fleximart.fleximart.domain.model.ProductListWithFilterWrapper;
import fleximart.fleximart.domain.model.ProductListWithoutFilterWrapper;
import fleximart.fleximart.domain.model.StateListWrapper;
import fleximart.fleximart.domain.model.SubcategoryWrapper;
import fleximart.fleximart.domain.model.TreeWrapper;
import fleximart.fleximart.domain.model.UserInfoWrapper;
import fleximart.fleximart.domain.model.UserProfileDataWrapper;
import fleximart.fleximart.domain.model.WalletDetailsWrapper;
import fleximart.fleximart.domain.model.WalletHistory;
import fleximart.fleximart.domain.model.WalletHistoryWrapper;
import fleximart.fleximart.domain.model.WishlistWrapper;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.Header;

public class AppRepositoryImpl {
    AppRepository mRepository;

    public AppRepositoryImpl() {
        mRepository = APIclient.createService(AppRepository.class);
    }

    public MainDataWrapper getMainData() {
        MainDataWrapper mainDataWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> tendersList = mRepository.getMainData();

            Response<ResponseBody> response = tendersList.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    mainDataWrapper = null;
                }else{
                    mainDataWrapper = gson.fromJson(responseBody, MainDataWrapper.class);

                }
            } else {
                mainDataWrapper = null;
            }
        }catch (Exception e){
            mainDataWrapper = null;
        }
        return mainDataWrapper;
    }

    public SubcategoryWrapper getSubcategories(int id) {
        SubcategoryWrapper subcategoryWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> tendersList = mRepository.getSubCategories(id);

            Response<ResponseBody> response = tendersList.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    subcategoryWrapper = null;
                }else{
                    subcategoryWrapper = gson.fromJson(responseBody, SubcategoryWrapper.class);
                }
            } else {
                subcategoryWrapper = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", e.getMessage());
            subcategoryWrapper = null;
        }
        return subcategoryWrapper;
    }

    public ProductListWithFilterWrapper getProductListWithFilter(int secondCategory, int page) {
        ProductListWithFilterWrapper productListWithFilterWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> tendersList = mRepository.getProductListWithFilters(secondCategory, page);

            Response<ResponseBody> response = tendersList.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    productListWithFilterWrapper = null;
                }else{
                    productListWithFilterWrapper = gson.fromJson(responseBody, ProductListWithFilterWrapper.class);
                }
            } else {
                productListWithFilterWrapper = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", e.getMessage());
            productListWithFilterWrapper = null;
        }
        return productListWithFilterWrapper;
    }

    public ProductListWithoutFilterWrapper getProductListWithoutFilter(int secondCategory,
                                                                       ArrayList<Integer> brandIds,
                                                                       ArrayList<Integer> sizeIds,
                                                                       ArrayList<Integer> colorIds,
                                                                       long priceFrom,
                                                                       long priceTo,
                                                                       int sortBy,
                                                                       int page) {

        for (int i = 0; i < brandIds.size(); i++) {
            Log.e("LogMsg", "Brand Id: " + brandIds.get(i));
        }

        ProductListWithoutFilterWrapper productListWithoutFilterWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> tendersList = mRepository.getProductListWithoutFilters(secondCategory, brandIds, sizeIds, colorIds, priceFrom, priceTo, sortBy, page);

            Response<ResponseBody> response = tendersList.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    productListWithoutFilterWrapper = null;
                }else{
                    productListWithoutFilterWrapper = gson.fromJson(responseBody, ProductListWithoutFilterWrapper.class);
                }
            } else {
                productListWithoutFilterWrapper = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", e.getMessage());
            productListWithoutFilterWrapper = null;
        }
        return productListWithoutFilterWrapper;
    }

    public ProductDetailsWrapper getProductDetails(int productId) {
        ProductDetailsWrapper productDetailsWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> tendersList = mRepository.getProductDetails(productId);

            Response<ResponseBody> response = tendersList.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    productDetailsWrapper = null;
                }else{
                    productDetailsWrapper = gson.fromJson(responseBody, ProductDetailsWrapper.class);
                }
            } else {
                productDetailsWrapper = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", e.getMessage());
            productDetailsWrapper = null;
        }
        return productDetailsWrapper;
    }

    public UserInfoWrapper registerUser(String name, String email, String password, String confirmPassword, String mobile) {
        UserInfoWrapper userInfoWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> register = mRepository.registerUser(name, email, password, confirmPassword, mobile);

            Response<ResponseBody> response = register.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    userInfoWrapper = null;
                }else{
                    userInfoWrapper = gson.fromJson(responseBody, UserInfoWrapper.class);
                }
            } else {
                userInfoWrapper = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", e.getMessage());
            userInfoWrapper = null;
        }
        return userInfoWrapper;
    }

    public CommonResponse addToCart(String authorization,
                                    int userId,
                                    int productId,
                                    int quantity,
                                    int colorId,
                                    int sizeId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> add = mRepository.addToCart("Bearer " + authorization, userId, productId, quantity, colorId, sizeId);

            Response<ResponseBody> response = add.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", e.getMessage());
            commonResponse = null;
        }
        return commonResponse;
    }

    public CartDetailsWrapper fetchCartDetails(int userId, String apiToken) {
        CartDetailsWrapper cartDetailsWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchCartDetails("Bearer " + apiToken, userId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "ErrorResponse: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    cartDetailsWrapper = null;
                }else{
                    cartDetailsWrapper = gson.fromJson(responseBody, CartDetailsWrapper.class);
                }
            } else {
                cartDetailsWrapper = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", e.getMessage());
            cartDetailsWrapper = null;
        }
        return cartDetailsWrapper;
    }

    public CommonResponse updateCart(String authorization,
                                     int userId,
                                     int cartId,
                                     int quantity,
                                     int sizeId) {

        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;

        try {
            Call<ResponseBody> updateCart = mRepository.updateCart("Bearer " + authorization, userId,cartId, sizeId, quantity);

            Response<ResponseBody> response = updateCart.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", e.getMessage());
            commonResponse = null;
        }
        return commonResponse;

    }

    public CommonResponse deleteCart(String authorization, int cartId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;

        try {
            Call<ResponseBody> deleteCart = mRepository.deleteCart("Bearer " + authorization, cartId);

            Response<ResponseBody> response = deleteCart.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", e.getMessage());
            commonResponse = null;
        }
        return commonResponse;
    }

    public AddressListWrapper fetchShippingAddressList(int userId, String apiToken) {
        AddressListWrapper addressListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;

        try {
            Call<ResponseBody> fetch = mRepository.fetchShippingAddressList("Bearer " + apiToken, userId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    addressListWrapper = null;
                }else{
                    addressListWrapper = gson.fromJson(responseBody, AddressListWrapper.class);
                }
            } else {
                addressListWrapper = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", e.getMessage());
            addressListWrapper = null;
        }
        return addressListWrapper;
    }

    public StateListWrapper fetchStateList() {
        StateListWrapper stateListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;

        try {
            Call<ResponseBody> fetch = mRepository.fetchStates();

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    stateListWrapper = null;
                }else{
                    stateListWrapper = gson.fromJson(responseBody, StateListWrapper.class);
                }
            } else {
                stateListWrapper = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", e.getMessage());
            stateListWrapper = null;
        }
        return stateListWrapper;
    }

    public CitiesWrapper fetchCities(int stateId) {
        CitiesWrapper citiesWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;

        try {
            Call<ResponseBody> fetch = mRepository.fetchCities(stateId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    citiesWrapper = null;
                }else{
                    citiesWrapper = gson.fromJson(responseBody, CitiesWrapper.class);
                }
            } else {
                citiesWrapper = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", e.getMessage());
            citiesWrapper = null;
        }
        return citiesWrapper;
    }

    public CommonResponse addShippingAddress(String authorization,
                                             int userId,
                                             String name,
                                             String email,
                                             String mobile,
                                             String alternativeMobile,
                                             String landmark,
                                             int state,
                                             int city,
                                             String pin,
                                             String address) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> add = mRepository.addShippingAddress("Bearer " + authorization, userId, name, email, mobile, alternativeMobile, landmark, state, city, pin, address);
            Response<ResponseBody> response = add.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Error Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", e.getMessage());
            commonResponse = null;
        }
        return commonResponse;
    }

    public AddressDetailsWrapper getShippingAddressDetails(String apiToken, int addressId) {
        AddressDetailsWrapper addressDetailsWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;

        try {
            Call<ResponseBody> get = mRepository.getShippingAddressDetails("Bearer " + apiToken, addressId);

            Response<ResponseBody> response = get.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    addressDetailsWrapper = null;
                }else{
                    addressDetailsWrapper = gson.fromJson(responseBody, AddressDetailsWrapper.class);
                }
            } else {
                addressDetailsWrapper = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", e.getMessage());
            addressDetailsWrapper = null;
        }
        return addressDetailsWrapper;
    }

    public CommonResponse updateShippingAddress(String authorization,
                                             int shippingAddressId,
                                             String name,
                                             String email,
                                             String mobile,
                                             String alternativeMobile,
                                             String landmark,
                                             int state,
                                             int city,
                                             String pin,
                                             String address) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> add = mRepository.updateShippingAddress("Bearer " + authorization, shippingAddressId, name, email, mobile, alternativeMobile, landmark, state, city, pin, address);
            Response<ResponseBody> response = add.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Error Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", e.getMessage());
            commonResponse = null;
        }
        return commonResponse;
    }

    public UserProfileDataWrapper fetchUserProfile(int userId, String apiToken) {
        UserProfileDataWrapper userProfileDataWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;

        try {
            Call<ResponseBody> fetch = mRepository.fetchUserProfile("Bearer " + apiToken, userId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    userProfileDataWrapper = null;
                }else{
                    userProfileDataWrapper = gson.fromJson(responseBody, UserProfileDataWrapper.class);
                }
            } else {
                userProfileDataWrapper = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", e.getMessage());
            userProfileDataWrapper = null;
        }
        return userProfileDataWrapper;
    }

    public CommonResponse updateProfile(String authorization,
                                        int userId,
                                        String name,
                                        String email,
                                        String mobile,
                                        String DOB,
                                        String gender,
                                        String area,
                                        int state,
                                        int city,
                                        String pin,
                                        String address) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> update = mRepository.updateProfile("Bearer " + authorization, userId, name, email, mobile, DOB, gender, area, state, city, pin, address);
            Response<ResponseBody> response = update.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Error Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", e.getMessage());
            commonResponse = null;
        }
        return commonResponse;
    }

    public CheckPhoneDataResponse checkBuyerMobile(String apiToken, String mobileNo) {
        CheckPhoneDataResponse checkPhoneDataResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;

        try {
            Call<ResponseBody> check = mRepository.checkBuyerMobileNo("Bearer " + apiToken, mobileNo);

            Response<ResponseBody> response = check.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    checkPhoneDataResponse = null;
                }else{
                    checkPhoneDataResponse = gson.fromJson(responseBody, CheckPhoneDataResponse.class);
                }
            } else {
                checkPhoneDataResponse = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", e.getMessage());
            checkPhoneDataResponse = null;
        }
        return checkPhoneDataResponse;
    }

    public CommonResponse removeShippingAddress(String authorization, int addressId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;

        try {
            Call<ResponseBody> remove = mRepository.removeShippingAddress("Bearer " + authorization, addressId);

            Response<ResponseBody> response = remove.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", e.getMessage());
            commonResponse = null;
        }
        return commonResponse;
    }

    public NodeDetailsWrapper checkNode(String apiToken, String nodeId) {
        NodeDetailsWrapper nodeDetailsWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;

        try {
            Call<ResponseBody> remove = mRepository.checkNode("Bearer " + apiToken, nodeId);

            Response<ResponseBody> response = remove.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    nodeDetailsWrapper = null;
                }else{
                    nodeDetailsWrapper = gson.fromJson(responseBody, NodeDetailsWrapper.class);
                }
            } else {
                nodeDetailsWrapper = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", e.getMessage());
            nodeDetailsWrapper = null;
        }
        return nodeDetailsWrapper;
    }

    public WalletDetailsWrapper getWalletDetails(String authorization, int userId) {
        WalletDetailsWrapper walletDetailsWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> get = mRepository.getWalletDetails("Bearer " + authorization, userId);

            Response<ResponseBody> response = get.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    walletDetailsWrapper = null;
                }else{
                    walletDetailsWrapper = gson.fromJson(responseBody, WalletDetailsWrapper.class);
                }
            } else {
                walletDetailsWrapper = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", e.getMessage());
            walletDetailsWrapper = null;
        }
        return walletDetailsWrapper;
    }

    public CommonResponse buyNodes(String authorization,
                                   int userId,
                                   int registrationMethod,
                                   String sponsorNode,
                                   int totalNode,
                                   String leg,
                                   String name,
                                   String email,
                                   String DOB,
                                   String gender,
                                   int state,
                                   int city,
                                   String aadhar,
                                   String pan,
                                   String pin,
                                   String address,
                                   String mobile
                                   ) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;

        try {
            Call<ResponseBody> buy = mRepository.buyNode("Bearer " + authorization, userId, registrationMethod, sponsorNode, totalNode, leg, name, email, DOB, gender, state, city, aadhar, pan, pin, address, mobile);

            Response<ResponseBody> response = buy.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", e.getMessage());
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse buyNewNode(String authorization,
                                     int userId,
                                     String sponsorNode,
                                     int totalNode,
                                     String leg) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;

        try {
            Call<ResponseBody> buy = mRepository.buyNewNode("Bearer " + authorization, userId, sponsorNode, totalNode, leg);

            Response<ResponseBody> response = buy.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", e.getMessage());
            commonResponse = null;
        }
        return commonResponse;
    }

    public UserInfoWrapper checkLogin(String email, String password) {
        UserInfoWrapper userInfoWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;

        try {
            Call<ResponseBody> check = mRepository.checkLogin(email, password);

            Response<ResponseBody> response = check.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    userInfoWrapper = null;
                }else{
                    userInfoWrapper = gson.fromJson(responseBody, UserInfoWrapper.class);
                }
            } else {
                userInfoWrapper = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", e.getMessage());
            userInfoWrapper = null;
        }
        return userInfoWrapper;
    }

    public WalletHistoryWrapper fetchWalletHistory(String apiToken, int userId, int page) {
        WalletHistoryWrapper walletHistoryWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;

        try {
            Call<ResponseBody> fetch = mRepository.getWalletHistory("Bearer " + apiToken, userId, page);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    walletHistoryWrapper = null;
                }else{
                    walletHistoryWrapper = gson.fromJson(responseBody, WalletHistoryWrapper.class);
                }
            } else {
                walletHistoryWrapper = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", e.getMessage());
            walletHistoryWrapper = null;
        }
        return walletHistoryWrapper;
    }

    public TreeWrapper fetchMyTree(String apiToken, int nodeId, int payNodeId, int rank) {
        TreeWrapper treeWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        Log.e("LogMsg", "Node Id: " + nodeId);
        Log.e("LogMsg", "Pay Node Id: " + payNodeId);
        Log.e("LogMsg", "Rank: " + rank);
        try {
            Call<ResponseBody> fetch = mRepository.myTree("Bearer " + apiToken, nodeId, payNodeId, rank);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "My Tree: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "My Tree: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    treeWrapper = null;
                }else{
                    treeWrapper = gson.fromJson(responseBody, TreeWrapper.class);
                }
            } else {
                treeWrapper = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", e.getMessage());
            treeWrapper = null;
        }
        return treeWrapper;
    }

    public MyNodesWrapper fetchMyNodes(String apiToken, int userId) {
        MyNodesWrapper myNodesWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;

        try {
            Call<ResponseBody> fetch = mRepository.fetchMyNodes("Bearer " + apiToken, userId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "My Nodes: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "My Nodes: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    myNodesWrapper = null;
                }else{
                    myNodesWrapper = gson.fromJson(responseBody, MyNodesWrapper.class);
                }
            } else {
                myNodesWrapper = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", e.getMessage());
            myNodesWrapper = null;
        }
        return myNodesWrapper;
    }

    public DownlineWrapper fetchMyDownlines(String apiToken, int nodeId, int page) {
        DownlineWrapper downlineWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;

        try {
            Call<ResponseBody> fetch = mRepository.fetchMyDownlines("Bearer " + apiToken, nodeId, page);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "My Downlines: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "My Downlines: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    downlineWrapper = null;
                }else{
                    downlineWrapper = gson.fromJson(responseBody, DownlineWrapper.class);
                }
            } else {
                downlineWrapper = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", e.getMessage());
            downlineWrapper = null;
        }
        return downlineWrapper;
    }

    public CommisionTotalWrapper fetchMyTotalCommision(String apiToken, int userId) {
        CommisionTotalWrapper commisionTotalWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;

        try {
            Call<ResponseBody> fetch = mRepository.fetchMyTotalCommision("Bearer " + apiToken, userId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "My Total Commision: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "My Total Commision: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commisionTotalWrapper = null;
                }else{
                    commisionTotalWrapper = gson.fromJson(responseBody, CommisionTotalWrapper.class);
                }
            } else {
                commisionTotalWrapper = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", e.getMessage());
            commisionTotalWrapper = null;
        }
        return commisionTotalWrapper;
    }

    public MyCommissionListWrapper fetchApprovedCommission(String apiToken, int userId, int page) {
        MyCommissionListWrapper myCommissionListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;

        try {
            Call<ResponseBody> fetch = mRepository.fetchMyApprovedCommision("Bearer " + apiToken, userId, page);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "My Approved Commision: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "My Approved Commision: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    myCommissionListWrapper = null;
                }else{
                    myCommissionListWrapper = gson.fromJson(responseBody, MyCommissionListWrapper.class);
                }
            } else {
                myCommissionListWrapper = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", e.getMessage());
            myCommissionListWrapper = null;
        }
        return myCommissionListWrapper;
    }

    public MyCommissionListWrapper fetchUnapprovedCommission(String apiToken, int userId, int page) {
        MyCommissionListWrapper myCommissionListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;

        try {
            Call<ResponseBody> fetch = mRepository.fetchMyUnApprovedCommision("Bearer " + apiToken, userId, page);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "My Approved Commision: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "My Approved Commision: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    myCommissionListWrapper = null;
                }else{
                    myCommissionListWrapper = gson.fromJson(responseBody, MyCommissionListWrapper.class);
                }
            } else {
                myCommissionListWrapper = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", e.getMessage());
            myCommissionListWrapper = null;
        }
        return myCommissionListWrapper;
    }

    public CommonResponse addToWishlist(String apiToken, int userId, int productId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;

        try {
            Call<ResponseBody> add = mRepository.addToWishlist("Bearer " + apiToken, userId, productId);

            Response<ResponseBody> response = add.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", e.getMessage());
            commonResponse = null;
        }
        return commonResponse;
    }

    public WishlistWrapper fetchWishlist(String apiToken, int userId) {
        WishlistWrapper wishlistWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;

        try {
            Call<ResponseBody> fetch = mRepository.fetchWishlist("Bearer " + apiToken, userId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    wishlistWrapper = null;
                }else{
                    wishlistWrapper = gson.fromJson(responseBody, WishlistWrapper.class);
                }
            } else {
                wishlistWrapper = null;
            }
        }catch (Exception e){
            wishlistWrapper = null;
        }
        return wishlistWrapper;
    }

    public CommonResponse removeFromWishlist(String apiToken, int wishlistId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;

        try {
            Call<ResponseBody> remove = mRepository.removeFromWishlist("Bearer " + apiToken, wishlistId);

            Response<ResponseBody> response = remove.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", e.getMessage());
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse moveToCartFromWishlist(String apiToken, int userId, int productId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;

        try {
            Call<ResponseBody> move = mRepository.moveToCartFromWishlist("Bearer " + apiToken, userId, productId);

            Response<ResponseBody> response = move.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public DashboardSummaryWrapper fetchMemberDashboardSummary(String apiToken, int userId) {
        DashboardSummaryWrapper dashboardSummaryWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;

        try {
            Call<ResponseBody> fetch = mRepository.fetchMemberDashboardSummary("Bearer " + apiToken, userId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    dashboardSummaryWrapper = null;
                }else{
                    dashboardSummaryWrapper = gson.fromJson(responseBody, DashboardSummaryWrapper.class);
                }
            } else {
                dashboardSummaryWrapper = null;
            }
        }catch (Exception e){
            dashboardSummaryWrapper = null;
        }
        return dashboardSummaryWrapper;
    }

    public PlaceOrderResponse placeOrder(String apiToken, int userId, int shippingAddressId, int paymentMethodId, int walletPay) {
        PlaceOrderResponse placeOrderResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> placeOrder = mRepository.placeOrder("Bearer " + apiToken, userId, shippingAddressId, paymentMethodId, walletPay);

            Response<ResponseBody> response = placeOrder.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Order Place Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Order Place Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    placeOrderResponse = null;
                }else{
                    placeOrderResponse = gson.fromJson(responseBody, PlaceOrderResponse.class);
                }
            } else {
                placeOrderResponse = null;
            }
        }catch (Exception e){
            placeOrderResponse = null;
        }
        return  placeOrderResponse;
    }

    public ChecksumDataWrapper generateChecksum(String authorization,
                                                String MID,
                                                int ORDER_ID,
                                                int CUST_ID,
                                                String MOBILE_NO,
                                                String EMAIL,
                                                String CHANNEL_ID,
                                                String TXN_AMOUNT,
                                                String WEBSITE,
                                                String INDUSTRY_TYPE_ID,
                                                String CALLBACK_URL) {
        ChecksumDataWrapper checksumDataWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> generate = mRepository.generateChecksum("Bearer " + authorization, MID, ORDER_ID, CUST_ID, MOBILE_NO, EMAIL, CHANNEL_ID, TXN_AMOUNT, WEBSITE, INDUSTRY_TYPE_ID, CALLBACK_URL);
            Response<ResponseBody> response = generate.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Generate Checksum: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Generate Checksum: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    checksumDataWrapper = null;
                }else{
                    checksumDataWrapper = gson.fromJson(responseBody, ChecksumDataWrapper.class);
                }
            } else {
                checksumDataWrapper = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", "Ezception : " + e.getMessage());
            checksumDataWrapper = null;
        }
        return checksumDataWrapper;
    }

    public MyLevelsWrapper fetchMyLevels(String apiToken, int nodeId) {
        MyLevelsWrapper myLevelsWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchMyLevels("Bearer " + apiToken, nodeId);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "My Levels : " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "My Levels: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    myLevelsWrapper = null;
                }else{
                    myLevelsWrapper = gson.fromJson(responseBody, MyLevelsWrapper.class);
                }
            } else {
                myLevelsWrapper = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", "Exception : " + e.getMessage());
            myLevelsWrapper = null;
        }
        return myLevelsWrapper;
    }

    public LevelDownlinesWrapper fetchMyDownlinesOfLevels(String apiToken, int nodeId, int level) {
        LevelDownlinesWrapper levelDownlinesWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchDownlinesOfLevels("Bearer " + apiToken, nodeId, level);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "My Downlines Levels : " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "My Downlines Levels: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    levelDownlinesWrapper = null;
                }else{
                    levelDownlinesWrapper = gson.fromJson(responseBody, LevelDownlinesWrapper.class);
                }
            } else {
                levelDownlinesWrapper = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", "Exception : " + e.getMessage());
            levelDownlinesWrapper = null;
        }
        return levelDownlinesWrapper;
    }

    public PlaceOrderResponse addWalletMoney(String apiToken, int userId, String amount) {
        PlaceOrderResponse placeOrderResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> add = mRepository.addWalletMoney("Bearer " + apiToken, userId, amount);

            Response<ResponseBody> response = add.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Order Place Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Order Place Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    placeOrderResponse = null;
                }else{
                    placeOrderResponse = gson.fromJson(responseBody, PlaceOrderResponse.class);
                }
            } else {
                placeOrderResponse = null;
            }
        }catch (Exception e){
            placeOrderResponse = null;
        }
        return  placeOrderResponse;
    }

    public CommonResponse completeWalletPay(String apiToken, int orderId, String transactionId) {
        Log.e("LogMsg", "Order Id: "  + orderId);
        Log.e("LogMsg", "Transaction Id: " + transactionId);
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;

        try {
            Call<ResponseBody> complete = mRepository.completeWalletPay("Bearer " + apiToken, orderId, transactionId);

            Response<ResponseBody> response = complete.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Wallet Add response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Wallet Add response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
            Log.e("LogMsg", "Exception: " + e.getMessage());
        }
        return commonResponse;
    }

    public LevelSponsorsWrapper fetchLevelSponsors(String apiToken, int nodeId, int level) {
        LevelSponsorsWrapper levelSponsorsWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchSponsorsOfLevels("Bearer " + apiToken, nodeId, level);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "My Sponsors Levels : " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "My Sponsors Levels: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    levelSponsorsWrapper = null;
                }else{
                    levelSponsorsWrapper = gson.fromJson(responseBody, LevelSponsorsWrapper.class);
                }
            } else {
                levelSponsorsWrapper = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", "Exception : " + e.getMessage());
            levelSponsorsWrapper = null;
        }
        return levelSponsorsWrapper;
    }

}
