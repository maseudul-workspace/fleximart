package fleximart.fleximart.repository;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface AppRepository {

    @GET("app/load")
    Call<ResponseBody> getMainData();

    @GET("category/{main_category}")
    Call<ResponseBody> getSubCategories(@Path("main_category") int id);

    @GET("product/list/second/category/{second_category}/{page}")
    Call<ResponseBody> getProductListWithFilters(@Path("second_category") int secondCatgeory,
                                                 @Path("page") int page);

    @POST("product/filter")
    @FormUrlEncoded
    Call<ResponseBody> getProductListWithoutFilters(@Field("second_category") int secondCategory,
                                                    @Field("brand_id[]") ArrayList<Integer> brandIds,
                                                    @Field("size_id[]") ArrayList<Integer> sizeIds,
                                                    @Field("color_id[]") ArrayList<Integer> colorIds,
                                                    @Field("price_from") long priceFrom,
                                                    @Field("price_to") long priceTo,
                                                    @Field("sort_by") int sortBy,
                                                    @Field("page") int page
                                                    );

    @GET("single/product/view/{product_id}")
    Call<ResponseBody> getProductDetails(@Path("product_id") int productId);


    @POST("user/registration")
    @FormUrlEncoded
    Call<ResponseBody> registerUser(@Field("name") String name,
                                    @Field("email") String email,
                                    @Field("password") String password,
                                    @Field("confirm_password") String confirmPassword,
                                    @Field("mobile") String mobile
    );

    @POST("user/login")
    @FormUrlEncoded
    Call<ResponseBody> checkLogin(@Field("email") String email,
                                    @Field("password") String password
    );

    @POST("cart/add")
    @FormUrlEncoded
    Call<ResponseBody> addToCart(@Header("Authorization") String authorization,
                                    @Field("user_id") int userId,
                                    @Field("product_id") int productId,
                                    @Field("quantity") int quantity,
                                    @Field("color_id") int colorId,
                                    @Field("size_id") int sizeId
    );

    @GET("cart/products/{user_id}")
    Call<ResponseBody> fetchCartDetails(@Header("Authorization") String authorization,
                                        @Path("user_id") int userId
    );

    @POST("cart/update")
    @FormUrlEncoded
    Call<ResponseBody> updateCart(@Header("Authorization") String authorization,
                                 @Field("user_id") int userId,
                                 @Field("cart_id") int cartId,
                                 @Field("size_id") int sizeId,
                                  @Field("quantity") int quantity
    );

    @GET("cart/delete/{cart_id}")
    Call<ResponseBody> deleteCart(@Header("Authorization") String authorization,
                                        @Path("cart_id") int cartId
    );

    @GET("user/shipping/address/list/{user_id}")
    Call<ResponseBody> fetchShippingAddressList(@Header("Authorization") String authorization,
                                        @Path("user_id") int userId
    );

    @GET("state/list")
    Call<ResponseBody> fetchStates();

    @GET("city/list/{state_id}")
    Call<ResponseBody> fetchCities(@Path("state_id") int stateId);

    @POST("user/shipping/address/add")
    @FormUrlEncoded
    Call<ResponseBody> addShippingAddress(@Header("Authorization") String authorization,
                                  @Field("user_id") int userId,
                                  @Field("name") String name,
                                  @Field("email") String email,
                                  @Field("mobile") String mobile,
                                  @Field("alternative_mobile") String alternativeMobile,
                                  @Field("landmark") String landmark,
                                  @Field("state") int state,
                                  @Field("city") int city,
                                  @Field("pin") String pin,
                                  @Field("address") String address
                                  );

    @GET("user/shipping/address/{address_id}")
    Call<ResponseBody> getShippingAddressDetails(@Header("Authorization") String authorization,
                                                @Path("address_id") int addressId
    );

    @POST("user/shipping/update")
    @FormUrlEncoded
    Call<ResponseBody> updateShippingAddress(@Header("Authorization") String authorization,
                                          @Field("shipping_address_id") int shippingAddressId,
                                          @Field("name") String name,
                                          @Field("email") String email,
                                          @Field("mobile") String mobile,
                                          @Field("alternative_mobile") String alternativeMobile,
                                          @Field("landmark") String landmark,
                                          @Field("state") int state,
                                          @Field("city") int city,
                                          @Field("pin") String pin,
                                          @Field("address") String address
    );

    @GET("user/profile/{user_id}")
    Call<ResponseBody> fetchUserProfile(@Header("Authorization") String authorization,
                                                @Path("user_id") int userId
    );

    @POST("user/profile/update")
    @FormUrlEncoded
    Call<ResponseBody> updateProfile(@Header("Authorization") String authorization,
                                             @Field("user_id") int userId,
                                             @Field("name") String name,
                                             @Field("email") String email,
                                             @Field("mobile") String mobile,
                                             @Field("dob") String DOB,
                                             @Field("gender") String gender,
                                             @Field("area") String area,
                                             @Field("state") int state,
                                             @Field("city") int city,
                                             @Field("pin") String pin,
                                             @Field("address") String address
    );

    @GET("node/buyer/check/{mobile_no}")
    Call<ResponseBody> checkBuyerMobileNo(@Header("Authorization") String authorization,
                                        @Path("mobile_no") String mobileNo
    );

    @GET("user/shipping/address/delete/{address_id}")
    Call<ResponseBody> removeShippingAddress(@Header("Authorization") String authorization,
                                                @Path("address_id") int addressId
    );

    @GET("sponsor/node/check/{node_id}")
    Call<ResponseBody> checkNode(@Header("Authorization") String authorization,
                                             @Path("node_id") String node_id
    );

    @GET("wallet/amount/{user_id}")
    Call<ResponseBody> getWalletDetails(@Header("Authorization") String authorization,
                                 @Path("user_id") int user_id
    );

    @POST("new/member/Registration/new")
    @FormUrlEncoded
    Call<ResponseBody> buyNode( @Header("Authorization") String authorization,
                                @Field("user_id") int userId,
                                @Field("registration_method") int registrationMethod,
                                @Field("sponsor_node") String sponsorNode,
                                @Field("total_node") int totalNode,
                                @Field("leg") String leg,
                                @Field("name") String name,
                                @Field("email") String email,
                                @Field("dob") String DOB,
                                @Field("gender") String gender,
                                @Field("state") int state,
                                @Field("city") int city,
                                @Field("aadhar") String aadhar,
                                @Field("pan") String pan,
                                @Field("pin") String pin,
                                @Field("address") String address,
                                @Field("mobile") String mobile
    );

    @POST("buy/another/node")
    @FormUrlEncoded
    Call<ResponseBody> buyNewNode(  @Header("Authorization") String authorization,
                                    @Field("user_id") int userId,
                                    @Field("sponsor_node") String sponsorNode,
                                    @Field("total_node") int totalNode,
                                    @Field("leg") String leg
    );

    @GET("wallet/history/{user_id}/{page}")
    Call<ResponseBody> getWalletHistory(@Header("Authorization") String authorization,
                                        @Path("user_id") int user_id,
                                        @Path("page") int page
    );

    @GET("my/tree/{node_id}/{pay_to_node}/{rank}")
    Call<ResponseBody> myTree(  @Header("Authorization") String authorization,
                                @Path("node_id") int node_id,
                                @Path("pay_to_node") int pay_to_node,
                                @Path("rank") int rank
    );

    @GET("my/node/{user_id}")
    Call<ResponseBody> fetchMyNodes(@Header("Authorization") String authorization,
                                        @Path("user_id") int user_id
    );

    @GET("my/downline/{node_id}/{page}")
    Call<ResponseBody> fetchMyDownlines(@Header("Authorization") String authorization,
                                        @Path("node_id") int nodeId,
                                        @Path("page") int page

    );

    @GET("fetch/commission/{user_id}")
    Call<ResponseBody> fetchMyTotalCommision(@Header("Authorization") String authorization,
                                        @Path("user_id") int userId
    );

    @GET("approved/commission/{user_id}/{page}")
    Call<ResponseBody> fetchMyApprovedCommision(@Header("Authorization") String authorization,
                                                @Path("user_id") int userId,
                                                @Path("page") int page
    );

    @GET("unapproved/commission/{user_id}/{page}")
    Call<ResponseBody> fetchMyUnApprovedCommision(@Header("Authorization") String authorization,
                                                  @Path("user_id") int userId,
                                                  @Path("page") int page
    );

    @GET("wishlist/add/{user_id}/{product_id}")
    Call<ResponseBody> addToWishlist(@Header("Authorization") String authorization,
                                     @Path("user_id") int userId,
                                     @Path("product_id") int productId
    );

    @GET("wishlist/products/{user_id}")
    Call<ResponseBody> fetchWishlist(@Header("Authorization") String authorization,
                                     @Path("user_id") int userId
    );

    @GET("wishlist/remove/{id}")
    Call<ResponseBody> removeFromWishlist(@Header("Authorization") String authorization,
                                          @Path("id") int wishlistId
    );

    @GET("wishlist/to/cart/{user_id}/{product_id}")
    Call<ResponseBody> moveToCartFromWishlist(@Header("Authorization") String authorization,
                                              @Path("user_id") int userId,
                                              @Path("product_id") int productId

    );

    @GET("dashboard/summary/{user_id}")
    Call<ResponseBody> fetchMemberDashboardSummary(@Header("Authorization") String authorization,
                                                   @Path("user_id") int userId
    );

    @POST("product/order/place")
    @FormUrlEncoded
    Call<ResponseBody> placeOrder(  @Header("Authorization") String authorization,
                                    @Field("user_id") int userId,
                                    @Field("shipping_address_id") int shippingAddressId,
                                    @Field("payment_method") int paymentMethod,
                                    @Field("is_wallet_pay") int isWalletPay
    );

    @POST("product/order/generate/checksum")
    @FormUrlEncoded
    Call<ResponseBody> generateChecksum( @Header("Authorization") String authorization,
                                         @Field("MID") String MID,
                                         @Field("ORDER_ID") int ORDER_ID,
                                         @Field("CUST_ID") int CUST_ID,
                                         @Field("MOBILE_NO") String MOBILE_NO,
                                         @Field("EMAIL") String EMAIL,
                                         @Field("CHANNEL_ID") String CHANNEL_ID,
                                         @Field("TXN_AMOUNT") String TXN_AMOUNT,
                                         @Field("WEBSITE") String WEBSITE,
                                         @Field("INDUSTRY_TYPE_ID") String INDUSTRY_TYPE_ID,
                                         @Field("CALLBACK_URL") String CALLBACK_URL
                                         );

    @GET("node/levels/{node_id}")
    Call<ResponseBody> fetchMyLevels(@Header("Authorization") String authorization,
                                     @Path("node_id") int nodeId
    );

    @GET("node/level/wise/downline/{node_id}/{label}")
    Call<ResponseBody> fetchDownlinesOfLevels(@Header("Authorization") String authorization,
                                              @Path("node_id") int nodeId,
                                              @Path("label") int level
    );

    @POST("wallet/amount/add")
    @FormUrlEncoded
    Call<ResponseBody> addWalletMoney(@Header("Authorization") String authorization,
                                      @Field("user_id") int userId,
                                      @Field("amount") String amount
    );

    @POST("wallet/pay/success")
    @FormUrlEncoded
    Call<ResponseBody> completeWalletPay(@Header("Authorization") String authorization,
                                         @Field("order") int orderId,
                                         @Field("transaction_id") String transactionId
    );

    @GET("node/levels/sponsors/{node_id}/{label}")
    Call<ResponseBody> fetchSponsorsOfLevels(@Header("Authorization") String authorization,
                                              @Path("node_id") int nodeId,
                                              @Path("label") int level
    );

}
