package fleximart.fleximart.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fleximart.fleximart.R;
import fleximart.fleximart.domain.executors.impl.ThreadExecutor;
import fleximart.fleximart.presentation.presenters.WishlistPresenter;
import fleximart.fleximart.presentation.presenters.impl.WishlistPresenterImpl;
import fleximart.fleximart.presentation.ui.adapters.WishlistAdapter;
import fleximart.fleximart.threading.MainThreadImpl;
import fleximart.fleximart.util.Helper;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

public class WishlistActivity extends AppCompatActivity implements WishlistPresenter.View {

    @BindView(R.id.recycler_view_wishlist)
    RecyclerView recyclerViewWishlist;
    @BindView(R.id.layout_message)
    View layoutMessage;
    @BindView(R.id.txt_view_message)
    TextView txtViewMessage;
    @BindView(R.id.layout_cart_status)
    View layoutCartStatus;
    @BindView(R.id.txt_view_cart_amount)
    TextView txtViewCartAmount;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.wishlist_empty_layout)
    View wishlistEmptyLayout;
    ProgressDialog progressDialog;
    WishlistPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wishlist);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("My Wishlist");
        initialisePresenter();
        setUpProgressDialog();
        showLoader();
    }

    public void initialisePresenter() {
        mPresenter = new WishlistPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }


    @Override
    public void loadWishlist(WishlistAdapter adapter) {
        recyclerViewWishlist.setVisibility(View.VISIBLE);
        wishlistEmptyLayout.setVisibility(View.GONE);
        recyclerViewWishlist.setAdapter(adapter);
        recyclerViewWishlist.setLayoutManager(new GridLayoutManager(this, 2));
    }

    @Override
    public void loadCartCount(int count) {
        if (count > 0) {
            layoutCartStatus.setVisibility(View.VISIBLE);
            txtViewCartAmount.setText(Integer.toString(count));
        } else {
            layoutCartStatus.setVisibility(View.INVISIBLE);
        }

    }

    @Override
    public void showErrorMsg(String msg) {
        layoutMessage.setVisibility(View.VISIBLE);
        txtViewMessage.setText(msg);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                layoutMessage.setVisibility(View.GONE);
            }
        }, 3000);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void goToProductDetails(int productId) {

    }

    @Override
    public void showBottomSheet() {

    }

    @Override
    public void onWishlistEmpty() {
        recyclerViewWishlist.setVisibility(View.GONE);
        wishlistEmptyLayout.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Helper.isNetworkConnected(this)) {
            mPresenter.fetchWishlist();
            mPresenter.fetchCartDetails();
        } else {
            showErrorMsg("No Internet Connection");
        }
    }

    @OnClick(R.id.layout_cart) void onCartClicked() {
        if (Helper.isNetworkConnected(this)) {
            Intent intent = new Intent(this, CartListActivity.class);
            startActivity(intent);
        } else {
            showErrorMsg("No Internet Connection");
        }
    }

    @OnClick(R.id.btn_shop_now) void onShopNowClicked() {
        if (Helper.isNetworkConnected(this)) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        } else {
            showErrorMsg("No Internet Connection");
        }
    }

}
