package fleximart.fleximart.presentation.ui.activities;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Handler;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fleximart.fleximart.AndroidApplication;
import fleximart.fleximart.R;
import fleximart.fleximart.domain.executors.impl.ThreadExecutor;
import fleximart.fleximart.domain.model.Address;
import fleximart.fleximart.domain.model.MainCategory;
import fleximart.fleximart.domain.model.MainData;
import fleximart.fleximart.domain.model.Other.HomeHeaderSliders;
import fleximart.fleximart.domain.model.Testing.ProductList;
import fleximart.fleximart.domain.model.UserInfo;
import fleximart.fleximart.presentation.presenters.MainPresenter;
import fleximart.fleximart.presentation.presenters.impl.MainPresenterImpl;
import fleximart.fleximart.presentation.ui.adapters.HomeSlidingImagesAdapter;
import fleximart.fleximart.presentation.ui.adapters.ProductListHorizontalAdapter;
import fleximart.fleximart.presentation.ui.bottomsheets.AddressBottomSheet;
import fleximart.fleximart.presentation.ui.bottomsheets.LoginBottomSheet;
import fleximart.fleximart.threading.MainThreadImpl;
import fleximart.fleximart.util.GlideHelper;
import fleximart.fleximart.util.Helper;

public class MainActivity extends BaseActivity implements MainPresenter.View, ProductListHorizontalAdapter.Callback {

    @BindView(R.id.img_view_mens_wear)
    ImageView imgViewMensWear;
    @BindView(R.id.img_view_womens_wear)
    ImageView imgViewWomensWear;
    @BindView(R.id.img_view_kids_wear)
    ImageView imgViewKidsWear;
    @BindView(R.id.image_slider_viewpager)
    ViewPager viewPager;
    @BindView(R.id.recycler_view_best_selling)
    RecyclerView recyclerViewBestSelling;
    @BindView(R.id.img_view_first_poster)
    ImageView imgViewFirstPoster;
    @BindView(R.id.recycler_view_newest_arrivals)
    RecyclerView recyclerViewNewestArrivals;
    @BindView(R.id.img_view_second_poster)
    ImageView imgViewSecondPoster;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    MainPresenterImpl mPresenter;
    MainCategory[] mainCategories;
    AndroidApplication androidApplication;
    @BindView(R.id.txt_view_cart_amount)
    TextView txtViewCartAmount;
    @BindView(R.id.layout_message)
    View layoutMessage;
    @BindView(R.id.txt_view_error_msg)
    TextView txtViewErrorMsg;
    @BindView(R.id.img_view_mens_wear_banner)
    ImageView imgViewMensBanner;
    @BindView(R.id.img_view_womens_wear_banner)
    ImageView imgViewWomensBanner;
    @BindView(R.id.img_view_kids_wear_banner)
    ImageView imgViewKidsBanner;
    @BindView(R.id.layout_wishlist_count)
    View layoutWishlistCount;
    @BindView(R.id.layout_cart_status)
    View layoutCartStatus;
    @BindView(R.id.txt_view_wishlist_count)
    TextView txtViewWishlistCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflateContent(R.layout.activity_main);
        ButterKnife.bind(this);
        setImageSlider();
        setImages();
        initialisePresenter();
        mPresenter.fetchMainData();
        showCredentials();
    }

    public void initialisePresenter() {
        mPresenter = new MainPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void showCredentials() {
        androidApplication = (AndroidApplication) getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(this);
        if (userInfo != null) {
            Log.e("LogMsg", "API Key: " + userInfo.apiToken);
            Log.e("LogMsg", "User Id: " + userInfo.id);
            Log.e("LogMsg", "Member Status: " + userInfo.memberStatus);
        }
    }

    public void setImageSlider(){
        HomeHeaderSliders homeHeaderSliders1 = new HomeHeaderSliders("https://m00375694.files.wordpress.com/2012/02/philica-clothing-poster-design.png");
        HomeHeaderSliders homeHeaderSliders2 = new HomeHeaderSliders("http://sacreativenetwork.co.za/wp-content/uploads/2013/10/NEW-sa-creative-Temp-copy-copy-copy-copy-copy-copy-copy1.jpg");
        HomeHeaderSliders homeHeaderSliders3 = new HomeHeaderSliders("https://media.creativebrands.co.za/stores/za.co.storefront7.creativebrands/pictures/636879774865967069/amrod-clothing-(686x820).png?quality=100");
        HomeHeaderSliders homeHeaderSliders4 = new HomeHeaderSliders("https://www.platosclosetsaginaw.com/users/pc-80319/images/JMM/Events/PC-0719-50-Clear-Web.png");

        final ArrayList<HomeHeaderSliders> homeHeaderSliders = new ArrayList<>();
        homeHeaderSliders.add(homeHeaderSliders1);
        homeHeaderSliders.add(homeHeaderSliders2);
        homeHeaderSliders.add(homeHeaderSliders3);
        homeHeaderSliders.add(homeHeaderSliders4);

        HomeSlidingImagesAdapter homeSlidingImagesAdapter = new HomeSlidingImagesAdapter(this, homeHeaderSliders);
        viewPager.setAdapter(homeSlidingImagesAdapter);

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if(currentPage == homeHeaderSliders.size())
                {
                    currentPage = 0;
                }
                viewPager.setCurrentItem(currentPage, true);
                currentPage++;
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 1000, 3000);

    }

    public void setImages(){
        GlideHelper.setImageView(this, imgViewFirstPoster, "http://appily.co/wp-content/uploads/2019/01/view-template-promotion-card-discount-posters.jpg" );
        GlideHelper.setImageView(this, imgViewSecondPoster, "https://mir-s3-cdn-cf.behance.net/project_modules/disp/78f35b36495443.571eb988dc448.jpg" );
        GlideHelper.setImageView(this, imgViewMensBanner, "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSzbsaiEO_tZwe_KpObuRLNUD0hVhzy3ivne4FLgFvMOeIUxnvF");
        GlideHelper.setImageView(this, imgViewWomensBanner, "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTX3VN2VSGqiNcWnLVXcczJl7HOlOaY_9WR62bCC8ndeQYsn4UV");
        GlideHelper.setImageView(this, imgViewKidsBanner, "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSwCVbMMDB7C66wykA9zFy1eha9fsRTGgcsx5yNYSSsPj77ZzZK");
    }


    @OnClick(R.id.men_category_card) void goToMensSubcategory() {
        if (Helper.isNetworkConnected(this)) {
            Intent intent = new Intent(this, SubcategoryActivity.class);
            intent.putExtra("image", getResources().getString(R.string.base_url) + "images/category/main_category/" + mainCategories[0].categoryImage);
            intent.putExtra("title", "Mens Wear");
            intent.putExtra("categoryId", 1);
            Pair[] pairs = new Pair[1];
            pairs[0] = new Pair<View, String> (imgViewMensWear, "imageTransition");
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(MainActivity.this, pairs);
                startActivity(intent, options.toBundle());
            }else{
                startActivity(intent);
            }
        } else {
            showErrorMessage("No Internet Connection");
        }
    }

    @OnClick(R.id.women_category_card) void onWomenCategoryClicked() {
        if (Helper.isNetworkConnected(this)) {
            Intent intent = new Intent(this, SubcategoryActivity.class);
            intent.putExtra("image", getResources().getString(R.string.base_url) + "images/category/main_category/" + mainCategories[1].categoryImage);
            intent.putExtra("title", "Womens Wear");
            intent.putExtra("categoryId", 2);
            Pair[] pairs = new Pair[1];
            pairs[0] = new Pair<View, String> (imgViewWomensWear, "imageTransition");
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(MainActivity.this, pairs);
                startActivity(intent, options.toBundle());
            }else{
                startActivity(intent);
            }
        } else {
            showErrorMessage("No Internet Connection");
        }
    }

    @OnClick(R.id.kid_category_card) void onKidCategoryClicked() {
        if (Helper.isNetworkConnected(this)) {
            Intent intent = new Intent(this, SubcategoryActivity.class);
            intent.putExtra("image", getResources().getString(R.string.base_url) + "images/category/main_category/" + mainCategories[2].categoryImage);
            intent.putExtra("title", "Kids Wear");
            intent.putExtra("categoryId", 3);
            Pair[] pairs = new Pair[1];
            pairs[0] = new Pair<View, String> (imgViewKidsWear, "imageTransition");
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(MainActivity.this, pairs);
                startActivity(intent, options.toBundle());
            }else{
                startActivity(intent);
            }
        } else {
            showErrorMessage("No Internet Connection");
        }
    }

    @Override
    public void loadMainData(MainData mainData) {

//        Set main category images
        mainCategories = mainData.mainCategories;
        GlideHelper.setImageView(this, imgViewMensWear, getResources().getString(R.string.base_url) + "images/category/main_category/" + mainCategories[0].categoryImage);
        GlideHelper.setImageView(this, imgViewWomensWear, getResources().getString(R.string.base_url) + "images/category/main_category/" + mainCategories[1].categoryImage);
        GlideHelper.setImageView(this, imgViewKidsWear, getResources().getString(R.string.base_url) + "images/category/main_category/" + mainCategories[2].categoryImage);

//        Set best selling products
        ProductListHorizontalAdapter bestSellingAdapter = new ProductListHorizontalAdapter(this, mainData.popularProducts, this::onProductClicked);
        recyclerViewBestSelling.setAdapter(bestSellingAdapter);
        recyclerViewBestSelling.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

//        Set newest arrivals
        ProductListHorizontalAdapter newestArrivalsAdapter = new ProductListHorizontalAdapter(this, mainData.new_arrivals, this::onProductClicked);
        recyclerViewNewestArrivals.setAdapter(newestArrivalsAdapter);
        recyclerViewNewestArrivals.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));

    }

    @Override
    public void loadCartCount(int count) {
        if (count > 0) {
            layoutCartStatus.setVisibility(View.VISIBLE);
            txtViewCartAmount.setText(Integer.toString(count));
        } else {
            layoutCartStatus.setVisibility(View.INVISIBLE);
        }

    }

    @Override
    public void loadWishlistCount(int count) {
        if (count > 0) {
            layoutWishlistCount.setVisibility(View.VISIBLE);
            txtViewWishlistCount.setText(Integer.toString(count));
        } else {
            layoutWishlistCount.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.fetchCartDetails();
        mPresenter.fetchWishlist();
    }

    @Override
    public void onProductClicked(int id) {

    }

    @OnClick(R.id.layout_cart) void onCartClicked() {
        if (checkLogin()) {
            if (Helper.isNetworkConnected(this)) {
                Intent intent = new Intent(this, CartListActivity.class);
                startActivity(intent);
            } else {
                showErrorMessage("No Internet Connection");
            }
        } else {
            showBottomSheet();
        }
    }

    @OnClick(R.id.layout_wishlist) void onWishlistClicked() {
        if (checkLogin()) {
            if (Helper.isNetworkConnected(this)) {
                Intent intent = new Intent(this, WishlistActivity.class);
                startActivity(intent);
            } else {
                showErrorMessage("No Internet Connection");
            }
        } else {
            showBottomSheet();
        }
    }

    public boolean checkLogin() {
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(this);
        if (user == null) {
            return false;
        } else {
            return true;
        }
    }

    public void showBottomSheet() {
        LoginBottomSheet loginBottomSheet = new LoginBottomSheet();
        loginBottomSheet.show(getSupportFragmentManager(), "");
    }

    void showErrorMessage(String errorMsg) {
        layoutMessage.setVisibility(View.VISIBLE);
        txtViewErrorMsg.setText(errorMsg);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                layoutMessage.setVisibility(View.GONE);
            }
        }, 3000);
    }

}
