package fleximart.fleximart.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fleximart.fleximart.AndroidApplication;
import fleximart.fleximart.R;
import fleximart.fleximart.domain.executors.impl.ThreadExecutor;
import fleximart.fleximart.domain.model.UserInfo;
import fleximart.fleximart.presentation.presenters.LoginPresenter;
import fleximart.fleximart.presentation.presenters.impl.LoginPresenterImpl;
import fleximart.fleximart.threading.MainThreadImpl;
import fleximart.fleximart.util.Helper;

public class LoginActivity extends AppCompatActivity implements LoginPresenter.View {

    ProgressDialog progressDialog;
    @BindView(R.id.layout_message)
    View layoutMessage;
    @BindView(R.id.txt_view_message)
    TextView txtViewMessage;
    @BindView(R.id.edit_text_email)
    EditText editTextEmail;
    @BindView(R.id.edit_text_password)
    EditText editTextPassword;
    @BindView(R.id.txt_input_email_layout)
    TextInputLayout txtInputEmailLayout;
    @BindView(R.id.txt_input_password_layout)
    TextInputLayout txtInputPasswordLayout;
    LoginPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().setTitle((Html.fromHtml("<font color=\"#00000\"> Log In </font>")));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        setProgressDialog();
        initialisePresenter();
    }

    public void initialisePresenter() {
        mPresenter = new LoginPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void goToMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void showMessage(String errorMsg) {
        layoutMessage.setVisibility(View.VISIBLE);
        txtViewMessage.setText(errorMsg);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                layoutMessage.setVisibility(View.GONE);
            }
        }, 3000);
    }

    @OnClick(R.id.btn_login) void onLoginClicked() {
        if (Helper.isNetworkConnected(this)) {
            if (editTextEmail.getText().toString().trim().isEmpty()) {
                txtInputEmailLayout.setError("Please enter an email");
                editTextEmail.requestFocus();
            } else if (editTextPassword.getText().toString().trim().isEmpty()) {
                txtInputPasswordLayout.setError("Please enter password");
                editTextPassword.requestFocus();
            } else if(!Patterns.EMAIL_ADDRESS.matcher(editTextEmail.getText().toString()).matches()){
                txtInputEmailLayout.setError("Please enter an valid email");
                editTextEmail.requestFocus();
            } else {
                mPresenter.checkLogin(editTextEmail.getText().toString(), editTextPassword.getText().toString());
                showLoader();
            }
        } else {
            showMessage("No Internet Connection");
        }
    }

    @OnClick(R.id.btn_sign_up) void onSignUpClicked() {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }
}
