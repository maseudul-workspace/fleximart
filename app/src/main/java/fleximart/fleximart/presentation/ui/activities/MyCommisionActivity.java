package fleximart.fleximart.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fleximart.fleximart.R;
import fleximart.fleximart.domain.executors.impl.ThreadExecutor;
import fleximart.fleximart.domain.model.CommisionTotal;
import fleximart.fleximart.presentation.presenters.MyCommisionPresenter;
import fleximart.fleximart.presentation.presenters.impl.MyCommisionPresenterImpl;
import fleximart.fleximart.threading.MainThreadImpl;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MyCommisionActivity extends AppCompatActivity implements MyCommisionPresenter.View {

    @BindView(R.id.txt_view_approved_commision)
    TextView txtViewApprovedCommision;
    @BindView(R.id.txt_view_unapproved_commision)
    TextView txtViewUnapprovedCommison;
    @BindView(R.id.layout_approved_commission)
    View layoutApprovedCommission;
    @BindView(R.id.layout_unapproved_commission)
    View layoutUnapprovedCommission;
    @BindView(R.id.txt_view_no_data_found)
    TextView txtViewNoDataFound;
    @BindView(R.id.view_line)
    View viewLine;
    MyCommisionPresenterImpl mPresenter;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_commision);
        getSupportActionBar().setTitle("My Commission");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        setUpProgressDialog();
        initialisePresenter();
        mPresenter.fetchMyTotalCommision();
        showLoader();
    }

    public void initialisePresenter() {
        mPresenter = new MyCommisionPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadData(CommisionTotal commisionTotal) {
        if ((commisionTotal.approved == null || Double.parseDouble(commisionTotal.approved) < 1) && (commisionTotal.unapproved == null || Double.parseDouble(commisionTotal.unapproved) < 1)) {
            txtViewNoDataFound.setVisibility(View.VISIBLE);
        } else {
            if (commisionTotal.approved == null || Double.parseDouble(commisionTotal.approved) < 1) {
                layoutApprovedCommission.setVisibility(View.GONE);
            } else {
                layoutApprovedCommission.setVisibility(View.VISIBLE);
                txtViewApprovedCommision.setText("₹ " + commisionTotal.approved);
            }
            if (commisionTotal.unapproved == null || Double.parseDouble(commisionTotal.unapproved) < 1) {
                layoutUnapprovedCommission.setVisibility(View.GONE);
            } else {
                layoutUnapprovedCommission.setVisibility(View.VISIBLE);
                txtViewUnapprovedCommison.setText("₹ " + commisionTotal.unapproved);
                viewLine.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void onError(String errorMsg) {
        txtViewNoDataFound.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.btn_approved_commission) void onApprovedCommissionClicked() {
        Intent intent = new Intent(this, MyCommisionListActivity.class);
        intent.putExtra("isApprovedCommission", true);
        startActivity(intent);
    }

    @OnClick(R.id.btn_unapproved_commission) void onUnapprovedCommissionClicked() {
        Intent intent = new Intent(this, MyCommisionListActivity.class);
        intent.putExtra("isApprovedCommission", false);
        startActivity(intent);
    }
}
