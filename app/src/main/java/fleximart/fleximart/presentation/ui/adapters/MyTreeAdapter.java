package fleximart.fleximart.presentation.ui.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tooltip.OnClickListener;
import com.tooltip.Tooltip;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.blox.graphview.BaseGraphAdapter;
import de.blox.graphview.Graph;
import de.blox.graphview.ViewHolder;
import fleximart.fleximart.R;

public class MyTreeAdapter extends BaseGraphAdapter<MyTreeAdapter.MyTreeViewHolder> {

    public interface Callback {
        void onNodeClicked(int nodeId, int rank);
    }

    Context mContext;
    Callback mCallback;

    public MyTreeAdapter(@NonNull Graph graph, Context mContext, Callback mCallback) {
        super(graph);
        this.mContext = mContext;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public MyTreeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_treeview, parent, false);
        MyTreeViewHolder myTreeViewHolder = new MyTreeViewHolder(view);
        return myTreeViewHolder;
    }

    @Override
    public void onBindViewHolder(MyTreeViewHolder viewHolder, Object data, int position) {
        String[] dataString = data.toString().split("-");
        viewHolder.txtViewId.setText(dataString[2]);
        boolean paymentStatus = Boolean.valueOf(dataString[5]);
        final String paymentString;
        if (paymentStatus) {
            viewHolder.mainLayout.setCardBackgroundColor(mContext.getResources().getColor(R.color.blue2));
            paymentString = "paid";
        } else {
            viewHolder.mainLayout.setCardBackgroundColor(mContext.getResources().getColor(R.color.orange1));
            paymentString = "unpaid";
        }
        viewHolder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Tooltip tooltip = new Tooltip.Builder(viewHolder.itemView)
                        .setText(dataString[4] + "\n" + "Payment Status: " + paymentString + "\n" + "Rank: " + dataString[3])
                        .setTextColor(mContext.getResources().getColor(R.color.white))
                        .setBackgroundColor(mContext.getResources().getColor(R.color.materialRed))
                        .setDrawableEnd(mContext.getDrawable(R.drawable.double_forward))
                        .setCancelable(true)
                        .setDismissOnClick(true)
                        .setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(@NonNull Tooltip tooltip) {
                                tooltip.dismiss();
                                mCallback.onNodeClicked(Integer.parseInt(dataString[2]), Integer.parseInt(dataString[3]));
                            }
                        })
                        .show();
            }
        });
    }

    public class MyTreeViewHolder extends ViewHolder {

        @BindView(R.id.main_layout)
        CardView mainLayout;
        @BindView(R.id.txt_view_id)
        TextView txtViewId;

        MyTreeViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}
