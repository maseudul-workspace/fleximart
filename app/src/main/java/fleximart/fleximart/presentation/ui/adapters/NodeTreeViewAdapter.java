package fleximart.fleximart.presentation.ui.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.blox.graphview.BaseGraphAdapter;
import de.blox.graphview.Graph;
import de.blox.graphview.ViewHolder;
import fleximart.fleximart.R;

public class NodeTreeViewAdapter extends BaseGraphAdapter<NodeTreeViewAdapter.NodeTreeViewHolder> {

    public interface Callback {
        void onCardClicked(String leg);
    }

    Context mContext;
    Callback mCallback;

    public NodeTreeViewAdapter(@NonNull Graph graph, Context context, Callback callback) {
        super(graph);
        mCallback = callback;
        mContext = context;
    }

    @NonNull
    @Override
    public NodeTreeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_treeview, parent, false);
        NodeTreeViewHolder nodeTreeViewHolder = new NodeTreeViewHolder(view);
        return nodeTreeViewHolder;
    }

    @Override
    public void onBindViewHolder(NodeTreeViewHolder viewHolder, Object data, int position) {
        String[] dataString = data.toString().split("-");

        Log.e("LogMsg", "String1: " + dataString[0]);

        if (dataString[2].equals("false")) {
            viewHolder.mainLayout.setCardBackgroundColor(mContext.getResources().getColor(R.color.darkGrey));
            viewHolder.mainLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(mContext, "Not Available", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            viewHolder.mainLayout.setCardBackgroundColor(mContext.getResources().getColor(R.color.md_green_400));
            viewHolder.mainLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCallback.onCardClicked(dataString[4]);
                }
            });
        }
        viewHolder.txtViewId.setText(dataString[4]);
    }

    public class NodeTreeViewHolder extends ViewHolder {

        @BindView(R.id.main_layout)
        CardView mainLayout;
        @BindView(R.id.txt_view_id)
        TextView txtViewId;

        NodeTreeViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
