package fleximart.fleximart.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import android.view.View;

import androidx.appcompat.app.AlertDialog;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fleximart.fleximart.R;

public class GenderSelectDialog {

    public interface Callback {
        void onGenderClicked(String gender);
    }

    Context mContext;
    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    Callback mCallback;

    public GenderSelectDialog(Context mContext, Activity mActivity, Callback mCallback) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        this.mCallback = mCallback;
    }

    public void setUpDialogView() {
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.layout_gender_select_dialog, null);
        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Light_Dialog_NoActionBar_MinWidth);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        ButterKnife.bind(this, dialogContainer);
    }

    @OnClick(R.id.txt_view_male) void onMaleClicked() {
        mCallback.onGenderClicked("Male");
        dialog.dismiss();
    }

    @OnClick(R.id.txt_view_female) void onFemaleClicked() {
        mCallback.onGenderClicked("Female");
        dialog.dismiss();
    }

    public void showDialog() {
        dialog.show();
    }

}
