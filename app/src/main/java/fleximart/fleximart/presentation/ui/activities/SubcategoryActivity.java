package fleximart.fleximart.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import fleximart.fleximart.R;
import fleximart.fleximart.domain.executors.impl.ThreadExecutor;
import fleximart.fleximart.presentation.presenters.SubcategoryPresenter;
import fleximart.fleximart.presentation.presenters.impl.SubcategoryPresenterImpl;
import fleximart.fleximart.presentation.ui.adapters.FirstCategoryAdapter;
import fleximart.fleximart.threading.MainThreadImpl;
import fleximart.fleximart.util.GlideHelper;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import static android.widget.LinearLayout.HORIZONTAL;
import static android.widget.LinearLayout.VERTICAL;

public class SubcategoryActivity extends AppCompatActivity implements SubcategoryPresenter.View {

    @BindView(R.id.img_view_main_category)
    ImageView imgViewMainCategory;
    String categoryImage;
    String categoryTitle;
    int catgeoryId;
    @BindView(R.id.recycler_view_first_category)
    RecyclerView recyclerViewFirstCategory;
    SubcategoryPresenterImpl mPresenter;
    @BindView(R.id.layout_message)
    View layoutMessage;
    @BindView(R.id.txt_view_error_msg)
    TextView txtViewErrorMsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subcategory);
        ButterKnife.bind(this);
        catgeoryId = getIntent().getIntExtra("categoryId", 0);
        categoryImage = getIntent().getStringExtra("image");
        categoryTitle = getIntent().getStringExtra("title");
        GlideHelper.setImageView(this, imgViewMainCategory, categoryImage);
        getSupportActionBar().setTitle((Html.fromHtml("<font color=\"#00000\"> "+ categoryTitle +" </font>")));
        initialisePresenter();
        mPresenter.fetchSubcategory(catgeoryId);
    }

    public void initialisePresenter() {
        mPresenter = new SubcategoryPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @Override
    public void loadData(FirstCategoryAdapter firstCategoryAdapter) {
        recyclerViewFirstCategory.setAdapter(firstCategoryAdapter);
        recyclerViewFirstCategory.setLayoutManager(new LinearLayoutManager(this));
        DividerItemDecoration itemDecor = new DividerItemDecoration(this, VERTICAL);
        recyclerViewFirstCategory.addItemDecoration(itemDecor);
    }

    @Override
    public void goToProductListActivity(int subcategoryId) {
        Log.e("LogMsg", "Hello");
        Intent intent = new Intent(this, ProductListActivity.class);
        intent.putExtra("secondCategoryId", subcategoryId);
        startActivity(intent);
    }

    @Override
    public void showErrorMessage(String errorMsg) {
        layoutMessage.setVisibility(View.VISIBLE);
        txtViewErrorMsg.setText(errorMsg);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                layoutMessage.setVisibility(View.GONE);
            }
        }, 3000);
    }
}
