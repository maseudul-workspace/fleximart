package fleximart.fleximart.presentation.ui.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import fleximart.fleximart.R;
import fleximart.fleximart.domain.model.FirstCategory;

public class FirstCategoryAdapter extends RecyclerView.Adapter<FirstCategoryAdapter.ViewHolder> implements SecondCategoryAdapter.Callback {

    public interface Callback {
        void onSecondCategoryClicked(int id);
    }

    Context mContext;
    FirstCategory[] firstCategories;
    Callback mCallback;

    public FirstCategoryAdapter(Context mContext, FirstCategory[] firstCategories, Callback mCallback) {
        this.mContext = mContext;
        this.firstCategories = firstCategories;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_firstcategory, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewFirstCategory.setText(firstCategories[position].name);
        SecondCategoryAdapter secondCategoryAdapter = new SecondCategoryAdapter(mContext, firstCategories[position].secondCategories, this::onCategoryClicked);
        holder.recyclerViewSecondCategory.setAdapter(secondCategoryAdapter);
        holder.recyclerViewSecondCategory.setLayoutManager(new GridLayoutManager(mContext, 3));
        holder.layoutFirstCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.imgViewArrowUp.getVisibility() == View.GONE){
                    holder.imgViewArrowUp.setVisibility(View.VISIBLE);
                    holder.imgViewArrowDown.setVisibility(View.GONE);
                    holder.recyclerViewSecondCategory.setVisibility(View.VISIBLE);
                } else {
                    holder.imgViewArrowUp.setVisibility(View.GONE);
                    holder.imgViewArrowDown.setVisibility(View.VISIBLE);
                    holder.recyclerViewSecondCategory.setVisibility(View.GONE);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return firstCategories.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_arrow_down)
        ImageView imgViewArrowDown;
        @BindView(R.id.img_view_arrow_up)
        ImageView imgViewArrowUp;
        @BindView(R.id.txt_view_first_category)
        TextView txtViewFirstCategory;
        @BindView(R.id.recycler_view_second_category)
        RecyclerView recyclerViewSecondCategory;
        @BindView(R.id.layout_first_category)
        View layoutFirstCategory;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public void onCategoryClicked(int id) {
        mCallback.onSecondCategoryClicked(id);
    }

}
