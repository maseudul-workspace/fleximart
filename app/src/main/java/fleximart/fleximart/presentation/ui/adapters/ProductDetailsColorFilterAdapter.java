package fleximart.fleximart.presentation.ui.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import fleximart.fleximart.R;
import fleximart.fleximart.domain.model.Testing.FilterValues;

/**
 * Created by Raj on 09-09-2019.
 */

public class ProductDetailsColorFilterAdapter extends RecyclerView.Adapter<ProductDetailsColorFilterAdapter.ViewHolder> {

    public interface Callback {
        void onColorSelected(int id);
    }

    Context mContext;
    fleximart.fleximart.domain.model.Color[] colors;
    Callback mCallback;

    public ProductDetailsColorFilterAdapter(Context mContext, fleximart.fleximart.domain.model.Color[] colors, Callback callback) {
        this.mContext = mContext;
        this.colors = colors;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_product_details_color_filter, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
//        viewHolder.colorCardview.setCardBackgroundColor(Color.parseColor(filterValues.get(i).filterValue));
        Log.e("LogMsg", "Color value: " + colors[i].value);
        Drawable background = viewHolder.colorCardview.getBackground();
        GradientDrawable gradientDrawable = (GradientDrawable) background;
        gradientDrawable.setColor(Color.parseColor(colors[i].value));
        if (colors[i].isSelected) {
            viewHolder.colorContainer.setBackground(mContext.getResources().getDrawable(R.drawable.rectangle_grey_stroke));
        } else {
            viewHolder.colorContainer.setBackgroundResource(0);
        }
        viewHolder.colorContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onColorSelected(colors[i].id);
            }
        });
    }

    @Override
    public int getItemCount() {
        return colors.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.layout_color_container)
        View colorContainer;
        @BindView(R.id.cardview_color)
        View colorCardview;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateDataSet(fleximart.fleximart.domain.model.Color[] colors) {
        this.colors = colors;
        notifyDataSetChanged();
    }

}
