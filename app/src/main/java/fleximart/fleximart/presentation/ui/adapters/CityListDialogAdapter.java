package fleximart.fleximart.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import fleximart.fleximart.R;
import fleximart.fleximart.domain.model.Cities;

public class CityListDialogAdapter extends RecyclerView.Adapter<CityListDialogAdapter.ViewHolder> {

    public interface Callback {
        void onCityClicked(int id, String city);
    }

    Context mContext;
    Cities[] cities;
    Callback mCallback;

    public CityListDialogAdapter(Context mContext, Cities[] cities, Callback mCallback) {
        this.mContext = mContext;
        this.cities = cities;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_city_dialog, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewCity.setText(cities[position].name);
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onCityClicked(cities[position].id, cities[position].name);
            }
        });
    }

    @Override
    public int getItemCount() {
        return cities.length;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_city)
        TextView txtViewCity;
        @BindView(R.id.main_layout)
        View mainLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
