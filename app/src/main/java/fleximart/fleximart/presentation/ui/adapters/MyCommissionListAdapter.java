package fleximart.fleximart.presentation.ui.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import fleximart.fleximart.R;
import fleximart.fleximart.domain.model.MyCommissionList;

public class MyCommissionListAdapter extends RecyclerView.Adapter<MyCommissionListAdapter.ViewHolder> {

    Context mContext;
    MyCommissionList[] myCommissionLists;

    public MyCommissionListAdapter(Context mContext, MyCommissionList[] myCommissionLists) {
        this.mContext = mContext;
        this.myCommissionLists = myCommissionLists;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_my_commission_list, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewSlNo.setText(Integer.toString(position + 1));
        holder.txtViewAmount.setText("Rs. " + myCommissionLists[position].amount);
        holder.txtViewCommissionOf.setText(myCommissionLists[position].payToNode);
        holder.txtViewCommissionFrom.setText(myCommissionLists[position].payFromNode);
        holder.txtViewLevel.setText(myCommissionLists[position].level);
        String dateFormat = changeDataFormat(myCommissionLists[position].date);
        String dateString[] = dateFormat.split("-");
        holder.txtViewDate.setText(dateString[0]);
        holder.txtViewTime.setText(dateString[1]);
    }

    @Override
    public int getItemCount() {
        return myCommissionLists.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_sl_no)
        TextView txtViewSlNo;
        @BindView(R.id.txt_view_commission_of)
        TextView txtViewCommissionOf;
        @BindView(R.id.txt_view_commission_from)
        TextView txtViewCommissionFrom;
        @BindView(R.id.txt_view_level)
        TextView txtViewLevel;
        @BindView(R.id.txt_view_amount)
        TextView txtViewAmount;
        @BindView(R.id.txt_view_date)
        TextView txtViewDate;
        @BindView(R.id.txt_view_time)
        TextView txtViewTime;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateDataset(MyCommissionList[] myCommissionLists) {
        this.myCommissionLists = myCommissionLists;
    }

    public String changeDataFormat(String date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-m-d HH:mm:ss");
        Date sourceDate = null;
        try {
            sourceDate = dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat targetFormat = new SimpleDateFormat("EEE, MMM d-HH:mm aaa");
        String targetdatevalue= targetFormat.format(sourceDate);
        return targetdatevalue;
    }


}
