package fleximart.fleximart.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import fleximart.fleximart.R;
import fleximart.fleximart.domain.executors.impl.ThreadExecutor;
import fleximart.fleximart.presentation.presenters.MyLevelSponsorsPresenter;
import fleximart.fleximart.presentation.presenters.impl.MyLevelSponsorsPresenterImpl;
import fleximart.fleximart.presentation.ui.adapters.MyLevelSponsorsAdapter;
import fleximart.fleximart.threading.MainThreadImpl;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MyLevelSponsorsActivity extends AppCompatActivity implements MyLevelSponsorsPresenter.View {

    @BindView(R.id.recycler_view_my_level_sponsors)
    RecyclerView recyclerViewLevelSponsors;
    @BindView(R.id.img_view_no_record_found)
    ImageView imgViewNoRecordFound;
    ProgressDialog progressDialog;
    MyLevelSponsorsPresenterImpl mPresenter;
    int nodeId;
    int levelId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_level_sponsors);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Sponsors");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        nodeId = getIntent().getIntExtra("nodeId", 0);
        levelId = getIntent().getIntExtra("levelId", 0);
        setUpProgressDialog();
        initialisePresenter();
        mPresenter.fetchLevelSponsors(nodeId, levelId);
        showLoader();
    }

    public void initialisePresenter() {
        mPresenter = new MyLevelSponsorsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadAdapter(MyLevelSponsorsAdapter adapter) {
        recyclerViewLevelSponsors.setVisibility(View.VISIBLE);
        recyclerViewLevelSponsors.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewLevelSponsors.setAdapter(adapter);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void showMessage(String errorMsg) {
        imgViewNoRecordFound.setVisibility(View.VISIBLE);
    }
}
