package fleximart.fleximart.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fleximart.fleximart.R;

public class NodeConfirmDialog {

    public interface Callback {
        void onConfirmClicked();
    }

    Context mContext;
    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    Callback mCallback;
    @BindView(R.id.txt_view_node_count)
    TextView txtViewNodeCount;
    @BindView(R.id.txt_view_wallet_amount)
    TextView txtViewWalletAmount;

    public NodeConfirmDialog(Context mContext, Activity mActivity, Callback mCallback) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        this.mCallback = mCallback;
    }

    public void setUpDialog() {
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.layout_node_buy_confirmation_dialog, null);
        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Light_Dialog_NoActionBar_MinWidth);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        ButterKnife.bind(this, dialogContainer);
    }

    public void setData(int nodeCount, double walletAmount) {
        txtViewNodeCount.setText(Integer.toString(nodeCount));
        txtViewWalletAmount.setText(Double.toString(walletAmount));
    }

    public void showDialog() {
        dialog.show();
    }

    public void hideDialog() {
        dialog.dismiss();
    }

    @OnClick(R.id.btn_confirm) void onConfirmClicked() {
        hideDialog();
        mCallback.onConfirmClicked();
    }

    @OnClick(R.id.btn_cancel) void onCancelClicked() {
        hideDialog();
    }

}
