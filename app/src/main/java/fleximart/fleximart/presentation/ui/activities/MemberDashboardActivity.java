package fleximart.fleximart.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fleximart.fleximart.R;
import fleximart.fleximart.domain.executors.impl.ThreadExecutor;
import fleximart.fleximart.domain.model.DashboardData;
import fleximart.fleximart.presentation.presenters.MembersDashboardPresenter;
import fleximart.fleximart.presentation.presenters.impl.MemberDashboardPresenterImpl;
import fleximart.fleximart.threading.MainThreadImpl;

import android.animation.ValueAnimator;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;

public class MemberDashboardActivity extends MembersBaseActivity implements MembersDashboardPresenter.View {

    @BindView(R.id.txt_view_total_approved_commission)
    TextView txtViewTotalApprovedCommission;
    @BindView(R.id.txt_view_total_unapproved_commission)
    TextView txtViewTotalUnapprovedCommission;
    @BindView(R.id.txt_view_total_nodes)
    TextView txtViewTotalNodes;
    @BindView(R.id.txt_view_total_wallet_balance)
    TextView txtViewTotalWalletBalance;
    @BindView(R.id.txt_view_total_downlines)
    TextView txtViewTotalDownlines;
    @BindView(R.id.txt_view_level_completed)
    TextView txtViewLevelCompleted;
    @BindView(R.id.layout_message)
    View layoutMessage;
    @BindView(R.id.txt_view_error_msg)
    TextView txtViewErrorMsg;
    ProgressDialog progressDialog;
    MemberDashboardPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflateContent(R.layout.activity_member_dashboard);
        ButterKnife.bind(this);
        setUpProgressDialog();
        initialisePresenter();
        mPresenter.fetchSummary();
        showLoader();
    }

    public void initialisePresenter() {
        mPresenter = new MemberDashboardPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadDasboardSummary(DashboardData dashboardData) {

//        Node Animator
        ValueAnimator totalNodeAnimator = ValueAnimator.ofInt(0, dashboardData.nodeSummary.myNodes);
        totalNodeAnimator.setDuration(1500);
        totalNodeAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                txtViewTotalNodes.setText(animation.getAnimatedValue().toString());
            }
        });
        totalNodeAnimator.start();

        //        Node Animator
        ValueAnimator levelCompletedAnimator = ValueAnimator.ofInt(0, dashboardData.nodeSummary.totalLevelComplete);
        levelCompletedAnimator.setDuration(1500);
        levelCompletedAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                txtViewLevelCompleted.setText(animation.getAnimatedValue().toString());
            }
        });
        levelCompletedAnimator.start();

//        Downline Animator
        ValueAnimator totalDownlineAnimator = ValueAnimator.ofInt(0, dashboardData.nodeSummary.totalDownline);
        totalDownlineAnimator.setDuration(1500);
        totalDownlineAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                txtViewTotalDownlines.setText(animation.getAnimatedValue().toString());
            }
        });
        totalDownlineAnimator.start();

//        Wallet Balance Animator
        ValueAnimator totalWalletBalanceAnimator = ValueAnimator.ofFloat(0, dashboardData.walletSummary.totalBalance);
        totalWalletBalanceAnimator.setDuration(1500);
        totalWalletBalanceAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                txtViewTotalWalletBalance.setText("₹ " + animation.getAnimatedValue().toString());
            }
        });
        totalWalletBalanceAnimator.start();

        //  Commission Summary Approved Animator
        ValueAnimator totalApprovedCommissionAnimator = ValueAnimator.ofFloat(0, dashboardData.commissionSummary.approved);
        totalApprovedCommissionAnimator.setDuration(1500);
        totalApprovedCommissionAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                txtViewTotalApprovedCommission.setText("₹ " + animation.getAnimatedValue().toString());
            }
        });
        totalApprovedCommissionAnimator.start();

        //  Commission Summary Unapproved Animator
        ValueAnimator totalUnapprovedCommissionAnimator = ValueAnimator.ofFloat(0, dashboardData.commissionSummary.unapproved);
        totalUnapprovedCommissionAnimator.setDuration(1500);
        totalUnapprovedCommissionAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                txtViewTotalUnapprovedCommission.setText("₹ " + animation.getAnimatedValue().toString());
            }
        });
        totalUnapprovedCommissionAnimator.start();

    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void showErrorMsg(String msg) {
        layoutMessage.setVisibility(View.VISIBLE);
        txtViewErrorMsg.setText(msg);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                layoutMessage.setVisibility(View.GONE);
            }
        }, 3000);
    }

    @OnClick(R.id.layout_node) void onLayoutNodeClicked() {
        goToMyNodes();
    }

    @OnClick(R.id.layout_downlines) void onLayoutDownlinesClicked() {
        goToMyNodes();
    }

    @OnClick(R.id.layout_level_completed) void onLayoutLevelCompletedClicked() {
        goToMyNodes();
    }

    @OnClick(R.id.layout_balance) void onLayoutBalanceClicked() {
        goToWalletHistory();
    }

    public void goToWalletHistory() {
        Intent intent = new Intent(this, MyWalletActivity.class);
        startActivity(intent);
    }

    public void goToMyNodes() {
        Intent intent = new Intent(this, MyNodesActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.layout_approved_commission) void onApprovedCommissionClicked() {
        Intent intent = new Intent(this, MyCommisionListActivity.class);
        intent.putExtra("isApprovedCommission", true);
        startActivity(intent);
    }

    @OnClick(R.id.layout_unapproved_commission) void onUnapprovedCommissionClicked() {
        Intent intent = new Intent(this, MyCommisionListActivity.class);
        intent.putExtra("isApprovedCommission", false);
        startActivity(intent);
    }

}
