package fleximart.fleximart.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindInt;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fleximart.fleximart.R;
import fleximart.fleximart.domain.executors.impl.ThreadExecutor;
import fleximart.fleximart.domain.model.Address;
import fleximart.fleximart.domain.model.ChecksumData;
import fleximart.fleximart.domain.model.OrderPlaceData;
import fleximart.fleximart.presentation.presenters.CheckoutPresenter;
import fleximart.fleximart.presentation.presenters.impl.CheckoutPresenterImpl;
import fleximart.fleximart.threading.MainThreadImpl;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;

public class CheckoutActivity extends AppCompatActivity implements CheckoutPresenter.View {

    @BindView(R.id.txt_view_total_mrp)
    TextView txtViewTotalMRP;
    @BindView(R.id.txt_view_total_discount)
    TextView txtViewTotalDiscount;
    @BindView(R.id.txt_view_payable_amount)
    TextView txtViewPayableAmount;
    @BindView(R.id.txt_view_name)
    TextView txtViewName;
    @BindView(R.id.txt_view_address)
    TextView txtViewAddress;
    @BindView(R.id.txt_view_city_pin)
    TextView txtViewCityPin;
    @BindView(R.id.txt_view_state)
    TextView txtViewState;
    @BindView(R.id.txt_view_mobile)
    TextView txtViewMobile;
    @BindView(R.id.txt_view_total_price)
    TextView txtViewTotalPrice;
    double totalMRP;
    double totalPrice;
    double totalDiscount;
    int addressId;
    double walletPay = 0.0;
    @BindView(R.id.radio_btn_cash)
    RadioButton radioBtnCash;
    @BindView(R.id.radio_btn_online)
    RadioButton radioBtnOnline;
    @BindView(R.id.address_layout)
    View addressLayout;
    @BindView(R.id.checkbox_wallet_pay)
    CheckBox checkBoxWalletPay;
    @BindView(R.id.txt_view_wallet_amount)
    TextView txtViewWalletAmount;
    @BindView(R.id.layout_wallet)
    View layoutWallet;
    @BindView(R.id.txt_view_wallet_pay_amount)
    TextView txtViewWalletPayAmount;
    @BindView(R.id.btn_place_order)
    Button btnPlaceOrder;
    @BindView(R.id.txt_view_payment_option)
    TextView txtViewPaymentOption;
    @BindView(R.id.layout_payment_option)
    View layoutPaymentOption;
    ProgressDialog progressDialog;
    CheckoutPresenterImpl mPresenter;
    int paymentMethod;
    int walletPayMethod;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Checkout");
        setProgressDialog();
        totalMRP = getIntent().getDoubleExtra("totalMRP", 0);
        totalPrice = getIntent().getDoubleExtra("totalPrice", 0);
        totalDiscount = getIntent().getDoubleExtra("totalDiscount", 0);
        addressId = getIntent().getIntExtra("addressId", 0);
        Log.e("LogMsg", "Address Id: " + addressId);
        setData();
        initialisePresenter();
        mPresenter.fetchWalletAmount();
        showLoader();
        setRadioButtons();
        setWalletCheckboxListeners();
    }

    public void initialisePresenter() {
        mPresenter = new CheckoutPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setWalletCheckboxListeners() {
        checkBoxWalletPay.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    walletPayMethod = 1;
                    layoutWallet.setVisibility(View.VISIBLE);
                    if (totalPrice > walletPay) {
                        txtViewTotalPrice.setText("₹ " + (totalPrice - walletPay));
                        txtViewPayableAmount.setText("₹ " + (totalPrice - walletPay));
                        txtViewWalletPayAmount.setText("₹ " + walletPay);
                        txtViewWalletAmount.setText("₹ 0.0");
                        showPaymentOptionLayout();
                    } else {
                        txtViewTotalPrice.setText("₹ 0.0");
                        txtViewPayableAmount.setText("₹ 0.0");
                        txtViewWalletPayAmount.setText("₹ " + totalPrice);
                        txtViewWalletAmount.setText("₹ " + (walletPay - totalPrice));
                        paymentMethod = 2;
                        enablePayButton();
                        hidePaymentOptionLayout();
                    }
                } else {
                    if (paymentMethod == 2) {
                        disablePayButton();
                        showPaymentOptionLayout();
                    }
                    walletPayMethod = 2;
                    txtViewTotalPrice.setText("₹" + totalPrice);
                    txtViewPayableAmount.setText("₹" + totalPrice);
                    txtViewWalletPayAmount.setText("₹ 0.0");
                    txtViewWalletAmount.setText("₹ " + walletPay);
                    layoutWallet.setVisibility(View.GONE);
                }
            }
        });
    }

    public void setData() {
        txtViewTotalMRP.setText("₹ " + totalMRP);
        txtViewTotalPrice.setText("₹ " + totalPrice);
        txtViewTotalDiscount.setText("₹ " + totalDiscount);
        txtViewPayableAmount.setText("₹ " + totalPrice);
    }

    public void setRadioButtons() {
        radioBtnCash.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    paymentMethod = 1;
                    radioBtnOnline.setChecked(false);
                    enablePayButton();
                }
            }
        });
        radioBtnOnline.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    paymentMethod = 3;
                    radioBtnCash.setChecked(false);
                    enablePayButton();
                }
            }
        });
    }

    public void setProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadAddress(Address address) {
        addressLayout.setVisibility(View.VISIBLE);
        txtViewState.setText(address.stateName);
        txtViewAddress.setText(address.address + ", " + address.landmark);
        txtViewCityPin.setText(address.cityName + " - " + address.cityName);
        txtViewName.setText(address.name);
        txtViewMobile.setText(address.mobile);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void loadWalletAmount(Double walletAmount) {
        walletPay = walletAmount;
        if (walletAmount >= 1) {
            txtViewWalletAmount.setText("₹ " + walletAmount);
            checkBoxWalletPay.setEnabled(true);
        } else {
            txtViewWalletAmount.setText("₹ 0.0");
            checkBoxWalletPay.setEnabled(false);
        }
    }

    @Override
    public void loadOrderPlaceData(OrderPlaceData orderPlaceData) {
        if (orderPlaceData != null && paymentMethod == 3) {
            Intent intent = new Intent(this, PaymentActivity.class);
            intent.putExtra("paytmOrderId", orderPlaceData.orderId);
            intent.putExtra("amount", orderPlaceData.amount);
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(this, OrderConfirmationActivity.class);
            intent.putExtra("isSuccess", true);
            intent.putExtra("isOnline", false);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.fetchAddressId(addressId);
    }

    @OnClick(R.id.txt_view_change_address) void onChangeAddressClicked() {
        Intent intent = new Intent(this, EditShiippingAddressActivity.class);
        intent.putExtra("addressId", addressId);
        startActivity(intent);
    }

    @OnClick(R.id.btn_place_order) void onPlaceOrdeClicked() {
        mPresenter.placeOrder(addressId, 3, walletPayMethod);
        showLoader();
    }

    public void enablePayButton() {
        btnPlaceOrder.setEnabled(true);
        btnPlaceOrder.setBackgroundColor(getResources().getColor(R.color.materialRed));
    }

    public void disablePayButton() {
        btnPlaceOrder.setEnabled(false);
        btnPlaceOrder.setBackgroundColor(getResources().getColor(R.color.md_red_100));
    }

    public void showPaymentOptionLayout() {
        txtViewPaymentOption.setVisibility(View.VISIBLE);
        layoutPaymentOption.setVisibility(View.VISIBLE);
    }

    public void hidePaymentOptionLayout() {
        txtViewPaymentOption.setVisibility(View.GONE);
        layoutPaymentOption.setVisibility(View.GONE);
    }

}
