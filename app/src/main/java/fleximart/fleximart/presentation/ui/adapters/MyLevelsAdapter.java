package fleximart.fleximart.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import fleximart.fleximart.R;
import fleximart.fleximart.domain.model.MyLevels;

public class MyLevelsAdapter extends RecyclerView.Adapter<MyLevelsAdapter.ViewHolder> {

    public interface Callback {
        void onViewDownlinesClicked(int id);
        void onSponsorsClicked(int id);
    }

    Context mContext;
    MyLevels[] myLevels;
    Callback mCallback;

    public MyLevelsAdapter(Context mContext, MyLevels[] myLevels, Callback mCallback) {
        this.mContext = mContext;
        this.myLevels = myLevels;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_levels, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewSlNo.setText(Integer.toString(position + 1));
        if (myLevels[position].isComplete) {
            holder.txtViewIsCompleted.setText("Yes");
            holder.txtViewSponsors.setVisibility(View.INVISIBLE);
        } else {
            holder.txtViewIsCompleted.setText("No");
            holder.txtViewSponsors.setVisibility(View.VISIBLE);
        }
        holder.txtViewJoined.setText(Integer.toString(myLevels[position].totalJoined));
        holder.txtViewLevelId.setText(Integer.toString(myLevels[position].level));
        holder.txtViewTotalLeft.setText(Integer.toString(myLevels[position].totalLeft));
        holder.txtViewTotalNode.setText(Integer.toString(myLevels[position].totalNode));
        holder.txtViewDownlines.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onViewDownlinesClicked(myLevels[position].level);
            }
        });
        holder.txtViewSponsors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onSponsorsClicked(myLevels[position].level);
            }
        });
    }

    @Override
    public int getItemCount() {
        return myLevels.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_sl_no)
        TextView txtViewSlNo;
        @BindView(R.id.txt_view_level_id)
        TextView txtViewLevelId;
        @BindView(R.id.txt_view_total_node)
        TextView txtViewTotalNode;
        @BindView(R.id.txt_view_is_completed)
        TextView txtViewIsCompleted;
        @BindView(R.id.txt_view_joined)
        TextView txtViewJoined;
        @BindView(R.id.txt_view_total_left)
        TextView txtViewTotalLeft;
        @BindView(R.id.txt_view_downlines)
        TextView txtViewDownlines;
        @BindView(R.id.txt_view_sponsors)
        TextView txtViewSponsors;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

}
