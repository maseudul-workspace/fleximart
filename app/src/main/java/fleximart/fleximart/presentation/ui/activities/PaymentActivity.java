package fleximart.fleximart.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import fleximart.fleximart.AndroidApplication;
import fleximart.fleximart.R;
import fleximart.fleximart.domain.executors.impl.ThreadExecutor;
import fleximart.fleximart.domain.model.UserInfo;
import fleximart.fleximart.presentation.presenters.PaymentPresenter;
import fleximart.fleximart.presentation.presenters.impl.PaymentPresenterImpl;
import fleximart.fleximart.threading.MainThreadImpl;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

public class PaymentActivity extends AppCompatActivity implements PaymentPresenter.View {

    PaytmPGService service;
    PaytmOrder order;
    AndroidApplication androidApplication;
    int paytmOrderId;
    String checksum;
    String merchantId = "KvAqBg36683835335486";
    HashMap<String, String> paramMap = new HashMap<String,String>();
    UserInfo userInfo;
    ProgressDialog progressDialog;
    String amount;
    PaymentPresenterImpl mPresenter;
    String txnStatus = "";
    String transactionId = "";
    String transactionDate = "";
    String transactionAmount = "";
    String orderid = "";
    boolean isWalletPay = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        isWalletPay = getIntent().getBooleanExtra("isWalletPay", false);
        paytmOrderId = getIntent().getIntExtra("paytmOrderId", paytmOrderId);
        amount = getTwoDecimalPoints(getIntent().getDoubleExtra("amount", 0));
        Log.e("LogMsg", "Double Amount: " + amount);
        initialisePresenter();
        mPresenter.generatePresenter(merchantId, paytmOrderId, "WAP", amount, "WEBSTAGING", "Retail", "https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID="+paytmOrderId);
        setUpProgressDialog();
        getUserInfo();
    }

    public void initialisePresenter() {
        mPresenter = new PaymentPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void getUserInfo() {
        androidApplication = (AndroidApplication) getApplicationContext();
        userInfo = androidApplication.getUserInfo(this);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    public void setOrder() {
        Log.e("LogMsg", "mobile" + userInfo.mobile);
        paramMap.put( "MID" , merchantId);
// Key in your staging and production MID available in your dashboard
        paramMap.put( "ORDER_ID" , Integer.toString(paytmOrderId));
        paramMap.put( "CUST_ID" , Integer.toString(userInfo.id));
        paramMap.put( "MOBILE_NO" , userInfo.mobile);
        paramMap.put( "EMAIL" , userInfo.email);
        paramMap.put( "CHANNEL_ID" , "WAP");
        paramMap.put( "TXN_AMOUNT" , amount);
        paramMap.put( "WEBSITE" , "WEBSTAGING");
// This is the staging value. Production value is available in your dashboard
        paramMap.put( "INDUSTRY_TYPE_ID" , "Retail");
// This is the staging value. Production value is available in your dashboard
        paramMap.put( "CALLBACK_URL", "https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID="+paytmOrderId);
        paramMap.put( "CHECKSUMHASH" , checksum);
        order = new PaytmOrder(paramMap);
    }

    public void startPayment() {
        service = PaytmPGService.getStagingService("");
        service.initialize(order, null);
        service.startPaymentTransaction(this, true, true, new PaytmPaymentTransactionCallback() {
            /*Call Backs*/
            public void someUIErrorOccurred(String inErrorMessage) {
                Log.e("LogMsg", "Error Message: " + inErrorMessage);
            }
            public void onTransactionResponse(Bundle inResponse) {
                txnStatus = inResponse.getString("STATUS");
                transactionId = inResponse.getString("TXNID");
                transactionDate = inResponse.getString("TXNDATE");
                transactionAmount = inResponse.getString("TXNAMOUNT");
                orderid = inResponse.getString("ORDERID");
                boolean isSuccess;
                if (txnStatus.equals("TXN_SUCCESS")) {
                   isSuccess = true;
                } else {
                    isSuccess = false;
                }
                if (isWalletPay) {
                    if (isSuccess) {
                        mPresenter.completeWalletPay(paytmOrderId, transactionId);
                        showLoader();
                    } else {
                        goToWalletAddSuccessActvity(false);
                    }
                } else {
                    goToOrderConfirmationActivity(isSuccess, transactionId, transactionDate, transactionAmount, orderid);
                }
            }
            public void networkNotAvailable() {
                Log.e("LogMsg", "Network Unavailable");
                if (isWalletPay) {
                    goToWalletAddSuccessActvity(false);
                } else {
                    goToOrderConfirmationActivity(false, "", "", "", "");
                }
            }
            public void clientAuthenticationFailed(String inErrorMessage) {
                Log.e("LogMsg", "Client Authentication Fail: " + inErrorMessage);
                if (isWalletPay) {
                    goToWalletAddSuccessActvity(false);
                } else {
                    goToOrderConfirmationActivity(false, "", "", "", "");
                }            }
            public void onErrorLoadingWebPage(int iniErrorCode, String inErrorMessage, String inFailingUrl) {
                Log.e("LogMsg", "Error Loading Page: " + inErrorMessage);
                Log.e("LogMsg", "In Failing Url: " + inFailingUrl);
                if (isWalletPay) {
                    goToWalletAddSuccessActvity(false);
                } else {
                    goToOrderConfirmationActivity(false, "", "", "", "");
                }            }
            public void onBackPressedCancelTransaction() {
                Log.e("LogMsg", "On Back Presssed");
                if (isWalletPay) {
                    goToWalletAddSuccessActvity(false);
                } else {
                    goToOrderConfirmationActivity(false, "", "", "", "");
                }            }
            public void onTransactionCancel(String inErrorMessage, Bundle inResponse) {
                Log.e("LogMsg", "Transaction Cancel ErrorMessage: " + inErrorMessage);
                Log.e("LogMsg", "Transaction Bundle: " + inResponse.toString());
                if (isWalletPay) {
                    goToWalletAddSuccessActvity(false);
                } else {
                    goToOrderConfirmationActivity(false, "", "", "", "");
                }            }
        });
    }

    @Override
    public void initiatePayment(String checkSum) {
        this.checksum = checkSum;
        setOrder();
        startPayment();
    }

    public void goToOrderConfirmationActivity(boolean isSuccess, String transactionId, String transactionDate, String transactionAmount, String orderId) {
        Intent intent = new Intent(this, OrderConfirmationActivity.class);
        intent.putExtra("isSuccess", isSuccess);
        intent.putExtra("transactionId", transactionId);
        intent.putExtra("transactionDate", transactionDate);
        intent.putExtra("transactionAmount", transactionAmount);
        intent.putExtra("orderId", orderId);
        intent.putExtra("isOnline", true);
        startActivity(intent);
        finish();
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void goToWalletAddSuccessActvity(boolean isSuccess) {
        Intent intent = new Intent(this, WalletMoneyAddConfirmationActivity.class);
        intent.putExtra("isSuccess", isSuccess);
        intent.putExtra("transactionId", transactionId);
        intent.putExtra("transactionDate", transactionDate);
        intent.putExtra("transactionAmount", transactionAmount);
        intent.putExtra("orderId", Integer.toString(paytmOrderId));
        startActivity(intent);
        finish();
    }

    public String getTwoDecimalPoints(double amount) {
        DecimalFormat df2 = new DecimalFormat("#.##");
        return df2.format(amount);
    }

}
