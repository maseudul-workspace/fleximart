package fleximart.fleximart.presentation.ui.activities;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import fleximart.fleximart.AndroidApplication;
import fleximart.fleximart.R;
import fleximart.fleximart.domain.model.UserInfo;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.android.material.navigation.NavigationView;

public class MembersBaseActivity extends AppCompatActivity {

    @BindView(R.id.toolbar) @Nullable
    Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle actionBarDrawerToggle;
    @BindView(R.id.main_navigation_view)
    NavigationView navigationView;
    TextView txtViewHeaderStatus;
    AndroidApplication androidApplication;
    View headerMainLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_members_base);
    }

    protected void inflateContent(@LayoutRes int inflateResID){
        FrameLayout contentFramelayout = (FrameLayout) findViewById(R.id.content_frame);
        getLayoutInflater().inflate(inflateResID, contentFramelayout);
        ButterKnife.bind(this);
        setToolbar();
        setHeaderText();
    }

    public void setToolbar() {
        setSupportActionBar(toolbar);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.app_name, R.string.app_name);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        setUpNavigationView();
    }

    public void setHeaderText() {
        View headerLayout = navigationView.getHeaderView(0);
        txtViewHeaderStatus = (TextView) headerLayout.findViewById(R.id.txt_view_header_status);
        headerMainLayout = (View) headerLayout.findViewById(R.id.header_main_layout);
        androidApplication = (AndroidApplication) getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(this);
        Menu navMenu = navigationView.getMenu();
        if (user == null) {
            txtViewHeaderStatus.setText("Log In  .  Sign Up");
        } else {
            txtViewHeaderStatus.setText(user.name);
        }
        headerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (user == null) {
                    goToLogin();
                } else {
                    goToUserActivity();
                }
            }
        });
    }

    public void setUpNavigationView() {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch(id)
                {
                    case R.id.nav_new_registration:
                        goToAddMemberActivity();
                        break;
                    case R.id.nav_nodes:
                        goToMyNodes();
                        break;
                    case R.id.nav_commission:
                        goToMyCommision();
                        break;
                    case R.id.nav_shopping:
                        goToShopping();
                        break;
                }
                return false;
            }
        });
    }

    public void goToLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    public void goToUserActivity() {
        Intent intent = new Intent(this, UserActivity.class);
        startActivity(intent);
    }

    public void goToMyCommision() {
        Intent intent = new Intent(this, MyCommisionActivity.class);
        startActivity(intent);
    }

    public void goToMyNodes() {
        Intent intent = new Intent(this, MyNodesActivity.class);
        startActivity(intent);
    }

    public void goToShopping() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void goToAddMemberActivity() {
        Intent intent = new Intent(this, CheckNodeActivity.class);
        intent.putExtra("member_status", false);
        startActivity(intent);
    }

}
