package fleximart.fleximart.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import es.dmoral.toasty.Toasty;
import fleximart.fleximart.R;
import fleximart.fleximart.presentation.ui.adapters.BrandsAdapter;
import fleximart.fleximart.presentation.ui.adapters.CategoryFilterAdapter;
import fleximart.fleximart.presentation.ui.adapters.ColorsAdapter;
import fleximart.fleximart.presentation.ui.adapters.SizesAdapter;

public class FilterByDialog {

    public interface Callback {
        void onFilterApplied(long fromPrice, long toPrice);
    }

    Context mContext;
    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    RecyclerView recyclerViewCategories;
    RecyclerView recyclerViewColors;
    RecyclerView recyclerViewBrands;
    RecyclerView recyclerViewSizes;
    ImageView imageViewArrowUpCategory;
    ImageView imageViewArrowDownCategory;
    ImageView imageViewArrowUpColors;
    ImageView imageViewArrowDownColors;
    ImageView imageViewArrowUpBrands;
    ImageView imageViewArrowDownBrands;
    ImageView imageViewArrowUpSizes;
    ImageView imageViewArrowDownSizes;
    ImageView imageViewCancel;
    View layoutCategory;
    View layoutColor;
    View layoutBrands;
    View layoutSize;
    EditText editTextPriceFrom;
    EditText editTextPriceTo;
    long minPrice;
    long maxPrice;
    Button btnApply;
    Callback mCallback;

    public FilterByDialog(Context mContext, Activity mActivity, Callback callback) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        mCallback = callback;
    }

    public void setUpDialog() {
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.layout_filter_by_dialog, null);
        setUpViews();
        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    public void setUpViews() {

        recyclerViewCategories = (RecyclerView) dialogContainer.findViewById(R.id.recycler_view_second_category);
        recyclerViewColors = (RecyclerView) dialogContainer.findViewById(R.id.recycler_view_color);
        recyclerViewBrands = (RecyclerView) dialogContainer.findViewById(R.id.recycler_view_brands);
        recyclerViewSizes = (RecyclerView) dialogContainer.findViewById(R.id.recycler_view_size);

        imageViewArrowDownBrands = (ImageView) dialogContainer.findViewById(R.id.img_view_arrow_down_brand);
        imageViewArrowDownCategory = (ImageView) dialogContainer.findViewById(R.id.img_view_arrow_down_categories);
        imageViewArrowDownColors = (ImageView) dialogContainer.findViewById(R.id.img_view_arrow_down_color);
        imageViewArrowDownSizes = (ImageView) dialogContainer.findViewById(R.id.img_view_arrow_down_size);

        imageViewArrowUpBrands = (ImageView) dialogContainer.findViewById(R.id.img_view_arrow_up_brand);
        imageViewArrowUpCategory = (ImageView) dialogContainer.findViewById(R.id.img_view_arrow_up_categories);
        imageViewArrowUpColors = (ImageView) dialogContainer.findViewById(R.id.img_view_arrow_up_color);
        imageViewArrowUpSizes = (ImageView) dialogContainer.findViewById(R.id.img_view_arrow_up_size);
        imageViewCancel = (ImageView) dialogContainer.findViewById(R.id.img_view_cancel);

        layoutBrands = (View) dialogContainer.findViewById(R.id.layout_brand);
        layoutCategory = (View) dialogContainer.findViewById(R.id.layout_category);
        layoutColor = (View) dialogContainer.findViewById(R.id.layout_color);
        layoutSize = (View) dialogContainer.findViewById(R.id.layout_size);

        editTextPriceFrom = (EditText) dialogContainer.findViewById(R.id.edit_text_price_form);
        editTextPriceTo = (EditText) dialogContainer.findViewById(R.id.edit_text_price_to);

        btnApply = (Button) dialogContainer.findViewById(R.id.btn_apply);

        layoutBrands.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (recyclerViewBrands.getVisibility() == View.VISIBLE) {
                    recyclerViewBrands.setVisibility(View.GONE);
                    imageViewArrowDownBrands.setVisibility(View.VISIBLE);
                    imageViewArrowUpBrands.setVisibility(View.GONE);
                } else {
                    recyclerViewBrands.setVisibility(View.VISIBLE);
                    imageViewArrowDownBrands.setVisibility(View.GONE);
                    imageViewArrowUpBrands.setVisibility(View.VISIBLE);
                }
            }
        });

        layoutCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (recyclerViewCategories.getVisibility() == View.VISIBLE) {
                    recyclerViewCategories.setVisibility(View.GONE);
                    imageViewArrowDownCategory.setVisibility(View.VISIBLE);
                    imageViewArrowUpCategory.setVisibility(View.GONE);
                } else {
                    recyclerViewCategories.setVisibility(View.VISIBLE);
                    imageViewArrowDownCategory.setVisibility(View.GONE);
                    imageViewArrowUpCategory.setVisibility(View.VISIBLE);
                }
            }
        });

        layoutSize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (recyclerViewSizes.getVisibility() == View.VISIBLE) {
                    recyclerViewSizes.setVisibility(View.GONE);
                    imageViewArrowDownSizes.setVisibility(View.VISIBLE);
                    imageViewArrowUpSizes.setVisibility(View.GONE);
                } else {
                    recyclerViewSizes.setVisibility(View.VISIBLE);
                    imageViewArrowDownSizes.setVisibility(View.GONE);
                    imageViewArrowUpSizes.setVisibility(View.VISIBLE);
                }
            }
        });

        layoutColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (recyclerViewColors.getVisibility() == View.VISIBLE) {
                    recyclerViewColors.setVisibility(View.GONE);
                    imageViewArrowDownColors.setVisibility(View.VISIBLE);
                    imageViewArrowUpColors.setVisibility(View.GONE);
                } else {
                    recyclerViewColors.setVisibility(View.VISIBLE);
                    imageViewArrowDownColors.setVisibility(View.GONE);
                    imageViewArrowUpColors.setVisibility(View.VISIBLE);
                }
            }
        });

        imageViewCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editTextPriceFrom.getText().toString().trim().isEmpty() || editTextPriceTo.getText().toString().trim().isEmpty()){
                    minPrice = 0;
                    maxPrice = 0;
                }else{
                    minPrice = Integer.parseInt(editTextPriceFrom.getText().toString());
                    maxPrice = Integer.parseInt(editTextPriceTo.getText().toString());
                }
                if(minPrice > maxPrice){
                    Toasty.warning(mContext, "Minimum price should be less", Toast.LENGTH_SHORT, true).show();
                }else{
                    dialog.dismiss();
                    mCallback.onFilterApplied(minPrice, maxPrice);
                }
            }
        });

    }

    public void setUpAdapters(CategoryFilterAdapter categoryFilterAdapter, ColorsAdapter colorsAdapter, SizesAdapter sizesAdapter, BrandsAdapter brandsAdapter) {
        recyclerViewCategories.setAdapter(categoryFilterAdapter);
        recyclerViewCategories.setLayoutManager(new GridLayoutManager(mContext, 3));
        recyclerViewCategories.setNestedScrollingEnabled(false);

        recyclerViewColors.setAdapter(colorsAdapter);
        recyclerViewColors.setLayoutManager(new GridLayoutManager(mContext, 5));
        recyclerViewColors.setNestedScrollingEnabled(false);

        recyclerViewSizes.setAdapter(sizesAdapter);
        recyclerViewSizes.setLayoutManager(new GridLayoutManager(mContext, 5));
        recyclerViewSizes.setNestedScrollingEnabled(false);

        recyclerViewBrands.setAdapter(brandsAdapter);
        recyclerViewBrands.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerViewBrands.setNestedScrollingEnabled(false);

    }

    public void showDialog() {
        dialog.show();
    }


}
