package fleximart.fleximart.presentation.ui.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Paint;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import fleximart.fleximart.R;
import fleximart.fleximart.domain.model.CartDetails;
import fleximart.fleximart.domain.model.ProductDetailsSize;
import fleximart.fleximart.presentation.ui.dialogs.QuantityDialog;
import fleximart.fleximart.presentation.ui.dialogs.SizesCartDialog;
import fleximart.fleximart.presentation.ui.dialogs.SizesDialog;
import fleximart.fleximart.util.GlideHelper;

public class CartDetailsAdapter extends RecyclerView.Adapter<CartDetailsAdapter.ViewHolder> implements SizesCartDialog.Callback, QuantityDialog.Callback {


    @Override
    public void onSizeSelected(int cartId, int sizeId, int quantity) {
        mCallback.updateCart(cartId, sizeId, quantity);
    }

    @Override
    public void onQuantitySelected(int cartId, int sizeId, int quantity) {
        mCallback.updateCart(cartId, sizeId, quantity);
    }

    public interface Callback {
        void updateCart(int cartId, int sizeId, int quantity);
        void deleteFromCart(int cartId);
    }

    Context mContext;
    CartDetails[] cartDetails;
    Callback mCallback;
    Activity mActivity;

    public CartDetailsAdapter(Context mContext, Activity activity, CartDetails[] cartDetails, Callback mCallback) {
        this.mContext = mContext;
        this.cartDetails = cartDetails;
        this.mCallback = mCallback;
        mActivity = activity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_cart, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewProductName.setText(cartDetails[position].productTagName);
        holder.txtViewSellingPrice.setText("₹ " + cartDetails[position].productPrice);
        holder.txtViewQty.setText(Integer.toString(cartDetails[position].quantity));
        holder.txtViewSize.setText(cartDetails[position].sizeName);
        GlideHelper.setImageView(mContext, holder.imgViewProductPoster, mContext.getResources().getString(R.string.base_url) + "/images/product/" + cartDetails[position].productImage);
        holder.txtViewMRP.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        holder.txtViewMRP.setText("₹ " + cartDetails[position].product_mrp);

//        Set up size dialog
        SizesCartDialog sizesCartDialog = new SizesCartDialog(mContext, mActivity, this::onSizeSelected, cartDetails[position].quantity, cartDetails[position].cartId);
        sizesCartDialog.setUpDialogView();
        sizesCartDialog.setRecyclerView(cartDetails[position].sizes);

        holder.txtViewSize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sizesCartDialog.showDialog();
            }
        });

//        Setting up limit for quantity adapter
        ProductDetailsSize[] sizes = cartDetails[position].sizes;
        int limit = 1;
        for (int i = 0; i < sizes.length; i++) {
            if (sizes[i].sizeId == cartDetails[position].sizeId) {
                limit = sizes[i].stock;
                if (sizes[i].stock == 0) {
                    holder.txtViewErrorMsg.setText("Out of Stock");
                } else if (cartDetails[position].quantity > limit) {
                    holder.txtViewErrorMsg.setText("Only " + limit + " left");
                }
                break;
            }
        }

//        Set up quantity dialog
        QuantityDialog quantityDialog = new QuantityDialog(mContext, mActivity, cartDetails[position].sizeId, cartDetails[position].cartId, this::onQuantitySelected);
        quantityDialog.setUpDialogView();
        quantityDialog.setRecyclerView(limit);

        holder.txtViewQty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                quantityDialog.showDialog();
            }
        });

        holder.imgViewSizeArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sizesCartDialog.showDialog();
            }
        });

        holder.imgViewQtyArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                quantityDialog.showDialog();
            }
        });

        holder.layoutRemoveCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("Are You Sure ?");
                builder.setMessage("You are about to delete an item from your cart. Do you really want to proceed ?");
                builder.setCancelable(false);
                builder.setPositiveButton(Html.fromHtml("<font color='#f4454c'>Yes</font>"), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mCallback.deleteFromCart(cartDetails[position].cartId);
                    }
                });

                builder.setNegativeButton(Html.fromHtml("<font color='#f4454c'>No</font>"), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                builder.show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return cartDetails.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_product_poster)
        ImageView imgViewProductPoster;
        @BindView(R.id.txt_view_product_name)
        TextView txtViewProductName;
        @BindView(R.id.txt_view_selling_price)
        TextView txtViewSellingPrice;
        @BindView(R.id.txt_view_mrp)
        TextView txtViewMRP;
        @BindView(R.id.txt_view_qty)
        TextView txtViewQty;
        @BindView(R.id.txt_view_size)
        TextView txtViewSize;
        @BindView(R.id.img_view_qty_arrow)
        ImageView imgViewQtyArrow;
        @BindView(R.id.img_view_size_arrow)
        ImageView imgViewSizeArrow;
        @BindView(R.id.txt_view_error_msg)
        TextView txtViewErrorMsg;
        @BindView(R.id.layout_remove_cart)
        View layoutRemoveCart;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
