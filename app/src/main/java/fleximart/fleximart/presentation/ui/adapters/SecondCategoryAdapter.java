package fleximart.fleximart.presentation.ui.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import fleximart.fleximart.R;
import fleximart.fleximart.domain.model.SecondCategory;
import fleximart.fleximart.util.GlideHelper;

public class SecondCategoryAdapter extends RecyclerView.Adapter<SecondCategoryAdapter.ViewHolder> {

    public interface Callback {
        void onCategoryClicked(int id);
    }

    Context mContext;
    SecondCategory[] secondCategories;
    Callback mCallback;

    public SecondCategoryAdapter(Context mContext, SecondCategory[] secondCategories, Callback mCallback) {
        this.mContext = mContext;
        this.secondCategories = secondCategories;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_second_category, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewSecondCategory.setText(secondCategories[position].name);
        GlideHelper.setImageViewCustomRoundedCorners(mContext, holder.imgViewSecondCategory, mContext.getResources().getString(R.string.base_url) + "images/category/second_category/" + secondCategories[position].image, 100);
        holder.imgViewSecondCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("LogMsg", "Cliked1");
                mCallback.onCategoryClicked(secondCategories[position].id);
            }
        });
    }

    @Override
    public int getItemCount() {
        return secondCategories.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_second_category)
        ImageView imgViewSecondCategory;
        @BindView(R.id.txt_view_second_category)
        TextView txtViewSecondCategory;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}
