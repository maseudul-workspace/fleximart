package fleximart.fleximart.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fleximart.fleximart.R;
import fleximart.fleximart.presentation.ui.dialogs.PaymentConfirmationDialog;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class OrderConfirmationActivity extends AppCompatActivity {

    @BindView(R.id.txt_view_first_heading)
    TextView txtViewFirstHeading;
    @BindView(R.id.txt_view_second_heading)
    TextView txtViewSecondHeading;
    boolean isSuccess;
    boolean isOnline;
    String orderId = "";
    String transactionId = "";
    String amount = "";
    String orderDate = "";
    PaymentConfirmationDialog paymentConfirmationDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_confirmation);
        isSuccess = getIntent().getBooleanExtra("isSuccess", false);
        isOnline = getIntent().getBooleanExtra("isOnline", true);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("CONFIRMATION");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (isOnline) {
            orderId = getIntent().getStringExtra("orderId");
            transactionId = getIntent().getStringExtra("transactionId");
            amount = getIntent().getStringExtra("transactionAmount");
            orderDate = getIntent().getStringExtra("transactionDate");
            initialiseDialog();
            paymentConfirmationDialog.setData(isSuccess, orderDate, amount,orderId, transactionId);
            paymentConfirmationDialog.showDialog();
        }
        if (!isSuccess) {
            txtViewFirstHeading.setText("Failed !!!");
            txtViewSecondHeading.setText("Your order cannot be placed");
        }
    }

    public void initialiseDialog() {
        paymentConfirmationDialog = new PaymentConfirmationDialog(this, this);
        paymentConfirmationDialog.setUpDialog();
    }

    @OnClick(R.id.btn_continue_shopping) void onContinueShoppingClicked() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

}
