package fleximart.fleximart.presentation.ui.adapters;

import android.content.Context;
import android.graphics.Paint;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import fleximart.fleximart.R;
import fleximart.fleximart.domain.model.ProductDetails;
import fleximart.fleximart.domain.model.Testing.ProductList;
import fleximart.fleximart.util.GlideHelper;

/**
 * Created by Raj on 18-08-2019.
 */

public class ProductListVerticalAdapter extends RecyclerView.Adapter<ProductListVerticalAdapter.ViewHolder> {

    public interface Callback {
        void onProductClicked(int produuctId);
        void onAddToWishlistClicked(int productId, int position);
        void onRemoveFromWishlistClicked(int productId, int position);
    }

    Context mContext;
    ProductDetails[] productDetails;
    Callback mCallback;

    public ProductListVerticalAdapter(Context mContext, ProductDetails[] productDetails, Callback callback) {
        this.mContext = mContext;
        this.productDetails = productDetails;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_product_list_vertical, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.txtViewProductName.setText(productDetails[i].name);
        viewHolder.txtViewProductSellingPrice.setText("Rs. " + productDetails[i].price);
        GlideHelper.setImageView(mContext, viewHolder.imgViewPoster, mContext.getResources().getString(R.string.base_url) + "/images/product/" + productDetails[i].mainImage);
        viewHolder.txtViewProductPrice.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        viewHolder.txtViewProductPrice.setText("Rs. " + productDetails[i].mrp);
        viewHolder.txtViewTag.setText(productDetails[i].tagName);
        viewHolder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onProductClicked(productDetails[i].id);
            }
        });
        if (productDetails[i].isWishlistPresent) {
            viewHolder.imgViewHeartRed.setVisibility(View.VISIBLE);
            viewHolder.imgViewHeartGrey.setVisibility(View.GONE);
        } else {
            viewHolder.imgViewHeartRed.setVisibility(View.GONE);
            viewHolder.imgViewHeartGrey.setVisibility(View.VISIBLE);
        }
        viewHolder.imgViewHeartGrey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onAddToWishlistClicked(productDetails[i].id, i);
            }
        });
        viewHolder.imgViewHeartRed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onRemoveFromWishlistClicked(productDetails[i].id, i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return productDetails.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_product_poster)
        ImageView imgViewPoster;
        @BindView(R.id.txt_view_product_name)
        TextView txtViewProductName;
        @BindView(R.id.txt_view_product_price)
        TextView txtViewProductPrice;
        @BindView(R.id.txt_view_product_selling_price)
        TextView txtViewProductSellingPrice;
        @BindView(R.id.txt_view_tag)
        TextView txtViewTag;
        @BindView(R.id.main_layout)
        View mainLayout;
        @BindView(R.id.img_heart_grey)
        ImageView imgViewHeartGrey;
        @BindView(R.id.img_heart_red)
        ImageView imgViewHeartRed;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateDataSet(ProductDetails[] productDetails) {
        this.productDetails = productDetails;
        notifyDataSetChanged();
    }

    public void onAddToWishlistSuccess(int position) {
        this.productDetails[position].isWishlistPresent = true;
        notifyItemChanged(position);
    }

    public void onRemoveFromWishlistFail(int position) {
        this.productDetails[position].isWishlistPresent = false;
        notifyItemChanged(position);
    }

}
