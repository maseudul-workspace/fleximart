package fleximart.fleximart.presentation.ui.activities;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fleximart.fleximart.R;
import fleximart.fleximart.domain.executors.impl.ThreadExecutor;
import fleximart.fleximart.domain.model.ProductDetails;
import fleximart.fleximart.domain.model.Testing.ProductList;
import fleximart.fleximart.presentation.presenters.ProductsListPresenter;
import fleximart.fleximart.presentation.presenters.impl.ProductListPresenterImpl;
import fleximart.fleximart.presentation.ui.adapters.BrandsAdapter;
import fleximart.fleximart.presentation.ui.adapters.CategoryFilterAdapter;
import fleximart.fleximart.presentation.ui.adapters.ColorsAdapter;
import fleximart.fleximart.presentation.ui.adapters.ProductListVerticalAdapter;
import fleximart.fleximart.presentation.ui.adapters.SizesAdapter;
import fleximart.fleximart.presentation.ui.bottomsheets.LoginBottomSheet;
import fleximart.fleximart.presentation.ui.dialogs.FilterByDialog;
import fleximart.fleximart.presentation.ui.dialogs.SortByDialog;
import fleximart.fleximart.threading.MainThreadImpl;

public class ProductListActivity extends AppCompatActivity implements SortByDialog.Callback, ProductsListPresenter.View, FilterByDialog.Callback {

    @BindView(R.id.recycler_view_product_list_vertical)
    RecyclerView recyclerViewProductList;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    SortByDialog sortByDialog;
    FilterByDialog filterByDialog;
    ProductListPresenterImpl mPresenter;
    int pageNo = 1;
    int secondCategoryId;
    @BindView(R.id.layout_filter)
    View layoutFilter;
    @BindView(R.id.layout_progress)
    View layoutProgress;
    int totalPage;
    int sortBy = 1;
    long fromPrice = 0;
    long toPrice = 0;
    LinearLayoutManager layoutManager;
    Boolean isScrolling = false;
    Integer currentItems;
    Integer totalItems;
    Integer scrollOutItems;
    @BindView(R.id.layout_message)
    View layoutMessage;
    @BindView(R.id.txt_view_error_msg)
    TextView txtViewErrorMsg;
    @BindView(R.id.img_view_no_item_found)
    ImageView imgViewNoItemFound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setUpSortByDialog();
        setFilterByDialog();
        initialisePresenter();
        secondCategoryId = getIntent().getIntExtra("secondCategoryId", 0);
        mPresenter.fetchProductListWithFilter(secondCategoryId, pageNo);
    }

    public void initialisePresenter() {
        mPresenter = new ProductListPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setUpSortByDialog() {
        sortByDialog = new SortByDialog(this, this, this);
        sortByDialog.setUpDialogView();
    }

    public void setFilterByDialog() {
        filterByDialog = new FilterByDialog(this, this, this::onFilterApplied);
        filterByDialog.setUpDialog();
    }

    @OnClick(R.id.layout_sort_by) void onSortByLayoutClicked() {
        sortByDialog.showDialog();
    }

    @OnClick(R.id.layout_filter_by) void onFilterByClicked() {
        filterByDialog.showDialog();
    }

    @Override
    public void onSortApplyBtnClicked(int sort) {
        sortBy = sort;
        isScrolling = false;
        pageNo = 1;
        recyclerViewProductList.setVisibility(View.GONE);
        layoutFilter.setVisibility(View.GONE);
        showLoader();
        mPresenter.fetchProductListWithoutFilter(secondCategoryId, pageNo, fromPrice, toPrice, sortBy, "refresh");
    }

    @Override
    public void loadProductAdapters(ProductListVerticalAdapter productListVerticalAdapter, int totalPage) {
        this.totalPage = totalPage;
        layoutManager = new GridLayoutManager(this, 2);
        recyclerViewProductList.setVisibility(View.VISIBLE);
        layoutFilter.setVisibility(View.VISIBLE);
        recyclerViewProductList.setLayoutManager(layoutManager);
        recyclerViewProductList.setAdapter(productListVerticalAdapter);
        recyclerViewProductList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                {
                    isScrolling= true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems  = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstCompletelyVisibleItemPosition();
                if(!recyclerView.canScrollVertically(1))
                {
                    if(pageNo < totalPage) {
                        isScrolling = false;
                        pageNo = pageNo + 1;
                        mPresenter.fetchProductListWithoutFilter(secondCategoryId, pageNo, fromPrice, toPrice, sortBy, "");
                    }
                }
            }
        });
    }

    @Override
    public void loadOthersAdapters(CategoryFilterAdapter categoryFilterAdapter, ColorsAdapter colorsAdapter, BrandsAdapter brandsAdapter, SizesAdapter sizesAdapter) {
        filterByDialog.setUpAdapters(categoryFilterAdapter, colorsAdapter, sizesAdapter, brandsAdapter);
    }

    @Override
    public void showLoader() {
        layoutProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoader() {
        layoutProgress.setVisibility(View.GONE);
    }

    @Override
    public void goToProductDetails(int productId) {
        Intent intent = new Intent(this, ProductDetailsActivity.class);
        intent.putExtra("productId", productId);
        startActivity(intent);
    }

    @Override
    public void showErrorMessage(String errorMsg) {
        layoutMessage.setVisibility(View.VISIBLE);
        txtViewErrorMsg.setText(errorMsg);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                layoutMessage.setVisibility(View.GONE);
            }
        }, 3000);
    }

    @Override
    public void onNoProductsFound() {
        imgViewNoItemFound.setVisibility(View.VISIBLE);
    }

    @Override
    public void showBottomSheet() {
        LoginBottomSheet loginBottomSheet = new LoginBottomSheet();
        loginBottomSheet.show(getSupportFragmentManager(), "");
    }

    @Override
    public void onFilterApplied(long fromPrice, long toPrice) {
       this.fromPrice = fromPrice;
       this.toPrice = toPrice;
       isScrolling = false;
       pageNo = 1;
       recyclerViewProductList.setVisibility(View.GONE);
       layoutFilter.setVisibility(View.GONE);
       showLoader();
       mPresenter.fetchProductListWithoutFilter(secondCategoryId, pageNo, fromPrice, toPrice, sortBy, "refresh");
    }
}
