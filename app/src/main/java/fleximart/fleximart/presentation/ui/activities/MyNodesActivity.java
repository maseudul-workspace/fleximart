package fleximart.fleximart.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fleximart.fleximart.R;
import fleximart.fleximart.domain.executors.impl.ThreadExecutor;
import fleximart.fleximart.presentation.presenters.MyNodesPresenter;
import fleximart.fleximart.presentation.presenters.impl.MyNodesPresenterImpl;
import fleximart.fleximart.presentation.ui.adapters.NodeViewpagerAdapter;
import fleximart.fleximart.threading.MainThreadImpl;
import fleximart.fleximart.util.GlideHelper;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class MyNodesActivity extends AppCompatActivity implements MyNodesPresenter.View {

    @BindView(R.id.node_viewpager)
    ViewPager nodeViewpager;
    @BindView(R.id.dots_indicator_layout)
    LinearLayout dotsIndicatorLayout;
    ProgressDialog progressDialog;
    MyNodesPresenterImpl mPresenter;
    @BindView(R.id.img_view_no_record_found)
    ImageView imgViewNoRecordFound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_nodes);
//        getSupportActionBar().setTitle("My Nodes");
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        initialisePresenter();
        setUpProgressDialog();
        mPresenter.fetchMyNodes();
        showLoader();
    }

    public void initialisePresenter() {
        mPresenter = new MyNodesPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void loadAdapter(NodeViewpagerAdapter myNodesAdapter, int nodeLength) {
        nodeViewpager.setAdapter(myNodesAdapter);
        prepareDotsIndicator(0, nodeLength);
        nodeViewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                myNodesAdapter.notifyDataSetChanged();
                prepareDotsIndicator(position, nodeLength);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void goToMyDownlines(int nodeId) {
        Intent intent = new Intent(this, MyDownlinesActivity.class);
        intent.putExtra("nodeId", nodeId);
        startActivity(intent);
    }

    @Override
    public void goToMyTree(int nodeId) {
        Intent intent = new Intent(this, MyTreeActivity.class);
        intent.putExtra("nodeId", nodeId);
        intent.putExtra("payNodeId", nodeId);
        intent.putExtra("rank", 0);
        startActivity(intent);
    }

    @Override
    public void goToMyLevels(int nodeId) {
        Intent intent = new Intent(this, MyLevelsActivity.class);
        intent.putExtra("nodeId", nodeId);
        startActivity(intent);
    }

    @Override
    public void onError(String errorMsg) {
        imgViewNoRecordFound.setVisibility(View.VISIBLE);
    }

    public void prepareDotsIndicator(int sliderPosition, int imageCount){
        if(dotsIndicatorLayout.getChildCount() > 0){
            dotsIndicatorLayout.removeAllViews();
        }

        ImageView dots[] = new ImageView[imageCount];
        for(int i = 0; i < imageCount; i++){
            dots[i] = new ImageView(this);
            if(i == sliderPosition){
                GlideHelper.setImageViewWithDrawable(this, dots[i], R.drawable.active_dot);
            }else{
                GlideHelper.setImageViewWithDrawable(this, dots[i], R.drawable.inactive_dot);
            }

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(15, 15);
            layoutParams.setMargins(5, 0, 5, 0);
            dotsIndicatorLayout.addView(dots[i], layoutParams);

        }
    }

    @OnClick(R.id.layout_buy_node) void onBuyNodeClicked() {
        Intent intent = new Intent(this, CheckNodeActivity.class);
        intent.putExtra("member_status", true);
        startActivity(intent);
    }

}
