package fleximart.fleximart.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.EditText;

import com.google.android.material.textfield.TextInputLayout;

import androidx.appcompat.app.AlertDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fleximart.fleximart.R;

public class PhoneCheckDialog {

    public interface Callback {
        void onSearchClicked(String phoneNumber);
    }

    Context mContext;
    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    Callback mCallback;
    @BindView(R.id.edit_text_phone)
    EditText editTextPhone;
    @BindView(R.id.txt_input_phone_layout)
    TextInputLayout txtInputPhoneLayout;

    public PhoneCheckDialog(Context mContext, Activity mActivity, Callback mCallback) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        this.mCallback = mCallback;
    }

    public void setUpDialog() {
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.layout_check_phone_dialog, null);
        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Light_Dialog_NoActionBar_MinWidth);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        ButterKnife.bind(this, dialogContainer);
    }

    public void showDialog() {
        dialog.show();
    }

    public void hideDialog() {
        dialog.dismiss();
    }

    @OnClick(R.id.btn_search) void onSearchClicked() {
        if (editTextPhone.getText().toString().isEmpty()) {
            txtInputPhoneLayout.setError("Phone number required");
        } else if (editTextPhone.getText().toString().length() < 10) {
            txtInputPhoneLayout.setError("Phone number must be of 10 characters");
        } else {
            txtInputPhoneLayout.setError("");
            mCallback.onSearchClicked(editTextPhone.getText().toString());
            hideDialog();
        }
    }

}
