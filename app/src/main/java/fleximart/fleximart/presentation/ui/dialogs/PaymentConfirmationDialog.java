package fleximart.fleximart.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fleximart.fleximart.R;

public class PaymentConfirmationDialog {

    Context mContext;
    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    @BindView(R.id.img_view_success)
    ImageView imgViewSuccess;
    @BindView(R.id.img_view_failure)
    ImageView imgViewFailure;
    @BindView(R.id.txt_view_first_heading)
    TextView txtViewFirstHeading;
    @BindView(R.id.txt_view_second_heading)
    TextView txtViewSecondHeading;
    @BindView(R.id.txt_view_order_date)
    TextView txtViewOrderDate;
    @BindView(R.id.txt_view_amount)
    TextView txtViewAmount;
    @BindView(R.id.txt_view_order_id)
    TextView txtViewOrderId;
    @BindView(R.id.txt_view_transaction_id)
    TextView  txtViewTransactionId;

    public PaymentConfirmationDialog(Context mContext, Activity mActivity) {
        this.mContext = mContext;
        this.mActivity = mActivity;
    }

    public void setUpDialog() {
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.layout_payment_confirmation, null);
        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Light_Dialog_NoActionBar);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        ButterKnife.bind(this, dialogContainer);
    }

    public void setData(boolean paymentStatus, String date, String amount, String orderId, String transactionId) {
        if (!paymentStatus) {
            txtViewFirstHeading.setText("Payment Failed");
            txtViewSecondHeading.setText("Oops!! Something Went Wrong. Your Transaction Cannot Be Completed");
            imgViewFailure.setVisibility(View.VISIBLE);
        } else {
            imgViewSuccess.setVisibility(View.VISIBLE);
        }
        txtViewOrderDate.setText(date);
        txtViewAmount.setText("₹ " + amount);
        txtViewTransactionId.setText(transactionId);
        txtViewOrderId.setText(orderId);
    }

    public void showDialog() {
        dialog.show();
    }

    @OnClick(R.id.img_view_cancel_dialog) void onCancelDialogClicked() {
        dialog.dismiss();
    }

}
