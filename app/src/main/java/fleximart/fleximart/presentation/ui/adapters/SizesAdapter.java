package fleximart.fleximart.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import fleximart.fleximart.R;
import fleximart.fleximart.domain.model.Sizes;

public class SizesAdapter extends RecyclerView.Adapter<SizesAdapter.ViewHolder> {

    public interface Callback {
        void onSizeChecked(int id);
        void onSizeUnchecked(int id);
    }

    Context mContext;
    Sizes[] sizes;
    Callback mCallback;

    public SizesAdapter(Context mContext, Sizes[] sizes, Callback mCallback) {
        this.mContext = mContext;
        this.sizes = sizes;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_size_filter, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewSize.setText(sizes[position].name);
        holder.layoutSizeNotSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.layoutSizeSelected.setVisibility(View.VISIBLE);
                holder.layoutSizeNotSelected.setVisibility(View.GONE);
                mCallback.onSizeChecked(sizes[position].id);
            }
        });
        holder.layoutSizeSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.layoutSizeSelected.setVisibility(View.GONE);
                holder.layoutSizeNotSelected.setVisibility(View.VISIBLE);
                mCallback.onSizeUnchecked(sizes[position].id);
            }
        });
    }

    @Override
    public int getItemCount() {
        return sizes.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_size)
        TextView txtViewSize;
        @BindView(R.id.layout_size_selected)
        View layoutSizeSelected;
        @BindView(R.id.layout_size_not_selected)
        View layoutSizeNotSelected;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
