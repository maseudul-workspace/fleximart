package fleximart.fleximart.presentation.ui.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import fleximart.fleximart.R;
import fleximart.fleximart.domain.model.WalletHistory;

public class WalletAdapter extends RecyclerView.Adapter<WalletAdapter.ViewHolder> {

    Context mContext;
    WalletHistory[] walletHistories;

    public WalletAdapter(Context mContext, WalletHistory[] walletHistories) {
        this.mContext = mContext;
        this.walletHistories = walletHistories;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_wallet, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewTransactionMsg.setText(walletHistories[position].comment);
        holder.txtViewTransactionDate.setText(changeDateFormat(walletHistories[position].date));
        if (walletHistories[position].type == 1) {
            holder.layoutDebit.setVisibility(View.VISIBLE);
            holder.layoutCredit.setVisibility(View.GONE);
            holder.txtViewTransactionAmount.setText("- ₹ " + walletHistories[position].amount);
            holder.txtViewTransactionAmount.setTextColor(Color.parseColor("#c4606f"));
        } else {
            holder.layoutDebit.setVisibility(View.GONE);
            holder.layoutCredit.setVisibility(View.VISIBLE);
            holder.txtViewTransactionAmount.setText("+ ₹ " + walletHistories[position].amount);
            holder.txtViewTransactionAmount.setTextColor(Color.parseColor("#87d3d2"));
        }
    }

    @Override
    public int getItemCount() {
        return walletHistories.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_transaction_amount)
        TextView txtViewTransactionAmount;
        @BindView(R.id.txt_view_transaction_date)
        TextView txtViewTransactionDate;
        @BindView(R.id.txt_view_transaction_msg)
        TextView txtViewTransactionMsg;
        @BindView(R.id.layout_credit)
        View layoutCredit;
        @BindView(R.id.layout_debit)
        View layoutDebit;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public String changeDateFormat(String strCurrentDate) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
        Date newDate = null;
        try {
            newDate = format.parse(strCurrentDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        format = new SimpleDateFormat("dd MMM,yyyy");
        String date = format.format(newDate);
        return date;
    }

}
