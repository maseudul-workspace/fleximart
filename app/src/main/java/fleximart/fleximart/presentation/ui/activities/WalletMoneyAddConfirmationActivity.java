package fleximart.fleximart.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import fleximart.fleximart.R;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class WalletMoneyAddConfirmationActivity extends AppCompatActivity {

    @BindView(R.id.img_view_success)
    ImageView imgViewSuccess;
    @BindView(R.id.img_view_failure)
    ImageView imgViewFailure;
    @BindView(R.id.txt_view_first_heading)
    TextView txtViewFirstHeading;
    @BindView(R.id.txt_view_second_heading)
    TextView txtViewSecondHeading;
    @BindView(R.id.txt_view_order_date)
    TextView txtViewOrderDate;
    @BindView(R.id.txt_view_amount)
    TextView txtViewAmount;
    @BindView(R.id.txt_view_order_id)
    TextView txtViewOrderId;
    @BindView(R.id.txt_view_transaction_id)
    TextView  txtViewTransactionId;
    boolean isSuccess;
    String orderId = "";
    String transactionId = "";
    String amount = "";
    String orderDate = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_money_add_confirmation);
        ButterKnife.bind(this);
        isSuccess = getIntent().getBooleanExtra("isSuccess", false);
        orderId = getIntent().getStringExtra("orderId");
        transactionId = getIntent().getStringExtra("transactionId");
        amount = getIntent().getStringExtra("transactionAmount");
        orderDate = getIntent().getStringExtra("transactionDate");
        checkData();
    }

    public void checkData() {
        txtViewOrderDate.setText(orderDate);
        txtViewAmount.setText("₹ " + amount);
        txtViewTransactionId.setText(transactionId);
        txtViewOrderId.setText(orderId);
        if (!isSuccess) {
            imgViewSuccess.setVisibility(View.GONE);
            imgViewFailure.setVisibility(View.VISIBLE);
            txtViewFirstHeading.setText("Failed");
            txtViewSecondHeading.setText("Something Went Wromg");
        }
    }
}
