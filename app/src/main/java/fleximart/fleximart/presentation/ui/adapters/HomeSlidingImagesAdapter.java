package fleximart.fleximart.presentation.ui.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;

import fleximart.fleximart.domain.model.Other.HomeHeaderSliders;
import fleximart.fleximart.util.GlideHelper;

/**
 * Created by Raj on 13-07-2019.
 */

public class HomeSlidingImagesAdapter extends PagerAdapter {

    Context mContext;
    ArrayList<HomeHeaderSliders> headerSliders;
    int customPosition = 0;

    public HomeSlidingImagesAdapter(Context mContext, ArrayList<HomeHeaderSliders> headerSliders) {
        this.mContext = mContext;
        this.headerSliders = headerSliders;
    }

    @Override
    public int getCount() {
        return headerSliders.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == o;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        ImageView imageView = new ImageView(mContext);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        GlideHelper.setImageView(mContext, imageView, headerSliders.get(position).imageUrl);
        container.addView(imageView);
        return imageView;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

}
