package fleximart.fleximart.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fleximart.fleximart.R;
import fleximart.fleximart.domain.executors.impl.ThreadExecutor;
import fleximart.fleximart.presentation.presenters.CartDetailsPresenter;
import fleximart.fleximart.presentation.presenters.impl.CartDetailsPresenterImpl;
import fleximart.fleximart.presentation.ui.adapters.CartDetailsAdapter;
import fleximart.fleximart.presentation.ui.adapters.CartShippingAddressAdapter;
import fleximart.fleximart.threading.MainThreadImpl;
import fleximart.fleximart.util.Helper;

import android.animation.LayoutTransition;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetDialog;

public class CartListActivity extends AppCompatActivity implements CartDetailsPresenter.View {

    @BindView(R.id.recycler_view_cart_list)
    RecyclerView recyclerViewCartList;
    @BindView(R.id.txt_view_cart_item_count)
    TextView txtViewCartItemCount;
    CartDetailsPresenterImpl mPresenter;
    @BindView(R.id.txt_view_payable_amount)
    TextView txtViewPayableAmount;
    @BindView(R.id.txt_view_final_amount)
    TextView txtViewFinalAmount;
    @BindView(R.id.txt_view_total_mrp)
    TextView txtViewTotalMRP;
    @BindView(R.id.main_layout)
    View mainLayout;
    @BindView(R.id.main_bottom_layout)
    View mainBottomLayout;
    ProgressDialog progressDialog;
    BottomSheetDialog addressBottomSheetDialog;
    TextView txtViewAddAddress;
    ImageView imgViewAddAddress;
    RecyclerView recyclerViewShippingAddress;
    @BindView(R.id.txt_view_total_discount)
    TextView txtViewTotalDiscount;
    @BindView(R.id.cart_empty_layout)
    View cartEmptyLayout;
    int addressId;
    double totalMRP;
    double totalPrice;
    double totalDiscount;
    @BindView(R.id.layout_message)
    View layoutMessage;
    @BindView(R.id.txt_view_error_msg)
    TextView txtViewErrorMsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart_list);
        ButterKnife.bind(this);
        initialisePresenter();
        mPresenter.fetchCartDetails();
        getSupportActionBar().setTitle((Html.fromHtml("<font color=\"#00000\"><b> My Cart </b> </font>")));
        setUpProgressDialog();
        setAddressBottomSheetDialog();
        showLoader();
    }

    public void initialisePresenter() {
        mPresenter = new CartDetailsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this, this);
    }

    public void setAddressBottomSheetDialog() {
        if (addressBottomSheetDialog == null) {
            View view = LayoutInflater.from(this).inflate(R.layout.layout_address_bottom_sheet, null);
            addressBottomSheetDialog = new BottomSheetDialog(this);
            txtViewAddAddress = (TextView) view.findViewById(R.id.txt_view_add_address);
            imgViewAddAddress = (ImageView) view.findViewById(R.id.img_view_add_address);
            recyclerViewShippingAddress = (RecyclerView) view.findViewById(R.id.recycler_view_address);
            txtViewAddAddress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), AddAddressActivity.class);
                    startActivity(intent);
                }
            });
            addressBottomSheetDialog.setContentView(view);
        }
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadAdapter(CartDetailsAdapter adapter, int cartCount, double totalMRP, double totalPrice, double totalDiscount) {
        if (cartCount == 0) {
            mainLayout.setVisibility(View.GONE);
            mainBottomLayout.setVisibility(View.GONE);
        } else {
            this.totalMRP = totalMRP;
            this.totalPrice = totalPrice;
            this.totalDiscount = totalDiscount;
            recyclerViewCartList.setLayoutManager(new LinearLayoutManager(this));
            recyclerViewCartList.setAdapter(adapter);
            txtViewCartItemCount.setText("Item(" + cartCount + ")");
            recyclerViewCartList.setNestedScrollingEnabled(false);
            mainLayout.setVisibility(View.VISIBLE);
            mainBottomLayout.setVisibility(View.VISIBLE);
            txtViewFinalAmount.setText("₹ " + totalPrice);
            txtViewPayableAmount.setText("₹ " + totalPrice);
            txtViewTotalMRP.setText("₹ " + totalMRP);
            txtViewTotalDiscount.setText("-₹ " + totalDiscount);
        }
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.hide();
    }

    @Override
    public void loadShippingAddressAdapter(CartShippingAddressAdapter cartShippingAddressAdapter) {
        recyclerViewShippingAddress.setAdapter(cartShippingAddressAdapter);
        recyclerViewShippingAddress.setLayoutManager(new LinearLayoutManager(this));
        DividerItemDecoration itemDecor = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        recyclerViewShippingAddress.addItemDecoration(itemDecor);
    }

    @Override
    public void onShippingAddressEditClicked(int addressId) {
        Intent intent = new Intent(this, EditShiippingAddressActivity.class);
        intent.putExtra("addressId", addressId);
        startActivity(intent);
    }

    @Override
    public void onAddressSelected(int addressId) {
        this.addressId = addressId;
    }

    @Override
    public void goToCheckoutActivity() {
        Intent intent = new Intent(this, CheckoutActivity.class);
        intent.putExtra("totalDiscount", totalDiscount);
        intent.putExtra("totalMRP", totalMRP);
        intent.putExtra("totalPrice", totalPrice);
        intent.putExtra("addressId", addressId);
        startActivity(intent);
    }

    @Override
    public void showCartEmptyLayout() {
        cartEmptyLayout.setVisibility(View.VISIBLE);
        mainLayout.setVisibility(View.GONE);
        mainBottomLayout.setVisibility(View.GONE);
    }

    @Override
    public void showErrorMessage(String errorMsg) {
        layoutMessage.setVisibility(View.VISIBLE);
        txtViewErrorMsg.setText(errorMsg);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                layoutMessage.setVisibility(View.GONE);
            }
        }, 3000);
    }

    @OnClick(R.id.view_delivery_address) void onSetDeliveryAddressClicked() {
        addressBottomSheetDialog.show();
    }

    @OnClick(R.id.btn_shop_now) void onShopNowClicked() {
        if (Helper.isNetworkConnected(this)) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        } else {
            showErrorMessage("No Internet Connection");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.fetchShippingAddressList();
    }
}
