package fleximart.fleximart.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import fleximart.fleximart.R;
import fleximart.fleximart.domain.executors.impl.ThreadExecutor;
import fleximart.fleximart.presentation.presenters.MyCommissionListPresenter;
import fleximart.fleximart.presentation.presenters.impl.MyCommissionListPresenterImpl;
import fleximart.fleximart.presentation.ui.adapters.MyCommissionListAdapter;
import fleximart.fleximart.threading.MainThreadImpl;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;

public class MyCommisionListActivity extends AppCompatActivity implements MyCommissionListPresenter.View {

    @BindView(R.id.recycler_view_my_commissions)
    RecyclerView recyclerViewMyCommisions;
    @BindView(R.id.img_view_no_record_found)
    ImageView imgViewNoRecordFound;
    MyCommissionListPresenterImpl mPresenter;
    ProgressDialog progressDialog;
    Boolean isApprovedCommission;
    int pageNo = 1;
    int totalPage;
    LinearLayoutManager layoutManager;
    Boolean isScrolling = false;
    Integer currentItems;
    Integer totalItems;
    Integer scrollOutItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_commision_list);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        isApprovedCommission = getIntent().getBooleanExtra("isApprovedCommission", false);
        initialisePresenter();
        setUpProgressDialog();
        if (isApprovedCommission) {
            getSupportActionBar().setTitle("Approved Commission");
            mPresenter.fetchApprovedCommissions(pageNo, "refresh");
        } else {
            getSupportActionBar().setTitle("Unapproved Commission");
            mPresenter.fetchUnapprovedCommissions(pageNo, "refresh");
        }
        showLoader();
    }

    public void initialisePresenter() {
        mPresenter = new MyCommissionListPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadCommissionsList(MyCommissionListAdapter adapter, int totalPage) {
        if (totalPage == 0) {
            imgViewNoRecordFound.setVisibility(View.VISIBLE);
        } else {
            this.totalPage = totalPage;
            recyclerViewMyCommisions.setVisibility(View.VISIBLE);
            recyclerViewMyCommisions.setNestedScrollingEnabled(false);
            layoutManager = new LinearLayoutManager(this);
            recyclerViewMyCommisions.setLayoutManager(layoutManager);
            recyclerViewMyCommisions.setAdapter(adapter);
            recyclerViewMyCommisions.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                    if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                    {
                        isScrolling= true;
                    }
                }

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    currentItems = layoutManager.getChildCount();
                    totalItems  = layoutManager.getItemCount();
                    scrollOutItems = layoutManager.findFirstCompletelyVisibleItemPosition();
                    if(!recyclerView.canScrollVertically(1))
                    {
                        if(pageNo < totalPage) {
                            isScrolling = false;
                            pageNo = pageNo + 1;
                            if (isApprovedCommission) {
                                mPresenter.fetchApprovedCommissions(pageNo, "");
                            } else {
                                mPresenter.fetchUnapprovedCommissions(pageNo, "");
                            }
                        }
                    }
                }
            });
        }
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void onError(String errorMsg) {
        imgViewNoRecordFound.setVisibility(View.VISIBLE);
    }
}
