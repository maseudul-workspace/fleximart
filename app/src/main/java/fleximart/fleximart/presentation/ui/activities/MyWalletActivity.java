package fleximart.fleximart.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fleximart.fleximart.R;
import fleximart.fleximart.domain.executors.impl.ThreadExecutor;
import fleximart.fleximart.domain.model.OrderPlaceData;
import fleximart.fleximart.presentation.presenters.MyWalletPresenter;
import fleximart.fleximart.presentation.presenters.impl.MyWalletPresenterImpl;
import fleximart.fleximart.presentation.ui.adapters.WalletAdapter;
import fleximart.fleximart.presentation.ui.bottomsheets.AddWalletMoneyBottomSheet;
import fleximart.fleximart.threading.MainThreadImpl;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class MyWalletActivity extends AppCompatActivity implements MyWalletPresenter.View, AddWalletMoneyBottomSheet.Callback
{

    @BindView(R.id.txt_view_wallet_amount)
    TextView txtViewWalletAmount;
    @BindView(R.id.recycler_view_wallet_history)
    RecyclerView recyclerViewWalletHistory;
    @BindView(R.id.txt_view_transaction_count)
    TextView txtViewTransactionCount;
    MyWalletPresenterImpl mPresenter;
    ProgressDialog progressDialog;
    int pageNo = 1;
    AddWalletMoneyBottomSheet addWalletMoneyBottomSheet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_wallet);
        ButterKnife.bind(this);
        setUpProgressDialog();
        initialisePresenter();
        initialiseBottomSheet();
        showLoader();
        getSupportActionBar().setTitle("My Wallet");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void initialisePresenter() {
        mPresenter = new MyWalletPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void initialiseBottomSheet() {
        addWalletMoneyBottomSheet = new AddWalletMoneyBottomSheet(this::onSubmitClicked);
    }

    @Override
    public void loadAdapter(WalletAdapter walletAdapter, int totalPage, int currentPage) {
        recyclerViewWalletHistory.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewWalletHistory.setAdapter(walletAdapter);
        recyclerViewWalletHistory.setNestedScrollingEnabled(false);
    }

    @Override
    public void loadWalletAmount(double amount) {
        txtViewWalletAmount.setText("₹ " + amount);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void showErrorMessage(String errorMsg) {

    }

    @Override
    public void goToPaymentActivity(OrderPlaceData orderPlaceData) {
        Intent intent = new Intent(this, PaymentActivity.class);
        intent.putExtra("paytmOrderId", orderPlaceData.orderId);
        intent.putExtra("amount", orderPlaceData.amount);
        intent.putExtra("isWalletPay", true);
        startActivity(intent);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @OnClick(R.id.layout_add_money) void addMoneyClicked() {
        addWalletMoneyBottomSheet.show(getSupportFragmentManager(), "");
    }

    @Override
    public void onSubmitClicked(String amount) {
        addWalletMoneyBottomSheet.dismiss();
        mPresenter.addWalletMoney(amount);
        showLoader();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.fetchWalletAmount();
        mPresenter.fetchWalletHistory(pageNo);
    }

}
