package fleximart.fleximart.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import fleximart.fleximart.R;
import fleximart.fleximart.domain.model.LevelSponsors;

public class MyLevelSponsorsAdapter extends RecyclerView.Adapter<MyLevelSponsorsAdapter.ViewHolder> {

    LevelSponsors[] levelSponsors;

    public MyLevelSponsorsAdapter(LevelSponsors[] levelSponsors) {
        this.levelSponsors = levelSponsors;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_level_sponsors, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewDownlineName.setText(levelSponsors[position].name);
        holder.txtViewLeftId.setText(Integer.toString(levelSponsors[position].left));
        holder.txtViewRightId.setText(Integer.toString(levelSponsors[position].right));
        holder.txtViewNodeId.setText(Integer.toString(levelSponsors[position].nodeId));
        holder.txtViewSlNo.setText(Integer.toString(position + 1));
    }

    @Override
    public int getItemCount() {
        return levelSponsors.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_sl_no)
        TextView txtViewSlNo;
        @BindView(R.id.txt_view_downline_name)
        TextView txtViewDownlineName;
        @BindView(R.id.txt_view_node_id)
        TextView txtViewNodeId;
        @BindView(R.id.txt_view_left_id)
        TextView txtViewLeftId;
        @BindView(R.id.txt_view_right_id)
        TextView txtViewRightId;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

}
