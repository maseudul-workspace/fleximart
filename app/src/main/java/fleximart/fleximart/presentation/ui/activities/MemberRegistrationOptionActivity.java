package fleximart.fleximart.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fleximart.fleximart.R;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;

public class MemberRegistrationOptionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_registration_option);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle((Html.fromHtml("<font color=\"#00000\"><b> Add Member </b> </font>")));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @OnClick(R.id.layout_existing_member) void onExistingMemberClicked() {
        Intent intent = new Intent(this, CheckNodeActivity.class);
        intent.putExtra("member_status", true);
        startActivity(intent);
    }

    @OnClick(R.id.layout_new_user) void onNewUserClicked() {
        Intent intent = new Intent(this, CheckNodeActivity.class);
        intent.putExtra("member_status", false);
        startActivity(intent);
    }

}
