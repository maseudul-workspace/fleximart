package fleximart.fleximart.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import fleximart.fleximart.R;
import fleximart.fleximart.domain.executors.impl.ThreadExecutor;
import fleximart.fleximart.presentation.presenters.MyLevelsPresenter;
import fleximart.fleximart.presentation.presenters.impl.MyLevelsPresenterImpl;
import fleximart.fleximart.presentation.ui.adapters.MyLevelsAdapter;
import fleximart.fleximart.threading.MainThreadImpl;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MyLevelsActivity extends AppCompatActivity implements MyLevelsPresenter.View {

    @BindView(R.id.recycler_view_my_levels)
    RecyclerView recyclerViewMyLevels;
    ProgressDialog progressDialog;
    MyLevelsPresenterImpl mPresenter;
    int nodeId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_levels);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("My Levels");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        nodeId = getIntent().getIntExtra("nodeId", 0);
        initialisePresenter();
        setUpProgressDialog();
        showLoader();
        mPresenter.fetchMyLevels(nodeId);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    public void initialisePresenter() {
        mPresenter = new MyLevelsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @Override
    public void loadAdapter(MyLevelsAdapter adapter) {
        recyclerViewMyLevels.setAdapter(adapter);
        recyclerViewMyLevels.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewMyLevels.setVisibility(View.VISIBLE);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void goToSponsorsActivity(int levelId) {
        Intent intent = new Intent(this, MyLevelSponsorsActivity.class);
        intent.putExtra("levelId", levelId);
        intent.putExtra("nodeId", nodeId);
        startActivity(intent);
    }

    @Override
    public void goToLevelDownlinesActivity(int levelId) {
        Intent intent = new Intent(this, MyLevelDownlinesActivity.class);
        intent.putExtra("levelId", levelId);
        intent.putExtra("nodeId", nodeId);
        startActivity(intent);
    }
}
