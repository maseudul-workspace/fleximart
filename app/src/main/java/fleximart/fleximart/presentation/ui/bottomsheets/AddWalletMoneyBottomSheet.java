package fleximart.fleximart.presentation.ui.bottomsheets;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.textservice.TextInfo;
import android.widget.EditText;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.textfield.TextInputLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fleximart.fleximart.R;

public class AddWalletMoneyBottomSheet extends BottomSheetDialogFragment {

    public interface Callback {
        void onSubmitClicked(String amount);
    }

    Callback mCallback;
    @BindView(R.id.edit_text_amount)
    EditText editTextAmount;
    @BindView(R.id.txt_input_amount_layout)
    TextInputLayout txtInputNameLayout;

    public AddWalletMoneyBottomSheet(Callback mCallback) {
        this.mCallback = mCallback;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.layout_add_money_bottom_sheet, container, false);
    }
    @Override public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @OnClick(R.id.btn_submit) void onSubmitClicked() {
        txtInputNameLayout.setError("");
        if (editTextAmount.getText().toString().trim().isEmpty()) {
            txtInputNameLayout.setError("Amount Required");
        } else {
            mCallback.onSubmitClicked(editTextAmount.getText().toString());
        }
    }
}
