package fleximart.fleximart.presentation.ui.adapters;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import fleximart.fleximart.R;

public class QuantityAdapter extends RecyclerView.Adapter<QuantityAdapter.ViewHolder> {

    public interface Callback {
        void onQuantitySelected(int quantity);
    }

    Context mContext;
    int limit;
    Callback mCallback;

    public QuantityAdapter(Context mContext, int limit, Callback mCallback) {
        this.mContext = mContext;
        this.limit = limit;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_size_dialog, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (limit < position + 1) {
            holder.txtViewSize.setTextColor(mContext.getResources().getColor(R.color.darkerGrey));
            holder.txtViewSize.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        } else {
            holder.txtViewSize.setTextColor(mContext.getResources().getColor(R.color.black1));
            holder.txtViewSize.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCallback.onQuantitySelected(position+1);
                }
            });
        }
        holder.txtViewSize.setText(Integer.toString(position + 1));

    }

    @Override
    public int getItemCount() {
        return 10;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_size)
        TextView txtViewSize;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
