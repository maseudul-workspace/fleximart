package fleximart.fleximart.presentation.ui.adapters;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import fleximart.fleximart.R;
import fleximart.fleximart.domain.model.ProductDetails;
import fleximart.fleximart.domain.model.WishlistProducts;
import fleximart.fleximart.util.GlideHelper;

public class WishlistAdapter extends RecyclerView.Adapter<WishlistAdapter.ViewHolder> {

    public interface Callback {
        void onProductClicked(int produuctId);
        void onCancelClicked(int wishlistId);
        void onMoveToCartClicked(int productId);
    }

    Context mContext;
    WishlistProducts[] wishlistProducts;
    Callback mCallback;

    public WishlistAdapter(Context mContext, WishlistProducts[] wishlistProducts, Callback mCallback) {
        this.mContext = mContext;
        this.wishlistProducts = wishlistProducts;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_wishlist, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewProductName.setText(wishlistProducts[position].name);
        holder.txtViewProductSellingPrice.setText("Rs. " + wishlistProducts[position].price);
        GlideHelper.setImageView(mContext, holder.imgViewPoster, mContext.getResources().getString(R.string.base_url) + "/images/product/" + wishlistProducts[position].mainImage);
        holder.txtViewProductPrice.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        holder.txtViewProductPrice.setText("Rs. " + wishlistProducts[position].mrp);
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onProductClicked(wishlistProducts[position].productId);
            }
        });
        holder.layoutCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onCancelClicked(wishlistProducts[position].id);
            }
        });
        holder.txtViewMoveToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onMoveToCartClicked(wishlistProducts[position].productId);
            }
        });
    }

    @Override
    public int getItemCount() {
        return wishlistProducts.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_product_poster)
        ImageView imgViewPoster;
        @BindView(R.id.txt_view_product_name)
        TextView txtViewProductName;
        @BindView(R.id.txt_view_product_price)
        TextView txtViewProductPrice;
        @BindView(R.id.txt_view_product_selling_price)
        TextView txtViewProductSellingPrice;
        @BindView(R.id.main_layout)
        View mainLayout;
        @BindView(R.id.layout_cancel)
        View layoutCancel;
        @BindView(R.id.txt_view_move_to_cart)
        TextView txtViewMoveToCart;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
