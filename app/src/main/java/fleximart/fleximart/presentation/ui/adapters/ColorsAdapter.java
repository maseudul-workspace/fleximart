package fleximart.fleximart.presentation.ui.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import fleximart.fleximart.R;
import fleximart.fleximart.domain.model.Color;

public class ColorsAdapter extends RecyclerView.Adapter<ColorsAdapter.ViewHolder> {

    public interface Callback {
        void onColorChecked(int id);
        void onColorUnchecked(int id);
    }

    Context mContext;
    Color[] colors;
    Callback mCallback;

    public ColorsAdapter(Context mContext, Color[] colors, Callback mCallback) {
        this.mContext = mContext;
        this.colors = colors;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_color_filter, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Drawable background = holder.viewColor.getBackground();
        GradientDrawable gradientDrawable = (GradientDrawable) background;
        Log.e("LogMsg", "Colors: " + colors.length);
        gradientDrawable.setColor(android.graphics.Color.parseColor(colors[position].value));
        holder.layoutColorNotSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.layoutColorSelected.setVisibility(View.VISIBLE);
                holder.layoutColorNotSelected.setVisibility(View.GONE);
                mCallback.onColorChecked(colors[position].id);
            }
        });
        holder.layoutColorSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.layoutColorSelected.setVisibility(View.GONE);
                holder.layoutColorNotSelected.setVisibility(View.VISIBLE);
                mCallback.onColorUnchecked(colors[position].id);
            }
        });
    }

    @Override
    public int getItemCount() {
        return colors.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.view_color)
        View viewColor;
        @BindView(R.id.layout_color_selected)
        View layoutColorSelected;
        @BindView(R.id.layout_color_not_selected)
        View layoutColorNotSelected;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
