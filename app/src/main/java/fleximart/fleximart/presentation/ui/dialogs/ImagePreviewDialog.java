package fleximart.fleximart.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AlertDialog;
import androidx.viewpager2.widget.ViewPager2;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fleximart.fleximart.R;
import fleximart.fleximart.presentation.ui.adapters.ImageZoomViewPagerAdapter;
import fleximart.fleximart.util.GlideHelper;

public class ImagePreviewDialog {

    Context mContext;
    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    @BindView(R.id.zoom_image_viewpager)
    ViewPager2 viewPager2;
    @BindView(R.id.dots_indicator_layout)
    LinearLayout dotsIndicatorLayout;
    int imageCount;

    public ImagePreviewDialog(Context mContext, Activity mActivity) {
        this.mContext = mContext;
        this.mActivity = mActivity;
    }

    public void setUpDialog() {
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.layout_image_preview_dialog, null);
        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        ButterKnife.bind(this, dialogContainer);
    }

    public void setViewPager2(ImageZoomViewPagerAdapter adapter, int imageCount) {
        this.imageCount = imageCount;
        prepareDotsIndicator(0, imageCount);
        viewPager2.setAdapter(adapter);
        viewPager2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                prepareDotsIndicator(position, imageCount);
                super.onPageSelected(position);
            }
        });
    }

    public void showDialog(int position) {
        viewPager2.setCurrentItem(position);
        prepareDotsIndicator(position, imageCount);
        dialog.show();
    }

    public void hideDialog() {
        dialog.dismiss();
    }

    private void prepareDotsIndicator(int sliderPosition, int imageCount){
        if(dotsIndicatorLayout.getChildCount() > 0){
            dotsIndicatorLayout.removeAllViews();
        }

        ImageView dots[] = new ImageView[imageCount];
        for(int i = 0; i < imageCount; i++){
            dots[i] = new ImageView(mContext);
            if(i == sliderPosition){
                GlideHelper.setImageViewWithDrawable(mContext, dots[i], R.drawable.active_dot);
            }else{
                GlideHelper.setImageViewWithDrawable(mContext, dots[i], R.drawable.inactive_dot);
            }
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(15, 15);
            layoutParams.setMargins(5, 0, 5, 0);
            dotsIndicatorLayout.addView(dots[i], layoutParams);
        }
    }

    @OnClick(R.id.img_view_cancel) void onCancelClicked() {
        hideDialog();
    }

}
