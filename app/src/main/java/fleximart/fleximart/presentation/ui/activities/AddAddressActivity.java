package fleximart.fleximart.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fleximart.fleximart.R;
import fleximart.fleximart.domain.executors.impl.ThreadExecutor;
import fleximart.fleximart.presentation.presenters.AddAddressPresenter;
import fleximart.fleximart.presentation.presenters.impl.AddAddressPresenterImpl;
import fleximart.fleximart.presentation.ui.adapters.CityListDialogAdapter;
import fleximart.fleximart.presentation.ui.adapters.StateListDialogAdapter;
import fleximart.fleximart.presentation.ui.dialogs.SelectCityDialog;
import fleximart.fleximart.presentation.ui.dialogs.SelectStateDialog;
import fleximart.fleximart.threading.MainThreadImpl;
import fleximart.fleximart.util.Helper;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;

public class
AddAddressActivity extends AppCompatActivity implements AddAddressPresenter.View {

    AddAddressPresenterImpl mPresenter;
    SelectStateDialog selectStateDialog;
    @BindView(R.id.txt_view_state)
    TextView txtViewState;
    SelectCityDialog selectCityDialog;
    @BindView(R.id.txt_view_city_name)
    TextView txtViewCityName;
    @BindView(R.id.txt_input_name_layout)
    TextInputLayout txtInputNameLayout;
    @BindView(R.id.txt_input_email_layout)
    TextInputLayout txtInputEmailLayout;
    @BindView(R.id.txt_input_phone_layout)
    TextInputLayout txtInputPhoneLayout;
    @BindView(R.id.txt_input_address_layout)
    TextInputLayout txtInputAddressLayout;
    @BindView(R.id.txt_input_pincode_layout)
    TextInputLayout txtInputPincodeLayout;
    @BindView(R.id.edit_text_name)
    EditText editTextName;
    @BindView(R.id.edit_text_phone)
    EditText editTextPhone;
    @BindView(R.id.edit_text_alternate_number)
    EditText editTextAlternateNumber;
    @BindView(R.id.edit_text_email)
    EditText editTextEmail;
    @BindView(R.id.edit_text_pincode)
    EditText editTextPincode;
    @BindView(R.id.edit_text_address)
    EditText editTextAddress;
    @BindView(R.id.edit_text_landmark)
    EditText editTextLandmark;
    ProgressDialog progressDialog;
    @BindView(R.id.layout_message)
    View layoutMessage;
    @BindView(R.id.txt_view_error_msg)
    TextView txtViewErrorMsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_address);
        ButterKnife.bind(this);
        if (!Helper.isNetworkConnected(this)) {
            showErrorMessage("No Internet Connection");
        }
        initialisePresenter();
        setUpStateSelectDialogView();
        setSelectCityDialog();
        mPresenter.fetchStateList();
        setUpProgressDialog();
        getSupportActionBar().setTitle((Html.fromHtml("<font color=\"#00000\"><b> Add Address </b> </font>")));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void initialisePresenter() {
        mPresenter = new AddAddressPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    public void setUpStateSelectDialogView() {
        selectStateDialog = new SelectStateDialog(this, this);
        selectStateDialog.setUpDialogView();
    }

    public void setSelectCityDialog() {
        selectCityDialog = new SelectCityDialog(this, this);
        selectCityDialog.setUpDialogView();
    }

    @Override
    public void loadStateAdapter(StateListDialogAdapter stateListDialogAdapter) {
        selectStateDialog.setRecyclerView(stateListDialogAdapter);
    }

    @Override
    public void loadCitiesAdapter(CityListDialogAdapter cityListDialogAdapter) {
        selectCityDialog.setRecyclerView(cityListDialogAdapter);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.hide();
    }

    @Override
    public void setStateName(String stateName) {
        txtViewState.setText(stateName);
        txtViewState.setTextColor(this.getResources().getColor(R.color.black1));
        selectStateDialog.hideDialog();
    }

    @Override
    public void hideStateRecyclerView() {
        selectStateDialog.hideRecyclerView();
    }

    @Override
    public void setCityName(String cityName) {
        txtViewCityName.setText(cityName);
        txtViewCityName.setTextColor(this.getResources().getColor(R.color.black1));
        selectCityDialog.hideDialog();
    }

    @Override
    public void hideCityRecyclerView() {
        selectCityDialog.hideRecyclerView();
    }

    @Override
    public void onAddAddressSuccess() {
        finish();
    }

    @Override
    public void showErrorMessage(String errorMsg) {
        layoutMessage.setVisibility(View.VISIBLE);
        txtViewErrorMsg.setText(errorMsg);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                layoutMessage.setVisibility(View.GONE);
            }
        }, 3000);
    }

    @OnClick(R.id.state_linear_layout) void onStateClicked() {
        selectStateDialog.showDialog();
    }

    @OnClick(R.id.city_linear_layout) void onCityClicked() {
        selectCityDialog.showDialog();
    }

    @OnClick(R.id.btn_submit) void onSubmitClicked() {
        if (Helper.isNetworkConnected(this)) {
            if (editTextName.getText().toString().trim().isEmpty()) {
                txtInputNameLayout.setError("Name Required");
                editTextName.requestFocus();
            } else if (editTextEmail.getText().toString().trim().isEmpty()) {
                txtInputEmailLayout.setError("Email Required");
                editTextEmail.requestFocus();
            } else if (editTextPhone.getText().toString().trim().isEmpty()) {
                txtInputPhoneLayout.setError("Phone Required");
                editTextPhone.requestFocus();
            } else if (editTextPincode.getText().toString().trim().isEmpty()) {
                txtInputPincodeLayout.setError("Pincode Required");
                editTextPincode.requestFocus();
            } else if (editTextAddress.getText().toString().trim().isEmpty()) {
                txtInputAddressLayout.setError("Address Required");
                editTextAddress.requestFocus();
            } else if(!Patterns.EMAIL_ADDRESS.matcher(editTextEmail.getText().toString()).matches()){
                txtInputEmailLayout.setError("Enter a valid email");
                editTextEmail.requestFocus();
            } else {
                mPresenter.addAddress(
                        editTextName.getText().toString(),
                        editTextEmail.getText().toString(),
                        editTextPhone.getText().toString(),
                        editTextAlternateNumber.getText().toString(),
                        editTextLandmark.getText().toString(),
                        editTextPincode.getText().toString(),
                        editTextAddress.getText().toString()
                );
                progressDialog.show();
            }
        } else {
            showErrorMessage("No Internet Connection");
        }
    }

}
