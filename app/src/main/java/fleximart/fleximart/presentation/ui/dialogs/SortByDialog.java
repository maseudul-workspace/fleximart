package fleximart.fleximart.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import androidx.appcompat.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import fleximart.fleximart.R;

/**
 * Created by Raj on 04-09-2019.
 */

public class SortByDialog {
    Context mContext;
    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    TextView txtViewNewestArrivals;
    TextView txtViewPriceLow;
    TextView txtViewPriceHigh;
    TextView txtViewAZ;
    TextView txtViewZA;
    View layoutNewestArrivals;
    View layoutPriceLow;
    View layoutPriceHigh;
    View layoutAZ;
    View layoutZA;
    ImageView imgViewNewestSelected;
    ImageView imgViewPriceLow;
    ImageView imgViewPriceHigh;
    ImageView imgViewAZ;
    ImageView imgViewZA;
    ImageView imgViewCancel;
    int sort;
    Button btnApply;

    public interface Callback{
        void onSortApplyBtnClicked(int sort);
    }

    Callback mCallback;

    public SortByDialog(Context mContext, Activity mActivity, Callback mCallback) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        this.mCallback = mCallback;
    }

    public void setUpDialogView(){

        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.layout_sort_by_dialog, null);
        txtViewNewestArrivals = (TextView) dialogContainer.findViewById(R.id.txt_view_newest_first);
        txtViewPriceLow = (TextView) dialogContainer.findViewById(R.id.txt_view_price_low);
        txtViewPriceHigh = (TextView) dialogContainer.findViewById(R.id.txt_view_price_high);
        imgViewCancel = (ImageView) dialogContainer.findViewById(R.id.img_view_cancel);
        btnApply = (Button) dialogContainer.findViewById(R.id.btn_apply);

        layoutNewestArrivals = (View) dialogContainer.findViewById(R.id.layout_newest_first);
        layoutPriceLow = (View) dialogContainer.findViewById(R.id.layout_low_to_high);
        layoutPriceHigh = (View) dialogContainer.findViewById(R.id.layout_high_to_low);
        layoutAZ = (View) dialogContainer.findViewById(R.id.layout_az);
        layoutZA = (View) dialogContainer.findViewById(R.id.layout_za);

        imgViewNewestSelected = (ImageView) dialogContainer.findViewById(R.id.img_view_new_selected);
        imgViewPriceLow = (ImageView) dialogContainer.findViewById(R.id.img_view_low_selected);
        imgViewPriceHigh = (ImageView) dialogContainer.findViewById(R.id.img_view_high_selected);
        imgViewAZ = (ImageView) dialogContainer.findViewById(R.id.img_view_az_selected);
        imgViewZA = (ImageView) dialogContainer.findViewById(R.id.img_view_za);

        imgViewNewestSelected.setVisibility(View.VISIBLE);

        layoutNewestArrivals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgViewNewestSelected.setVisibility(View.VISIBLE);
                imgViewPriceHigh.setVisibility(View.GONE);
                imgViewPriceLow.setVisibility(View.GONE);
                imgViewAZ.setVisibility(View.GONE);
                imgViewZA.setVisibility(View.GONE);
                sort = 1;
            }
        });

        layoutPriceLow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgViewNewestSelected.setVisibility(View.GONE);
                imgViewPriceHigh.setVisibility(View.GONE);
                imgViewPriceLow.setVisibility(View.VISIBLE);
                imgViewAZ.setVisibility(View.GONE);
                imgViewZA.setVisibility(View.GONE);
                sort = 2;
            }
        });

        layoutPriceHigh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgViewNewestSelected.setVisibility(View.GONE);
                imgViewPriceHigh.setVisibility(View.VISIBLE);
                imgViewPriceLow.setVisibility(View.GONE);
                imgViewAZ.setVisibility(View.GONE);
                imgViewZA.setVisibility(View.GONE);
                sort = 3;
            }
        });

        layoutAZ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgViewNewestSelected.setVisibility(View.GONE);
                imgViewPriceHigh.setVisibility(View.GONE);
                imgViewPriceLow.setVisibility(View.GONE);
                imgViewAZ.setVisibility(View.VISIBLE);
                imgViewZA.setVisibility(View.GONE);
                sort = 4;
            }
        });

        layoutZA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgViewNewestSelected.setVisibility(View.GONE);
                imgViewPriceHigh.setVisibility(View.GONE);
                imgViewPriceLow.setVisibility(View.GONE);
                imgViewAZ.setVisibility(View.GONE);
                imgViewZA.setVisibility(View.VISIBLE);
                sort = 5;
            }
        });

        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                mCallback.onSortApplyBtnClicked(sort);
            }
        });

        imgViewCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    public void showDialog() {
        dialog.show();
    }

}
