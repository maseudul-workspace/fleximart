package fleximart.fleximart.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.Arrays;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fleximart.fleximart.R;
import fleximart.fleximart.domain.model.MyNodes;

public class NodeViewpagerAdapter extends PagerAdapter {

    public interface Callback {
        void onDownlineViewClicked(int id);
        void onTreeViewClicked(int id);
        void onLevelsClicked(int id);
    }

    Context mContext;
    MyNodes[] myNodes;
    Callback mCallback;
    @BindView(R.id.txt_view_node_id)
    TextView txtViewNodeId;
    @BindView(R.id.txt_view_left_user)
    TextView txtViewLeftUser;
    @BindView(R.id.txt_view_right_user)
    TextView txtViewRightUser;
    @BindView(R.id.txt_view_level)
    TextView txtViewLevel;
    @BindView(R.id.txt_view_downlines)
    TextView txtViewDownlines;
    @BindView(R.id.btn_view_downlines)
    Button btnViewDownlines;
    @BindView(R.id.btn_view_levels)
    Button btnViewLevels;
    @BindView(R.id.btn_view_tree)
    Button btnViewTree;

    public NodeViewpagerAdapter(Context mContext, MyNodes[] myNodes, Callback mCallback) {
        this.mContext = mContext;
        this.myNodes = myNodes;
        this.mCallback = mCallback;
    }

    @Override
    public int getCount() {
        return myNodes.length;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.layout_my_nodes, container, false);
        container.addView(layout);
        ButterKnife.bind(this, container);
        txtViewNodeId.setText(Integer.toString(myNodes[position].id));
        txtViewLeftUser.setText(myNodes[position].leftUser);
        txtViewRightUser.setText(myNodes[position].rightUser);
        txtViewLevel.setText(Integer.toString(myNodes[position].downlineInfo.totalLevelCompleted));
        txtViewDownlines.setText(Integer.toString(myNodes[position].downlineInfo.totalDownline));
        btnViewDownlines.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onDownlineViewClicked(myNodes[position].id);
            }
        });
        btnViewTree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onTreeViewClicked(myNodes[position].id);
            }
        });
        btnViewLevels.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onLevelsClicked(myNodes[position].id);
            }
        });
        return layout;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public int getItemPosition(Object object) {
        MyNodes newNode = (MyNodes) ((View) object).getTag();
        int position = Arrays.asList(myNodes).indexOf(newNode);
        if (position >= 0) {
            // The current data matches the data in this active fragment, so let it be as it is.
            return position;
        } else {
            // Returning POSITION_NONE means the current data does not matches the data this fragment is showing right now.  Returning POSITION_NONE constant will force the fragment to redraw its view layout all over again and show new data.
            return POSITION_NONE;
        }
    }

}
