package fleximart.fleximart.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import fleximart.fleximart.R;
import fleximart.fleximart.domain.executors.impl.ThreadExecutor;
import fleximart.fleximart.presentation.presenters.MyDownlinesPresenter;
import fleximart.fleximart.presentation.presenters.impl.MyDownlinesPresenterImpl;
import fleximart.fleximart.presentation.ui.adapters.MyDownlinesAdapter;
import fleximart.fleximart.threading.MainThreadImpl;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;

import static android.widget.LinearLayout.HORIZONTAL;

public class MyDownlinesActivity extends AppCompatActivity implements MyDownlinesPresenter.View {

    @BindView(R.id.recycler_view_my_downlines)
    RecyclerView recyclerViewDownlines;
    @BindView(R.id.img_view_no_record_found)
    ImageView imgViewNoRecordFound;
    MyDownlinesPresenterImpl mPresenter;
    int pageNo = 1;
    int totalPage;
    LinearLayoutManager layoutManager;
    Boolean isScrolling = false;
    Integer currentItems;
    Integer totalItems;
    Integer scrollOutItems;
    int nodeId;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_downlines);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("My Downlines");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        nodeId = getIntent().getIntExtra("nodeId",0);
        initialisePresenter();
        setUpProgressDialog();
        mPresenter.fetchMyDownlines(nodeId, pageNo, "refresh");
        showLoader();
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    public void initialisePresenter() {
        mPresenter = new MyDownlinesPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @Override
    public void loadAdapter(MyDownlinesAdapter myDownlinesAdapter, int currentPage, int totalPage) {
        this.totalPage = totalPage;
        recyclerViewDownlines.setVisibility(View.VISIBLE);
        recyclerViewDownlines.setNestedScrollingEnabled(false);
        layoutManager = new LinearLayoutManager(this);
        recyclerViewDownlines.setLayoutManager(layoutManager);
        recyclerViewDownlines.setAdapter(myDownlinesAdapter);
        recyclerViewDownlines.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                {
                    isScrolling= true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItems = layoutManager.getChildCount();
                totalItems  = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstCompletelyVisibleItemPosition();
                if(!recyclerView.canScrollVertically(1))
                {
                    if(pageNo < totalPage) {
                        isScrolling = false;
                        pageNo = pageNo + 1;
                        mPresenter.fetchMyDownlines(nodeId, pageNo, "");
                    }
                }
            }
        });
        DividerItemDecoration itemDecor = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        recyclerViewDownlines.addItemDecoration(itemDecor);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void onError(String errorMsg) {
        imgViewNoRecordFound.setVisibility(View.VISIBLE);
    }
}
