package fleximart.fleximart.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import android.view.View;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import fleximart.fleximart.R;
import fleximart.fleximart.domain.model.ProductDetailsSize;
import fleximart.fleximart.presentation.ui.adapters.SizeDialogAdapter;

public class SizesCartDialog implements SizeDialogAdapter.Callback {

    public interface Callback {
        void onSizeSelected(int cartId, int sizeId, int quantity);
    }

    Context mContext;
    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    @BindView(R.id.recycler_view_size_dialog)
    RecyclerView recyclerView;
    int quantity;
    int productId;
    SizeDialogAdapter adapter;
    Callback mCallback;

    public SizesCartDialog(Context mContext, Activity mActivity, Callback callback, int quantity, int productId) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        this.quantity = quantity;
        this.productId = productId;
        mCallback = callback;
    }

    public void setUpDialogView() {
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.size_dialog_layout, null);
        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Light_Dialog_NoActionBar);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        ButterKnife.bind(this, dialogContainer);
    }

    public void setRecyclerView(ProductDetailsSize[] productDetailsSizes) {
        adapter = new SizeDialogAdapter(mContext, productDetailsSizes, this::onSizeClicked);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerView.setAdapter(adapter);
    }

    public void hideDialog() {
        dialog.dismiss();
    }

    public void showDialog() {
        dialog.show();
    }

    @Override
    public void onSizeClicked(int id) {
        dialog.dismiss();
        mCallback.onSizeSelected(productId, id, quantity);
    }
}
