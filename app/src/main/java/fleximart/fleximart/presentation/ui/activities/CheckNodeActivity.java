package fleximart.fleximart.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.blox.graphview.Graph;
import de.blox.graphview.GraphView;
import de.blox.graphview.Node;
import de.blox.graphview.tree.BuchheimWalkerAlgorithm;
import de.blox.graphview.tree.BuchheimWalkerConfiguration;
import fleximart.fleximart.R;
import fleximart.fleximart.domain.executors.impl.ThreadExecutor;
import fleximart.fleximart.domain.model.NodeDetails;
import fleximart.fleximart.presentation.presenters.CheckNodePresenter;
import fleximart.fleximart.presentation.presenters.impl.CheckNodePresenterImpl;
import fleximart.fleximart.presentation.ui.adapters.NodeTreeViewAdapter;
import fleximart.fleximart.presentation.ui.dialogs.NodeConfirmDialog;
import fleximart.fleximart.presentation.ui.dialogs.NodeSelectDialog;
import fleximart.fleximart.threading.MainThreadImpl;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;

public class CheckNodeActivity extends AppCompatActivity implements NodeTreeViewAdapter.Callback, CheckNodePresenter.View, NodeSelectDialog.Callback, NodeConfirmDialog.Callback {

    @BindView(R.id.layout_treeview)
    GraphView treeview;
    NodeTreeViewAdapter nodeTreeViewAdapter;
    @BindView(R.id.edit_text_node_id)
    EditText editTextNodeId;
    @BindView(R.id.txt_input_node_id_layout)
    TextInputLayout txtInputLayout;
    @BindView(R.id.layout_color_indication)
    View layoutColorIndication;
    CheckNodePresenterImpl mPresenter;
    ProgressDialog progressDialog;
    private int nodeCount = 1;
    NodeSelectDialog nodeSelectDialog;
    NodeConfirmDialog nodeConfirmDialog;
    @BindView(R.id.layout_message)
    View layoutMessage;
    @BindView(R.id.txt_view_message)
    TextView txtViewMessage;
    int nodeAmount;
    int nodes;
    String leg;
    Boolean memberStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_node);
        ButterKnife.bind(this);
        memberStatus = getIntent().getBooleanExtra("member_status", false);
        getSupportActionBar().setTitle((Html.fromHtml("<font color=\"#00000\"><b> Check Node </b> </font>")));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initialisePresenter();
        setProgressDialog();
        setNodeSelectDialog();
        setNodeConfirmDialog();
    }

    public void initialisePresenter() {
        mPresenter = new CheckNodePresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setNodeSelectDialog() {
        nodeSelectDialog = new NodeSelectDialog(this, this, this::onNodeQtySelected);
        nodeSelectDialog.setUpDialog();
        nodeSelectDialog.setRecyclerView();
    }

    public void setNodeConfirmDialog() {
        nodeConfirmDialog = new NodeConfirmDialog(this, this, this);
        nodeConfirmDialog.setUpDialog();
    }

    @Override
    public void onCardClicked(String leg) {
        nodeSelectDialog.showDialog();
        this.leg = leg;
    }

    public void setProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadData(NodeDetails nodeDetails) {
            Node node1;
            Node node2;
            Node node3;
            if (nodeDetails.nodeStatus.equals("LR")) {
                node1 = new Node(getNodeText("false" + "-" + "true" + "-" + editTextNodeId.getText().toString()));
                node2 = new Node(getNodeText("true" + "-" + "false" + "-" + "L"));
                node3 = new Node(getNodeText("true" + "-" + "false" + "-" + "R"));
            } else if (nodeDetails.nodeStatus.equals("R")) {
                node1 = new Node(getNodeText("false" + "-" + "true" + "-" + editTextNodeId.getText().toString()));
                node2 = new Node(getNodeText("false" + "-" + "false" + "-" + "L"));
                node3 = new Node(getNodeText("true" + "-" + "false" + "-" + "R"));
            } else if (nodeDetails.nodeStatus.equals("L")) {
                node1 = new Node(getNodeText("false" + "-" + "true" + "-" + editTextNodeId.getText().toString()));
                node2 = new Node(getNodeText("true" + "-" + "false" + "-" + "L"));
                node3 = new Node(getNodeText("false" + "-" + "false" + "-" + "R"));
            } else {
                node1 = new Node(getNodeText("false" + "-" + "true" + "-" + editTextNodeId.getText().toString()));
                node2 = new Node(getNodeText("false" + "-" + "false" + "-" + "L"));
                node3 = new Node(getNodeText("false" + "-" + "false" + "-" + "R"));
            }

            final Graph graph = new Graph();

            graph.addEdge(node1, node2);
            graph.addEdge(node1, node3);
            nodeTreeViewAdapter = new NodeTreeViewAdapter(graph, this, this);
            treeview.setAdapter(nodeTreeViewAdapter);
            // set the algorithm here
            final BuchheimWalkerConfiguration configuration = new BuchheimWalkerConfiguration.Builder()
                    .setSiblingSeparation(200)
                    .setLevelSeparation(200)
                    .setSubtreeSeparation(400)
                    .setOrientation(BuchheimWalkerConfiguration.ORIENTATION_TOP_BOTTOM)
                    .build();
            nodeTreeViewAdapter.setAlgorithm(new BuchheimWalkerAlgorithm(configuration));
            layoutColorIndication.setVisibility(View.VISIBLE);
    }

    @Override
    public void onGettingDataFailed() {
        treeview.setVisibility(View.GONE);
        layoutColorIndication.setVisibility(View.GONE);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void showMessage(String errorMsg) {
        layoutMessage.setVisibility(View.VISIBLE);
        txtViewMessage.setText(errorMsg);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                layoutMessage.setVisibility(View.GONE);
            }
        }, 3000);
    }

    @Override
    public void loadWalletAmount(Double amount) {
        if (amount >= nodeAmount) {
            nodeConfirmDialog.setData(nodes, nodeAmount);
            nodeConfirmDialog.showDialog();
        } else {
            showMessage("Insufficient wallet amount");
        }
    }

    @Override
    public void goToOrderResponseActivity() {
        Intent intent = new Intent(this, OrderSuccessResponseActivity.class);
        intent.putExtra("amount", nodeAmount);
        intent.putExtra("node-count", nodeCount);
        intent.putExtra("leg", leg);
        intent.putExtra("nodeId", editTextNodeId.getText().toString());
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.btn_node) void onCheckNodeClicked() {
        if (editTextNodeId.getText().toString().isEmpty()) {
            txtInputLayout.setError("Please Enter Node Id");
//            txtInputLayout.requestFocus();
        } else {
            mPresenter.checkNode(editTextNodeId.getText().toString());
            showLoader();
        }
    }

    private String getNodeText(String msg) {
        return "Node-" + nodeCount++ + "-" + msg ;
    }

    @Override
    public void onNodeQtySelected(int node) {
        nodes = node;
        nodeAmount = node * 1600;
        showLoader();
        mPresenter.getWalletDetails();
    }

    @Override
    public void onConfirmClicked() {
        if (memberStatus) {
            mPresenter.registerNodes(editTextNodeId.getText().toString(), nodes, leg);
            showLoader();
        } else {
            Intent intent = new Intent(this, NewMemberRegistrationActivity.class);
            intent.putExtra("sponsorNode", editTextNodeId.getText().toString());
            intent.putExtra("totalNode", nodes);
            intent.putExtra("leg", leg);
            intent.putExtra("amount", nodeAmount);
            startActivity(intent);
            finish();
        }
    }
}
