package fleximart.fleximart.presentation.ui.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import fleximart.fleximart.R;
import fleximart.fleximart.domain.model.LevelDownlines;

public class MyLevelDownlinesAdapter extends RecyclerView.Adapter<MyLevelDownlinesAdapter.ViewHolder> {

    LevelDownlines[] levelDownlines;

    public MyLevelDownlinesAdapter(LevelDownlines[] levelDownlines) {
        this.levelDownlines = levelDownlines;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_level_downlines, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewDownlineName.setText(levelDownlines[position].name);
        holder.txtViewLeftId.setText(Integer.toString(levelDownlines[position].left));
        holder.txtViewRightId.setText(Integer.toString(levelDownlines[position].right));
        holder.txtViewNodeId.setText(Integer.toString(levelDownlines[position].nodeId));
        holder.txtViewSlNo.setText(Integer.toString(position + 1));
    }

    @Override
    public int getItemCount() {
        return levelDownlines.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_sl_no)
        TextView txtViewSlNo;
        @BindView(R.id.txt_view_downline_name)
        TextView txtViewDownlineName;
        @BindView(R.id.txt_view_node_id)
        TextView txtViewNodeId;
        @BindView(R.id.txt_view_left_id)
        TextView txtViewLeftId;
        @BindView(R.id.txt_view_right_id)
        TextView txtViewRightId;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

}
