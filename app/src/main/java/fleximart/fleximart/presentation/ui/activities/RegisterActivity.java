package fleximart.fleximart.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import fleximart.fleximart.R;
import fleximart.fleximart.domain.executors.impl.ThreadExecutor;
import fleximart.fleximart.domain.model.UserInfoWrapper;
import fleximart.fleximart.presentation.presenters.RegisterPresenter;
import fleximart.fleximart.presentation.presenters.impl.RegisterPresenterImpl;
import fleximart.fleximart.threading.MainThreadImpl;
import fleximart.fleximart.util.Helper;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class RegisterActivity extends AppCompatActivity implements RegisterPresenter.View {

    @BindView(R.id.edit_text_email)
    EditText editTextEmail;
    @BindView(R.id.edit_text_password)
    EditText editTextPassword;
    @BindView(R.id.edit_text_name)
    EditText editTextName;
    @BindView(R.id.edit_text_repeat_password)
    EditText editTextRepeatPassword;
    @BindView(R.id.edit_text_phone)
    EditText editTextPhone;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    RegisterPresenterImpl mPresenter;
    @BindView(R.id.progress_bar)
    View progressBar;
    @BindView(R.id.txt_input_name_layout)
    TextInputLayout txtInputNameLayout;
    @BindView(R.id.txt_input_email_layout)
    TextInputLayout txtInputEmailLayout;
    @BindView(R.id.txt_input_phone_layout)
    TextInputLayout txtInputPhoneLayout;
    @BindView(R.id.txt_input_password_layout)
    TextInputLayout txtInputPasswordLayout;
    @BindView(R.id.txt_input_repeat_password_layout)
    TextInputLayout txtInputRepeatPasswordLayout;
    @BindView(R.id.layout_message)
    View layoutMessage;
    @BindView(R.id.txt_view_error_msg)
    TextView txtViewErrorMsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        getSupportActionBar().setTitle((Html.fromHtml("<font color=\"#00000\"><b> Sign Up </b> </font>")));
        ButterKnife.bind(this);
        initialisePresenter();
    }

    public void initialisePresenter() {
        mPresenter = new RegisterPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @Override
    public void onRegisterSuccess() {
        progressBar.setVisibility(View.INVISIBLE);
        btnSubmit.setText("Sign Up");
        btnSubmit.setEnabled(true);
        editTextName.setText("");
        editTextEmail.setText("");
        editTextPassword.setText("");
        editTextPhone.setText("");
        editTextRepeatPassword.setText("");
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void onRegsiterFail(UserInfoWrapper userInfoWrapper) {
        progressBar.setVisibility(View.INVISIBLE);
        btnSubmit.setText("Sign Up");
        btnSubmit.setEnabled(true);
        if (userInfoWrapper != null) {

            if (userInfoWrapper.errorMessage.email != null) {
                txtInputEmailLayout.setError(userInfoWrapper.errorMessage.email[0]);
            }

            if (userInfoWrapper.errorMessage.mobile != null) {
                txtInputPhoneLayout.setError(userInfoWrapper.errorMessage.mobile[0]);
            }

        }
    }

    @Override
    public void showErrorMessage(String errorMsg) {
        layoutMessage.setVisibility(View.VISIBLE);
        txtViewErrorMsg.setText(errorMsg);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                layoutMessage.setVisibility(View.GONE);
            }
        }, 3000);
    }

    @OnClick(R.id.btn_submit) void submitClicked() {

        if (Helper.isNetworkConnected(this)) {
            txtInputEmailLayout.setError("");
            txtInputPasswordLayout.setError("");
            txtInputRepeatPasswordLayout.setError("");
            txtInputPasswordLayout.setError("");
            txtInputPhoneLayout.setError("");

            if (editTextEmail.getText().toString().trim().isEmpty() || editTextName.getText().toString().trim().isEmpty() || editTextPassword.getText().toString().trim().isEmpty() || editTextRepeatPassword.getText().toString().trim().isEmpty()) {
                Toasty.warning(this, "Please fill all the fields", Toast.LENGTH_SHORT, true).show();
            } else if(!Patterns.EMAIL_ADDRESS.matcher(editTextEmail.getText().toString()).matches()){
                txtInputEmailLayout.setError("Please give an proper email");
            } else if(editTextPassword.getText().toString().length() < 8) {
                txtInputPasswordLayout.setError("Minimum 8 characters required");
            } else if (!editTextPassword.getText().toString().equals(editTextRepeatPassword.getText().toString())){
                txtInputRepeatPasswordLayout.setError("Password Mismatch");
            } else if (editTextPhone.getText().toString().trim().length() < 10) {
                txtInputPhoneLayout.setError("Less than 10 digits");
            } else {
                mPresenter.registerUser(
                        editTextName.getText().toString(),
                        editTextEmail.getText().toString(),
                        editTextPassword.getText().toString(),
                        editTextRepeatPassword.getText().toString(),
                        editTextPhone.getText().toString()
                );
                progressBar.setVisibility(View.VISIBLE);
                btnSubmit.setEnabled(false);
                btnSubmit.setText("Please Wait...");
            }
        } else {
            showErrorMessage("No Internet Connection");
        }
    }

}
