package fleximart.fleximart.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fleximart.fleximart.R;
import fleximart.fleximart.domain.executors.impl.ThreadExecutor;
import fleximart.fleximart.domain.model.Address;
import fleximart.fleximart.presentation.presenters.EditShippingAddressPresenter;
import fleximart.fleximart.presentation.presenters.impl.EditShippingAddressPresenterImpl;
import fleximart.fleximart.presentation.ui.adapters.CityListDialogAdapter;
import fleximart.fleximart.presentation.ui.adapters.StateListDialogAdapter;
import fleximart.fleximart.presentation.ui.dialogs.SelectCityDialog;
import fleximart.fleximart.presentation.ui.dialogs.SelectStateDialog;
import fleximart.fleximart.threading.MainThreadImpl;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.Html;
import android.util.Patterns;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;

public class EditShiippingAddressActivity extends AppCompatActivity implements EditShippingAddressPresenter.View {

    SelectStateDialog selectStateDialog;
    @BindView(R.id.txt_view_state)
    TextView txtViewState;
    SelectCityDialog selectCityDialog;
    @BindView(R.id.txt_view_city_name)
    TextView txtViewCityName;
    @BindView(R.id.txt_input_name_layout)
    TextInputLayout txtInputNameLayout;
    @BindView(R.id.txt_input_email_layout)
    TextInputLayout txtInputEmailLayout;
    @BindView(R.id.txt_input_phone_layout)
    TextInputLayout txtInputPhoneLayout;
    @BindView(R.id.txt_input_address_layout)
    TextInputLayout txtInputAddressLayout;
    @BindView(R.id.txt_input_pincode_layout)
    TextInputLayout txtInputPincodeLayout;
    @BindView(R.id.edit_text_name)
    EditText editTextName;
    @BindView(R.id.edit_text_phone)
    EditText editTextPhone;
    @BindView(R.id.edit_text_alternate_number)
    EditText editTextAlternateNumber;
    @BindView(R.id.edit_text_email)
    EditText editTextEmail;
    @BindView(R.id.edit_text_pincode)
    EditText editTextPincode;
    @BindView(R.id.edit_text_address)
    EditText editTextAddress;
    @BindView(R.id.edit_text_landmark)
    EditText editTextLandmark;
    ProgressDialog progressDialog;
    int addressId;
    EditShippingAddressPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_shiipping_address);
        ButterKnife.bind(this);
        addressId = getIntent().getIntExtra("addressId", 0);
        setUpStateSelectDialogView();
        setSelectCityDialog();
        setUpProgressDialog();
        initialisePresenter();
        mPresenter.getShippingAddressDetails(addressId);
        mPresenter.fetchStateList();
        showLoader();
        getSupportActionBar().setTitle((Html.fromHtml("<font color=\"#00000\"><b> Update Address </b> </font>")));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    public void initialisePresenter() {
        mPresenter = new EditShippingAddressPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setUpStateSelectDialogView() {
        selectStateDialog = new SelectStateDialog(this, this);
        selectStateDialog.setUpDialogView();
    }

    public void setSelectCityDialog() {
        selectCityDialog = new SelectCityDialog(this, this);
        selectCityDialog.setUpDialogView();
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.hide();
    }

    @Override
    public void setData(Address address) {
        editTextName.setText(address.name);
        editTextAddress.setText(address.address);
        editTextAlternateNumber.setText(address.alternativeMobile);
        editTextEmail.setText(address.email);
        editTextLandmark.setText(address.landmark);
        editTextPhone.setText(address.mobile);
        editTextPincode.setText(address.pin);
        txtViewState.setText(address.stateName);
        txtViewState.setTextColor(this.getResources().getColor(R.color.black1));
        txtViewCityName.setTextColor(this.getResources().getColor(R.color.black1));
        txtViewCityName.setText(address.cityName);
    }

    @Override
    public void loadStateAdapter(StateListDialogAdapter stateListDialogAdapter) {
        selectStateDialog.setRecyclerView(stateListDialogAdapter);
    }

    @Override
    public void loadCitiesAdapter(CityListDialogAdapter cityListDialogAdapter) {
        selectCityDialog.setRecyclerView(cityListDialogAdapter);
    }

    @Override
    public void setStateName(String stateName) {
        txtViewState.setText(stateName);
        txtViewState.setTextColor(this.getResources().getColor(R.color.black1));
        selectStateDialog.hideDialog();
    }

    @Override
    public void setCityName(String cityName) {
        txtViewCityName.setText(cityName);
        txtViewCityName.setTextColor(this.getResources().getColor(R.color.black1));
        selectCityDialog.hideDialog();
    }

    @Override
    public void hideStateRecyclerView() {
        selectCityDialog.hideRecyclerView();
    }

    @Override
    public void hideCityRecyclerView() {
        selectCityDialog.hideRecyclerView();
    }

    @Override
    public void onUpdateAddressSuccess() {
        finish();
    }

    @OnClick(R.id.state_linear_layout) void onStateClicked() {
        selectStateDialog.showDialog();
    }

    @OnClick(R.id.city_linear_layout) void onCityClicked() {
        selectCityDialog.showDialog();
    }

    @OnClick(R.id.btn_update) void onUpdateClicked() {
        if (editTextName.getText().toString().trim().isEmpty()) {
            txtInputNameLayout.setError("Name Required");
            editTextName.requestFocus();
        } else if (editTextEmail.getText().toString().trim().isEmpty()) {
            txtInputEmailLayout.setError("Email Required");
            editTextEmail.requestFocus();
        } else if (editTextPhone.getText().toString().trim().isEmpty()) {
            txtInputPhoneLayout.setError("Phone Required");
            editTextPhone.requestFocus();
        } else if (editTextPincode.getText().toString().trim().isEmpty()) {
            txtInputPincodeLayout.setError("Pincode Required");
            editTextPincode.requestFocus();
        } else if (editTextAddress.getText().toString().trim().isEmpty()) {
            txtInputAddressLayout.setError("Address Required");
            editTextAddress.requestFocus();
        } else if(!Patterns.EMAIL_ADDRESS.matcher(editTextEmail.getText().toString()).matches()){
            txtInputEmailLayout.setError("Enter a valid email");
            editTextEmail.requestFocus();
        } else {
            mPresenter.updateAddress(
                    editTextName.getText().toString(),
                    editTextEmail.getText().toString(),
                    editTextPhone.getText().toString(),
                    editTextAlternateNumber.getText().toString(),
                    editTextLandmark.getText().toString(),
                    editTextPincode.getText().toString(),
                    editTextAddress.getText().toString()
            );
            progressDialog.show();
        }
    }

}
