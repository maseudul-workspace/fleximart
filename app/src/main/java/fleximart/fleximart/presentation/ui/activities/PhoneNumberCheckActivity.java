package fleximart.fleximart.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.blox.graphview.Node;
import fleximart.fleximart.R;
import fleximart.fleximart.domain.executors.impl.ThreadExecutor;
import fleximart.fleximart.domain.model.CheckPhoneData;
import fleximart.fleximart.presentation.presenters.PhoneNumberCheckPresenter;
import fleximart.fleximart.presentation.presenters.impl.PhoneNumberCheckPresenterImpl;
import fleximart.fleximart.presentation.ui.dialogs.PhoneCheckDialog;
import fleximart.fleximart.threading.MainThreadImpl;
import fleximart.fleximart.util.GlideHelper;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;

public class PhoneNumberCheckActivity extends AppCompatActivity implements PhoneNumberCheckPresenter.View {

    @BindView(R.id.txt_view_name)
    TextView txtViewName;
    @BindView(R.id.txt_view_email)
    TextView txtViewEmail;
    @BindView(R.id.img_view_user)
    ImageView imgViewUser;
    ProgressDialog progressDialog;
    PhoneNumberCheckPresenterImpl mPresenter;
    @BindView(R.id.btn_check_node)
    Button btnCheckNode;
    @BindView(R.id.edit_text_phone)
    EditText editTextPhone;
    @BindView(R.id.txt_input_phone_layout)
    TextInputLayout txtInputPhoneLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_number_check);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle((Html.fromHtml("<font color=\"#00000\"><b> Check Member </b> </font>")));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initialisePresenter();
        GlideHelper.setImageViewCustomRoundedCornersDrawable(this, imgViewUser, R.drawable.user_bg_black, 100);
        setProgressDialog();
    }

    public void initialisePresenter() {
        mPresenter = new PhoneNumberCheckPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }



    @OnClick(R.id.btn_search) void onSearchClicked() {
        if (editTextPhone.getText().toString().isEmpty()) {
            txtInputPhoneLayout.setError("Phone number required");
        } else if (editTextPhone.getText().toString().length() < 10) {
            txtInputPhoneLayout.setError("Phone number must be of 10 characters");
        } else {
            txtInputPhoneLayout.setError("");
            mPresenter.checkPhoneNumber(editTextPhone.getText().toString());
            showLoader();
        }
    }

    @Override
    public void loadData(CheckPhoneData checkPhoneData) {
        txtViewEmail.setText(checkPhoneData.email);
        txtViewName.setText(checkPhoneData.name);
        btnCheckNode.setVisibility(View.VISIBLE);

    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void clearData() {
        txtViewName.setText("");
        txtViewEmail.setText("");
        btnCheckNode.setVisibility(View.INVISIBLE);
    }

    @OnClick(R.id.btn_check_node) void onCheckNodeClicked() {
        Intent intent = new Intent(this, CheckNodeActivity.class);
        intent.putExtra("phone", editTextPhone.getText().toString());
        intent.putExtra("member_status", true);
        Pair[] pairs = new Pair[1];
        pairs[0] = new Pair<View, String> (btnCheckNode, "buttonTransition");
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(PhoneNumberCheckActivity.this, pairs);
            startActivity(intent, options.toBundle());
        } else {
            startActivity(intent);
        }
    }

}
