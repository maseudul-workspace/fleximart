package fleximart.fleximart.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import android.view.View;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import fleximart.fleximart.R;
import fleximart.fleximart.presentation.ui.adapters.QuantityAdapter;

public class QuantityDialog implements QuantityAdapter.Callback {

    @Override
    public void onQuantitySelected(int quantity) {
        dialog.dismiss();
        mCallback.onQuantitySelected(productId, sizeId, quantity);
    }

    public interface Callback {
        void onQuantitySelected(int cartId, int sizeId, int quantity);
    }

    Context mContext;
    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    @BindView(R.id.recycler_view_quantity_dialog)
    RecyclerView recyclerView;
    int sizeId;
    int productId;
    Callback mCallback;
    QuantityAdapter adapter;

    public QuantityDialog(Context mContext, Activity mActivity, int sizeId, int productId, Callback mCallback) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        this.sizeId = sizeId;
        this.productId = productId;
        this.mCallback = mCallback;
    }

    public void setUpDialogView() {
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.quantity_dialog_layout, null);
        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Light_Dialog_NoActionBar);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        ButterKnife.bind(this, dialogContainer);
    }

    public void setRecyclerView(int limit) {
        adapter = new QuantityAdapter(mContext, limit, this::onQuantitySelected);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerView.setAdapter(adapter);
    }

    public void showDialog() {
        dialog.show();
    }

    public void hideDialog() {
        dialog.dismiss();
    }

}
