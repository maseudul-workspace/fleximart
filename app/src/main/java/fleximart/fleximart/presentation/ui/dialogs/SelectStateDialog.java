package fleximart.fleximart.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import fleximart.fleximart.R;
import fleximart.fleximart.presentation.ui.adapters.StateListDialogAdapter;

public class SelectStateDialog {

    Context mContext;
    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    @BindView(R.id.recycler_view_state)
    RecyclerView recyclerView;
    @BindView(R.id.txt_view_state_error)
    TextView txtViewStateError;

    public SelectStateDialog(Context mContext, Activity mActivity) {
        this.mContext = mContext;
        this.mActivity = mActivity;
    }

    public void setUpDialogView() {
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.layout_state_select_dialog, null);
        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Light_Dialog_NoActionBar_MinWidth);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        ButterKnife.bind(this, dialogContainer);
    }

    public void setRecyclerView(StateListDialogAdapter adapter) {
        txtViewStateError.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.setLayoutManager(new GridLayoutManager(mContext, 2));
        recyclerView.setAdapter(adapter);
    }

    public void hideDialog() {
        dialog.dismiss();
    }

    public void showDialog() {
        dialog.show();
    }

    public void hideRecyclerView() {
        txtViewStateError.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
    }

}
