package fleximart.fleximart.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import fleximart.fleximart.R;

import android.os.Bundle;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class OrderSuccessResponseActivity extends AppCompatActivity {

    @BindView(R.id.txt_view_amount)
    TextView txtViewAmount;
    @BindView(R.id.txt_view_node_count)
    TextView txtViewNodeCount;
    @BindView(R.id.txt_view_order_date)
    TextView txtViewOrderDate;
    @BindView(R.id.txt_view_payment_type)
    TextView txtViewPaymentType;
    @BindView(R.id.txt_view_leg)
    TextView txtViewLeg;
    @BindView(R.id.txt_view_node_id)
    TextView txtViewNodeId;
    int amount;
    int nodeCount;
    String leg;
    String nodeId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_success_response);
        ButterKnife.bind(this);
        amount = getIntent().getIntExtra("amount", 0);
        nodeCount = getIntent().getIntExtra("node-count", 0);
        leg = getIntent().getStringExtra("leg");
        nodeId = getIntent().getStringExtra("nodeId");
        setData();
    }

    public void setData() {
        txtViewAmount.setText("Rs. " + amount);
        txtViewNodeCount.setText(Integer.toString(nodeCount));
        txtViewPaymentType.setText("Wallet");
        txtViewLeg.setText(leg);
        txtViewNodeId.setText(nodeId);
        txtViewOrderDate.setText(getDate());
    }

    public static String getDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "MMMM dd, yyyy", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

}
