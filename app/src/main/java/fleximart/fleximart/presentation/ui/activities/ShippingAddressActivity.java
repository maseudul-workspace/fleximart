package fleximart.fleximart.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fleximart.fleximart.R;
import fleximart.fleximart.domain.executors.impl.ThreadExecutor;
import fleximart.fleximart.presentation.presenters.ShippingAddressPresenter;
import fleximart.fleximart.presentation.presenters.impl.ShippingAddressPresenterImpl;
import fleximart.fleximart.presentation.ui.adapters.ShippingAddressAdapter;
import fleximart.fleximart.threading.MainThreadImpl;
import fleximart.fleximart.util.Helper;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

public class ShippingAddressActivity extends AppCompatActivity implements ShippingAddressPresenter.View{

    @BindView(R.id.recycler_view_address)
    RecyclerView recyclerViewAddress;
    ShippingAddressPresenterImpl mPresenter;
    ProgressDialog progressDialog;
    @BindView(R.id.layout_message)
    View layoutMessage;
    @BindView(R.id.txt_view_error_msg)
    TextView txtViewErrorMsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shipping_address);
        ButterKnife.bind(this);
        if (!Helper.isNetworkConnected(this)) {
            showErrorMessage("No Internet Connection");
        }
        getSupportActionBar().setTitle((Html.fromHtml("<font color=\"#00000\"><b> My Address </b> </font>")));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setUpProgressDialog();
        initialisePresenter();
    }

    public void initialisePresenter() {
        mPresenter = new ShippingAddressPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadAdapter(ShippingAddressAdapter adapter) {
        recyclerViewAddress.setVisibility(View.VISIBLE);
        recyclerViewAddress.setAdapter(adapter);
        recyclerViewAddress.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.hide();
    }

    @Override
    public void hideAddressRecyclerView() {
        recyclerViewAddress.setVisibility(View.GONE);
    }

    @Override
    public void goToAddressEditActivity(int id) {
        Intent intent = new Intent(this, EditShiippingAddressActivity.class);
        intent.putExtra("addressId", id);
        startActivity(intent);
    }

    @Override
    public void showErrorMessage(String errorMsg) {
        layoutMessage.setVisibility(View.VISIBLE);
        txtViewErrorMsg.setText(errorMsg);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                layoutMessage.setVisibility(View.GONE);
            }
        }, 3000);
    }

    @OnClick(R.id.layout_add_address) void onAddAddressClicked() {
        if (Helper.isNetworkConnected(this)) {
            Intent intent = new Intent(this, AddAddressActivity.class);
            startActivity(intent);
        } else {
            showErrorMessage("No Internet Connection");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.fetchShippingAddress();
        showLoader();
    }

}
