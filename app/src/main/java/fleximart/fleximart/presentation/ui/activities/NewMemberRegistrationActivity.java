package fleximart.fleximart.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fleximart.fleximart.R;
import fleximart.fleximart.domain.executors.impl.ThreadExecutor;
import fleximart.fleximart.presentation.presenters.NewMemberRegistrationPresenter;
import fleximart.fleximart.presentation.presenters.impl.NewMemberRegistrationPresenterImpl;
import fleximart.fleximart.presentation.ui.adapters.CityListDialogAdapter;
import fleximart.fleximart.presentation.ui.adapters.StateListDialogAdapter;
import fleximart.fleximart.presentation.ui.dialogs.GenderSelectDialog;
import fleximart.fleximart.presentation.ui.dialogs.SelectCityDialog;
import fleximart.fleximart.presentation.ui.dialogs.SelectStateDialog;
import fleximart.fleximart.threading.MainThreadImpl;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;

import java.util.Calendar;

public class NewMemberRegistrationActivity extends AppCompatActivity implements GenderSelectDialog.Callback, NewMemberRegistrationPresenter.View {

    @BindView(R.id.edit_text_name)
    EditText editTextName;
    @BindView(R.id.edit_text_email)
    EditText editTextEmail;
    @BindView(R.id.edit_text_phone)
    EditText editTextPhone;
    @BindView(R.id.edit_text_pan)
    EditText editTextTextPan;
    @BindView(R.id.edit_text_adhar)
    EditText editTextAadhar;
    @BindView(R.id.edit_text_address)
    EditText editTextAddress;
    @BindView(R.id.edit_text_landmark)
    EditText editTextLandmark;
    @BindView(R.id.edit_text_pincode)
    EditText editTextPin;
    @BindView(R.id.txt_view_gender)
    TextView txtViewGender;
    @BindView(R.id.txt_view_dob)
    TextView txtViewDOB;
    @BindView(R.id.txt_view_city_name)
    TextView txtViewCity;
    @BindView(R.id.txt_view_state)
    TextView txtViewState;
    ProgressDialog progressDialog;
    SelectStateDialog selectStateDialog;
    SelectCityDialog selectCityDialog;
    String DOB;
    String gender;
    GenderSelectDialog genderSelectDialog;
    @BindView(R.id.txt_input_name_layout)
    TextInputLayout txtInputNameLayout;
    @BindView(R.id.txt_input_pincode_layout)
    TextInputLayout txtInputPinLayout;
    @BindView(R.id.txt_input_phone_layout)
    TextInputLayout txtInputPhoneLayout;
    @BindView(R.id.txt_input_email_layout)
    TextInputLayout txtInputEmailLayout;
    NewMemberRegistrationPresenterImpl mPresenter;
    @BindView(R.id.layout_message)
    View layoutMessage;
    @BindView(R.id.txt_view_message)
    TextView txtViewMessage;
    String sponsorNode;
    int totalNode;
    String leg;
    int nodeAmount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_member_registration);
        ButterKnife.bind(this);
        sponsorNode = getIntent().getStringExtra("sponsorNode");
        totalNode = getIntent().getIntExtra("totalNode", 0);
        nodeAmount = getIntent().getIntExtra("amount", 0);
        leg = getIntent().getStringExtra("leg");
        getSupportActionBar().setTitle("Enter Member Details");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setEditTextListeners();
        setSelectCityDialog();
        setUpStateSelectDialogView();
        setGenderSelectDialog();
        setEditTextListeners();
        setUpProgressDialog();
        initialisePresenter();
        mPresenter.fetchStateList();
    }

    public void initialisePresenter() {
        mPresenter = new NewMemberRegistrationPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setEditTextListeners() {
        editTextName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                txtInputNameLayout.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        editTextEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                txtInputEmailLayout.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        editTextPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                txtInputPhoneLayout.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onGenderClicked(String gender) {
        txtViewGender.setText(gender);
        txtViewGender.setTextColor(Color.BLACK);
        if (gender.equals("Male")) {
            this.gender = "M";
        } else {
            this.gender = "F";
        }
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    public void setGenderSelectDialog() {
        genderSelectDialog = new GenderSelectDialog(this, this, this::onGenderClicked);
        genderSelectDialog.setUpDialogView();
    }

    public void setUpStateSelectDialogView() {
        selectStateDialog = new SelectStateDialog(this, this);
        selectStateDialog.setUpDialogView();
    }

    public void setSelectCityDialog() {
        selectCityDialog = new SelectCityDialog(this, this);
        selectCityDialog.setUpDialogView();
    }

    @OnClick(R.id.state_linear_layout) void onStateClicked() {
        selectStateDialog.showDialog();
    }

    @OnClick(R.id.city_linear_layout) void onCityClicked() {
        selectCityDialog.showDialog();
    }

    @OnClick(R.id.dob_linear_layout) void onDOBLayoutClicked() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        DOB = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        txtViewDOB.setText(DOB);
                        txtViewDOB.setTextColor(getResources().getColor(R.color.black1));

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
        datePickerDialog.getButton(DatePickerDialog.BUTTON_NEGATIVE).setTextColor(Color.BLACK);
        datePickerDialog.getButton(DatePickerDialog.BUTTON_POSITIVE).setTextColor(Color.BLACK);
    }

    @OnClick(R.id.gender_linear_layout) void onGenderClicked() {
        genderSelectDialog.showDialog();
    }


    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void loadStateAdapter(StateListDialogAdapter stateListDialogAdapter) {
        selectStateDialog.setRecyclerView(stateListDialogAdapter);
    }

    @Override
    public void loadCitiesAdapter(CityListDialogAdapter cityListDialogAdapter) {
        selectCityDialog.setRecyclerView(cityListDialogAdapter);
    }

    @Override
    public void setStateName(String stateName) {
        txtViewState.setText(stateName);
        txtViewState.setTextColor(this.getResources().getColor(R.color.black1));
        selectStateDialog.hideDialog();
    }

    @Override
    public void setCityName(String cityName) {
        txtViewCity.setText(cityName);
        txtViewCity.setTextColor(this.getResources().getColor(R.color.black1));
        selectCityDialog.hideDialog();
    }

    @Override
    public void hideStateRecyclerView() {
        selectStateDialog.hideRecyclerView();
    }

    @Override
    public void hideCityRecyclerView() {
        selectCityDialog.hideRecyclerView();
    }

    @Override
    public void showMessage(String errorMsg) {
        layoutMessage.setVisibility(View.VISIBLE);
        txtViewMessage.setText(errorMsg);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                layoutMessage.setVisibility(View.GONE);
            }
        }, 3000);
    }

    @Override
    public void goToOrderResponseActivity() {
        Intent intent = new Intent(this, OrderSuccessResponseActivity.class);
        intent.putExtra("amount", nodeAmount);
        intent.putExtra("node-count", totalNode);
        intent.putExtra("leg", leg);
        intent.putExtra("nodeId", sponsorNode);
        startActivity(intent);
        finish();
    }

    @Override
    public void onEmailError(String errorMsg) {
        txtInputEmailLayout.setError(errorMsg);
        txtInputEmailLayout.requestFocus();
    }

    @Override
    public void onPhoneError(String errorMsg) {
        txtInputPhoneLayout.setError(errorMsg);
        txtInputPhoneLayout.requestFocus();
    }

    @OnClick(R.id.btn_register_user)void onRegisterUserClicked() {
        if (editTextName.getText().toString().trim().isEmpty()) {
            txtInputNameLayout.setError("Name Required");
            txtInputNameLayout.requestFocus();
        } else if (editTextEmail.getText().toString().trim().isEmpty()) {
            txtInputEmailLayout.setError("Email Required");
            txtInputEmailLayout.requestFocus();
        } else if (editTextPhone.getText().toString().trim().isEmpty()) {
            txtInputPhoneLayout.setError("Phone Required ");
            txtInputPhoneLayout.requestFocus();
        } else if (editTextPhone.getText().toString().length() < 10) {
            txtInputPhoneLayout.setError("Phone Number Must Be 10 digits");
            txtInputPhoneLayout.requestFocus();
        } else if (gender == null) {
            showMessage("Please Specify Your Gender");
        } else if(!Patterns.EMAIL_ADDRESS.matcher(editTextEmail.getText().toString()).matches()) {
            txtInputEmailLayout.setError("Please Enter A Proper Email");
            txtInputEmailLayout.requestFocus();
        } else {
            showLoader();
            mPresenter.registerNewMember(1, editTextPhone.getText().toString(), sponsorNode, totalNode, leg, editTextName.getText().toString(), editTextEmail.getText().toString(), txtViewDOB.getText().toString(), gender, editTextAadhar.getText().toString(), editTextTextPan.getText().toString(), editTextPin.getText().toString(), editTextAddress.getText().toString());
        }
    }

}
