package fleximart.fleximart.presentation.ui.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import fleximart.fleximart.R;
import fleximart.fleximart.domain.model.Downline;

public class MyDownlinesAdapter extends RecyclerView.Adapter<MyDownlinesAdapter.ViewHolder> {

    Context mContext;
    Downline[] downlines;

    public MyDownlinesAdapter(Context mContext, Downline[] downlines) {
        this.mContext = mContext;
        this.downlines = downlines;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_my_dowlines, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String dateFormat = changeDataFormat(downlines[position].date);
        String dateString[] = dateFormat.split("-");
        holder.txtViewSlNo.setText(Integer.toString(position + 1));
        holder.txtViewDate.setText(dateString[0]);
        holder.txtViewTime.setText(dateString[1]);
        holder.txtViewLeftId.setText(downlines[position].leftUser);
        holder.txtViewRightUser.setText(downlines[position].rightUser);
        holder.txtViewOrderBy.setText(downlines[position].orderByName);
        holder.txtViewTransactionId.setText(downlines[position].transactionId);
        holder.txtViewParentId.setText(downlines[position].parrentId);
    }

    @Override
    public int getItemCount() {
        return downlines.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_sl_no)
        TextView txtViewSlNo;
        @BindView(R.id.txt_view_left_id)
        TextView txtViewLeftId;
        @BindView(R.id.txt_view_right_id)
        TextView txtViewRightUser;
        @BindView(R.id.txt_view_parent_id)
        TextView txtViewParentId;
        @BindView(R.id.txt_view_name)
        TextView txtViewName;
        @BindView(R.id.txt_view_order_by)
        TextView txtViewOrderBy;
        @BindView(R.id.txt_view_transaction_id)
        TextView txtViewTransactionId;
        @BindView(R.id.txt_view_date)
        TextView txtViewDate;
        @BindView(R.id.txt_view_time)
        TextView txtViewTime;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateDataSet(Downline[] newDownlines) {
        downlines = newDownlines;
        notifyDataSetChanged();
    }

    public String changeDataFormat(String date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-m-d HH:mm:ss");
        Date sourceDate = null;
        try {
            sourceDate = dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat targetFormat = new SimpleDateFormat("EEE, MMM d-HH:mm aaa");
        String targetdatevalue= targetFormat.format(sourceDate);
        return targetdatevalue;
    }

}
