package fleximart.fleximart.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fleximart.fleximart.R;
import fleximart.fleximart.domain.executors.impl.ThreadExecutor;
import fleximart.fleximart.domain.model.UserProfileData;
import fleximart.fleximart.presentation.presenters.UserProfilePresenter;
import fleximart.fleximart.presentation.presenters.impl.UserProfilePresenterImpl;
import fleximart.fleximart.presentation.ui.adapters.CityListDialogAdapter;
import fleximart.fleximart.presentation.ui.adapters.StateListDialogAdapter;
import fleximart.fleximart.presentation.ui.dialogs.GenderSelectDialog;
import fleximart.fleximart.presentation.ui.dialogs.SelectCityDialog;
import fleximart.fleximart.presentation.ui.dialogs.SelectStateDialog;
import fleximart.fleximart.threading.MainThreadImpl;
import fleximart.fleximart.util.Helper;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;

import java.util.Calendar;

public class UserProfileActivity extends AppCompatActivity implements UserProfilePresenter.View, GenderSelectDialog.Callback {

    @BindView(R.id.edit_text_name)
    EditText editTextName;
    @BindView(R.id.edit_text_email)
    EditText editTextEmail;
    @BindView(R.id.edit_text_phone)
    EditText editTextPhone;
    @BindView(R.id.edit_text_pan)
    EditText editTextTextPan;
    @BindView(R.id.edit_text_adhar)
    EditText editTextAadhar;
    @BindView(R.id.edit_text_address)
    EditText editTextAddress;
    @BindView(R.id.edit_text_landmark)
    EditText editTextLandmark;
    @BindView(R.id.edit_text_pincode)
    EditText editTextPin;
    @BindView(R.id.txt_view_gender)
    TextView txtViewGender;
    @BindView(R.id.txt_view_dob)
    TextView txtViewDOB;
    @BindView(R.id.txt_view_city_name)
    TextView txtViewCity;
    @BindView(R.id.txt_view_state)
    TextView txtViewState;
    ProgressDialog progressDialog;
    UserProfilePresenterImpl mPresenter;
    SelectStateDialog selectStateDialog;
    SelectCityDialog selectCityDialog;
    @BindView(R.id.txt_view_update_profile)
    TextView txtViewUpdateProfile;
    String DOB;
    String gender;
    GenderSelectDialog genderSelectDialog;
    @BindView(R.id.txt_input_name_layout)
    TextInputLayout txtInputNameLayout;
    @BindView(R.id.txt_input_pincode_layout)
    TextInputLayout txtInputPinLayout;
    @BindView(R.id.txt_input_address_layout)
    TextInputLayout txtInputAddressLayout;
    @BindView(R.id.layout_message)
    View layoutMessage;
    @BindView(R.id.txt_view_error_msg)
    TextView txtViewErrorMsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle((Html.fromHtml("<font color=\"#00000\"><b> My Profile </b> </font>")));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setUpProgressDialog();
        initialisePresenter();
        mPresenter.fetchUserProfile();
        mPresenter.fetchStateList();
        showLoader();
        setSelectCityDialog();
        setUpStateSelectDialogView();
        setGenderSelectDialog();
        setEditTextListeners();
    }

    public void initialisePresenter() {
        mPresenter = new UserProfilePresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setEditTextListeners() {
        editTextName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                txtInputNameLayout.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        editTextPin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                txtInputPinLayout.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        editTextAddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                txtInputAddressLayout.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    public void setGenderSelectDialog() {
        genderSelectDialog = new GenderSelectDialog(this, this, this::onGenderClicked);
        genderSelectDialog.setUpDialogView();
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void loadUserProfileData(UserProfileData userProfileData) {
        if (userProfileData.user.name != null) {
            editTextName.setText(userProfileData.user.name);
        }

        if (userProfileData.user.email != null) {
            editTextEmail.setText(userProfileData.user.email);
        }

        if (userProfileData.user.mobile != null) {
            editTextPhone.setText(userProfileData.user.mobile);
        }

        if (userProfileData.userProfile.aadhar != null) {
            editTextAadhar.setText(userProfileData.userProfile.aadhar);
        }

        if (userProfileData.userProfile.dob != null) {
            txtViewDOB.setText(userProfileData.userProfile.dob);
            DOB = userProfileData.userProfile.dob;
        }

        if (userProfileData.userProfile.gender != null) {
            if (userProfileData.userProfile.gender.equals("M")) {
                gender = "Male";
            } else {
                gender = "Female";
            }
            txtViewGender.setText(gender);
            txtViewGender.setTextColor(Color.BLACK);        }

        if (userProfileData.userProfile.pan != null) {
            editTextTextPan.setText(userProfileData.userProfile.pan);
        }

        if (userProfileData.userAddress.address != null) {
            editTextAddress.setText(userProfileData.userAddress.address);
        }

        if (userProfileData.userAddress.area != null) {
            editTextLandmark.setText(userProfileData.userAddress.area);
        }

        if (userProfileData.userAddress.pin != null) {
            editTextTextPan.setText(userProfileData.userAddress.pin);
        }

        if (userProfileData.userAddress.cityName != null) {
            txtViewCity.setText(userProfileData.userAddress.cityName);
            txtViewCity.setTextColor(this.getResources().getColor(R.color.black1));
        }

        if (userProfileData.userAddress.stateName != null) {
            txtViewState.setText(userProfileData.userAddress.stateName);
            txtViewState.setTextColor(this.getResources().getColor(R.color.black1));
        }

        if (userProfileData.userAddress.pin != null) {
            editTextPin.setText(userProfileData.userAddress.pin);
        }

        if (userProfileData.user.name == null ||
                userProfileData.user.mobile == null ||
                userProfileData.user.email == null ||
                userProfileData.userAddress.stateName == null ||
                userProfileData.userAddress.cityName == null ||
                userProfileData.userAddress.pin == null
            ) {
            txtViewUpdateProfile.setText("COMPLETE PROFILE");
        } else {
            txtViewUpdateProfile.setText("UPDATE PROFILE");
        }

    }

    public void setUpStateSelectDialogView() {
        selectStateDialog = new SelectStateDialog(this, this);
        selectStateDialog.setUpDialogView();
    }

    public void setSelectCityDialog() {
        selectCityDialog = new SelectCityDialog(this, this);
        selectCityDialog.setUpDialogView();
    }

    @Override
    public void loadStateAdapter(StateListDialogAdapter stateListDialogAdapter) {
        selectStateDialog.setRecyclerView(stateListDialogAdapter);
    }

    @Override
    public void loadCitiesAdapter(CityListDialogAdapter cityListDialogAdapter) {
        selectCityDialog.setRecyclerView(cityListDialogAdapter);
    }

    @Override
    public void setStateName(String stateName) {
        txtViewState.setText(stateName);
        txtViewState.setTextColor(this.getResources().getColor(R.color.black1));
        selectStateDialog.hideDialog();
    }

    @Override
    public void setCityName(String cityName) {
        txtViewCity.setText(cityName);
        txtViewCity.setTextColor(this.getResources().getColor(R.color.black1));
        selectCityDialog.hideDialog();
    }

    @Override
    public void hideStateRecyclerView() {
        selectStateDialog.hideRecyclerView();
    }

    @Override
    public void hideCityRecyclerView() {
        selectCityDialog.hideRecyclerView();
    }

    @Override
    public void showErrorMsg(String msg) {
        layoutMessage.setVisibility(View.VISIBLE);
        txtViewErrorMsg.setText(msg);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                layoutMessage.setVisibility(View.GONE);
            }
        }, 3000);
    }

    @OnClick(R.id.state_linear_layout) void onStateClicked() {
        selectStateDialog.showDialog();
    }

    @OnClick(R.id.city_linear_layout) void onCityClicked() {
        selectCityDialog.showDialog();
    }

    @OnClick(R.id.dob_linear_layout) void onDOBLayoutClicked() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        DOB = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        txtViewDOB.setText(DOB);
                        txtViewDOB.setTextColor(getResources().getColor(R.color.black1));

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
        datePickerDialog.getButton(DatePickerDialog.BUTTON_NEGATIVE).setTextColor(Color.BLACK);
        datePickerDialog.getButton(DatePickerDialog.BUTTON_POSITIVE).setTextColor(Color.BLACK);
    }

    @OnClick(R.id.gender_linear_layout) void onGenderClicked() {
        genderSelectDialog.showDialog();
    }

    @Override
    public void onGenderClicked(String gender) {
        txtViewGender.setText(gender);
        txtViewGender.setTextColor(Color.BLACK);
        if (gender.equals("Male")) {
            this.gender = "M";
        } else {
            this.gender = "F";
        }
    }

    @OnClick(R.id.btn_update) void onUpdateClicked() {
        if (Helper.isNetworkConnected(this)) {
            if (editTextName.getText().toString().trim().isEmpty()) {
                txtInputNameLayout.setError("Name required");
                txtInputNameLayout.requestFocus();
            } else if (editTextPin.getText().toString().trim().isEmpty()) {
                txtInputPinLayout.setError("Pin no required");
                txtInputPinLayout.requestFocus();
            } else if (editTextAddress.getText().toString().isEmpty()) {
                txtInputAddressLayout.setError("Address required");
                txtInputAddressLayout.requestFocus();
            } else if (DOB == null) {
                Toast.makeText(this, "Please enter your birthday", Toast.LENGTH_SHORT).show();
            } else if (gender == null) {
                Toast.makeText(this, "Please enter your gender", Toast.LENGTH_SHORT).show();
            } else {
                mPresenter.updateProfile(editTextName.getText().toString(), editTextEmail.getText().toString(), editTextPhone.getText().toString(), DOB, gender,editTextLandmark.getText().toString(), editTextPin.getText().toString(), editTextAddress.getText().toString());
            }
        } else {
            showErrorMsg("No Internet Connection");
        }
    }

}
