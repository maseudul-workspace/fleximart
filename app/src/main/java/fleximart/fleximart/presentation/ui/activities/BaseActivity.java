package fleximart.fleximart.presentation.ui.activities;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.android.material.navigation.NavigationView;

import androidx.drawerlayout.widget.DrawerLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import fleximart.fleximart.AndroidApplication;
import fleximart.fleximart.R;
import fleximart.fleximart.domain.model.User;
import fleximart.fleximart.domain.model.UserInfo;
import fleximart.fleximart.presentation.ui.bottomsheets.LoginBottomSheet;

public class BaseActivity extends AppCompatActivity {

    @BindView(R.id.toolbar) @Nullable
    Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle actionBarDrawerToggle;
    @BindView(R.id.main_navigation_view)
    NavigationView navigationView;
    TextView txtViewHeaderStatus;
    AndroidApplication androidApplication;
    View headerMainLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected void inflateContent(@LayoutRes int inflateResID){
        setContentView(R.layout.activity_base);
        FrameLayout contentFramelayout = (FrameLayout) findViewById(R.id.content_frame);
        getLayoutInflater().inflate(inflateResID, contentFramelayout);
        ButterKnife.bind(this);
        setToolbar();
        setHeaderText();
    }

    public void setToolbar() {
        setSupportActionBar(toolbar);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.app_name, R.string.app_name);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        setUpNavigationView();
    }

    public void setHeaderText() {
        View headerLayout = navigationView.getHeaderView(0);
        txtViewHeaderStatus = (TextView) headerLayout.findViewById(R.id.txt_view_header_status);
        headerMainLayout = (View) headerLayout.findViewById(R.id.header_main_layout);
        androidApplication = (AndroidApplication) getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(this);
        Menu navMenu = navigationView.getMenu();
        if (user == null) {
            txtViewHeaderStatus.setText("Log In  .  Sign Up");
            navMenu.findItem(R.id.nav_member_dashboard).setVisible(false);
            navMenu.findItem(R.id.nav_wallet).setVisible(false);
        } else {
            txtViewHeaderStatus.setText(user.name);
            if (user.memberStatus == 1) {
                navMenu.findItem(R.id.nav_member_dashboard).setVisible(true);
                navMenu.findItem(R.id.nav_wallet).setVisible(true);
            } else {
                navMenu.findItem(R.id.nav_member_dashboard).setVisible(false);
                navMenu.findItem(R.id.nav_wallet).setVisible(true);
            }
        }
        headerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (user == null) {
                    goToLogin();
                } else {
                    goToUserActivity();
                }
            }
        });
    }

    public void setUpNavigationView() {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch(id)
                {
                    case R.id.nav_user_account:
                        if (checkLogin()) {
                            goToUserAccount();
                        } else {
                            showBottomSheet();
                        }
                        break;
                    case R.id.nav_address:
                        if (checkLogin()) {
                            goToAddressActivity();
                        } else {
                            showBottomSheet();
                        }
                        break;
                    case R.id.nav_men:
                        break;
                    case  R.id.nav_kid:
                        break;
                    case R.id.nav_women:
                        break;
                    case R.id.nav_orders:
                        if (checkLogin()) {
                            goToMyTree();
                        } else {
                            showBottomSheet();
                        }
                        break;
                    case R.id.nav_wallet:
                        if (checkLogin()) {
                            goToWalletHistory();
                        } else {
                            showBottomSheet();
                        }
                        break;
                    case R.id.nav_member_dashboard:
                        goToMembersArea();
                        break;
                }
                return false;
            }
        });
    }

    public void goToUserAccount() {
        Intent intent = new Intent(this, UserProfileActivity.class);
        startActivity(intent);
    }

    public void goToAddressActivity() {
        Intent intent = new Intent(this, ShippingAddressActivity.class);
        startActivity(intent);
    }

    public void goToLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    public void goToUserActivity() {
        Intent intent = new Intent(this, UserActivity.class);
        startActivity(intent);
    }

    public void goToMyTree() {
        Intent intent = new Intent(this, MyTreeActivity.class);
        startActivity(intent);
    }

    public boolean checkLogin() {
        androidApplication = (AndroidApplication) getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(this);
        if (user == null) {
            return false;
        } else {
            return true;
        }
    }

    public void goToWalletHistory() {
        Intent intent = new Intent(this, MyWalletActivity.class);
        startActivity(intent);
    }

    public void goToMembersArea() {
        Intent intent = new Intent(this, MemberDashboardActivity.class);
        startActivity(intent);
    }

    public void showBottomSheet() {
        LoginBottomSheet loginBottomSheet = new LoginBottomSheet();
        loginBottomSheet.show(getSupportFragmentManager(), "");
    }

}
