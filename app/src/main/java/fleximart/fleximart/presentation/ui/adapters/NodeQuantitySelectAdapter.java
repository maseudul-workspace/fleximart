package fleximart.fleximart.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import fleximart.fleximart.R;

public class NodeQuantitySelectAdapter extends RecyclerView.Adapter<NodeQuantitySelectAdapter.ViewHolder> {

    public interface Callback {
        void onNodeSelected(int node);
    }

    Context mContext;
    Callback mCallback;

    public NodeQuantitySelectAdapter(Context mContext, Callback mCallback) {
        this.mContext = mContext;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_size_dialog, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewNode.setText(Integer.toString(position + 1));
        holder.txtViewNode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onNodeSelected(position + 1);
            }
        });
    }

    @Override
    public int getItemCount() {
        return 10;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_size)
        TextView txtViewNode;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
