package fleximart.fleximart.presentation.ui.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import fleximart.fleximart.R;
import fleximart.fleximart.domain.model.Images;
import fleximart.fleximart.util.GlideHelper;

public class ProductDetailsSlider extends PagerAdapter {

    public interface Callback {
        void onImageClicked(int position);
    }

    Context mContext;
    Images[] images;
    Callback mCallback;

    public ProductDetailsSlider(Context mContext, Images[] images, Callback callback) {
        this.mContext = mContext;
        this.images = images;
        this.mCallback = callback;
    }

    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == o;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        ImageView imageView = new ImageView(mContext);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        GlideHelper.setImageView(mContext, imageView, mContext.getResources().getString(R.string.base_url) + "/images/product/" + images[position].image);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onImageClicked(position);
            }
        });
        container.addView(imageView);
        return imageView;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

}
