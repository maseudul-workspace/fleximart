package fleximart.fleximart.presentation.ui.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import fleximart.fleximart.R;
import fleximart.fleximart.domain.model.ProductDetails;
import fleximart.fleximart.domain.model.Testing.ProductList;
import fleximart.fleximart.util.GlideHelper;

/**
 * Created by Raj on 23-07-2019.
 */

public class ProductListHorizontalAdapter extends RecyclerView.Adapter<ProductListHorizontalAdapter.ViewHolder> {

    public interface Callback {
        void onProductClicked(int id);
    }

    Context mContext;
    ProductDetails[] productDetails;
    Callback mCallback;

    public ProductListHorizontalAdapter(Context mContext, ProductDetails[] productDetails,Callback callback) {
        this.mContext = mContext;
        this.productDetails = productDetails;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_product_list_horizontal, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.txtViewProductName.setText(productDetails[i].name);
        viewHolder.txtViewProductPrice.setText("Rs. " + productDetails[i].price);
        GlideHelper.setImageView(mContext, viewHolder.imgViewPoster, "http://fleximartindia.com/images/product/thumb/" + productDetails[i].mainImage);
        viewHolder.imgViewPoster.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onProductClicked(productDetails[i].id);
            }
        });
        viewHolder.txtViewTagName.setText(productDetails[i].tagName);
        viewHolder.txtViewMRP.setText("Rs. " + productDetails[i].mrp);
        viewHolder.txtViewMRP.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
    }

    @Override
    public int getItemCount() {
        return productDetails.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_product_poster)
        ImageView imgViewPoster;
        @BindView(R.id.txt_view_product_name)
        TextView txtViewProductName;
        @BindView(R.id.txt_view_product_price)
        TextView txtViewProductPrice;
        @BindView(R.id.txt_view_tag_name)
        TextView txtViewTagName;
        @BindView(R.id.txt_view_mrp)
        TextView txtViewMRP;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
