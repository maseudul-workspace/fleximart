package fleximart.fleximart.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fleximart.fleximart.AndroidApplication;
import fleximart.fleximart.R;
import fleximart.fleximart.domain.model.UserInfo;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import org.w3c.dom.Text;

public class UserActivity extends AppCompatActivity {

    @BindView(R.id.txt_view_user_name)
    TextView txtViewUserName;
    AndroidApplication androidApplication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        getSupportActionBar().setTitle("User Activity");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        setUserName();
    }

    public void setUserName() {
        androidApplication = (AndroidApplication) getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(this);
        if (userInfo != null) {
            txtViewUserName.setText(userInfo.name);
        }
    }

    @OnClick(R.id.btn_logout) void onLogOutClicked() {
        androidApplication = (AndroidApplication) getApplicationContext();
        androidApplication.setUserInfo(this, null);
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

}
