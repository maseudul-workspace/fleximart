package fleximart.fleximart.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.blox.graphview.GraphView;
import de.blox.graphview.tree.BuchheimWalkerAlgorithm;
import de.blox.graphview.tree.BuchheimWalkerConfiguration;
import fleximart.fleximart.R;
import fleximart.fleximart.domain.executors.impl.ThreadExecutor;
import fleximart.fleximart.presentation.presenters.MyTreePresenter;
import fleximart.fleximart.presentation.presenters.impl.MyTreePresenterImpl;
import fleximart.fleximart.presentation.ui.adapters.MyTreeAdapter;
import fleximart.fleximart.threading.MainThreadImpl;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

public class MyTreeActivity extends AppCompatActivity implements MyTreePresenter.View {

    @BindView(R.id.layout_treeview)
    GraphView treeview;
    @BindView(R.id.img_view_no_record_found)
    ImageView imgViewNoRecordFound;
    MyTreePresenterImpl mPresenter;
    int nodeId;
    int payNodeId;
    int rank;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_tree);
        ButterKnife.bind(this);
        nodeId = getIntent().getIntExtra("nodeId", 0);
        payNodeId = getIntent().getIntExtra("payNodeId", 0);
        rank = getIntent().getIntExtra("rank", 0);
        setUpProgressDialog();
        initialisePresenter();
        mPresenter.fetchMyTree(nodeId, payNodeId, rank);
        getSupportActionBar().setTitle("My Tree");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        showLoader();
    }

    public void initialisePresenter() {
        mPresenter = new MyTreePresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadData(MyTreeAdapter myTreeAdapter) {
        treeview.setAdapter(myTreeAdapter);
        // set the algorithm here
        final BuchheimWalkerConfiguration configuration = new BuchheimWalkerConfiguration.Builder()
                .setSiblingSeparation(20)
                .setLevelSeparation(80)
                .setSubtreeSeparation(20)
                .setOrientation(BuchheimWalkerConfiguration.ORIENTATION_TOP_BOTTOM)
                .build();
        myTreeAdapter.setAlgorithm(new BuchheimWalkerAlgorithm(configuration));
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void goToMyTree(int nodeId, int rank) {
        Intent intent = new Intent(this, MyTreeActivity.class);
        intent.putExtra("nodeId", nodeId);
        intent.putExtra("payNodeId", payNodeId);
        intent.putExtra("rank", rank);
        startActivity(intent);
    }

    @Override
    public void onError(String errorMsg) {
        imgViewNoRecordFound.setVisibility(View.VISIBLE);
    }
}
