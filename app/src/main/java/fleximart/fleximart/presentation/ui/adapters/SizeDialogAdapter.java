package fleximart.fleximart.presentation.ui.adapters;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import fleximart.fleximart.R;
import fleximart.fleximart.domain.model.ProductDetailsSize;
import fleximart.fleximart.domain.model.Sizes;

public class SizeDialogAdapter extends RecyclerView.Adapter<SizeDialogAdapter.ViewHolder> {

    public interface Callback {
        void onSizeClicked(int id);
    }

    Context mContext;
    ProductDetailsSize[] sizes;
    Callback mCallback;

    public SizeDialogAdapter(Context mContext, ProductDetailsSize[] sizes, Callback mCallback) {
        this.mContext = mContext;
        this.sizes = sizes;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_size_dialog, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (sizes[position].stock == 0) {
            holder.txtViewSize.setTextColor(mContext.getResources().getColor(R.color.darkerGrey));
            holder.txtViewSize.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        } else {
            holder.txtViewSize.setTextColor(mContext.getResources().getColor(R.color.black1));
            holder.txtViewSize.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCallback.onSizeClicked(sizes[position].sizeId);
                }
            });
        }
        holder.txtViewSize.setText(sizes[position].sizeName);
    }

    @Override
    public int getItemCount() {
        return sizes.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_size)
        TextView txtViewSize;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

