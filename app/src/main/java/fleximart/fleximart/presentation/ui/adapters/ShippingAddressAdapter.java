package fleximart.fleximart.presentation.ui.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import fleximart.fleximart.R;
import fleximart.fleximart.domain.model.Address;

public class ShippingAddressAdapter extends RecyclerView.Adapter<ShippingAddressAdapter.ViewHolder> {

    public interface Callback {
        void onRemoveClicked(int id);
        void onEditClicked(int id);
    }

    Context mContext;
    Address[] addresses;
    Callback mCallback;

    public ShippingAddressAdapter(Context mContext, Address[] addresses, Callback mCallback) {
        this.mContext = mContext;
        this.addresses = addresses;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_shipping_address, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewUsername.setText(addresses[position].cityName);
        holder.txtViewAddress.setText(addresses[position].address);
        holder.txtViewCityPin.setText(addresses[position].cityName + " - " + addresses[position].pin);
        holder.txtViewState.setText(addresses[position].stateName);
        holder.txtViewMobile.setText("Mobile: " + addresses[position].mobile);
        holder.layoutEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onEditClicked(addresses[position].id);
            }
        });
        holder.layoutRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("Are You Sure ?");
                builder.setMessage("You are about to delete an address from your list. Do you really want to proceed ?");
                builder.setCancelable(false);
                builder.setPositiveButton(Html.fromHtml("<font color='#f4454c'>Yes</font>"), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mCallback.onRemoveClicked(addresses[position].id);
                    }
                });

                builder.setNegativeButton(Html.fromHtml("<font color='#f4454c'>No</font>"), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                builder.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return addresses.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_name)
        TextView txtViewUsername;
        @BindView(R.id.txt_view_city_pin)
        TextView txtViewCityPin;
        @BindView(R.id.txt_view_address)
        TextView txtViewAddress;
        @BindView(R.id.txt_view_mobile)
        TextView txtViewMobile;
        @BindView(R.id.txt_view_state)
        TextView txtViewState;
        @BindView(R.id.layout_edit)
        View layoutEdit;
        @BindView(R.id.layout_remove)
        View layoutRemove;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
