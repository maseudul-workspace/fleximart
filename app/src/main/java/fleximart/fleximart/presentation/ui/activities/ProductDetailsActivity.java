package fleximart.fleximart.presentation.ui.activities;

import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fleximart.fleximart.AndroidApplication;
import fleximart.fleximart.R;
import fleximart.fleximart.domain.executors.impl.ThreadExecutor;
import fleximart.fleximart.domain.model.ProductDetails;
import fleximart.fleximart.domain.model.UserInfo;
import fleximart.fleximart.presentation.presenters.ProductDetailsPresenter;
import fleximart.fleximart.presentation.presenters.impl.ProductDetailsPresenterImpl;
import fleximart.fleximart.presentation.ui.adapters.ImageZoomViewPagerAdapter;
import fleximart.fleximart.presentation.ui.adapters.ProductDetailsColorFilterAdapter;
import fleximart.fleximart.presentation.ui.adapters.ProductDetailsFilterValuesAdapter;
import fleximart.fleximart.presentation.ui.adapters.ProductDetailsSlider;
import fleximart.fleximart.presentation.ui.adapters.ProductListHorizontalAdapter;
import fleximart.fleximart.presentation.ui.adapters.SizeDialogAdapter;
import fleximart.fleximart.presentation.ui.bottomsheets.LoginBottomSheet;
import fleximart.fleximart.presentation.ui.dialogs.ImagePreviewDialog;
import fleximart.fleximart.presentation.ui.dialogs.SizesDialog;
import fleximart.fleximart.threading.MainThreadImpl;
import fleximart.fleximart.util.GlideHelper;
import fleximart.fleximart.util.Helper;

public class ProductDetailsActivity extends AppCompatActivity implements ProductDetailsPresenter.View {

    @BindView(R.id.image_slider_viewpager)
    ViewPager viewPager;
    @BindView(R.id.recycler_view_color)
    RecyclerView recyclerViewColor;
    @BindView(R.id.recycler_view_sizes)
    RecyclerView recyclerViewSizes;
    @BindView(R.id.recycler_view_related_products)
    RecyclerView recyclerViewRelatedProducts;
    @BindView(R.id.txt_view_short_desc)
    TextView txtViewShortDesc;
    @BindView(R.id.txt_view_long_desc)
    TextView txtViewLongDesc;
    @BindView(R.id.txt_view_product_name)
    TextView txtViewProductName;
    @BindView(R.id.txt_view_tag_name)
    TextView txtViewTagName;
    @BindView(R.id.dots_indicator_layout)
    LinearLayout dotsIndicatorLayout;
    @BindView(R.id.txt_selling_price)
    TextView txtViewSellingPrice;
    @BindView(R.id.txt_view_mrp)
    TextView txtViewMRP;
    @BindView(R.id.main_layout)
    View mainLayout;
    @BindView(R.id.layout_progress)
    View progressLayout;
    @BindView(R.id.btn_cart)
    Button btnCart;
    ProductDetailsPresenterImpl mPresenter;
    int productId = 0;
    SizeDialogAdapter sizeDialogAdapter;
    SizesDialog sizesDialog;
    boolean sizeSelected = false;
    ProgressDialog progressDialog;
    @BindView(R.id.txt_view_cart_amount)
    TextView txtViewCartAmount;
    @BindView(R.id.layout_message)
    View layoutMessage;
    @BindView(R.id.txt_view_error_msg)
    TextView txtViewErrorMsg;
    @BindView(R.id.img_view_heart_outline)
    ImageView imgViewHeartOutline;
    @BindView(R.id.img_view_heart_fill)
    ImageView imgViewHeartFill;
    @BindView(R.id.layout_wishlist_count)
    View layoutWishlistCount;
    @BindView(R.id.layout_cart_status)
    View layoutCartStatus;
    @BindView(R.id.txt_view_wishlist_count)
    TextView txtViewWishlistCount;
    ImagePreviewDialog imagePreviewDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        ButterKnife.bind(this);
        productId = getIntent().getIntExtra("productId", 0);
        initialisePresenter();
        mPresenter.getProductDetails(productId);
        setUpSizesDialog();
        setUpProgressDialog();
        setImagePreviewDialog();
    }

    public void setUpSizesDialog() {
        sizesDialog = new SizesDialog(this, this);
        sizesDialog.setUpDialogView();
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    public void setImagePreviewDialog() {
        imagePreviewDialog = new ImagePreviewDialog(this, this);
        imagePreviewDialog.setUpDialog();
    }

    public void initialisePresenter() {
        mPresenter = new ProductDetailsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @Override
    public void showLoader() {
        progressLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoader() {
        progressLayout.setVisibility(View.GONE);
    }

    @Override
    public void loadData(ProductDetails productDetails,
                         ProductListHorizontalAdapter adapter,
                         ProductDetailsColorFilterAdapter colorFilterAdapter,
                         ProductDetailsFilterValuesAdapter sizesAdapter,
                         SizeDialogAdapter sizeDialogAdapter,
                         ProductDetailsSlider slider,
                         ImageZoomViewPagerAdapter imageZoomViewPagerAdapter,
                         int imageCount) {

        mainLayout.setVisibility(View.VISIBLE);
        btnCart.setVisibility(View.VISIBLE);

        txtViewProductName.setText(productDetails.name);
        txtViewTagName.setText(productDetails.tagName);
        txtViewShortDesc.setText(Html.fromHtml(productDetails.shortDescription));
        txtViewLongDesc.setText(Html.fromHtml(productDetails.longDescription));

        txtViewSellingPrice.setText("Rs. " + productDetails.price);

        txtViewMRP.setText("Rs. " + productDetails.mrp);

        txtViewMRP.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);

        if (productDetails.isWishlistPresent) {
            imgViewHeartFill.setVisibility(View.VISIBLE);
        } else {
            imgViewHeartOutline.setVisibility(View.VISIBLE);
        }

        recyclerViewColor.setAdapter(colorFilterAdapter);
        recyclerViewColor.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));

        recyclerViewSizes.setAdapter(sizesAdapter);
        recyclerViewSizes.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));

        recyclerViewRelatedProducts.setAdapter(adapter);
        recyclerViewRelatedProducts.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));

        viewPager.setAdapter(slider);

        prepareDotsIndicator(0, imageCount);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                prepareDotsIndicator(position, imageCount);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        sizesDialog.setRecyclerView(sizeDialogAdapter);

        imagePreviewDialog.setViewPager2(imageZoomViewPagerAdapter, imageCount);

    }

    @Override
    public void disimisSizeDialog() {
        sizesDialog.hideDialog();
    }

    @Override
    public void onSizeSelected() {
        sizeSelected = true;
    }

    @Override
    public void hideDialogLoader() {
        progressDialog.hide();
    }

    @Override
    public void showDialogLoader() {
        progressDialog.show();
    }

    @Override
    public void setCartCount(int count) {
        if (count == 0) {
            layoutCartStatus.setVisibility(View.GONE);
        } else {
            txtViewCartAmount.setText(Integer.toString(count));
            layoutCartStatus.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showErrorMessage(String errorMsg) {
        layoutMessage.setVisibility(View.VISIBLE);
        txtViewErrorMsg.setText(errorMsg);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                layoutMessage.setVisibility(View.GONE);
            }
        }, 3000);
    }

    @Override
    public void onAddToWishlistSuccess() {
        imgViewHeartFill.setVisibility(View.VISIBLE);
        imgViewHeartOutline.setVisibility(View.GONE);
    }

    @Override
    public void onRemoveFromWishlistSuccess() {
        imgViewHeartFill.setVisibility(View.GONE);
        imgViewHeartOutline.setVisibility(View.VISIBLE);
    }

    @Override
    public void showBottomSheet() {
        LoginBottomSheet loginBottomSheet = new LoginBottomSheet();
        loginBottomSheet.show(getSupportFragmentManager(), "");
    }

    @Override
    public void setWishlistCount(int count) {
        if (count == 0) {
            layoutWishlistCount.setVisibility(View.GONE);
        } else {
            txtViewWishlistCount.setText(Integer.toString(count));
            layoutWishlistCount.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showZoomPreviewDialog(int position) {
        imagePreviewDialog.showDialog(position);
    }


    public void prepareDotsIndicator(int sliderPosition, int imageCount){
        if(dotsIndicatorLayout.getChildCount() > 0){
            dotsIndicatorLayout.removeAllViews();
        }

        ImageView dots[] = new ImageView[imageCount];
        for(int i = 0; i < imageCount; i++){
            dots[i] = new ImageView(this);
            if(i == sliderPosition){
                GlideHelper.setImageViewWithDrawable(this, dots[i], R.drawable.active_dot);
            }else{
                GlideHelper.setImageViewWithDrawable(this, dots[i], R.drawable.inactive_dot);
            }
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(15, 15);
            layoutParams.setMargins(5, 0, 5, 0);
            dotsIndicatorLayout.addView(dots[i], layoutParams);
        }
    }

    @OnClick(R.id.btn_cart) void addToCartClicked() {
        if (Helper.isNetworkConnected(this)) {
            if (checkLogin()) {
                if (sizeSelected) {
                    mPresenter.addToCart();
                    showDialogLoader();
                } else {
                    sizesDialog.showDialog();
                }
            } else {
                goToLogin();
            }
        } else {
           showErrorMessage("No Internet Connection");
        }
    }

    @OnClick(R.id.layout_cart) void onCartClicked() {
        if (Helper.isNetworkConnected(this)) {
            if (checkLogin()) {
                Intent intent = new Intent(this, CartListActivity.class);
                startActivity(intent);
            } else {
                showBottomSheet();
            }
        } else {
            showErrorMessage("No Internet Connection");
        }
    }

    public boolean checkLogin() {
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(this);
        if (user == null) {
            return false;
        } else {
            return true;
        }
    }

    public void goToLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.layout_wishlist) void onWishlistClicked() {
        if (checkLogin()) {
            if (Helper.isNetworkConnected(this)) {
                Intent intent = new Intent(this, WishlistActivity.class);
                startActivity(intent);
            } else {
                showErrorMessage("No Internet Connection");
            }
        } else {
            showBottomSheet();
        }
    }

    @OnClick(R.id.img_view_heart_fill) void onHeartFillClicked() {
        if (productId != 0) {
           mPresenter.removeFromWishlist(productId);
        }
    }

    @OnClick(R.id.img_view_heart_outline) void onHeartOutlineClicked() {
        if (productId != 0) {
            mPresenter.addToWishlist(productId);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.fetchCartCount();
        mPresenter.fetchWishlist();
    }

}
