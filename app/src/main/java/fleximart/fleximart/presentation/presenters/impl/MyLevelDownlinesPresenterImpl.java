package fleximart.fleximart.presentation.presenters.impl;

import android.content.Context;

import fleximart.fleximart.AndroidApplication;
import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.FetchLevelDowlinesInteractor;
import fleximart.fleximart.domain.interactors.impl.FetchLevelDownlinesInteractorImpl;
import fleximart.fleximart.domain.model.LevelDownlines;
import fleximart.fleximart.domain.model.UserInfo;
import fleximart.fleximart.presentation.presenters.MyLevelDownlinesPresenter;
import fleximart.fleximart.presentation.presenters.base.AbstractPresenter;
import fleximart.fleximart.presentation.ui.adapters.MyLevelDownlinesAdapter;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class MyLevelDownlinesPresenterImpl extends AbstractPresenter implements MyLevelDownlinesPresenter, FetchLevelDowlinesInteractor.Callback {

    Context mContext;
    MyLevelDownlinesPresenter.View mView;
    FetchLevelDownlinesInteractorImpl fetchLevelDownlinesInteractor;
    AndroidApplication androidApplication;

    public MyLevelDownlinesPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchLevelDownlines(int nodeId, int levelId) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo !=  null) {
            fetchLevelDownlinesInteractor = new FetchLevelDownlinesInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, nodeId, levelId);
            fetchLevelDownlinesInteractor.execute();
        }
    }

    @Override
    public void onFetchLevelDownlinesSuccess(LevelDownlines[] levelDownlines) {
        if (levelDownlines.length == 0) {
            mView.showMessage("No Record Found");
        } else {
            MyLevelDownlinesAdapter adapter = new MyLevelDownlinesAdapter(levelDownlines);
            mView.loadAdapter(adapter);
        }
        mView.hideLoader();
    }

    @Override
    public void onFetchLevelDownlinesFail(String errorMsg, int loginError) {
        mView.showMessage(errorMsg);
        mView.hideLoader();
    }
}
