package fleximart.fleximart.presentation.presenters;

import fleximart.fleximart.presentation.ui.adapters.CartDetailsAdapter;
import fleximart.fleximart.presentation.ui.adapters.CartShippingAddressAdapter;

public interface CartDetailsPresenter {
    void fetchCartDetails();
    void fetchShippingAddressList();
    interface View {
        void loadAdapter(CartDetailsAdapter adapter, int cartCount, double totalMRP, double totalPrice, double totalDiscount);
        void showLoader();
        void hideLoader();
        void loadShippingAddressAdapter(CartShippingAddressAdapter cartShippingAddressAdapter);
        void onShippingAddressEditClicked(int addressId);
        void onAddressSelected(int addressId);
        void goToCheckoutActivity();
        void showCartEmptyLayout();
        void showErrorMessage(String errorMsg);
    }
}
