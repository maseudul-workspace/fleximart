package fleximart.fleximart.presentation.presenters;

import fleximart.fleximart.domain.model.OrderPlaceData;
import fleximart.fleximart.presentation.ui.adapters.WalletAdapter;

public interface MyWalletPresenter {
    void fetchWalletAmount();
    void fetchWalletHistory(int pageNo);
    void addWalletMoney(String amount);
    interface View {
        void loadAdapter(WalletAdapter walletAdapter, int totalPage, int currentPage);
        void loadWalletAmount(double amount);
        void showLoader();
        void hideLoader();
        void showErrorMessage(String errorMsg);
        void goToPaymentActivity(OrderPlaceData orderPlaceData);
    }
}
