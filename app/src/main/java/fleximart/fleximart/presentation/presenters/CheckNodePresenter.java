package fleximart.fleximart.presentation.presenters;

import fleximart.fleximart.domain.model.NodeDetails;

public interface CheckNodePresenter {
    void checkNode(String nodeId);
    void registerNodes(String node, int totalNode, String leg);
    void getWalletDetails();
    interface View {
        void loadData(NodeDetails nodeDetails);
        void onGettingDataFailed();
        void showLoader();
        void hideLoader();
        void showMessage(String errorMsg);
        void loadWalletAmount(Double amount);
        void goToOrderResponseActivity();
    }
}
