package fleximart.fleximart.presentation.presenters.impl;

import android.content.Context;

import fleximart.fleximart.AndroidApplication;
import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.FetchMemberDashboardSummaryInteractor;
import fleximart.fleximart.domain.interactors.impl.FetchMemberDashboardSummaryInteractorImpl;
import fleximart.fleximart.domain.model.DashboardData;
import fleximart.fleximart.domain.model.UserInfo;
import fleximart.fleximart.presentation.presenters.MembersDashboardPresenter;
import fleximart.fleximart.presentation.presenters.base.AbstractPresenter;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class MemberDashboardPresenterImpl extends AbstractPresenter implements MembersDashboardPresenter, FetchMemberDashboardSummaryInteractor.Callback {

    Context mContext;
    MembersDashboardPresenter.View mView;
    AndroidApplication androidApplication;
    FetchMemberDashboardSummaryInteractorImpl fetchMemberDashboardSummaryInteractor;

    public MemberDashboardPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchSummary() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            fetchMemberDashboardSummaryInteractor = new FetchMemberDashboardSummaryInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.id);
            fetchMemberDashboardSummaryInteractor.execute();
        }
    }

    @Override
    public void onSummaryFetchSuccess(DashboardData dashboardData) {
        mView.loadDasboardSummary(dashboardData);
        mView.hideLoader();
    }

    @Override
    public void onSummaryFetchFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if (loginError == 0) {
            mView.showErrorMsg(errorMsg);
        }
    }
}
