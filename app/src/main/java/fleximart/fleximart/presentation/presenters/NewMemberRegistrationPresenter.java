package fleximart.fleximart.presentation.presenters;

import fleximart.fleximart.domain.model.UserProfileData;
import fleximart.fleximart.presentation.ui.adapters.CityListDialogAdapter;
import fleximart.fleximart.presentation.ui.adapters.StateListDialogAdapter;

public interface NewMemberRegistrationPresenter {
    void fetchStateList();
    void fetchCityList();
    void registerNewMember(int registrationMethod,
                           String phoneNo,
                           String sponsorNode,
                           int totalNode,
                           String leg,
                           String name,
                           String email,
                           String DOB,
                           String gender,
                           String aadhar,
                           String pan,
                           String pin,
                           String address);
    interface View {
        void showLoader();
        void hideLoader();
        void loadStateAdapter(StateListDialogAdapter stateListDialogAdapter);
        void loadCitiesAdapter(CityListDialogAdapter cityListDialogAdapter);
        void setStateName(String stateName);
        void setCityName(String cityName);
        void hideStateRecyclerView();
        void hideCityRecyclerView();
        void showMessage(String error);
        void goToOrderResponseActivity();
        void onEmailError(String errorMsg);
        void onPhoneError(String errorMsg);
    }
}
