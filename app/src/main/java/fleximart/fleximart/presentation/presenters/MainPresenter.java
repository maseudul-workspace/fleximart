package fleximart.fleximart.presentation.presenters;

import fleximart.fleximart.domain.model.MainData;

public interface MainPresenter {
    void fetchMainData();
    void fetchCartDetails();
    void fetchWishlist();
    interface View {
        void loadMainData(MainData mainData);
        void loadCartCount(int count);
        void loadWishlistCount(int count);
    }
}
