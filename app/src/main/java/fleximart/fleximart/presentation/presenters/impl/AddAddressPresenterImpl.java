package fleximart.fleximart.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import fleximart.fleximart.AndroidApplication;
import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.AddAddressInteractor;
import fleximart.fleximart.domain.interactors.FetchCitiesInteractor;
import fleximart.fleximart.domain.interactors.FetchStateListInteractor;
import fleximart.fleximart.domain.interactors.impl.AddAddressInteractorImpl;
import fleximart.fleximart.domain.interactors.impl.FetchCitiesInteractorImpl;
import fleximart.fleximart.domain.interactors.impl.FetchStateListInteractorImpl;
import fleximart.fleximart.domain.model.Cities;
import fleximart.fleximart.domain.model.StateList;
import fleximart.fleximart.domain.model.UserInfo;
import fleximart.fleximart.presentation.presenters.AddAddressPresenter;
import fleximart.fleximart.presentation.presenters.base.AbstractPresenter;
import fleximart.fleximart.presentation.ui.adapters.CityListDialogAdapter;
import fleximart.fleximart.presentation.ui.adapters.StateListDialogAdapter;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class AddAddressPresenterImpl extends AbstractPresenter implements AddAddressPresenter,
                                                                            FetchStateListInteractor.Callback,
                                                                            StateListDialogAdapter.Callback,
                                                                            FetchCitiesInteractor.Callback,
                                                                            CityListDialogAdapter.Callback,
                                                                            AddAddressInteractor.Callback
                                                                            {

    Context mContext;
    AddAddressPresenter.View mView;
    FetchStateListInteractorImpl fetchStateListInteractor;
    StateListDialogAdapter stateListDialogAdapter;
    FetchCitiesInteractorImpl fetchCitiesInteractor;
    int stateId = 0;
    CityListDialogAdapter cityListDialogAdapter;
    AndroidApplication androidApplication;
    AddAddressInteractorImpl addAddressInteractor;
    int cityId = 0;

    public AddAddressPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchStateList() {
        fetchStateListInteractor = new FetchStateListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this);
        fetchStateListInteractor.execute();
    }

    @Override
    public void fetchCityList() {
        fetchCitiesInteractor = new FetchCitiesInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, stateId);
        fetchCitiesInteractor.execute();
    }

    @Override
    public void addAddress(String name, String email, String mobile, String alternativeMobile, String landmark, String pin, String address) {
        if (stateId == 0) {
            Toast.makeText(mContext, "Please select a state", Toast.LENGTH_SHORT).show();
        } else if (cityId == 0) {
            Toast.makeText(mContext, "Please select a city", Toast.LENGTH_SHORT).show();
        } else {
            androidApplication = (AndroidApplication) mContext.getApplicationContext();
            UserInfo userInfo = androidApplication.getUserInfo(mContext);
            addAddressInteractor = new AddAddressInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.id, name, email, mobile, alternativeMobile, landmark, stateId, cityId, pin, address);
            addAddressInteractor.execute();
        }
    }

    @Override
    public void onGettingStateListSuccess(StateList[] states) {
        if (states.length == 0) {
            mView.hideStateRecyclerView();
        } else {
            stateListDialogAdapter = new StateListDialogAdapter(mContext, states, this::onStateClicked);
            mView.loadStateAdapter(stateListDialogAdapter);
        }
    }

    @Override
    public void onGettingStateListFail() {
        mView.hideStateRecyclerView();
    }

    @Override
    public void onStateClicked(int id, String stateName) {
        stateId = id;
        mView.setStateName(stateName);
        fetchCityList();
    }

    @Override
    public void onGettingCitiesSuccess(Cities[] cities) {
        if (cities.length == 0) {
            mView.hideCityRecyclerView();
        } else {
            cityListDialogAdapter = new CityListDialogAdapter(mContext, cities, this::onCityClicked);
            mView.loadCitiesAdapter(cityListDialogAdapter);
        }
    }

    @Override
    public void onGettingCitiesFail() {
        mView.hideCityRecyclerView();
    }

    @Override
    public void onCityClicked(int id, String city) {
        mView.setCityName(city);
        cityId = id;
    }

    @Override
    public void onAddAddressSuccess() {
        Toasty.info(mContext, "Address Saved Successfully", Toast.LENGTH_SHORT).show();
        mView.hideLoader();
        mView.onAddAddressSuccess();
    }

    @Override
    public void onAddAddressFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }
}
