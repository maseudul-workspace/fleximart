package fleximart.fleximart.presentation.presenters;

import fleximart.fleximart.presentation.ui.adapters.MyTreeAdapter;

public interface MyTreePresenter {
    void fetchMyTree(int nodeId, int payNodeId, int rank);
    interface View {
        void loadData(MyTreeAdapter myTreeAdapter);
        void showLoader();
        void hideLoader();
        void goToMyTree(int payNodeId, int rank);
        void onError(String errorMsg);
    }
}
