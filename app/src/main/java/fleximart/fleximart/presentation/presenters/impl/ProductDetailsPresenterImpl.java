package fleximart.fleximart.presentation.presenters.impl;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import fleximart.fleximart.AndroidApplication;
import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.AddToCartInteractor;
import fleximart.fleximart.domain.interactors.AddToWishlistInteractor;
import fleximart.fleximart.domain.interactors.FetchCartDetailsInteractor;
import fleximart.fleximart.domain.interactors.GetProductDetailsInteractor;
import fleximart.fleximart.domain.interactors.impl.AddToCartInteractorImpl;
import fleximart.fleximart.domain.interactors.impl.AddToWishlistInteractorImpl;
import fleximart.fleximart.domain.interactors.impl.FetchCartDetailsInteractorImpl;
import fleximart.fleximart.domain.interactors.impl.FetchWishlistInteractorImpl;
import fleximart.fleximart.domain.interactors.impl.GetProductDetailsInteractorImpl;
import fleximart.fleximart.domain.interactors.impl.RemoveFromWishlistInteratorImpl;
import fleximart.fleximart.domain.model.CartDetails;
import fleximart.fleximart.domain.model.Color;
import fleximart.fleximart.domain.model.ProductDetailsData;
import fleximart.fleximart.domain.model.ProductDetailsSize;
import fleximart.fleximart.domain.model.Sizes;
import fleximart.fleximart.domain.model.UserInfo;
import fleximart.fleximart.domain.model.WishlistProducts;
import fleximart.fleximart.presentation.presenters.ProductDetailsPresenter;
import fleximart.fleximart.presentation.presenters.base.AbstractPresenter;
import fleximart.fleximart.presentation.ui.adapters.ImageZoomViewPagerAdapter;
import fleximart.fleximart.presentation.ui.adapters.ProductDetailsColorFilterAdapter;
import fleximart.fleximart.presentation.ui.adapters.ProductDetailsFilterValuesAdapter;
import fleximart.fleximart.presentation.ui.adapters.ProductDetailsSlider;
import fleximart.fleximart.presentation.ui.adapters.ProductListHorizontalAdapter;
import fleximart.fleximart.presentation.ui.adapters.SizeDialogAdapter;
import fleximart.fleximart.repository.AppRepositoryImpl;
import fleximart.fleximart.util.Helper;

public class ProductDetailsPresenterImpl extends AbstractPresenter implements   ProductDetailsPresenter,
                                                                                GetProductDetailsInteractor.Callback,
                                                                                ProductListHorizontalAdapter.Callback,
                                                                                ProductDetailsColorFilterAdapter.Callback,
                                                                                ProductDetailsFilterValuesAdapter.Callback,
                                                                                SizeDialogAdapter.Callback,
                                                                                AddToCartInteractor.Callback,
                                                                                FetchCartDetailsInteractor.Callback,
                                                                                AddToWishlistInteractor.Callback,
                                                                                RemoveFromWishlistInteratorImpl.Callback,
                                                                                FetchWishlistInteractorImpl.Callback,
                                                                                ProductDetailsSlider.Callback
                                                                                {

    Context mContext;
    ProductDetailsPresenter.View mView;
    GetProductDetailsInteractorImpl mInteractor;
    Color[] colors;
    ProductDetailsSize[] sizes;
    ProductDetailsColorFilterAdapter colorFilterAdapter;
    ProductDetailsFilterValuesAdapter sizesAdapter;
    SizeDialogAdapter sizeDialogAdapter;
    AddToCartInteractorImpl addToCartInteractor;
    int sizeId;
    int colorId;
    AndroidApplication androidApplication;
    int productId;
    FetchCartDetailsInteractorImpl fetchCartDetailsInteractor;
    AddToWishlistInteractorImpl addToWishlistInteractor;
    RemoveFromWishlistInteratorImpl removeFromWishlistInterator;
    FetchWishlistInteractorImpl fetchWishlistInteractor;
    ProductDetailsSlider productDetailsSlider;

    public ProductDetailsPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void getProductDetails(int productId) {
        mInteractor = new GetProductDetailsInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, productId);
        mInteractor.execute();
    }

    @Override
    public void addToCart() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        addToCartInteractor = new AddToCartInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.id, productId, 1, colorId, sizeId);
        addToCartInteractor.execute();
    }

    @Override
    public void fetchCartCount() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            fetchCartDetailsInteractor = new FetchCartDetailsInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.id, userInfo.apiToken);
            fetchCartDetailsInteractor.execute();
        }
    }

    @Override
    public void fetchWishlist() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            fetchWishlistInteractor = new FetchWishlistInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.id, userInfo.apiToken);
            fetchWishlistInteractor.execute();
        }
    }

    @Override
    public void addToWishlist(int productId) {
        if (Helper.isNetworkConnected(mContext)) {
            AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
            UserInfo userInfo = androidApplication.getUserInfo(mContext);
            if (userInfo != null) {
                mView.showDialogLoader();
                addToWishlistInteractor = new AddToWishlistInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl(), userInfo.apiToken, userInfo.id, productId);
                addToWishlistInteractor.execute();
            } else {
                mView.showBottomSheet();
            }
        } else {
            mView.showErrorMessage("No Internet Connection");
        }
    }

    @Override
    public void removeFromWishlist(int productId) {
        if (Helper.isNetworkConnected(mContext)) {
            AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
            WishlistProducts[] wishlistProducts = androidApplication.getWishlistProducts(mContext);
            int wishlistId = 0;
            for (int i = 0; i < wishlistProducts.length; i++) {
                if (wishlistProducts[i].productId == productId) {
                    wishlistId = wishlistProducts[i].id;
                    break;
                }
            }
            UserInfo userInfo = androidApplication.getUserInfo(mContext);
            if (userInfo != null) {
                mView.showDialogLoader();
                removeFromWishlistInterator = new RemoveFromWishlistInteratorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl(), userInfo.apiToken, wishlistId);
                removeFromWishlistInterator.execute();
            } else {
                mView.showBottomSheet();
            }
        } else {
            mView.showErrorMessage("No Internet Connection");
        }
    }

    @Override
    public void onGettingProductDetailsSuccess(ProductDetailsData productDetailsData) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        WishlistProducts[] wishlistProducts = androidApplication.getWishlistProducts(mContext);
        if (wishlistProducts != null) {
            for (int i = 0; i < wishlistProducts.length; i++) {
                if (productDetailsData.productDetails.id == wishlistProducts[i].productId) {
                    productDetailsData.productDetails.isWishlistPresent = true;
                    break;
                }
            }
        }
        ProductListHorizontalAdapter adapter = new ProductListHorizontalAdapter(mContext, productDetailsData.products, this::getProductDetails);
        productId = productDetailsData.productDetails.id;
        colors = productDetailsData.colors;
        sizes = productDetailsData.sizes;
        colors[0].isSelected = true;
//        colorId = colors[0].id;
        colorFilterAdapter = new ProductDetailsColorFilterAdapter(mContext, colors, this);
        sizesAdapter = new ProductDetailsFilterValuesAdapter(mContext, sizes, this);
        sizeDialogAdapter = new SizeDialogAdapter(mContext, sizes, this);
        productDetailsSlider = new ProductDetailsSlider(mContext, productDetailsData.images, this);
        ImageZoomViewPagerAdapter imageZoomViewPagerAdapter = new ImageZoomViewPagerAdapter(mContext, productDetailsData.images);
        mView.loadData(productDetailsData.productDetails, adapter, colorFilterAdapter, sizesAdapter, sizeDialogAdapter, productDetailsSlider, imageZoomViewPagerAdapter, productDetailsData.images.length);
        mView.hideLoader();
    }

    @Override
    public void onProductDetailsFail(String errorMsg) {
        mView.hideLoader();
    }

    @Override
    public void onProductClicked(int id) {

    }

    @Override
    public void onColorSelected(int id) {
        for (int i = 0; i < colors.length; i++) {
            if (colors[i].id == id) {
                colors[i].isSelected = true;
            } else {
                colors[i].isSelected = false;
            }
        }
        colorFilterAdapter.updateDataSet(colors);
        colorId = id;
    }

    @Override
    public void onFilterSelect(int id) {
        for (int i = 0; i < sizes.length; i++) {
            if (sizes[i].sizeId == id) {
                sizes[i].isSelected = true;
            } else {
               sizes[i].isSelected = false;
            }
        }
        sizeId = id;
        sizesAdapter.updateDataSet(sizes);
        mView.onSizeSelected();
    }

    @Override
    public void onSizeClicked(int id) {
        sizeId = id;
        addToCart();
        mView.disimisSizeDialog();
        mView.showDialogLoader();

    }

    @Override
    public void onAddToCartSuccess() {
        mView.showErrorMessage("Successfully Added To Cart");
        mView.hideDialogLoader();
        fetchCartCount();
    }

    @Override
    public void onAddToCartFail(String errorMsg) {
        mView.showErrorMessage(errorMsg);
        mView.hideDialogLoader();
    }

    @Override
    public void onGettingCartDetailsSucess(CartDetails[] cartDetails) {
        mView.setCartCount(cartDetails.length);
    }

    @Override
    public void onGettingCartDetailsFail(String errorMsg) {
        mView.setCartCount(0);
    }

    @Override
    public void onAddToWishlistSuccess() {
        mView.hideDialogLoader();
        mView.showErrorMessage("Added To Wishlist");
        mView.onAddToWishlistSuccess();
        fetchWishlist();
    }

    @Override
    public void onAddToWishlistFail(String errorMsg, int loginError) {
        mView.hideDialogLoader();
        if (loginError == 0) {
            mView.showErrorMessage(errorMsg);
        }
    }

    @Override
    public void onWishlistFetchSuccess(WishlistProducts[] wishlistProducts) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setWishlistProducts(mContext, wishlistProducts);
        mView.setWishlistCount(wishlistProducts.length);
    }

    @Override
    public void onWishlistFetchFail(String errorMsg, int loginError) {
        if (loginError == 0) {
            AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
            androidApplication.setWishlistProducts(mContext, null);
            mView.setWishlistCount(0);
        }
    }

    @Override
    public void onWishlistRemoveSuccess() {
        mView.hideDialogLoader();
        mView.showErrorMessage("Removed From Wishlist");
        mView.onRemoveFromWishlistSuccess();
        fetchWishlist();
    }

    @Override
    public void onWishlistRemoveFail(String errorMsg, int loginError) {
        mView.hideDialogLoader();
        if (loginError == 0) {
            mView.showErrorMessage(errorMsg);
        }
    }

    @Override
    public void onImageClicked(int position) {
        mView.showZoomPreviewDialog(position);
    }
}
