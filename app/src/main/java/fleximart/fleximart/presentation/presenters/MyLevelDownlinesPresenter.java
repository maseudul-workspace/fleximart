package fleximart.fleximart.presentation.presenters;

import fleximart.fleximart.presentation.ui.adapters.MyLevelDownlinesAdapter;

public interface MyLevelDownlinesPresenter {
    void fetchLevelDownlines(int nodeId, int levelId);
    interface View {
        void loadAdapter(MyLevelDownlinesAdapter adapter);
        void showLoader();
        void hideLoader();
        void showMessage(String errorMsg);
    }
}
