package fleximart.fleximart.presentation.presenters.impl;

import android.content.Context;

import fleximart.fleximart.AndroidApplication;
import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.BuyNodeInteractor;
import fleximart.fleximart.domain.interactors.FetchCitiesInteractor;
import fleximart.fleximart.domain.interactors.FetchStateListInteractor;
import fleximart.fleximart.domain.interactors.impl.BuyNodeInteractorImpl;
import fleximart.fleximart.domain.interactors.impl.FetchCitiesInteractorImpl;
import fleximart.fleximart.domain.interactors.impl.FetchStateListInteractorImpl;
import fleximart.fleximart.domain.model.Cities;
import fleximart.fleximart.domain.model.ErrorMesage;
import fleximart.fleximart.domain.model.StateList;
import fleximart.fleximart.domain.model.UserInfo;
import fleximart.fleximart.presentation.presenters.NewMemberRegistrationPresenter;
import fleximart.fleximart.presentation.presenters.base.AbstractPresenter;
import fleximart.fleximart.presentation.ui.adapters.CityListDialogAdapter;
import fleximart.fleximart.presentation.ui.adapters.StateListDialogAdapter;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class NewMemberRegistrationPresenterImpl extends AbstractPresenter implements    NewMemberRegistrationPresenter,
                                                                                        FetchStateListInteractor.Callback,
                                                                                        FetchCitiesInteractor.Callback,
                                                                                        StateListDialogAdapter.Callback,
                                                                                        CityListDialogAdapter.Callback,
                                                                                        BuyNodeInteractor.Callback
{

    Context mContext;
    NewMemberRegistrationPresenter.View mView;
    StateListDialogAdapter stateListDialogAdapter;
    FetchCitiesInteractorImpl fetchCitiesInteractor;
    FetchStateListInteractorImpl fetchStateListInteractor;
    CityListDialogAdapter cityListDialogAdapter;
    int stateId;
    int cityId;
    AndroidApplication androidApplication;
    BuyNodeInteractorImpl buyNodeInteractor;

    public NewMemberRegistrationPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchStateList() {
        fetchStateListInteractor = new FetchStateListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this);
        fetchStateListInteractor.execute();
    }

    @Override
    public void fetchCityList() {
        fetchCitiesInteractor = new FetchCitiesInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, stateId);
        fetchCitiesInteractor.execute();
    }

    @Override
    public void registerNewMember(int registrationMethod, String phoneNo, String sponsorNode, int totalNode, String leg, String name, String email, String DOB, String gender, String aadhar, String pan, String pin, String address) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            buyNodeInteractor = new BuyNodeInteractorImpl(mExecutor,mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.id, registrationMethod, sponsorNode, totalNode, leg, name, email, DOB, gender, stateId, cityId, aadhar, pan, pin, address, phoneNo);
            buyNodeInteractor.execute();
        }
    }


    @Override
    public void onGettingCitiesSuccess(Cities[] cities) {
        if (cities.length == 0) {
            mView.hideCityRecyclerView();
        } else {
            cityListDialogAdapter = new CityListDialogAdapter(mContext, cities, this::onCityClicked);
            mView.loadCitiesAdapter(cityListDialogAdapter);
        }
    }

    @Override
    public void onGettingCitiesFail() {
        mView.hideCityRecyclerView();
    }

    @Override
    public void onGettingStateListSuccess(StateList[] states) {
        if (states.length == 0) {
            mView.hideStateRecyclerView();
        } else {
            stateListDialogAdapter = new StateListDialogAdapter(mContext, states, this::onStateClicked);
            mView.loadStateAdapter(stateListDialogAdapter);
        }
    }

    @Override
    public void onGettingStateListFail() {
        mView.hideStateRecyclerView();
    }

    @Override
    public void onCityClicked(int id, String city) {
        mView.setCityName(city);
        cityId = id;
    }

    @Override
    public void onStateClicked(int id, String state) {
        cityId = 0;
        stateId = id;
        mView.setCityName("Select City");
        mView.setStateName(state);
        fetchCityList();
    }

    @Override
    public void onBuyNodeSuccess() {
        mView.hideLoader();
        mView.goToOrderResponseActivity();
    }

    @Override
    public void onBuyNodeFail(String errorMsg, int loginError, boolean error, ErrorMesage errorMesage) {
        mView.hideLoader();
        if (error) {
            if (errorMesage.email != null) {
                mView.onEmailError(errorMesage.email[0]);
            }
            if (errorMesage.mobile != null) {
                mView.onPhoneError(errorMesage.mobile[0]);
            }
        } else {
            mView.showMessage(errorMsg);
        }
    }
}
