package fleximart.fleximart.presentation.presenters.impl;

import android.content.Context;

import fleximart.fleximart.AndroidApplication;
import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.FetchShippingAddressListInteractor;
import fleximart.fleximart.domain.interactors.RemoveShippingAddressInteractor;
import fleximart.fleximart.domain.interactors.impl.FetchShippingAddressListInteractorImpl;
import fleximart.fleximart.domain.interactors.impl.RemoveShippingAddressInteractorImpl;
import fleximart.fleximart.domain.model.Address;
import fleximart.fleximart.domain.model.UserInfo;
import fleximart.fleximart.presentation.presenters.ShippingAddressPresenter;
import fleximart.fleximart.presentation.presenters.base.AbstractPresenter;
import fleximart.fleximart.presentation.ui.adapters.ShippingAddressAdapter;
import fleximart.fleximart.repository.AppRepositoryImpl;
import fleximart.fleximart.util.Helper;

public class ShippingAddressPresenterImpl extends AbstractPresenter implements ShippingAddressPresenter,
                                                                                FetchShippingAddressListInteractor.Callback,
                                                                                ShippingAddressAdapter.Callback,

        RemoveShippingAddressInteractor.Callback
{

    Context mContext;
    ShippingAddressPresenter.View mView;
    ShippingAddressAdapter shippingAddressAdapter;
    AndroidApplication androidApplication;
    FetchShippingAddressListInteractorImpl fetchShippingAddressListInteractor;
    RemoveShippingAddressInteractorImpl removeShippingAddressInteractor;

    public ShippingAddressPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchShippingAddress() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        fetchShippingAddressListInteractor = new FetchShippingAddressListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.id, userInfo.apiToken);
        fetchShippingAddressListInteractor.execute();
    }

    @Override
    public void onGettingShippingAddressSuccess(Address[] addresses) {
        shippingAddressAdapter = new ShippingAddressAdapter(mContext, addresses, this);
        mView.loadAdapter(shippingAddressAdapter);
        mView.hideLoader();
    }

    @Override
    public void onGettingShippingAddressFail(String errorMsg, int isLoginError) {
        mView.hideLoader();
        mView.hideAddressRecyclerView();
    }

    @Override
    public void onRemoveClicked(int id) {
        if (Helper.isNetworkConnected(mContext)) {
            androidApplication = (AndroidApplication) mContext.getApplicationContext();
            UserInfo userInfo = androidApplication.getUserInfo(mContext);
            removeShippingAddressInteractor = new RemoveShippingAddressInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, id);
            removeShippingAddressInteractor.execute();
            mView.showLoader();
        } else {
            mView.showErrorMessage("No Internet Connection");
        }
    }

    @Override
    public void onEditClicked(int id) {
        if (Helper.isNetworkConnected(mContext)) {
            mView.goToAddressEditActivity(id);
        } else {
            mView.showErrorMessage("No Internet Connection");
        }
    }

    @Override
    public void onRemoveAddressSuccess() {
        fetchShippingAddress();
    }

    @Override
    public void onRemoveAddressFail(String errorMsg, int loginError) {
        mView.hideLoader();
        mView.showErrorMessage(errorMsg);
    }
}
