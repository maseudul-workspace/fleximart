package fleximart.fleximart.presentation.presenters.impl;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.FetchSubcategoriesInteractor;
import fleximart.fleximart.domain.interactors.impl.FetchSubcategoriesInteractorImpl;
import fleximart.fleximart.domain.model.FirstCategory;
import fleximart.fleximart.presentation.presenters.SubcategoryPresenter;
import fleximart.fleximart.presentation.presenters.base.AbstractPresenter;
import fleximart.fleximart.presentation.ui.adapters.FirstCategoryAdapter;
import fleximart.fleximart.repository.AppRepositoryImpl;
import fleximart.fleximart.util.Helper;

public class SubcategoryPresenterImpl extends AbstractPresenter implements SubcategoryPresenter, FetchSubcategoriesInteractor.Callback, FirstCategoryAdapter.Callback {

    Context mContext;
    SubcategoryPresenter.View mView;
    FetchSubcategoriesInteractorImpl mInteractor;

    public SubcategoryPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchSubcategory(int id) {
        mInteractor = new FetchSubcategoriesInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, id);
        mInteractor.execute();
    }

    @Override
    public void onGettingSubcategoriesSuccess(FirstCategory[] firstCategories) {
        FirstCategoryAdapter firstCategoryAdapter = new FirstCategoryAdapter(mContext, firstCategories, this);
        mView.loadData(firstCategoryAdapter);
    }

    @Override
    public void onGettingSubcategoriesFail(String errorMsg) {
        mView.showErrorMessage(errorMsg);
    }

    @Override
    public void onSecondCategoryClicked(int id) {
        if (Helper.isNetworkConnected(mContext)) {
            mView.goToProductListActivity(id);
        } else {
            mView.showErrorMessage("No Internet Connection");
        }
    }
}
