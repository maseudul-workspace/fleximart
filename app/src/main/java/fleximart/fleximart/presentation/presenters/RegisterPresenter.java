package fleximart.fleximart.presentation.presenters;

import fleximart.fleximart.domain.model.UserInfoWrapper;

public interface RegisterPresenter {
    void registerUser(String name, String email, String password, String confirmPassword, String mobile);
    interface View {
        void onRegisterSuccess();
        void onRegsiterFail(UserInfoWrapper userInfoWrapper);
        void showErrorMessage(String errorMsg);
    }
}
