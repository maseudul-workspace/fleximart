package fleximart.fleximart.presentation.presenters.impl;

import android.content.Context;

import fleximart.fleximart.AndroidApplication;
import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.CheckBuyerPhoneInteractor;
import fleximart.fleximart.domain.interactors.impl.CheckBuyerPhoneInteractorImpl;
import fleximart.fleximart.domain.model.CheckPhoneData;
import fleximart.fleximart.domain.model.UserInfo;
import fleximart.fleximart.presentation.presenters.PhoneNumberCheckPresenter;
import fleximart.fleximart.presentation.presenters.base.AbstractPresenter;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class PhoneNumberCheckPresenterImpl extends AbstractPresenter implements PhoneNumberCheckPresenter, CheckBuyerPhoneInteractor.Callback {

    Context mContext;
    PhoneNumberCheckPresenter.View mView;
    CheckBuyerPhoneInteractorImpl checkBuyerPhoneInteractor;
    AndroidApplication androidApplication;


    public PhoneNumberCheckPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void checkPhoneNumber(String phoneNumber) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        checkBuyerPhoneInteractor = new CheckBuyerPhoneInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, phoneNumber);
        checkBuyerPhoneInteractor.execute();
    }

    @Override
    public void onCheckingPhoneSuccess(CheckPhoneData checkPhoneData) {
        mView.hideLoader();
        mView.loadData(checkPhoneData);
    }

    @Override
    public void onCheckingPhoneFail(String errorMsg, int loginError) {
        mView.hideLoader();
        mView.clearData();
    }
}
