package fleximart.fleximart.presentation.presenters.impl;

import android.content.Context;

import fleximart.fleximart.AndroidApplication;
import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.CheckLoginInteractor;
import fleximart.fleximart.domain.interactors.impl.CheckLoginInteractorImpl;
import fleximart.fleximart.domain.model.UserInfo;
import fleximart.fleximart.presentation.presenters.LoginPresenter;
import fleximart.fleximart.presentation.presenters.base.AbstractPresenter;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class LoginPresenterImpl extends AbstractPresenter implements LoginPresenter, CheckLoginInteractor.Callback {

    Context mContext;
    LoginPresenter.View mView;
    AndroidApplication androidApplication;
    CheckLoginInteractorImpl checkLoginInteractor;

    public LoginPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void checkLogin(String email, String password) {
        checkLoginInteractor = new CheckLoginInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, email, password);
        checkLoginInteractor.execute();
    }

    @Override
    public void onLoginSuccess(UserInfo userInfo) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setUserInfo(mContext, userInfo);
        mView.hideLoader();
        mView.goToMainActivity();
    }

    @Override
    public void onLoginFail(String errorMsg) {
        mView.hideLoader();
        mView.showMessage(errorMsg);
    }
}
