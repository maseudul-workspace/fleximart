package fleximart.fleximart.presentation.presenters;

import fleximart.fleximart.presentation.ui.adapters.FirstCategoryAdapter;

public interface SubcategoryPresenter {
    void fetchSubcategory(int id);
    interface View {
        void loadData(FirstCategoryAdapter firstCategoryAdapter);
        void goToProductListActivity(int subcategoryId);
        void showErrorMessage(String errorMsg);
    }
}
