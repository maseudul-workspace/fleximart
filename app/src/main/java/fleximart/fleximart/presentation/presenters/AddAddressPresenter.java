package fleximart.fleximart.presentation.presenters;

import fleximart.fleximart.presentation.ui.adapters.CityListDialogAdapter;
import fleximart.fleximart.presentation.ui.adapters.StateListDialogAdapter;

public interface AddAddressPresenter {
    void fetchStateList();
    void fetchCityList();
    void addAddress(String name,
                    String email,
                    String mobile,
                    String alternativeMobile,
                    String landmark,
                    String pin,
                    String address);
    interface View {
        void loadStateAdapter(StateListDialogAdapter stateListDialogAdapter);
        void loadCitiesAdapter(CityListDialogAdapter cityListDialogAdapter);
        void showLoader();
        void hideLoader();
        void setStateName(String stateName);
        void setCityName(String cityName);
        void hideStateRecyclerView();
        void hideCityRecyclerView();
        void onAddAddressSuccess();
        void showErrorMessage(String errorMsg);
    }
}
