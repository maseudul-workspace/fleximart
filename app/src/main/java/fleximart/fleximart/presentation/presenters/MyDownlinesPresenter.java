package fleximart.fleximart.presentation.presenters;

import fleximart.fleximart.presentation.ui.adapters.MyDownlinesAdapter;

public interface MyDownlinesPresenter {
    void fetchMyDownlines(int nodeId, int page, String refresh);
    interface View {
        void loadAdapter(MyDownlinesAdapter myDownlinesAdapter, int currentPage, int totalPage);
        void showLoader();
        void hideLoader();
        void onError(String errorMsg);
    }
}
