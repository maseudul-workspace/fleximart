package fleximart.fleximart.presentation.presenters;

import fleximart.fleximart.domain.model.MyNodes;
import fleximart.fleximart.presentation.ui.adapters.MyLevelsAdapter;

public interface MyLevelsPresenter {
    void fetchMyLevels(int nodeId);
    interface View {
        void loadAdapter(MyLevelsAdapter adapter);
        void showLoader();
        void hideLoader();
        void goToSponsorsActivity(int levelId);
        void goToLevelDownlinesActivity(int levelId);
    }
}
