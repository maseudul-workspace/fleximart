package fleximart.fleximart.presentation.presenters.impl;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.logging.Handler;

import es.dmoral.toasty.Toasty;
import fleximart.fleximart.AndroidApplication;
import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.AddToWishlistInteractor;
import fleximart.fleximart.domain.interactors.FetchProductsWithFilterInteractor;
import fleximart.fleximart.domain.interactors.FetchProductsWithoutFilterInteractor;
import fleximart.fleximart.domain.interactors.impl.AddToWishlistInteractorImpl;
import fleximart.fleximart.domain.interactors.impl.FetchProductsWithFilterInteractorImpl;
import fleximart.fleximart.domain.interactors.impl.FetchProductsWithoutFilterInteractorImpl;
import fleximart.fleximart.domain.interactors.impl.FetchWishlistInteractorImpl;
import fleximart.fleximart.domain.interactors.impl.RemoveFromWishlistInteratorImpl;
import fleximart.fleximart.domain.model.ProductDetails;
import fleximart.fleximart.domain.model.ProductListWithFilter;
import fleximart.fleximart.domain.model.UserInfo;
import fleximart.fleximart.domain.model.WishlistProducts;
import fleximart.fleximart.presentation.presenters.ProductsListPresenter;
import fleximart.fleximart.presentation.presenters.base.AbstractPresenter;
import fleximart.fleximart.presentation.ui.adapters.BrandsAdapter;
import fleximart.fleximart.presentation.ui.adapters.CategoryFilterAdapter;
import fleximart.fleximart.presentation.ui.adapters.ColorsAdapter;
import fleximart.fleximart.presentation.ui.adapters.ProductListVerticalAdapter;
import fleximart.fleximart.presentation.ui.adapters.SizesAdapter;
import fleximart.fleximart.repository.AppRepositoryImpl;
import fleximart.fleximart.util.Helper;

public class ProductListPresenterImpl extends AbstractPresenter implements  ProductsListPresenter,
                                                                            FetchProductsWithFilterInteractor.Callback,
                                                                            CategoryFilterAdapter.Callback,
                                                                            BrandsAdapter.Callback,
                                                                            SizesAdapter.Callback,
                                                                            ColorsAdapter.Callback,
                                                                            ProductListVerticalAdapter.Callback,
                                                                            FetchProductsWithoutFilterInteractor.Callback,
                                                                            AddToWishlistInteractor.Callback,
                                                                            RemoveFromWishlistInteratorImpl.Callback,
                                                                            FetchWishlistInteractorImpl.Callback
                                                                            {

    Context mContext;
    ProductsListPresenter.View mView;
    FetchProductsWithFilterInteractorImpl fetchProductsWithFilterInteractor;
    CategoryFilterAdapter categoryFilterAdapter;
    BrandsAdapter brandsAdapter;
    ColorsAdapter colorsAdapter;
    SizesAdapter sizesAdapter;
    ProductListVerticalAdapter productListVerticalAdapter;
    ArrayList<Integer> colors;
    ArrayList<Integer> sizes;
    ArrayList<Integer> brands;
    FetchProductsWithoutFilterInteractorImpl fetchProductsWithoutFilterInteractor;
    ProductDetails[] newProductDetails;
    int position;
    FetchWishlistInteractorImpl fetchWishlistInteractor;
    AddToWishlistInteractorImpl addToWishlistInteractor;
    RemoveFromWishlistInteratorImpl removeFromWishlistInterator;

    public ProductListPresenterImpl(Executor executor, MainThread mainThread, Context context, ProductsListPresenter.View view) {
        super(executor, mainThread);
        mContext = context;
        mView = view;
        colors = new ArrayList<>();
        sizes = new ArrayList<>();
        brands = new ArrayList<>();
    }

    @Override
    public void fetchProductListWithFilter(int secondCategory, int page) {
        fetchProductsWithFilterInteractor = new FetchProductsWithFilterInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, secondCategory, page);
        fetchProductsWithFilterInteractor.execute();
    }

    @Override
    public void fetchProductListWithoutFilter(int secondCategory, int page, long priceFrom, long priceTo, int sortBy, String type) {
        if (type.equals("refresh")) {
            newProductDetails = null;
        }
        Log.e("LogMsg", "Brands id: " + brands.size());
        fetchProductsWithoutFilterInteractor = new FetchProductsWithoutFilterInteractorImpl(mExecutor,
                                                                                                mMainThread,
                                                                                                new AppRepositoryImpl(),
                                                                                                this,
                                                                                                secondCategory,
                                                                                                brands,
                                                                                                sizes,
                                                                                                colors,
                                                                                                priceFrom,
                                                                                                priceTo,
                                                                                                sortBy,
                                                                                                page
                                                                                                );
         fetchProductsWithoutFilterInteractor.execute();
    }

    @Override
    public void fetchWishlist() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            fetchWishlistInteractor = new FetchWishlistInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.id, userInfo.apiToken);
            fetchWishlistInteractor.execute();
        }
    }

    @Override
    public void onFetchingProductListWithFilterSuccess(ProductListWithFilter productListWithFilter, int totalPage) {
        productListVerticalAdapter = new ProductListVerticalAdapter(mContext, productListWithFilter.productDetails, this);
        brandsAdapter = new BrandsAdapter(mContext, productListWithFilter.brands, this);
        colorsAdapter = new ColorsAdapter(mContext, productListWithFilter.colors, this);
        sizesAdapter = new SizesAdapter(mContext, productListWithFilter.sizes, this);
        categoryFilterAdapter = new CategoryFilterAdapter(mContext, this, productListWithFilter.secondCategories);
        mView.loadOthersAdapters(categoryFilterAdapter, colorsAdapter, brandsAdapter, sizesAdapter);
        onFetchingProductListWithoutFilterSuccess(productListWithFilter.productDetails, totalPage);
    }


    @Override
    public void onFetchingProductListWithFilterFail(String errorMsg) {
        mView.hideLoader();
        mView.onNoProductsFound();
    }

    @Override
    public void onBrandChecked(int id) {
        brands.add(id);
    }

    @Override
    public void onBrandUnchecked(int id) {
        for (int i = 0; i < brands.size(); i++) {
            if (brands.get(i) == id){
                brands.remove(i);
                break;
            }
        }
    }

    @Override
    public void onColorChecked(int id) {
        colors.add(id);
    }

    @Override
    public void onColorUnchecked(int id) {
        for (int i = 0; i < colors.size(); i++) {
            if (colors.get(i) == id){
                colors.remove(i);
                break;
            }
        }
    }

    @Override
    public void onSizeChecked(int id) {
        sizes.add(id);
    }

    @Override
    public void onSizeUnchecked(int id) {
        for (int i = 0; i < sizes.size(); i++) {
            if (sizes.get(i) == id){
                sizes.remove(i);
                break;
            }
        }
    }

    @Override
    public void onProductClicked(int produuctId) {
        if (Helper.isNetworkConnected(mContext)) {
            mView.goToProductDetails(produuctId);
        } else {
            mView.showErrorMessage("No Internet Connection");
        }
    }

    @Override
    public void onAddToWishlistClicked(int productId, int position) {
        this.position = position;
        if (Helper.isNetworkConnected(mContext)) {
            AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
            UserInfo userInfo = androidApplication.getUserInfo(mContext);
            if (userInfo != null) {
                addToWishlistInteractor = new AddToWishlistInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl(), userInfo.apiToken, userInfo.id, productId);
                addToWishlistInteractor.execute();
            } else {
                mView.showBottomSheet();
            }
        } else {
            mView.showErrorMessage("No Internet Connection");
        }
    }

    @Override
    public void onRemoveFromWishlistClicked(int productId, int position) {
        this.position = position;
        if (Helper.isNetworkConnected(mContext)) {
            AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
            WishlistProducts[] wishlistProducts = androidApplication.getWishlistProducts(mContext);
            UserInfo userInfo = androidApplication.getUserInfo(mContext);
            if (userInfo != null) {
                if (wishlistProducts != null) {
                    int wishlistId = 0;
                    for (int i = 0; i < wishlistProducts.length; i++) {
                        if (wishlistProducts[i].productId == productId) {
                            wishlistId = wishlistProducts[i].id;
                            break;
                        }
                    }
                    removeFromWishlistInterator = new RemoveFromWishlistInteratorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl(), userInfo.apiToken, wishlistId);
                    removeFromWishlistInterator.execute();
                }
            } else {
                mView.showBottomSheet();
            }
        } else {
            mView.showErrorMessage("No Internet Connection");
        }
    }

    @Override
    public void onCategoryClicked(int id) {

    }

    @Override
    public void onFetchingProductListWithoutFilterSuccess(ProductDetails[] productDetails, int totalPage) {
        mView.hideLoader();
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        if (androidApplication.getWishlistProducts(mContext) != null) {
            WishlistProducts[] wishlistProducts = androidApplication.getWishlistProducts(mContext);
            for (int i = 0; i < productDetails.length; i++) {
                for (int j = 0; j < wishlistProducts.length; j++) {
                    if (productDetails[i].id == wishlistProducts[j].productId) {
                        productDetails[i].isWishlistPresent = true;
                        break;
                    }
                }
            }
        }
        ProductDetails[] tempProductDetails;
        tempProductDetails = newProductDetails;
        try {
            int len1 = tempProductDetails.length;
            int len2 = productDetails.length;
            newProductDetails = new ProductDetails[len1 + len2];
            System.arraycopy(tempProductDetails, 0, newProductDetails, 0, len1);
            System.arraycopy(productDetails, 0, newProductDetails, len1, len2);
            productListVerticalAdapter.updateDataSet(newProductDetails);
        }catch (NullPointerException e){
            newProductDetails = productDetails;
            productListVerticalAdapter = new ProductListVerticalAdapter(mContext, productDetails, this);
            mView.loadProductAdapters(productListVerticalAdapter, totalPage);
        }
    }

    @Override
    public void onFetchingProductListWithoutFilterFail(String errorMsg) {
        mView.hideLoader();
    }

    @Override
    public void onAddToWishlistSuccess() {
        productListVerticalAdapter.onAddToWishlistSuccess(position);
        mView.showErrorMessage("Added To Wishlist");
        fetchWishlist();
    }

    @Override
    public void onAddToWishlistFail(String errorMsg, int loginError) {
        if (loginError == 0) {
            mView.showErrorMessage(errorMsg);
        }
    }

    @Override
    public void onWishlistRemoveSuccess() {
        productListVerticalAdapter.onRemoveFromWishlistFail(position);
        mView.showErrorMessage("Removed From Wishlist");
        fetchWishlist();
    }

    @Override
    public void onWishlistRemoveFail(String errorMsg, int loginError) {
        if (loginError == 0) {
            mView.showErrorMessage(errorMsg);
        }
    }

    @Override
    public void onWishlistFetchSuccess(WishlistProducts[] wishlistProducts) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setWishlistProducts(mContext, wishlistProducts);
    }

    @Override
    public void onWishlistFetchFail(String errorMsg, int loginError) {
        if (loginError == 0) {
            AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
            androidApplication.setWishlistProducts(mContext, null);
        }
    }
}
