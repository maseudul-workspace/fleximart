package fleximart.fleximart.presentation.presenters.impl;

import android.content.Context;

import fleximart.fleximart.AndroidApplication;
import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.FetchMyLevelsInteractor;
import fleximart.fleximart.domain.interactors.impl.FetchMyLevelsInteractorImpl;
import fleximart.fleximart.domain.model.MyLevels;
import fleximart.fleximart.domain.model.UserInfo;
import fleximart.fleximart.presentation.presenters.MyLevelsPresenter;
import fleximart.fleximart.presentation.presenters.base.AbstractPresenter;
import fleximart.fleximart.presentation.ui.adapters.MyLevelsAdapter;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class MyLevelsPresenterImpl extends AbstractPresenter implements MyLevelsPresenter, FetchMyLevelsInteractor.Callback, MyLevelsAdapter.Callback {

    Context mContext;
    MyLevelsPresenter.View mView;
    FetchMyLevelsInteractorImpl fetchMyLevelsInteractor;
    AndroidApplication androidApplication;

    public MyLevelsPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchMyLevels(int nodeId) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            fetchMyLevelsInteractor = new FetchMyLevelsInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, nodeId);
            fetchMyLevelsInteractor.execute();
        }
    }

    @Override
    public void onGettingMyLevelSuccess(MyLevels[] myLevels) {
       MyLevelsAdapter myLevelsAdapter = new MyLevelsAdapter(mContext, myLevels, this);
       mView.loadAdapter(myLevelsAdapter);
       mView.hideLoader();
    }

    @Override
    public void onGettingMyLevelFail(String errorMsg, int loginError) {
        mView.hideLoader();
    }

    @Override
    public void onViewDownlinesClicked(int id) {
        mView.goToLevelDownlinesActivity(id);
    }

    @Override
    public void onSponsorsClicked(int id) {
        mView.goToSponsorsActivity(id);
    }
}
