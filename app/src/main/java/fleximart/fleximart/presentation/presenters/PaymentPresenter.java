package fleximart.fleximart.presentation.presenters;

public interface PaymentPresenter {
    void generatePresenter(String MID,
                           int ORDER_ID,
                           String CHANNEL_ID,
                           String TXN_AMOUNT,
                           String WEBSITE,
                           String INDUSTRY_TYPE_ID,
                           String CALLBACK_URL);
    void completeWalletPay(int orderId, String transactionId);
    interface View {
        void initiatePayment(String checkSum);
        void showLoader();
        void hideLoader();
        void goToWalletAddSuccessActvity(boolean isSuccess);
    }
}
