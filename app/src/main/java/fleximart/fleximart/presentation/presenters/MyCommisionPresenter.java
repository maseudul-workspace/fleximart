package fleximart.fleximart.presentation.presenters;

import fleximart.fleximart.domain.model.CommisionTotal;

public interface MyCommisionPresenter {
    void fetchMyTotalCommision();
    interface View {
        void loadData(CommisionTotal commisionTotal);
        void showLoader();
        void hideLoader();
        void onError(String errorMsg);
    }
}
