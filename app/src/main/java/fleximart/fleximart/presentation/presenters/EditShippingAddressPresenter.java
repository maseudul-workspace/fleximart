package fleximart.fleximart.presentation.presenters;

import fleximart.fleximart.domain.model.Address;
import fleximart.fleximart.presentation.ui.adapters.CityListDialogAdapter;
import fleximart.fleximart.presentation.ui.adapters.StateListDialogAdapter;

public interface EditShippingAddressPresenter {
    void getShippingAddressDetails(int addressId);
    void updateAddress(String name,
                        String email,
                        String mobile,
                        String alternativeMobile,
                        String landmark,
                        String pin,
                        String address);
    void fetchStateList();
    void fetchCityList();
    interface View {
        void showLoader();
        void hideLoader();
        void setData(Address address);
        void loadStateAdapter(StateListDialogAdapter stateListDialogAdapter);
        void loadCitiesAdapter(CityListDialogAdapter cityListDialogAdapter);
        void setStateName(String stateName);
        void setCityName(String cityName);
        void hideStateRecyclerView();
        void hideCityRecyclerView();
        void onUpdateAddressSuccess();
    }
}
