package fleximart.fleximart.presentation.presenters;

import fleximart.fleximart.domain.model.DashboardData;

public interface MembersDashboardPresenter {
    void fetchSummary();
    interface View {
        void loadDasboardSummary(DashboardData dashboardData);
        void showLoader();
        void hideLoader();
        void showErrorMsg(String msg);
    }
}
