package fleximart.fleximart.presentation.presenters.impl;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import fleximart.fleximart.AndroidApplication;
import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.FetchCitiesInteractor;
import fleximart.fleximart.domain.interactors.FetchStateListInteractor;
import fleximart.fleximart.domain.interactors.FetchUserProfileInteractor;
import fleximart.fleximart.domain.interactors.UpdateProfileInteractor;
import fleximart.fleximart.domain.interactors.impl.FetchCitiesInteractorImpl;
import fleximart.fleximart.domain.interactors.impl.FetchStateListInteractorImpl;
import fleximart.fleximart.domain.interactors.impl.FetchUserProfileInteractorImpl;
import fleximart.fleximart.domain.interactors.impl.UpdateProfileInteractorImpl;
import fleximart.fleximart.domain.model.Cities;
import fleximart.fleximart.domain.model.StateList;
import fleximart.fleximart.domain.model.User;
import fleximart.fleximart.domain.model.UserInfo;
import fleximart.fleximart.domain.model.UserProfileData;
import fleximart.fleximart.presentation.presenters.UserProfilePresenter;
import fleximart.fleximart.presentation.presenters.base.AbstractPresenter;
import fleximart.fleximart.presentation.ui.adapters.CityListDialogAdapter;
import fleximart.fleximart.presentation.ui.adapters.StateListDialogAdapter;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class UserProfilePresenterImpl extends AbstractPresenter implements UserProfilePresenter,
                                                                            FetchUserProfileInteractor.Callback,
                                                                            FetchStateListInteractor.Callback,
                                                                            FetchCitiesInteractor.Callback,
                                                                            StateListDialogAdapter.Callback,
                                                                            CityListDialogAdapter.Callback,
                                                                            UpdateProfileInteractor.Callback

{

    Context mContext;
    UserProfilePresenter.View mView;
    AndroidApplication androidApplication;
    FetchUserProfileInteractorImpl fetchUserProfileInteractor;
    StateListDialogAdapter stateListDialogAdapter;
    FetchCitiesInteractorImpl fetchCitiesInteractor;
    FetchStateListInteractorImpl fetchStateListInteractor;
    CityListDialogAdapter cityListDialogAdapter;
    UpdateProfileInteractorImpl updateProfileInteractor;
    int stateId = 0;
    int cityId = 0;

    public UserProfilePresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchUserProfile() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        fetchUserProfileInteractor = new FetchUserProfileInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.id, user.apiToken);
        fetchUserProfileInteractor.execute();
    }

    @Override
    public void fetchStateList() {
        fetchStateListInteractor = new FetchStateListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this);
        fetchStateListInteractor.execute();
    }

    @Override
    public void fetchCityList() {
        fetchCitiesInteractor = new FetchCitiesInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, stateId);
        fetchCitiesInteractor.execute();
    }

    @Override
    public void updateProfile(String name, String email, String mobile, String DOB, String gender, String area, String pin, String address) {
        if (stateId == 0) {
            Toast.makeText(mContext, "Please select state", Toast.LENGTH_SHORT).show();
        } else if (cityId == 0) {
            Toast.makeText(mContext, "Please select city", Toast.LENGTH_SHORT).show();
        } else {
            mView.showLoader();
            androidApplication = (AndroidApplication) mContext.getApplicationContext();
            UserInfo user = androidApplication.getUserInfo(mContext);
            updateProfileInteractor = new UpdateProfileInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.id, name, email, mobile, DOB, gender, area, stateId, cityId, pin, address);
            updateProfileInteractor.execute();
        }
    }

    @Override
    public void onGettingUserProfileSuccess(UserProfileData userProfileData) {
        mView.hideLoader();
        mView.loadUserProfileData(userProfileData);
        this.cityId = userProfileData.userAddress.cityId;
        this.stateId = userProfileData.userAddress.stateId;
    }

    @Override
    public void onGettingUserProfileFail(String errorMsg, int loginError) {
        mView.hideLoader();
    }

    @Override
    public void onGettingCitiesSuccess(Cities[] cities) {
        if (cities.length == 0) {
            mView.hideCityRecyclerView();
        } else {
            cityListDialogAdapter = new CityListDialogAdapter(mContext, cities, this::onCityClicked);
            mView.loadCitiesAdapter(cityListDialogAdapter);
        }
    }

    @Override
    public void onGettingCitiesFail() {
        mView.hideCityRecyclerView();
    }

    @Override
    public void onGettingStateListSuccess(StateList[] states) {
        if (states.length == 0) {
            mView.hideStateRecyclerView();
        } else {
            stateListDialogAdapter = new StateListDialogAdapter(mContext, states, this::onStateClicked);
            mView.loadStateAdapter(stateListDialogAdapter);
        }
    }

    @Override
    public void onGettingStateListFail() {
        mView.hideStateRecyclerView();
    }

    @Override
    public void onCityClicked(int id, String city) {
        mView.setCityName(city);
        cityId = id;
    }

    @Override
    public void onStateClicked(int id, String state) {
        cityId = 0;
        stateId = id;
        mView.setCityName("Select City");
        mView.setStateName(state);
        fetchCityList();
    }

    @Override
    public void onProfileUpdateSuccess() {
        mView.showErrorMsg("Profile updated successfully");
        fetchUserProfile();
    }

    @Override
    public void onProfileUpdateFail(String errorMsg, int loginError) {
        mView.hideLoader();
        mView.showErrorMsg(errorMsg);
    }
}
