package fleximart.fleximart.presentation.presenters.impl;

import android.content.Context;

import fleximart.fleximart.AndroidApplication;
import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.FetchApprovedCommissionListInteractor;
import fleximart.fleximart.domain.interactors.FetchUnapprovedCommissionListInteractor;
import fleximart.fleximart.domain.interactors.impl.FetchApprovedCommissionListInteractorImpl;
import fleximart.fleximart.domain.interactors.impl.FetchUnapprovedCommissionListInteractorImpl;
import fleximart.fleximart.domain.model.MyCommissionList;
import fleximart.fleximart.domain.model.UserInfo;
import fleximart.fleximart.presentation.presenters.MyCommissionListPresenter;
import fleximart.fleximart.presentation.presenters.base.AbstractPresenter;
import fleximart.fleximart.presentation.ui.adapters.MyCommissionListAdapter;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class MyCommissionListPresenterImpl extends AbstractPresenter implements MyCommissionListPresenter, FetchApprovedCommissionListInteractor.Callback, FetchUnapprovedCommissionListInteractor.Callback {

    Context mContext;
    MyCommissionListPresenter.View mView;
    AndroidApplication androidApplication;
    MyCommissionList[] newCommissionList;
    MyCommissionListAdapter adapter;
    FetchUnapprovedCommissionListInteractorImpl fetchUnapprovedCommissionListInteractor;
    FetchApprovedCommissionListInteractorImpl fetchApprovedCommissionListInteractor;

    public MyCommissionListPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchApprovedCommissions(int page, String type) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (type.equals("refresh")) {
            newCommissionList = null;
        }
        if (userInfo != null) {
            fetchApprovedCommissionListInteractor = new FetchApprovedCommissionListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.id, userInfo.apiToken, page);
            fetchApprovedCommissionListInteractor.execute();
        }
    }

    @Override
    public void fetchUnapprovedCommissions(int page, String type) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (type.equals("refresh")) {
            newCommissionList = null;
        }
        if (userInfo != null) {
            fetchUnapprovedCommissionListInteractor = new FetchUnapprovedCommissionListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.id, userInfo.apiToken, page);
            fetchUnapprovedCommissionListInteractor.execute();
        }
    }

    @Override
    public void onFetchApprovedCommisionListSuccess(MyCommissionList[] commissionLists, int totalPage, int currentPage) {
        mView.hideLoader();
        if (commissionLists.length > 0) {
            MyCommissionList[] tempCommissionList;
            tempCommissionList = newCommissionList;
            try {
                int len1 = tempCommissionList.length;
                int len2 = commissionLists.length;
                newCommissionList = new MyCommissionList[len1 + len2];
                System.arraycopy(tempCommissionList, 0, newCommissionList, 0, len1);
                System.arraycopy(commissionLists, 0, newCommissionList, len1, len2);
                adapter.updateDataset(newCommissionList);
            }catch (NullPointerException e){
                newCommissionList = commissionLists;
                adapter = new MyCommissionListAdapter(mContext, commissionLists);
                mView.loadCommissionsList(adapter, totalPage);
            }
        }
    }

    @Override
    public void onFetchApprovedCommisionListFail(String errorMsg, int loginError) {
        mView.hideLoader();
    }

    @Override
    public void onFetchUnapprovedCommisionListSuccess(MyCommissionList[] commissionLists, int totalPage, int currentPage) {
        mView.hideLoader();
        if (commissionLists.length > 0) {
            MyCommissionList[] tempCommissionList;
            tempCommissionList = newCommissionList;
            try {
                int len1 = tempCommissionList.length;
                int len2 = commissionLists.length;
                newCommissionList = new MyCommissionList[len1 + len2];
                System.arraycopy(tempCommissionList, 0, newCommissionList, 0, len1);
                System.arraycopy(commissionLists, 0, newCommissionList, len1, len2);
                adapter.updateDataset(newCommissionList);
            }catch (NullPointerException e){
                newCommissionList = commissionLists;
                adapter = new MyCommissionListAdapter(mContext, commissionLists);
                mView.loadCommissionsList(adapter, totalPage);
            }
        }
    }

    @Override
    public void onFetchUnapprovedCommisionListFail(String errorMsg, int loginError) {
        mView.onError(errorMsg);
        mView.hideLoader();
    }
}
