package fleximart.fleximart.presentation.presenters;

import fleximart.fleximart.domain.model.UserProfileData;
import fleximart.fleximart.presentation.ui.adapters.CityListDialogAdapter;
import fleximart.fleximart.presentation.ui.adapters.StateListDialogAdapter;

public interface UserProfilePresenter {
    void fetchUserProfile();
    void fetchStateList();
    void fetchCityList();
    void updateProfile(String name,
                       String email,
                       String mobile,
                       String DOB,
                       String gender,
                       String area,
                       String pin,
                       String address);
    interface View {
        void showLoader();
        void hideLoader();
        void loadUserProfileData(UserProfileData userProfileData);
        void loadStateAdapter(StateListDialogAdapter stateListDialogAdapter);
        void loadCitiesAdapter(CityListDialogAdapter cityListDialogAdapter);
        void setStateName(String stateName);
        void setCityName(String cityName);
        void hideStateRecyclerView();
        void hideCityRecyclerView();
        void showErrorMsg(String msg);
    }
}
