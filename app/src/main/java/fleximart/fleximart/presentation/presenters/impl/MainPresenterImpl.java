package fleximart.fleximart.presentation.presenters.impl;

import android.content.Context;

import fleximart.fleximart.AndroidApplication;
import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.FetchCartDetailsInteractor;
import fleximart.fleximart.domain.interactors.FetchMainDataInteractor;
import fleximart.fleximart.domain.interactors.FetchWishlistInteractor;
import fleximart.fleximart.domain.interactors.impl.FetchCartDetailsInteractorImpl;
import fleximart.fleximart.domain.interactors.impl.FetchMainDataInteractorImpl;
import fleximart.fleximart.domain.interactors.impl.FetchWishlistInteractorImpl;
import fleximart.fleximart.domain.model.CartDetails;
import fleximart.fleximart.domain.model.MainData;
import fleximart.fleximart.domain.model.ProductDetails;
import fleximart.fleximart.domain.model.UserInfo;
import fleximart.fleximart.domain.model.WishlistProducts;
import fleximart.fleximart.presentation.presenters.MainPresenter;
import fleximart.fleximart.presentation.presenters.base.AbstractPresenter;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class MainPresenterImpl extends AbstractPresenter implements MainPresenter, FetchMainDataInteractor.Callback, FetchCartDetailsInteractor.Callback, FetchWishlistInteractor.Callback {

    Context mContext;
    MainPresenter.View mView;
    FetchMainDataInteractorImpl mInteractor;
    AndroidApplication androidApplication;
    FetchCartDetailsInteractorImpl fetchCartDetailsInteractor;
    FetchWishlistInteractorImpl fetchWishlistInteractor;

    public MainPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchMainData() {
        mInteractor = new FetchMainDataInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this);
        mInteractor.execute();
    }

    @Override
    public void fetchCartDetails() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            fetchCartDetailsInteractor = new FetchCartDetailsInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.id, userInfo.apiToken);
            fetchCartDetailsInteractor.execute();
        }
    }

    @Override
    public void fetchWishlist() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            fetchWishlistInteractor = new FetchWishlistInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.id, userInfo.apiToken);
            fetchWishlistInteractor.execute();
        }
    }

    @Override
    public void onGettingMainDataSuccess(MainData mainData) {
        mView.loadMainData(mainData);
    }

    @Override
    public void onGettingMaiDataFail() {

    }

    @Override
    public void onGettingCartDetailsSucess(CartDetails[] cartDetails) {
        mView.loadCartCount(cartDetails.length);
    }

    @Override
    public void onGettingCartDetailsFail(String errorMsg) {

    }

    @Override
    public void onWishlistFetchSuccess(WishlistProducts[] wishlistProducts) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setWishlistProducts(mContext, wishlistProducts);
        mView.loadWishlistCount(wishlistProducts.length);
    }

    @Override
    public void onWishlistFetchFail(String errorMsg, int loginError) {
        mView.loadWishlistCount(0);
    }
}
