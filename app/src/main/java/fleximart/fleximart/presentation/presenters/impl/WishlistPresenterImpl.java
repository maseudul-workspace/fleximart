package fleximart.fleximart.presentation.presenters.impl;

import android.content.Context;

import fleximart.fleximart.AndroidApplication;
import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.FetchCartDetailsInteractor;
import fleximart.fleximart.domain.interactors.FetchWishlistInteractor;
import fleximart.fleximart.domain.interactors.MoveToCartFromWishlistInteractor;
import fleximart.fleximart.domain.interactors.RemoveFromWishlistInteractor;
import fleximart.fleximart.domain.interactors.base.AbstractInteractor;
import fleximart.fleximart.domain.interactors.impl.FetchCartDetailsInteractorImpl;
import fleximart.fleximart.domain.interactors.impl.FetchWishlistInteractorImpl;
import fleximart.fleximart.domain.interactors.impl.MoveToCartFromWishlistInteractorImpl;
import fleximart.fleximart.domain.interactors.impl.RemoveFromWishlistInteratorImpl;
import fleximart.fleximart.domain.model.CartDetails;
import fleximart.fleximart.domain.model.ProductDetails;
import fleximart.fleximart.domain.model.UserInfo;
import fleximart.fleximart.domain.model.WishlistProducts;
import fleximart.fleximart.presentation.presenters.WishlistPresenter;
import fleximart.fleximart.presentation.presenters.base.AbstractPresenter;
import fleximart.fleximart.presentation.ui.adapters.WishlistAdapter;
import fleximart.fleximart.repository.AppRepositoryImpl;
import fleximart.fleximart.util.Helper;

public class WishlistPresenterImpl extends AbstractPresenter implements WishlistPresenter,
                                                                        FetchWishlistInteractor.Callback,
                                                                        FetchCartDetailsInteractor.Callback,
                                                                        WishlistAdapter.Callback,
                                                                        RemoveFromWishlistInteractor.Callback,
                                                                        MoveToCartFromWishlistInteractor.Callback
                                                                        {

    Context mContext;
    WishlistPresenter.View mView;
    FetchCartDetailsInteractorImpl fetchCartDetailsInteractor;
    FetchWishlistInteractorImpl fetchWishlistInteractor;
    RemoveFromWishlistInteratorImpl removeFromWishlistInterator;
    AndroidApplication androidApplication;
    MoveToCartFromWishlistInteractorImpl moveToCartFromWishlistInteractor;

    public WishlistPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchWishlist() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            fetchWishlistInteractor = new FetchWishlistInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.id, userInfo.apiToken);
            fetchWishlistInteractor.execute();
        }
    }

    @Override
    public void fetchCartDetails() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            fetchCartDetailsInteractor = new FetchCartDetailsInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.id, userInfo.apiToken);
            fetchCartDetailsInteractor.execute();
        }
    }

    @Override
    public void onWishlistFetchSuccess(WishlistProducts[] wishlistProducts) {
        if (wishlistProducts.length > 0) {
            WishlistAdapter wishlistAdapter = new WishlistAdapter(mContext, wishlistProducts, this);
            mView.loadWishlist(wishlistAdapter);
            mView.hideLoader();
            androidApplication = (AndroidApplication) mContext.getApplicationContext();
            androidApplication.setWishlistProducts(mContext, wishlistProducts);
        } else {
            mView.onWishlistEmpty();
        }
    }

    @Override
    public void onWishlistFetchFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if (loginError == 0) {
            mView.onWishlistEmpty();
            androidApplication = (AndroidApplication) mContext.getApplicationContext();
            androidApplication.setWishlistProducts(mContext, null);
        } else {

        }
    }

    @Override
    public void onGettingCartDetailsSucess(CartDetails[] cartDetails) {
        mView.loadCartCount(cartDetails.length);
    }

    @Override
    public void onGettingCartDetailsFail(String errorMsg) {
    }

    @Override
    public void onProductClicked(int productId) {
        mView.goToProductDetails(productId);
    }

    @Override
    public void onCancelClicked(int wishlistId) {
        if (Helper.isNetworkConnected(mContext)) {
            androidApplication = (AndroidApplication) mContext.getApplicationContext();
            UserInfo userInfo = androidApplication.getUserInfo(mContext);
            if (userInfo != null) {
                mView.showLoader();
                removeFromWishlistInterator = new RemoveFromWishlistInteratorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl(), userInfo.apiToken, wishlistId);
                removeFromWishlistInterator.execute();
            }
        } else {
            mView.showErrorMsg("No Internet Connection");
        }
    }

    @Override
    public void onMoveToCartClicked(int productId) {
        if (Helper.isNetworkConnected(mContext)) {
            androidApplication = (AndroidApplication) mContext.getApplicationContext();
            UserInfo userInfo = androidApplication.getUserInfo(mContext);
            if (userInfo != null) {
                mView.showLoader();
                moveToCartFromWishlistInteractor = new MoveToCartFromWishlistInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.id, productId);
                moveToCartFromWishlistInteractor.execute();
            }
        } else {
            mView.showErrorMsg("No Internet Connection");
        }
    }

    @Override
    public void onWishlistRemoveSuccess() {
        fetchWishlist();
    }

    @Override
    public void onWishlistRemoveFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if (loginError == 0) {
            mView.showErrorMsg(errorMsg);
        }
    }

    @Override
    public void onMoveToCartSuccess() {
        mView.hideLoader();
        mView.showErrorMsg("Added To Cart");
        fetchWishlist();
        fetchCartDetails();
    }

    @Override
    public void onMoveToCartFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if (loginError == 0) {
            mView.showErrorMsg(errorMsg);
        }
    }
}
