package fleximart.fleximart.presentation.presenters.impl;

import android.content.Context;

import fleximart.fleximart.AndroidApplication;
import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.BuyNewNodeInteractor;
import fleximart.fleximart.domain.interactors.BuyNodeInteractor;
import fleximart.fleximart.domain.interactors.CheckNodeInteractor;
import fleximart.fleximart.domain.interactors.GetWalletDetailsInteractor;
import fleximart.fleximart.domain.interactors.impl.BuyNewNodeInteractorImpl;
import fleximart.fleximart.domain.interactors.impl.BuyNodeInteractorImpl;
import fleximart.fleximart.domain.interactors.impl.CheckNodeInteractorImpl;
import fleximart.fleximart.domain.interactors.impl.GetWalletDetailsInteractorImpl;
import fleximart.fleximart.domain.model.NodeDetails;
import fleximart.fleximart.domain.model.User;
import fleximart.fleximart.domain.model.UserInfo;
import fleximart.fleximart.domain.model.WalletDetails;
import fleximart.fleximart.presentation.presenters.CheckNodePresenter;
import fleximart.fleximart.presentation.presenters.base.AbstractPresenter;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class CheckNodePresenterImpl extends AbstractPresenter implements    CheckNodePresenter,
                                                                            CheckNodeInteractor.Callback,
                                                                            GetWalletDetailsInteractor.Callback,
                                                                            BuyNewNodeInteractor.Callback
                                                                            {

    Context mContext;
    CheckNodePresenter.View mView;
    CheckNodeInteractorImpl checkNodeInteractor;
    AndroidApplication androidApplication;
    GetWalletDetailsInteractorImpl getWalletDetailsInteractor;
    BuyNewNodeInteractorImpl buyNewNodeInteractor;

    public CheckNodePresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void checkNode(String nodeId) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        checkNodeInteractor = new CheckNodeInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, nodeId);
        checkNodeInteractor.execute();
    }

    @Override
    public void registerNodes(String node, int totalNode, String leg) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        buyNewNodeInteractor = new BuyNewNodeInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.id, node, totalNode, leg);
        buyNewNodeInteractor.execute();
    }

    @Override
    public void getWalletDetails() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        getWalletDetailsInteractor = new GetWalletDetailsInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.id);
        getWalletDetailsInteractor.execute();
    }

    @Override
    public void onNodeCheckingSuccess(NodeDetails nodeDetails) {
        mView.loadData(nodeDetails);
        mView.hideLoader();
    }

    @Override
    public void onNodeCheckingFail(String errorMsg, int loginError) {
        mView.hideLoader();
    }

    @Override
    public void onGettingWalletDetailsSuccess(WalletDetails walletDetails) {
        mView.hideLoader();
        mView.loadWalletAmount(walletDetails.withdrawalAmount);
    }

    @Override
    public void onGettingWalletDetailsFail(String errorMsg, int loginError) {
        mView.hideLoader();
        mView.showMessage(errorMsg);
    }

    @Override
    public void onBuyNewNodeSuccess() {
        mView.hideLoader();
        mView.goToOrderResponseActivity();
    }

    @Override
    public void onBuyNewNodeFail(String errorMsg, int loginError) {
        mView.hideLoader();
        mView.showMessage(errorMsg);
    }
}
