package fleximart.fleximart.presentation.presenters.impl;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import fleximart.fleximart.AndroidApplication;
import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.DeleteFromCartInteractor;
import fleximart.fleximart.domain.interactors.FetchCartDetailsInteractor;
import fleximart.fleximart.domain.interactors.FetchCitiesInteractor;
import fleximart.fleximart.domain.interactors.FetchShippingAddressListInteractor;
import fleximart.fleximart.domain.interactors.UpdateCartInteractor;
import fleximart.fleximart.domain.interactors.impl.DeleteFromFromCartInteractorImpl;
import fleximart.fleximart.domain.interactors.impl.FetchCartDetailsInteractorImpl;
import fleximart.fleximart.domain.interactors.impl.FetchCitiesInteractorImpl;
import fleximart.fleximart.domain.interactors.impl.FetchShippingAddressListInteractorImpl;
import fleximart.fleximart.domain.interactors.impl.UpdateCartInteractorImpl;
import fleximart.fleximart.domain.model.Address;
import fleximart.fleximart.domain.model.CartDetails;
import fleximart.fleximart.domain.model.Cities;
import fleximart.fleximart.domain.model.UserInfo;
import fleximart.fleximart.presentation.presenters.CartDetailsPresenter;
import fleximart.fleximart.presentation.presenters.base.AbstractPresenter;
import fleximart.fleximart.presentation.ui.adapters.CartDetailsAdapter;
import fleximart.fleximart.presentation.ui.adapters.CartShippingAddressAdapter;
import fleximart.fleximart.repository.AppRepositoryImpl;
import fleximart.fleximart.util.Helper;

public class CartDetailsPresenterImpl extends AbstractPresenter implements CartDetailsPresenter,
                                                                            FetchCartDetailsInteractor.Callback,
                                                                            CartDetailsAdapter.Callback,
                                                                            UpdateCartInteractor.Callback,
                                                                            DeleteFromCartInteractor.Callback,
                                                                            CartShippingAddressAdapter.Callback,
                                                                            FetchShippingAddressListInteractor.Callback
{

    Context mContext;
    CartDetailsPresenter.View mView;
    FetchCartDetailsInteractorImpl fetchCartDetailsInteractor;
    AndroidApplication androidApplication;
    CartDetailsAdapter cartDetailsAdapter;
    Activity mActivity;
    UpdateCartInteractorImpl updateCartInteractor;
    DeleteFromFromCartInteractorImpl deleteFromFromCartInteractor;
    FetchShippingAddressListInteractorImpl fetchShippingAddressListInteractor;
    CartShippingAddressAdapter cartShippingAddressAdapter;
    Address[] addresses;

    public CartDetailsPresenterImpl(Executor executor, MainThread mainThread, Context mContext, Activity activity, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
        mActivity = activity;
    }

    @Override
    public void fetchCartDetails() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        fetchCartDetailsInteractor = new FetchCartDetailsInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.id, userInfo.apiToken);
        fetchCartDetailsInteractor.execute();
    }

    @Override
    public void fetchShippingAddressList() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        fetchShippingAddressListInteractor = new FetchShippingAddressListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.id, userInfo.apiToken);
        fetchShippingAddressListInteractor.execute();
    }

    @Override
    public void onGettingCartDetailsSucess(CartDetails[] cartDetails) {
        if (cartDetails.length > 0) {
            cartDetailsAdapter = new CartDetailsAdapter(mContext, mActivity, cartDetails, this);
            double totalMRP = 0;
            double totalDiscount = 0;
            double totalPrice = 0;
            for (int i = 0; i < cartDetails.length; i++) {
                totalMRP = totalMRP + cartDetails[i].product_mrp * cartDetails[i].quantity;
                totalPrice = totalPrice + cartDetails[i].productPrice * cartDetails[i].quantity;
                totalDiscount = totalDiscount + (cartDetails[i].product_mrp - cartDetails[i].productPrice)*cartDetails[i].quantity;
            }
            mView.loadAdapter(cartDetailsAdapter, cartDetails.length, totalMRP, totalPrice, totalDiscount);
        } else {
            mView.showCartEmptyLayout();
        }
        mView.hideLoader();
    }

    @Override
    public void onGettingCartDetailsFail(String errorMsg) {
        mView.hideLoader();
        mView.showCartEmptyLayout();
    }

    @Override
    public void updateCart(int cartId, int sizeId, int quantity) {
        if (Helper.isNetworkConnected(mContext)) {
            androidApplication = (AndroidApplication) mContext.getApplicationContext();
            UserInfo userInfo = androidApplication.getUserInfo(mContext);
            updateCartInteractor = new UpdateCartInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.id, cartId, quantity, sizeId);
            updateCartInteractor.execute();
            mView.showLoader();
        } else {
            mView.showErrorMessage("No Internet Connection");
        }
    }

    @Override
    public void deleteFromCart(int cartId) {
        if (Helper.isNetworkConnected(mContext)) {
            androidApplication = (AndroidApplication) mContext.getApplicationContext();
            UserInfo userInfo = androidApplication.getUserInfo(mContext);
            deleteFromFromCartInteractor = new DeleteFromFromCartInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, cartId);
            deleteFromFromCartInteractor.execute();
            mView.showLoader();
        } else {
            mView.showErrorMessage("No Internet Connection");
        }
    }

    @Override
    public void onUpdateSuccess() {
        fetchCartDetails();
    }

    @Override
    public void onUpdateFailed(String errorMsg) {
        mView.hideLoader();
        mView.showErrorMessage(errorMsg);
    }

    @Override
    public void onDeleteFromCartSuccess() {
        fetchCartDetails();
    }

    @Override
    public void onDeleteFromCartFailed(String errorMsg) {
        mView.hideLoader();
        mView.showErrorMessage(errorMsg);
    }

    @Override
    public void onAddressSelected(int id) {
        for (int i = 0; i < addresses.length; i++) {
            if (addresses[i].id == id) {
                addresses[i].isSelected = true;
            } else {
                addresses[i].isSelected = false;
            }
        }
        cartShippingAddressAdapter.updateDataSet(addresses);
        mView.onAddressSelected(id);
    }

    @Override
    public void onEditClicked(int id) {
        if (Helper.isNetworkConnected(mContext)) {
            mView.onShippingAddressEditClicked(id);
        } else {
            mView.showErrorMessage("No Internet Connection");
        }
    }

    @Override
    public void onDeliverButtonClicked() {
        if (Helper.isNetworkConnected(mContext)) {
            mView.goToCheckoutActivity();
        } else {
            mView.showErrorMessage("No Internet Connection");
        }
    }

    @Override
    public void onGettingShippingAddressSuccess(Address[] addresses) {
        this.addresses = addresses;
        cartShippingAddressAdapter = new CartShippingAddressAdapter(mContext, addresses, this);
        mView.loadShippingAddressAdapter(cartShippingAddressAdapter);
    }

    @Override
    public void onGettingShippingAddressFail(String errorMsg, int isLoginError) {

    }
}
