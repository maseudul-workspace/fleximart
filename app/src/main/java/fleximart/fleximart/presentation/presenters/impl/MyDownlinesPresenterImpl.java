package fleximart.fleximart.presentation.presenters.impl;

import android.content.Context;

import fleximart.fleximart.AndroidApplication;
import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.FetchMyDownlinesInteractor;
import fleximart.fleximart.domain.interactors.impl.FetchMyDownlineInteractorImpl;
import fleximart.fleximart.domain.model.Downline;
import fleximart.fleximart.domain.model.UserInfo;
import fleximart.fleximart.presentation.presenters.MyDownlinesPresenter;
import fleximart.fleximart.presentation.presenters.base.AbstractPresenter;
import fleximart.fleximart.presentation.ui.adapters.MyDownlinesAdapter;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class MyDownlinesPresenterImpl extends AbstractPresenter implements MyDownlinesPresenter, FetchMyDownlinesInteractor.Callback {

    Context mContext;
    MyDownlinesPresenter.View mView;
    FetchMyDownlineInteractorImpl fetchMyDownlineInteractor;
    AndroidApplication androidApplication;
    MyDownlinesAdapter downlinesAdapter;
    Downline[] newDownlines;

    public MyDownlinesPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchMyDownlines(int nodeId, int page, String refresh) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (refresh.equals("refresh")) {
            newDownlines = null;
        }
        if (userInfo != null) {
            fetchMyDownlineInteractor = new FetchMyDownlineInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, nodeId, page);
            fetchMyDownlineInteractor.execute();
        }
    }

    @Override
    public void onFetchMyDownlineSuccess(Downline[] downlines, int currentPage, int totalPage) {
        mView.hideLoader();
        if (downlines.length > 0) {
            Downline[] tempDownlines;
            tempDownlines = newDownlines;
            try {
                int len1 = tempDownlines.length;
                int len2 = downlines.length;
                newDownlines = new Downline[len1 + len2];
                System.arraycopy(tempDownlines, 0, newDownlines, 0, len1);
                System.arraycopy(downlines, 0, newDownlines, len1, len2);
                downlinesAdapter.updateDataSet(newDownlines);
            }catch (NullPointerException e){
                newDownlines = downlines;
                downlinesAdapter = new MyDownlinesAdapter(mContext, downlines);
                mView.loadAdapter(downlinesAdapter, currentPage, totalPage);
            }
        } else {
            mView.onError("No Record Found");
        }
    }

    @Override
    public void onFetchMyDownlineFail(String errorMsg, int loginError) {
        mView.hideLoader();
        mView.onError(errorMsg);
    }
}
