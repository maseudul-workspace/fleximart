package fleximart.fleximart.presentation.presenters;

import fleximart.fleximart.presentation.ui.adapters.NodeViewpagerAdapter;

public interface MyNodesPresenter {
    void fetchMyNodes();
    interface View {
        void showLoader();
        void hideLoader();
        void loadAdapter(NodeViewpagerAdapter myNodesAdapter, int length);
        void goToMyDownlines(int nodeId);
        void goToMyTree(int nodeId);
        void goToMyLevels(int nodeId);
        void onError(String errorMsg);
    }
}
