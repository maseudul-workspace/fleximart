package fleximart.fleximart.presentation.presenters.impl;

import android.content.Context;
import android.util.Log;

import fleximart.fleximart.AndroidApplication;
import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.executors.impl.ThreadExecutor;
import fleximart.fleximart.domain.interactors.CompleteWalletPayInteractor;
import fleximart.fleximart.domain.interactors.GenerateChecksumInteractor;
import fleximart.fleximart.domain.interactors.impl.CompleteWalletPayInteractorImpl;
import fleximart.fleximart.domain.interactors.impl.GenerateChecksumInteractorImpl;
import fleximart.fleximart.domain.model.ChecksumData;
import fleximart.fleximart.domain.model.UserInfo;
import fleximart.fleximart.presentation.presenters.PaymentPresenter;
import fleximart.fleximart.presentation.presenters.base.AbstractPresenter;
import fleximart.fleximart.repository.AppRepositoryImpl;
import fleximart.fleximart.threading.MainThreadImpl;

public class PaymentPresenterImpl extends AbstractPresenter implements PaymentPresenter, GenerateChecksumInteractor.Callback, CompleteWalletPayInteractor.Callback {

    Context mContext;
    PaymentPresenter.View mView;
    GenerateChecksumInteractorImpl generateChecksumInteractor;
    CompleteWalletPayInteractorImpl completeWalletPayInteractor;
    AndroidApplication androidApplication;

    public PaymentPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }


    @Override
    public void generatePresenter(String MID, int ORDER_ID, String CHANNEL_ID, String TXN_AMOUNT, String WEBSITE, String INDUSTRY_TYPE_ID, String CALLBACK_URL) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            generateChecksumInteractor = new GenerateChecksumInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, MID, ORDER_ID, userInfo.id, userInfo.mobile, userInfo.email, CHANNEL_ID, TXN_AMOUNT, WEBSITE, INDUSTRY_TYPE_ID, CALLBACK_URL);
            generateChecksumInteractor.execute();
        }
    }

    @Override
    public void completeWalletPay(int orderId, String transactionId) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            completeWalletPayInteractor = new CompleteWalletPayInteractorImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), new AppRepositoryImpl(), this, userInfo.apiToken, orderId, transactionId);
            completeWalletPayInteractor.execute();
        }
    }

    @Override
    public void onGenerateChecksumSuccess(ChecksumData checksumData) {
        mView.initiatePayment(checksumData.checkSum);
    }

    @Override
    public void onGenerateChecksumFail(String errorMsg, int loginError) {
        mView.hideLoader();
    }

    @Override
    public void onCompleteWalletPaySuccess() {
        mView.hideLoader();
        mView.goToWalletAddSuccessActvity(true);
    }

    @Override
    public void onCompleteWalletPayFail(String errorMsg, int loginError) {
        mView.hideLoader();
        mView.goToWalletAddSuccessActvity(false);
    }
}
