package fleximart.fleximart.presentation.presenters;

import fleximart.fleximart.domain.model.CheckPhoneData;

public interface PhoneNumberCheckPresenter {
    void checkPhoneNumber(String phoneNumber);
    interface View {
        void loadData(CheckPhoneData checkPhoneData);
        void showLoader();
        void hideLoader();
        void clearData();
    }
}
