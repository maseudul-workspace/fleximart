package fleximart.fleximart.presentation.presenters;

import fleximart.fleximart.presentation.ui.adapters.WishlistAdapter;

public interface WishlistPresenter {
    void fetchWishlist();
    void fetchCartDetails();
    interface View {
        void loadWishlist(WishlistAdapter adapter);
        void loadCartCount(int count);
        void showErrorMsg(String msg);
        void showLoader();
        void hideLoader();
        void goToProductDetails(int productId);
        void showBottomSheet();
        void onWishlistEmpty();
    }
}
