package fleximart.fleximart.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import fleximart.fleximart.AndroidApplication;
import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.FetchCitiesInteractor;
import fleximart.fleximart.domain.interactors.FetchStateListInteractor;
import fleximart.fleximart.domain.interactors.GetAddressDetailsInteractor;
import fleximart.fleximart.domain.interactors.UpdateAddressInteractor;
import fleximart.fleximart.domain.interactors.impl.FetchCitiesInteractorImpl;
import fleximart.fleximart.domain.interactors.impl.FetchStateListInteractorImpl;
import fleximart.fleximart.domain.interactors.impl.GetAddressDetailsInteractorImpl;
import fleximart.fleximart.domain.interactors.impl.UpdateAddressInteractorImpl;
import fleximart.fleximart.domain.model.Address;
import fleximart.fleximart.domain.model.Cities;
import fleximart.fleximart.domain.model.StateList;
import fleximart.fleximart.domain.model.UserInfo;
import fleximart.fleximart.presentation.presenters.EditShippingAddressPresenter;
import fleximart.fleximart.presentation.presenters.base.AbstractPresenter;
import fleximart.fleximart.presentation.ui.adapters.CityListDialogAdapter;
import fleximart.fleximart.presentation.ui.adapters.StateListDialogAdapter;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class EditShippingAddressPresenterImpl extends AbstractPresenter implements EditShippingAddressPresenter,
                                                                                    GetAddressDetailsInteractor.Callback,
                                                                                    FetchStateListInteractor.Callback,
                                                                                    FetchCitiesInteractor.Callback,
                                                                                    StateListDialogAdapter.Callback,
                                                                                    CityListDialogAdapter.Callback,
                                                                                    UpdateAddressInteractor.Callback
{

    Context mContext;
    EditShippingAddressPresenter.View mView;
    GetAddressDetailsInteractorImpl getAddressDetailsInteractor;
    AndroidApplication androidApplication;
    int stateId = 0;
    int cityId = 0;
    FetchStateListInteractorImpl fetchStateListInteractor;
    StateListDialogAdapter stateListDialogAdapter;
    FetchCitiesInteractorImpl fetchCitiesInteractor;
    CityListDialogAdapter cityListDialogAdapter;
    UpdateAddressInteractorImpl updateAddressInteractor;
    int addressId;

    public EditShippingAddressPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void getShippingAddressDetails(int addressId) {
        this.addressId = addressId;
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        getAddressDetailsInteractor = new GetAddressDetailsInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken,  addressId);
        getAddressDetailsInteractor.execute();
    }

    @Override
    public void updateAddress(String name, String email, String mobile, String alternativeMobile, String landmark, String pin, String address) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        updateAddressInteractor = new UpdateAddressInteractorImpl(
                mExecutor,
                mMainThread,
                new AppRepositoryImpl(),
                this,
                userInfo.apiToken,
                addressId,
                name,
                email,
                mobile,
                alternativeMobile,
                landmark,
                stateId,
                cityId,
                pin,
                address

        );
        updateAddressInteractor.execute();
    }

    @Override
    public void fetchStateList() {
        fetchStateListInteractor = new FetchStateListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this);
        fetchStateListInteractor.execute();
    }

    @Override
    public void fetchCityList() {
        fetchCitiesInteractor = new FetchCitiesInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, stateId);
        fetchCitiesInteractor.execute();
    }

    @Override
    public void onGettingAddressDetailsSuccess(Address address) {
        stateId = address.stateId;
        cityId = address.cityId;
        fetchCityList();
        mView.setData(address);
        mView.hideLoader();
    }

    @Override
    public void onGettingAddressDetailsFail(String errorMsg, int loginError) {

    }

    @Override
    public void onGettingCitiesSuccess(Cities[] cities) {
        if (cities.length == 0) {
            mView.hideCityRecyclerView();
        } else {
            cityListDialogAdapter = new CityListDialogAdapter(mContext, cities, this::onCityClicked);
            mView.loadCitiesAdapter(cityListDialogAdapter);
        }
    }

    @Override
    public void onGettingCitiesFail() {
        mView.hideCityRecyclerView();
    }

    @Override
    public void onGettingStateListSuccess(StateList[] states) {
        if (states.length == 0) {
            mView.hideStateRecyclerView();
        } else {
            stateListDialogAdapter = new StateListDialogAdapter(mContext, states, this::onStateClicked);
            mView.loadStateAdapter(stateListDialogAdapter);
        }
    }

    @Override
    public void onGettingStateListFail() {
        mView.hideStateRecyclerView();
    }

    @Override
    public void onCityClicked(int id, String city) {
        mView.setCityName(city);
        cityId = id;
    }

    @Override
    public void onStateClicked(int id, String state) {
        stateId = id;
        mView.setStateName(state);
        fetchCityList();
    }

    @Override
    public void onUpdateAddressSuccess() {
        Toasty.info(mContext, "Address Updated Successfully", Toast.LENGTH_SHORT).show();
        mView.hideLoader();
        mView.onUpdateAddressSuccess();
    }

    @Override
    public void onUpdateAddressFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }
}
