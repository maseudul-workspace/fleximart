package fleximart.fleximart.presentation.presenters;

import fleximart.fleximart.domain.model.MyCommissionList;
import fleximart.fleximart.presentation.presenters.base.AbstractPresenter;
import fleximart.fleximart.presentation.ui.adapters.MyCommissionListAdapter;

public interface MyCommissionListPresenter{
    void fetchApprovedCommissions(int page, String type);
    void fetchUnapprovedCommissions(int page, String type);
    interface View {
        void loadCommissionsList(MyCommissionListAdapter adapter, int totalPage);
        void showLoader();
        void hideLoader();
        void onError(String errorMsg);
    }
}
