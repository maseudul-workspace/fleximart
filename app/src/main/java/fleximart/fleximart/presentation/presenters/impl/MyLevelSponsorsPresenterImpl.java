package fleximart.fleximart.presentation.presenters.impl;

import android.content.Context;

import fleximart.fleximart.AndroidApplication;
import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.FetchLevelSponsorsInteractor;
import fleximart.fleximart.domain.interactors.impl.FetchLevelSponsorsInteractorImpl;
import fleximart.fleximart.domain.model.LevelSponsors;
import fleximart.fleximart.domain.model.UserInfo;
import fleximart.fleximart.presentation.presenters.MyLevelSponsorsPresenter;
import fleximart.fleximart.presentation.presenters.base.AbstractPresenter;
import fleximart.fleximart.presentation.ui.adapters.MyLevelSponsorsAdapter;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class MyLevelSponsorsPresenterImpl extends AbstractPresenter implements MyLevelSponsorsPresenter, FetchLevelSponsorsInteractor.Callback {

    Context mContext;
    MyLevelSponsorsPresenter.View mView;
    FetchLevelSponsorsInteractorImpl fetchLevelSponsorsInteractor;
    AndroidApplication androidApplication;

    public MyLevelSponsorsPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }


    @Override
    public void fetchLevelSponsors(int nodeId, int levelId) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            fetchLevelSponsorsInteractor = new FetchLevelSponsorsInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, nodeId, levelId);
            fetchLevelSponsorsInteractor.execute();
        }
    }

    @Override
    public void onGettingLevelSponsorsSuccess(LevelSponsors[] levelSponsors) {
        if (levelSponsors.length == 0) {
            mView.showMessage("No Record Found");
        } else {
            MyLevelSponsorsAdapter adapter = new MyLevelSponsorsAdapter(levelSponsors);
            mView.loadAdapter(adapter);
        }
        mView.hideLoader();
    }

    @Override
    public void onGettingLevelSponsorsFail(String errorMsg, int loginError) {
        mView.hideLoader();
        mView.showMessage(errorMsg);
    }
}
