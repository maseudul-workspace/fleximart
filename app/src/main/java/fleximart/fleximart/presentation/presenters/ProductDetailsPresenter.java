package fleximart.fleximart.presentation.presenters;

import fleximart.fleximart.domain.model.ProductDetails;
import fleximart.fleximart.domain.model.ProductDetailsData;
import fleximart.fleximart.presentation.ui.adapters.ImageZoomViewPagerAdapter;
import fleximart.fleximart.presentation.ui.adapters.ProductDetailsColorFilterAdapter;
import fleximart.fleximart.presentation.ui.adapters.ProductDetailsFilterValuesAdapter;
import fleximart.fleximart.presentation.ui.adapters.ProductDetailsSlider;
import fleximart.fleximart.presentation.ui.adapters.ProductListHorizontalAdapter;
import fleximart.fleximart.presentation.ui.adapters.SizeDialogAdapter;

public interface ProductDetailsPresenter {
    void getProductDetails(int productId);
    void addToCart();
    void fetchCartCount();
    void fetchWishlist();
    void addToWishlist(int productId);
    void removeFromWishlist(int productId);
    interface View {
        void showLoader();
        void hideLoader();
        void loadData(ProductDetails productDetails, ProductListHorizontalAdapter adapter, ProductDetailsColorFilterAdapter colorFilterAdapter, ProductDetailsFilterValuesAdapter sizesAdapter, SizeDialogAdapter sizeDialogAdapter, ProductDetailsSlider slider, ImageZoomViewPagerAdapter imageZoomViewPagerAdapter, int imageCount);
        void disimisSizeDialog();
        void onSizeSelected();
        void hideDialogLoader();
        void showDialogLoader();
        void setCartCount(int count);
        void showErrorMessage(String errorMsg);
        void onAddToWishlistSuccess();
        void onRemoveFromWishlistSuccess();
        void showBottomSheet();
        void setWishlistCount(int count);
        void showZoomPreviewDialog(int position);
    }
}
