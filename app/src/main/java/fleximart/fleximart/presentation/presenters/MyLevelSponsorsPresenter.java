package fleximart.fleximart.presentation.presenters;

import fleximart.fleximart.presentation.ui.adapters.MyLevelSponsorsAdapter;

public interface MyLevelSponsorsPresenter {
    void fetchLevelSponsors(int nodeId, int levelId);
    interface View {
        void loadAdapter(MyLevelSponsorsAdapter adapter);
        void showLoader();
        void hideLoader();
        void showMessage(String errorMsg);
    }
}
