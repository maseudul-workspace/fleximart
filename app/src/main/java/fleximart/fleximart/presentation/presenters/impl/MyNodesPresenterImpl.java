package fleximart.fleximart.presentation.presenters.impl;

import android.content.Context;

import fleximart.fleximart.AndroidApplication;
import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.FetchMyNodesInteractor;
import fleximart.fleximart.domain.interactors.impl.FetchMyNodesInteractorImpl;
import fleximart.fleximart.domain.model.MyNodes;
import fleximart.fleximart.domain.model.UserInfo;
import fleximart.fleximart.presentation.presenters.MyNodesPresenter;
import fleximart.fleximart.presentation.presenters.base.AbstractPresenter;
import fleximart.fleximart.presentation.ui.adapters.NodeViewpagerAdapter;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class MyNodesPresenterImpl extends AbstractPresenter implements MyNodesPresenter, FetchMyNodesInteractor.Callback, NodeViewpagerAdapter.Callback {

    Context mContext;
    MyNodesPresenter.View mView;
    FetchMyNodesInteractorImpl fetchMyNodesInteractor;
    AndroidApplication androidApplication;

    public MyNodesPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchMyNodes() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            fetchMyNodesInteractor = new FetchMyNodesInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.id);
            fetchMyNodesInteractor.execute();
        }
    }

    @Override
    public void onFetchingNodesSuccess(MyNodes[] myNodes) {
        mView.hideLoader();
        if (myNodes.length == 0) {
            mView.onError("No Record Found");
        } else {
            NodeViewpagerAdapter myNodesAdapter = new NodeViewpagerAdapter(mContext, myNodes, this);
            mView.loadAdapter(myNodesAdapter, myNodes.length);
        }
    }

    @Override
    public void onFetchingNodesFail(String errorMsg, int loginError) {
        mView.hideLoader();
        mView.onError(errorMsg);
    }

    @Override
    public void onDownlineViewClicked(int id) {
        mView.goToMyDownlines(id);
    }

    @Override
    public void onTreeViewClicked(int id) {
        mView.goToMyTree(id);
    }

    @Override
    public void onLevelsClicked(int id) {
        mView.goToMyLevels(id);
    }
}
