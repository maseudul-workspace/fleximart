package fleximart.fleximart.presentation.presenters;

import fleximart.fleximart.presentation.ui.adapters.ShippingAddressAdapter;

public interface ShippingAddressPresenter {
    void fetchShippingAddress();
    interface View {
        void loadAdapter(ShippingAddressAdapter adapter);
        void showLoader();
        void hideLoader();
        void hideAddressRecyclerView();
        void goToAddressEditActivity(int id);
        void showErrorMessage(String errorMsg);
    }
}
