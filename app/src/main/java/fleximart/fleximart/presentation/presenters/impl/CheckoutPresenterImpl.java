package fleximart.fleximart.presentation.presenters.impl;

import android.content.Context;
import android.util.Log;

import fleximart.fleximart.AndroidApplication;
import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.GetAddressDetailsInteractor;
import fleximart.fleximart.domain.interactors.GetWalletDetailsInteractor;
import fleximart.fleximart.domain.interactors.PlaceOrderInteractor;
import fleximart.fleximart.domain.interactors.impl.GetAddressDetailsInteractorImpl;
import fleximart.fleximart.domain.interactors.impl.GetWalletDetailsInteractorImpl;
import fleximart.fleximart.domain.interactors.impl.PlaceOrderInteractorImpl;
import fleximart.fleximart.domain.model.Address;
import fleximart.fleximart.domain.model.OrderPlaceData;
import fleximart.fleximart.domain.model.UserInfo;
import fleximart.fleximart.domain.model.WalletDetails;
import fleximart.fleximart.presentation.presenters.CheckoutPresenter;
import fleximart.fleximart.presentation.presenters.base.AbstractPresenter;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class CheckoutPresenterImpl extends AbstractPresenter implements CheckoutPresenter,
                                                                        GetAddressDetailsInteractor.Callback,
                                                                        GetWalletDetailsInteractor.Callback,
                                                                        PlaceOrderInteractor.Callback
{

    Context mContext;
    CheckoutPresenter.View mView;
    GetAddressDetailsInteractorImpl getAddressDetailsInteractor;
    GetWalletDetailsInteractorImpl getWalletDetailsInteractor;
    PlaceOrderInteractorImpl placeOrderInteractor;
    AndroidApplication androidApplication;

    public CheckoutPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchAddressId(int addressId) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            getAddressDetailsInteractor = new GetAddressDetailsInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, addressId);
            getAddressDetailsInteractor.execute();
        }
    }

    @Override
    public void fetchWalletAmount() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            getWalletDetailsInteractor = new GetWalletDetailsInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.id);
            getWalletDetailsInteractor.execute();
        }
    }

    @Override
    public void placeOrder(int addressId, int paymentMethod, int isWalletPay) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            placeOrderInteractor = new PlaceOrderInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.id, addressId, paymentMethod, isWalletPay);
            placeOrderInteractor.execute();
        }
    }

    @Override
    public void onGettingAddressDetailsSuccess(Address address) {
        mView.hideLoader();
        mView.loadAddress(address);
    }

    @Override
    public void onGettingAddressDetailsFail(String errorMsg, int loginError) {
        mView.hideLoader();
    }

    @Override
    public void onGettingWalletDetailsSuccess(WalletDetails walletDetails) {
        mView.loadWalletAmount(walletDetails.withdrawalAmount);
    }

    @Override
    public void onGettingWalletDetailsFail(String errorMsg, int loginError) {
        mView.loadWalletAmount(0.0);
    }

    @Override
    public void onOrderPlaceSuccess(OrderPlaceData orderPlaceData) {
        mView.hideLoader();
        mView.loadOrderPlaceData(orderPlaceData);
    }

    @Override
    public void onOrderPlaceFail(String errorMsg, int loginError) {
        mView.hideLoader();
    }
}
