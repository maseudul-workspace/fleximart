package fleximart.fleximart.presentation.presenters.impl;

import android.content.Context;

import fleximart.fleximart.AndroidApplication;
import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.FetchMyTotalCommisionInteractor;
import fleximart.fleximart.domain.interactors.impl.FetchMyTotalCommisionInteractorImpl;
import fleximart.fleximart.domain.model.CommisionTotal;
import fleximart.fleximart.domain.model.UserInfo;
import fleximart.fleximart.presentation.presenters.MyCommisionPresenter;
import fleximart.fleximart.presentation.presenters.base.AbstractPresenter;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class MyCommisionPresenterImpl extends AbstractPresenter implements MyCommisionPresenter, FetchMyTotalCommisionInteractor.Callback {

    Context mContext;
    MyCommisionPresenter.View mView;
    AndroidApplication androidApplication;
    FetchMyTotalCommisionInteractorImpl fetchMyTotalCommisionInteractor;

    public MyCommisionPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchMyTotalCommision() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            fetchMyTotalCommisionInteractor = new FetchMyTotalCommisionInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.id);
            fetchMyTotalCommisionInteractor.execute();
        }
    }

    @Override
    public void onGettingTotalCommisionSuccess(CommisionTotal commisionTotal) {
        mView.hideLoader();
        mView.loadData(commisionTotal);
    }

    @Override
    public void onGettingTotalCommisionFail(int loginError, String errorMsg) {
        mView.onError(errorMsg);
        mView.hideLoader();
    }
}
