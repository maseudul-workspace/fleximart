package fleximart.fleximart.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import fleximart.fleximart.AndroidApplication;
import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.RegisterUserInteractor;
import fleximart.fleximart.domain.interactors.impl.RegisterUserInteractorImpl;
import fleximart.fleximart.domain.model.UserInfo;
import fleximart.fleximart.domain.model.UserInfoWrapper;
import fleximart.fleximart.presentation.presenters.RegisterPresenter;
import fleximart.fleximart.presentation.presenters.base.AbstractPresenter;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class RegisterPresenterImpl extends AbstractPresenter implements RegisterPresenter, RegisterUserInteractor.Callback {

    Context mContext;
    RegisterPresenter.View mView;
    RegisterUserInteractorImpl mInteractor;

    public RegisterPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    public RegisterPresenterImpl(Executor executor, MainThread mainThread) {
        super(executor, mainThread);
    }

    @Override
    public void registerUser(String name, String email, String password, String confirmPassword, String mobile) {
        mInteractor = new RegisterUserInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, name, email, password, confirmPassword, mobile);
        mInteractor.execute();
    }

    @Override
    public void onRegisterUser(UserInfo userInfo) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setUserInfo(mContext, userInfo);
        Toasty.success(mContext, "Registered successfully", Toast.LENGTH_SHORT, true).show();
        mView.onRegisterSuccess();
    }

    @Override
    public void onRegisterFail(UserInfoWrapper userInfoWrapper) {
        if (userInfoWrapper == null) {
            mView.onRegsiterFail(null);
            mView.showErrorMessage("Something Went Wrong");
        } else {
            if (userInfoWrapper.errorCode && userInfoWrapper.errorMessage != null) {
                mView.onRegsiterFail(userInfoWrapper);
            } else {
                mView.showErrorMessage(userInfoWrapper.message);
                mView.onRegsiterFail(null);
            }
        }
    }
}
