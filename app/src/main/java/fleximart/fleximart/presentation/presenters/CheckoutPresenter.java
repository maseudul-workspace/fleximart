package fleximart.fleximart.presentation.presenters;

import fleximart.fleximart.domain.model.Address;
import fleximart.fleximart.domain.model.ChecksumData;
import fleximart.fleximart.domain.model.OrderPlaceData;

public interface CheckoutPresenter {
    void fetchAddressId(int addressId);
    void fetchWalletAmount();
    void placeOrder(int addressId, int paymentMethod, int isWalletPay);
    interface View {
        void loadAddress(Address address);
        void showLoader();
        void hideLoader();
        void loadWalletAmount(Double walletAmount);
        void loadOrderPlaceData(OrderPlaceData orderPlaceData);
    }
}
