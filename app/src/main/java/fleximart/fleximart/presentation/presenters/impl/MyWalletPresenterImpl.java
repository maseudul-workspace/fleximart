package fleximart.fleximart.presentation.presenters.impl;

import android.content.Context;

import com.google.android.material.button.MaterialButton;

import fleximart.fleximart.AndroidApplication;
import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.AddWalletMoneyInteractor;
import fleximart.fleximart.domain.interactors.FetchWalletHistoryInteractor;
import fleximart.fleximart.domain.interactors.GetWalletDetailsInteractor;
import fleximart.fleximart.domain.interactors.impl.AddWalletMoneyInteractorImpl;
import fleximart.fleximart.domain.interactors.impl.FetchWalletHistoryInteractorImpl;
import fleximart.fleximart.domain.interactors.impl.GetWalletDetailsInteractorImpl;
import fleximart.fleximart.domain.model.OrderPlaceData;
import fleximart.fleximart.domain.model.UserInfo;
import fleximart.fleximart.domain.model.WalletDetails;
import fleximart.fleximart.domain.model.WalletHistory;
import fleximart.fleximart.presentation.presenters.MyWalletPresenter;
import fleximart.fleximart.presentation.presenters.base.AbstractPresenter;
import fleximart.fleximart.presentation.ui.adapters.WalletAdapter;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class MyWalletPresenterImpl extends AbstractPresenter implements MyWalletPresenter, GetWalletDetailsInteractor.Callback, FetchWalletHistoryInteractor.Callback, AddWalletMoneyInteractor.Callback {

    Context mContext;
    MyWalletPresenter.View mView;
    GetWalletDetailsInteractorImpl getWalletDetailsInteractor;
    FetchWalletHistoryInteractorImpl fetchWalletHistoryInteractor;
    AndroidApplication androidApplication;
    WalletAdapter walletAdapter;
    AddWalletMoneyInteractorImpl addWalletMoneyInteractor;

    public MyWalletPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchWalletAmount() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            getWalletDetailsInteractor = new GetWalletDetailsInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.id);
            getWalletDetailsInteractor.execute();
        }
    }

    @Override
    public void fetchWalletHistory(int pageNo) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            fetchWalletHistoryInteractor = new FetchWalletHistoryInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.id, pageNo);
            fetchWalletHistoryInteractor.execute();
        }
    }

    @Override
    public void addWalletMoney(String amount) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            addWalletMoneyInteractor = new AddWalletMoneyInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.id, amount);
            addWalletMoneyInteractor.execute();
        }
    }

    @Override
    public void onWalletHistoryFetchSuccess(WalletHistory[] walletHistories, int totalPage, int currentPage) {
        mView.hideLoader();
        walletAdapter = new WalletAdapter(mContext, walletHistories);
        mView.loadAdapter(walletAdapter, totalPage, currentPage);
    }

    @Override
    public void onWalletHistoryFetchFail(String errorMsg, int loginError) {
        mView.hideLoader();
    }

    @Override
    public void onGettingWalletDetailsSuccess(WalletDetails walletDetails) {
        mView.loadWalletAmount(walletDetails.withdrawalAmount);
    }

    @Override
    public void onGettingWalletDetailsFail(String errorMsg, int loginError) {

    }

    @Override
    public void onAddWalletMoneySuccess(OrderPlaceData orderPlaceData) {
        mView.hideLoader();
        mView.goToPaymentActivity(orderPlaceData);
    }

    @Override
    public void onAddWalletMoneyFail(String errorMsg, int loginError) {
        mView.hideLoader();
        mView.showErrorMessage(errorMsg);
    }
}
