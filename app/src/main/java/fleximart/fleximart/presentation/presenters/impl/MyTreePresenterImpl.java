package fleximart.fleximart.presentation.presenters.impl;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;

import de.blox.graphview.Graph;
import de.blox.graphview.Node;
import fleximart.fleximart.AndroidApplication;
import fleximart.fleximart.domain.executors.Executor;
import fleximart.fleximart.domain.executors.MainThread;
import fleximart.fleximart.domain.interactors.FetchMyTreeInteractor;
import fleximart.fleximart.domain.interactors.impl.FetchMyTreeInteractorImpl;
import fleximart.fleximart.domain.model.NodeArray;
import fleximart.fleximart.domain.model.Tree;
import fleximart.fleximart.domain.model.UserInfo;
import fleximart.fleximart.presentation.presenters.MyTreePresenter;
import fleximart.fleximart.presentation.presenters.base.AbstractPresenter;
import fleximart.fleximart.presentation.ui.adapters.MyTreeAdapter;
import fleximart.fleximart.repository.AppRepositoryImpl;

public class MyTreePresenterImpl extends AbstractPresenter implements MyTreePresenter, FetchMyTreeInteractor.Callback, MyTreeAdapter.Callback {

    Context mContext;
    MyTreePresenter.View mView;
    ArrayList<NodeArray> nodeArrays;
    private int nodeCount = 1;
    FetchMyTreeInteractorImpl fetchMyTreeInteractor;
    AndroidApplication androidApplication;

    public MyTreePresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchMyTree(int nodeId, int payNodeId, int rank) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            fetchMyTreeInteractor = new FetchMyTreeInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, nodeId, payNodeId, rank);
            fetchMyTreeInteractor.execute();
        }
    }

    @Override
    public void onMyTreeFetchSuccess(Tree[] trees) {
        if (trees.length == 0) {
            mView.onError("No Record Found");
        } else {
            nodeArrays = new ArrayList<>();
            final Graph graph = new Graph();
            for (int i = 0; i < trees.length; i++) {
                Node rootNode;
                if (checkNodeInArray(trees[i].id)) {
                    rootNode = getNode(trees[i].id);
                } else {
                    rootNode = new Node(getNodeText(trees[i].id + "-" + trees[i].rank + "-" + trees[i].name + "-" + trees[i].isPaid));
                    NodeArray nodeArray = new NodeArray(trees[i].id, rootNode);
                    nodeArrays.add(nodeArray);
                }

                if (trees[i].leftId != 0) {
                    Node leftNode = new Node(getNodeText(trees[i].leftId + "-" + trees[i].rank + "-" + trees[i].name + "-" + trees[i].isPaid));
                    NodeArray leftNodeArray = new NodeArray(trees[i].leftId, leftNode);
                    nodeArrays.add(leftNodeArray);
                    graph.addEdge(rootNode, leftNode);
                }

                if (trees[i].rightId != 0) {
                    Node rightNode = new Node(getNodeText(trees[i].rightId + "-" + trees[i].rank + "-" + trees[i].name + "-" + trees[i].isPaid));
                    NodeArray rightNodeArray = new NodeArray(trees[i].rightId, rightNode);
                    nodeArrays.add(rightNodeArray);
                    graph.addEdge(rootNode, rightNode);
                }
            }
            MyTreeAdapter myTreeAdapter = new MyTreeAdapter(graph, mContext, this);
            mView.loadData(myTreeAdapter);
        }
        mView.hideLoader();
    }

    public boolean checkNodeInArray(int id) {
        for (int i = 0; i < nodeArrays.size(); i++) {
            if (nodeArrays.get(i).nodeId == id) {
                return true;
            }
        }
        return false;
    }

    public Node getNode(int id) {
        for (int i = 0; i < nodeArrays.size(); i++) {
            if (nodeArrays.get(i).nodeId == id) {
                return nodeArrays.get(i).node;
            }
        }
        return null;
    }

    private String getNodeText(String msg) {
        Log.e("LogMsg", "Message: " + msg);
        return "Node-" + nodeCount++ + "-" + msg ;
    }

    @Override
    public void onMyTreeFetchFail(String errorMsg, int loginError) {
        mView.hideLoader();
        mView.onError(errorMsg);
    }

    @Override
    public void onNodeClicked(int nodeId, int rank) {
        mView.goToMyTree(nodeId, rank);
    }
}
