package fleximart.fleximart.presentation.presenters;

import fleximart.fleximart.presentation.ui.adapters.BrandsAdapter;
import fleximart.fleximart.presentation.ui.adapters.CategoryFilterAdapter;
import fleximart.fleximart.presentation.ui.adapters.ColorsAdapter;
import fleximart.fleximart.presentation.ui.adapters.ProductListVerticalAdapter;
import fleximart.fleximart.presentation.ui.adapters.SizesAdapter;

public interface ProductsListPresenter {
    void fetchProductListWithFilter(int secondCategory, int page);
    void fetchProductListWithoutFilter(int secondCategory, int page, long priceFrom, long priceTo, int sortBy, String type);
    void fetchWishlist();
    interface View {
        void loadProductAdapters(ProductListVerticalAdapter productListVerticalAdapter, int totalPage);
        void loadOthersAdapters(CategoryFilterAdapter categoryFilterAdapter, ColorsAdapter colorsAdapter, BrandsAdapter brandsAdapter, SizesAdapter sizesAdapter);
        void showLoader();
        void hideLoader();
        void goToProductDetails(int productId);
        void showErrorMessage(String errorMsg);
        void onNoProductsFound();
        void showBottomSheet();
    }
}
